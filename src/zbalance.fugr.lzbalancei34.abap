*----------------------------------------------------------------------*
*   INCLUDE LZBALANCEI34                                               *
*----------------------------------------------------------------------*
************************************************************************
* ROUTINES                                                             *
************************************************************************

*&---------------------------------------------------------------------*
*& Modifications                                                       *
*&---------------------------------------------------------------------*
*& Project            : SGS-SAP Upgrade ECC5_TO_ECC6                   *
*& Author SGID        : D1924182                                       *
*& Author name       	: Devendran Krishnan                             *
*& Modifications TAG 	: DKR001                         	               *
*& Date               : 29.11.2010                                     *
*& QB DevT #          :                                                *
*& Panaya Task        : 184                                            *
*& Description        : change function module                         *
*&                      POPUP_TO_CONFIRM_WITH_MESSAGE                  *
*&---------------------------------------------------------------------*


*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_prix DEFINITION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_prix DEFINITION.

  PUBLIC SECTION.

*** METHOD handle_double_click.
    METHODS:
    handle_double_click
        FOR EVENT double_click OF cl_gui_alv_grid
            IMPORTING e_row e_column.


*** METHOD handle_hotspot_click.
    METHODS:
        handle_hotspot_click
            FOR EVENT hotspot_click OF cl_gui_alv_grid
                IMPORTING e_row_id e_column_id.

*** METHOD set_select_cell.
*    METHODS:
*        set_selected_cell_base
*            FOR EVENT user_command OF cl_gui_alv_grid.
*
*

  PRIVATE SECTION.

ENDCLASS.                    "cl_event_receiver_prix DEFINITION

*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_prix IMPLEMENTATION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_prix IMPLEMENTATION.

*** METHOD handle_double_click.
  METHOD handle_double_click.

    READ TABLE wt_alv_prix INDEX e_row-index INTO ls_alv_prix.
*
    CASE e_column-fieldname.
      WHEN 'EBELN'.

        CALL FUNCTION 'MIGO_DIALOG'
         EXPORTING
           i_action                  = 'A01'
           i_refdoc                  = 'R01'
           i_ebeln                   = ls_alv_prix-ebeln
           i_mjahr                   = ls_alv_prix-gjahr
* EXCEPTIONS
*   ILLEGAL_COMBINATION       = 1
*   OTHERS                    = 2
*
  .
    ENDCASE.

  ENDMETHOD.                           "handle_double_click

*** METHOD handle_hotspot_click.
  METHOD handle_hotspot_click.

*   READ TABLE wt_alv_prix INDEX e_row-index INTO ls_alv_prix.
**
*    CASE e_column-fieldname.
*      WHEN 'EBELN'.
*
*      CALL FUNCTION 'MIGO_DIALOG'
*       EXPORTING
*         i_action                  = 'A01'
*         i_refdoc                  = 'R01'
*         i_ebeln                   = ls_alv_prix-ebeln
*         i_mjahr                   = ls_alv_prix-gjahr
** EXCEPTIONS
**   ILLEGAL_COMBINATION       = 1
**   OTHERS                    = 2
**
*.
*ENDCASE.

  ENDMETHOD.                           "handle_hotspot_click

*  METHOD  set_selected_cell_base.
*
*  ENDMETHOD.                             "set_selected_cell_base

ENDCLASS.                    "cl_event_receiver_prix IMPLEMENTATION


*&---------------------------------------------------------------------*
*&      Module  user_command_9200  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9200 INPUT.

*data: t_row type table of lvc_s_row.
*data: t_roid type table of lvc_s_roid.
  DATA:ls_row TYPE lvc_s_row.
  DATA:ls_roid TYPE lvc_s_roid.

  CASE w_9200_okcode.

    WHEN 'DISPLAY'.
* Affichage facture
      SET PARAMETER ID 'RBN' FIELD w_belnr.
      SET PARAMETER ID 'GJR' FIELD w_gjahr.
      CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.

    WHEN 'GENAVOIR'. "Génération d'avoir
      PERFORM f_validation.

    WHEN 'POSTVAL'. "Postes valides
      PERFORM f_validation.

    WHEN 'ATTAVOIR'. "Attente avoir
      w_decision = 'ATTAVOIR'.
      SET SCREEN 0.LEAVE SCREEN .

    WHEN 'RETCOMPTA'. "Retour litige à la compta
      w_decision = 'RETCOMPTA'.
      SET SCREEN 0.LEAVE SCREEN .

    WHEN 'IMGF'.
*----------------------------------------------------------------------*
* ITGGH   16.04.2007      Display image
*     get idoc number
      IF w_idoc-docnum IS INITIAL.
*>> ajout rgi010.
        SELECT SINGLE * FROM rbkp WHERE belnr = w_belnr
                                    AND gjahr = w_gjahr.
        IF sy-subrc = 0.
          DATA objkey LIKE srrelroles-objkey.
          CONCATENATE w_belnr w_gjahr INTO objkey.
          SELECT SINGLE * FROM srrelroles WHERE objkey = objkey
                                            AND objtype = 'BUS2081'.
          IF sy-subrc = 0.
            SELECT SINGLE * FROM idocrel WHERE role_b = srrelroles-roleid.
            IF sy-subrc = 0.
              SELECT SINGLE * FROM srrelroles WHERE roleid = idocrel-role_a.
*                                      AND objtype = 'BUS2081'.
              IF sy-subrc = 0.
                MOVE srrelroles-objkey TO w_idoc-docnum.
              ELSE.
                SELECT docnum credat FROM edids
                  INTO CORRESPONDING FIELDS OF TABLE t_idoc
                  WHERE stapa1 = w_belnr
                  AND   status = '53'.
                IF sy-subrc = 0.
                  SORT t_idoc BY credat DESCENDING.
                  READ TABLE t_idoc INTO w_idoc INDEX 1.
                ENDIF.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
*>> Ajout RGI010
      CHECK NOT w_idoc-docnum IS INITIAL.
*       display image
      CALL FUNCTION 'ZBALANCE_IMAGE_DISPLAY'
        EXPORTING
          docnum                   = w_idoc-docnum
        EXCEPTIONS
          error_archiv             = 1
          error_communicationtable = 2
          error_kernel             = 3
          balance_config           = 4
          OTHERS                   = 5.
      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ELSE.
        CLEAR w_idoc-docnum.
      ENDIF.
*----------------------------------------------------------------------*

    WHEN 'BACK' OR 'CANCEL'.
      SET SCREEN 0.LEAVE SCREEN .
*
  ENDCASE.
*
*  IF NOT L_CODE IS INITIAL.
** mise à jour du conteneur workflow
*    SWC_SET_ELEMENT WI_CONTAINER 'CodeReponse' L_CODE.
*
*    CALL FUNCTION 'SWW_WI_CONTAINER_MODIFY'
*         EXPORTING
*              WI_ID        = SWPSTEPLOG-WF_ID
*         TABLES
*              WI_CONTAINER = WI_CONTAINER.
*
*    SET SCREEN 0.LEAVE SCREEN .
*  ENDIF.

ENDMODULE.                 " user_command_9200  INPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_9200  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9200 OUTPUT.

  CASE w_listmode.
    WHEN '1'.  " Affichage
      SET PF-STATUS 'MAIN_9200'.
      SET TITLEBAR 'TITRE_MM_DNP_9200'.
    WHEN '2'.  " Validation acheteur
      SET PF-STATUS 'MAIN_9110'.
      SET TITLEBAR 'TITRE_MM_DNP_9110'.
    WHEN '3'.  " Validation compta
      SET PF-STATUS 'MAIN_9120'.
      SET TITLEBAR 'TITRE_MM_DNP_9120'.
    WHEN '4'. "Attente avoir
      SET PF-STATUS 'MAIN_9120'.
      SET TITLEBAR 'TITRE_MM_DNP_9130'.
  ENDCASE.

* create objects
  IF o_prix_container IS INITIAL.
    CREATE OBJECT o_prix_container
      EXPORTING container_name = 'ALV_PRIX'.

    CREATE OBJECT o_prix_grid
      EXPORTING
        i_parent = o_prix_container.
  ENDIF.

  PERFORM load_data_prix.

*  if w_listmode = '3' or w_listmode = '4'.
*    perform load_data_prix.
*  endif.

ENDMODULE.                 " STATUS_9200  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  exit_9200  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE exit_9200 INPUT.

  CASE w_9200_okcode.
    WHEN 'EXIT'.
      SET SCREEN 0.
      LEAVE SCREEN .
  ENDCASE.

ENDMODULE.                 " exit_9200  INPUT

*&---------------------------------------------------------------------*
*&      Form  load_data_into_grid
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM load_data_prix.
  DATA : ws_variante_prix TYPE disvariant.
  DATA : ws_layout_prix   TYPE lvc_s_layo.
  DATA : ws_toolbar_prix TYPE ui_functions.
  DATA : w_save.

  SORT t_item_prix DESCENDING BY belnr gjahr buzei.

  PERFORM sort_params_prix.

  PERFORM fieldcatalog_params_prix.

  PERFORM toolbar_params_prix CHANGING ws_toolbar_prix.

  ws_layout_prix-edit = 'X'.
* Begin of DEL NTN001                QB#9759
*  ws_layout_prix-ctab_fname = 'black'.
* End of DEL NTN001                  QB#9759
  ws_layout_prix-zebra = 'X'.
  ws_layout_prix-cwidth_opt = 'X'.


  CASE w_listmode.
    WHEN '2'.
      ws_layout_prix-grid_title = text-t37.
    WHEN '3'.
      ws_layout_prix-grid_title = text-t50.
    WHEN OTHERS.
      ws_layout_prix-grid_title = text-t37.
  ENDCASE.
  .
  wt_alv_prix[] = t_item_prix[].
  ws_variante_prix = sy-uname.
  ws_variante_prix = 'ZBALANCE_LISTPOSTBLO_PRIX'.
  w_save = 'X'.

  CALL METHOD o_prix_grid->set_table_for_first_display
    EXPORTING
      i_structure_name     = 'tp_item_prix'
      is_variant           = ws_variante_prix
      is_layout            = ws_layout_prix
      i_save               = w_save
      it_toolbar_excluding = ws_toolbar_prix
    CHANGING
      it_fieldcatalog      = wt_catalog_prix
      it_sort              = wt_sort_prix
      it_outtab            = wt_alv_prix.


  CREATE OBJECT event_receiver_prix.
  SET HANDLER event_receiver_prix->handle_double_click FOR o_prix_grid.
  SET HANDLER event_receiver_prix->handle_hotspot_click FOR o_prix_grid.

*SET HANDLER event_receiver_prix->set_selected_cell_base FOR o_prix_grid
  ..


ENDFORM.                    " load_data_into_grid
*&---------------------------------------------------------------------*
*&      Form  sort_params_prix
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM sort_params_prix.

  DATA: wa_sort_prix TYPE lvc_s_sort.

*  wa_sort_prix-fieldname = 'BELNR'.
*  wa_sort_prix-up = 'X'.
*  wa_sort_prix-group = '01'.
*  APPEND wa_sort_prix TO wt_sort_prix.
*
*  wa_sort_prix-fieldname = 'GJAHR'.
*  wa_sort_prix-group = '01'.
*  APPEND wa_sort_prix TO wt_sort_prix.

  wa_sort_prix-fieldname = 'BUZEI'.
  wa_sort_prix-group = '01'.
  APPEND wa_sort_prix TO wt_sort_prix.


ENDFORM.                    " sort_params_for_alv
*&---------------------------------------------------------------------*
*&      Form  fieldcatalog_params_prix
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fieldcatalog_params_prix.

  DATA: wa_fieldcatalog_prix TYPE lvc_s_fcat.

*  wa_fieldcatalog-fieldname = 'SEL'.
*  wa_fieldcatalog-ref_table = 'SYST'.
*  wa_fieldcatalog-ref_field = 'SUBRC'.
*  wa_fieldcatalog-checkbox   = 'X'.
**  wa_fieldcatalog-edit   = 'X'.
*  wa_fieldcatalog-coltext   = 'Impression'.
*  APPEND wa_fieldcatalog TO t_catalog.
*  CLEAR wa_fieldcatalog.


*  wa_fieldcatalog_prix-fieldname = 'BELNR'.
*  wa_fieldcatalog_prix-ref_table = 'RBKP'.
*  wa_fieldcatalog_prix-ref_field = 'BELNR'.
*  wa_fieldcatalog_prix-hotspot   = 'X'.
*  wa_fieldcatalog_prix-coltext   = 'N° facture'.
**  wa_fieldcatalog_prix-edit = 'X'.
*  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
*  CLEAR wa_fieldcatalog_prix.
*
*  wa_fieldcatalog_prix-fieldname = 'GJAHR'.
*  wa_fieldcatalog_prix-ref_table = 'RBKP'.
*  wa_fieldcatalog_prix-ref_field = 'GJAHR'.
*  wa_fieldcatalog_prix-hotspot   = 'X'.
*  wa_fieldcatalog_prix-coltext   = 'Exercice'.
*  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
*  CLEAR wa_fieldcatalog_prix.

  wa_fieldcatalog_prix-fieldname = 'BUZEI'.
  wa_fieldcatalog_prix-ref_table = 'RSEG'.
  wa_fieldcatalog_prix-ref_field = 'BUZEI'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'Poste facture'(c07).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.


  wa_fieldcatalog_prix-fieldname = 'EBELN'.
  wa_fieldcatalog_prix-ref_table = 'RSEG'.
  wa_fieldcatalog_prix-ref_field = 'EBELN'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'N° Commande'(c06).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.

  wa_fieldcatalog_prix-fieldname = 'EBELP'.
  wa_fieldcatalog_prix-ref_table = 'RSEG'.
  wa_fieldcatalog_prix-ref_field = 'EBELP'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'Poste commande'(c05).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.

  wa_fieldcatalog_prix-fieldname = 'NETPR'.
  wa_fieldcatalog_prix-ref_table = 'EKPO'.
  wa_fieldcatalog_prix-ref_field = 'NETPR'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'Prix net commande'(c04).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.

  wa_fieldcatalog_prix-fieldname = 'NETPR2'.
  wa_fieldcatalog_prix-ref_table = 'EKPO'.
  wa_fieldcatalog_prix-ref_field = 'NETPR'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'Prix net facture'(c03).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.

  wa_fieldcatalog_prix-fieldname = 'DWERT'.
  wa_fieldcatalog_prix-ref_table = 'ARSEG'.
  wa_fieldcatalog_prix-ref_field = 'DWERT'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'Ecart'(c02).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.

  wa_fieldcatalog_prix-fieldname = 'WAERS'.
  wa_fieldcatalog_prix-ref_table = 'EKKO'.
  wa_fieldcatalog_prix-ref_field = 'WAERS'.
  wa_fieldcatalog_prix-hotspot   = 'X'.
  wa_fieldcatalog_prix-coltext   = 'Devise'(c01).
  APPEND wa_fieldcatalog_prix TO wt_catalog_prix.
  CLEAR wa_fieldcatalog_prix.



ENDFORM.                    " fieldcatalog_params_for_alv
*&---------------------------------------------------------------------*
*&      Form  toolbar_params_for_alv
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM toolbar_params_prix CHANGING pt_exclude TYPE ui_functions.


  DATA ls_exclude TYPE ui_func.

  ls_exclude = cl_gui_alv_grid=>mc_fc_auf.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_average.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_back_classic.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_abc.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_chain.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_crbatch.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_crweb.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_lineitems.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_master_data.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_more.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_report.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_xint.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_xxl.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_check.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_col_invisible.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_col_optimize.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_current_variant.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_data_save.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_delete_filter.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_deselect_all.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_detail.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_excl_all.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrdata.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrdesig.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrtempl.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expmdb.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_extend.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_f4.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_filter.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_find.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_fix_columns.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_graph.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_help.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_html.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_info.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_load_variant.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_append_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_cut.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_delete_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_move_row.
  APPEND ls_exclude TO pt_exclude.
*
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste_new_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_undo.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_maintain_variant.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_maximum.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_minimum.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_pc_file.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_print.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_print_back.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_print_prev.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_refresh.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_reprep.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_save_variant.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_select_all.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_send.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_separator.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_sort.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_sort_asc.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_sort_dsc.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_subtot.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_sum.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_to_office.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_to_rep_tree.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_unfix_columns.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_views.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_view_crystal.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_view_excel.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_view_grid.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_word_processor.
*  APPEND ls_exclude TO pt_exclude.


ENDFORM.                               " TOOLBAR_PARAMS_FOR_ALV
*&---------------------------------------------------------------------*
*&      Form  f_validation
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_validation.

*\--> itvla20070417 - POPUP for "Credit note" handling
  CASE w_9200_okcode.
    WHEN 'GENAVOIR'.
      CLEAR w_answer.
* Begin of UPG_ECC5_TO_ECC6 DEL DKR001
*      CALL FUNCTION 'POPUP_TO_CONFIRM_WITH_MESSAGE'
*        EXPORTING
**       DEFAULTOPTION        = 'Y'
*diagnosetext1 = 'Note de crédit doit être manuellement avec MIRO.'(t59)
**       DIAGNOSETEXT2        = ' '
**       DIAGNOSETEXT3        = ' '
*       textline1            = 'Note de crédit créée?'(t60)
**       TEXTLINE2            = ' '
*          titel = 'Note de crédit'(t58)
**       START_COLUMN         = 25
**       START_ROW            = 6
**       CANCEL_DISPLAY       = 'X'
*       IMPORTING
*         answer               = w_answer.

*      IF w_answer = 'J'.
*        CLEAR w_answer.
*      ELSE.
*        CLEAR w_decision.
*        EXIT.
*      ENDIF.
* End of   UPG_ECC5_TO_ECC6 DEL DKR001
* Begin of UPG_ ECC5_TO_ECC6 INS DKR001
      CALL FUNCTION 'POPUP_TO_CONFIRM'
        EXPORTING
         TITLEBAR                    = 'Note de crédit'(t58)
         TEXT_QUESTION               = 'Note de crédit créée?'(t60)
         TEXT_BUTTON_1               = 'Ja'(001)
         TEXT_BUTTON_2               = 'Nein'(002)
         DEFAULT_BUTTON              = '1'
       IMPORTING
         ANSWER                      = w_answer
       EXCEPTIONS
         TEXT_NOT_FOUND              = 1
         OTHERS                      = 2.


      IF w_answer = '1'.
* End of   UPG_ECC5_TO_ECC6 INS DKR001
        CLEAR w_answer.
      ELSE.
        CLEAR w_decision.
        EXIT.
      ENDIF.

    WHEN OTHERS.
*nothing!
  ENDCASE.
*\<-- itvla20070417 - POPUP for "Credit note" handling



  CLEAR w_answer.
* Begin of UPG_ECC5_TO_ECC6 DEL DKR001
*  CALL FUNCTION 'POPUP_TO_CONFIRM_WITH_MESSAGE'
*    EXPORTING
**       DEFAULTOPTION        = 'Y'
*      diagnosetext1        = 'Déblocage facture'(t38)
**       DIAGNOSETEXT2        = ' '
**       DIAGNOSETEXT3        = ' '
*      textline1            = 'Les postes sélectionnés'(t39)
*      textline2            = 'vont être débloqués'(t40)
*      titel                = 'Déblocage facture'(t38)
**       START_COLUMN         = 25
**       START_ROW            = 6
**       CANCEL_DISPLAY       = 'X'
*   IMPORTING
*     answer               = w_answer
*            .
*  IF w_answer = 'J'.
* End of   UPG_ECC5_TO_ECC6 DEL DKR001
* Begin of UPG_ ECC5_TO_ECC6 INS DKR001
        CALL FUNCTION 'POPUP_TO_CONFIRM'
        EXPORTING
         TITLEBAR                    = 'Déblocage facture'(t38)
         TEXT_QUESTION               = 'Les postes sélectionnés'(t39)
         TEXT_BUTTON_1               = 'Ja'(001)
         ICON_BUTTON_1               = ' '
         TEXT_BUTTON_2               = 'Nein'(002)
         DEFAULT_BUTTON              = '1'
       IMPORTING
         ANSWER                      = w_answer
       EXCEPTIONS
         TEXT_NOT_FOUND              = 1
         OTHERS                      = 2.


      IF w_answer = '1'.
* End of   UPG_ECC5_TO_ECC6 INS DKR001
* Récupération des lignes sélectionnées
    REFRESH: wt_row_prix, wt_roid_prix.
    CALL METHOD o_prix_grid->get_selected_rows
      IMPORTING
        et_index_rows = wt_row_prix
        et_row_no     = wt_roid_prix.

    t_item_prix[] = wt_alv_prix[].
    LOOP AT wt_row_prix INTO ls_row.
      READ TABLE t_item_prix INDEX ls_row-index.
      t_item_prix-sel = 'X'.
      MODIFY t_item_prix INDEX ls_row-index.
    ENDLOOP.


*----------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 24.08.2006
* Obliger l'utilisateur à sélectionner au moins une ligne

    IF LINES( wt_row_prix ) = 0.
      MESSAGE 'Veuillez sélectionner au moins un poste' TYPE 'W'.
      EXIT.
    ENDIF.

* T. NGUYEN (Appia Consulting) - 24.08.2006
*----------------------------------------------------------------------*

*        call function 'ZBALANCE_DEBLO_FACT_ECARTPRIX'
*          exporting
*            i_belnr                         = t_item_prix-belnr
*            i_gjahr                         = t_item_prix-gjahr
**         tables
**            ti_selected_invoice_items       =
    .


    w_decision = 'POSTVAL'.
    SET SCREEN 0.LEAVE SCREEN .
  ELSE.
    EXIT.
  ENDIF.
ENDFORM.                    " f_validation
