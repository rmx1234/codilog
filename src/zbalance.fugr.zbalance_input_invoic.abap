************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Module d'intégration des factures MM sans poste     *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : Balance For MySAP Business Suite                            *
*                                                                      *
* Auteur : Tan NGUYEN (Appiaconsulting)                                *
*                                                                      *
* Date   : 27/06/06                                                    *
*                                                                      *
* Ordre de transport: GEDK900028                                       *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
* Auteur : Reynald GIL RGI003                                          *
* Date   : 16/04/2007                                                  *
*	  : Ajout Contrôle sur EM   		                       *
*        : Avant : si EM manquante -> PaS d'ajout postes idocs         *
*        : Après : Même si EM manquante -> Ajout Postes Idocs
************************************************************************
* Auteur : Reynald GIL RGI008                                          *
* Date   : 18/07/2007                                                  *
*	  : Ajout Contrôle TVA   		                       *
************************************************************************
*  02.2007 ! ITGGH        ! CHG_01                          DE5K900049 *
*          !              ! Save invoice in archive                    *
*          !              !                                            *
*          !              !                                            *
*          !              !                                            *
************************************************************************
*  10.2007 ! OHA          ! FI_322_DIA                          OHA001 *
*          !              ! Adding controle on company code IDOC and   *
*          !              ! purchase order                             *
*          !              !                                            *
************************************************************************
*  10.2007 ! OHA          ! FI_321_DIA                          OHA002 *
*          !              ! Adding controle on order item deletion     *
*          !              !                                            *
************************************************************************
*  10.2007 ! OHA          ! FI_332_DIA                          OHA003 *
*          !              ! Create parked credit memo for type CRME    *
*          !              !                                            *
************************************************************************
*  10.2007 ! OHA          ! FI_339_DIA                          OHA004 *
*          !              ! Add purchase order in ZBAL_IDOC            *
*          !              !                                            *
************************************************************************
*  12.2007 ! JMJ          ! FI_365_DIA                          JMJ001 *
*          !              ! New step for missing receipt               *
*          !              !                                            *
************************************************************************
*  01.2008 ! JMJ          ! FI_365_DIA                          JMJ002 *
*          !              ! Change logic for missing receipt .         *
*          !              !                                            *
************************************************************************
*  01.2008 ! JMJ          ! FI_398_DIA                          JMJ003 *
*          !              ! Change decimals number for value in E1EDP26*
*          !              !                                            *
************************************************************************
*  05.2008 ! FCH          ! DIA0000148                          FCH001 *
*          !              ! Changed calculation of item amount for     *
*          !              ! Services.                                  *
************************************************************************


FUNCTION zbalance_input_invoic.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(INPUT_METHOD) LIKE  BDWFAP_PAR-INPUTMETHD
*"     VALUE(MASS_PROCESSING) LIKE  BDWFAP_PAR-MASS_PROC
*"  EXPORTING
*"     VALUE(WORKFLOW_RESULT) LIKE  BDWFAP_PAR-RESULT
*"     VALUE(APPLICATION_VARIABLE) LIKE  BDWFAP_PAR-APPL_VAR
*"     VALUE(IN_UPDATE_TASK) LIKE  BDWFAP_PAR-UPDATETASK
*"     VALUE(CALL_TRANSACTION_DONE) LIKE  BDWFAP_PAR-CALLTRANS
*"  TABLES
*"      IDOC_CONTRL STRUCTURE  EDIDC
*"      IDOC_DATA STRUCTURE  EDIDD
*"      IDOC_STATUS STRUCTURE  BDIDOCSTAT
*"      RETURN_VARIABLES STRUCTURE  BDWFRETVAR
*"      SERIALIZATION_INFO STRUCTURE  BDI_SER
*"  EXCEPTIONS
*"      WRONG_FUNCTION_CALLED
*"----------------------------------------------------------------------

* Vérifier la présence de postes dans l'IDoc
  DATA: w_sans_poste   TYPE c,
        w_new_docctrl  TYPE edidc,
        wt_new_docdata TYPE TABLE OF edidd,
        wt_return      TYPE bal_t_msg.       "ITGGH  12.02.2007  CHG_01

* BEGIN INSERT FI_300_DIA
  DATA : st_e1edp02 TYPE e1edp02,
         st_e1edk14 TYPE e1edk14,
         st_e1edk02 TYPE e1edk02,
         st_e1edka1 TYPE e1edka1.
* END INSERT FI_300_DIA
* JMJ001 -> add new step for Missing Recept.
  DATA:    new_idoc.
*

  DATA iw_status TYPE bdidocstat.
  DATA w_status TYPE bdidocstat.

  PERFORM verif_presence_postes TABLES idoc_data CHANGING w_sans_poste.

*"----------------------------------------------------------------------
* Si l'IDoc a au moins un poste
*"----------------------------------------------------------------------

  IF w_sans_poste IS INITIAL.
*>> JMJ002 - Change logic
*"Add JMJ001 -> New step for missing-receipt process
*    clear new_idoc.
*    perform control_recept tables idoc_data
*                                 idoc_contrl
*                                 idoc_status
*                          changing new_idoc.
*   check new_idoc ne 'X'.
*"Add JMJ001 <- New step for missing-receipt process
    CALL FUNCTION 'ZBALANCE_INPUT_INVOIC_MRM'
      EXPORTING
        input_method          = input_method
        mass_processing       = mass_processing
      IMPORTING
        workflow_result       = workflow_result
        application_variable  = application_variable
        in_update_task        = in_update_task
        call_transaction_done = call_transaction_done
      TABLES
        idoc_contrl           = idoc_contrl
        idoc_data             = idoc_data
        idoc_status           = idoc_status
        return_variables      = return_variables
        serialization_info    = serialization_info
      EXCEPTIONS
        wrong_function_called = 1
        OTHERS                = 2.

* BEGIN INSERT FI_300_DIA
    READ TABLE idoc_contrl INDEX 1.
    IF sy-subrc = 0 AND
    ( idoc_contrl-mescod = 'FI' OR idoc_contrl-mescod = 'MM' ).

      CLEAR zbal_idoc.

      MOVE-CORRESPONDING idoc_contrl TO zbal_idoc.

      LOOP AT idoc_data WHERE segnam = 'E1EDP02'.
        MOVE idoc_data-sdata TO st_e1edp02.
        CHECK st_e1edp02-qualf = '001'.
        CLEAR: zbal_idoc-bukrs, zbal_idoc-werks.
        SELECT SINGLE werks
                 FROM ekpo
*                 INTO (zbal_idoc-bukrs, zbal_idoc-werks)
                 INTO zbal_idoc-werks
                WHERE ebeln = st_e1edp02-belnr
                  AND ebelp = st_e1edp02-zeile.
        IF sy-subrc = 0.
* >> OHA003 ++ FI_339_DIA
          zbal_idoc-ebeln = st_e1edp02-belnr.
* >> OHA003 ++ FI_339_DIA
          EXIT.
        ENDIF.
      ENDLOOP.
*      IF sy-subrc NE 0.
      LOOP AT idoc_data WHERE segnam = 'E1EDK14'.
        MOVE idoc_data-sdata TO st_e1edk14.
        IF st_e1edk14-qualf = '011'.
          WRITE st_e1edk14-orgid TO zbal_idoc-bukrs.
          EXIT.
        ENDIF.
      ENDLOOP.
*      ENDIF.

      LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
        MOVE idoc_data-sdata TO st_e1edka1.
        IF st_e1edka1-parvw = 'LF'.
          WRITE st_e1edka1-partn TO zbal_idoc-sndprn RIGHT-JUSTIFIED.
          TRANSLATE zbal_idoc-sndprn USING ' 0'.
        ELSEIF st_e1edka1-parvw = 'RE'.
          IF st_e1edka1-name1(10) IS NOT INITIAL
          AND st_e1edka1-name1(10) CO ' 0123456789'.
            MOVE st_e1edka1-name1(10) TO zbal_idoc-ebeln.
          ENDIF.
        ELSE.
        ENDIF.
      ENDLOOP.

      LOOP AT idoc_data WHERE segnam = 'E1EDK02'.
        MOVE idoc_data-sdata TO st_e1edk02.
        CHECK st_e1edk02-qualf = '009'.
        MOVE st_e1edk02-belnr TO zbal_idoc-belnr.
        EXIT.
      ENDLOOP.

*>> RGI008 FI_309
      DATA xe1eds01 LIKE e1eds01.
      LOOP AT idoc_data WHERE segnam = 'E1EDS01'.
        MOVE idoc_data-sdata TO xe1eds01.
        REPLACE ',' WITH '.' INTO xe1eds01-summe.
        CASE idoc_data-sdata+0(3).
          WHEN '011'. "TTC
            MOVE xe1eds01-summe TO zbal_idoc-betrg.
          WHEN '010'. "HT
            MOVE xe1eds01-summe TO zbal_idoc-wrbtr.
        ENDCASE.
        IF xe1eds01-sunit IS INITIAL.
          MOVE xe1eds01-waerq TO zbal_idoc-waers.
        ELSE.
          MOVE xe1eds01-sunit TO zbal_idoc-waers.
        ENDIF.
      ENDLOOP.
*>> RGI FI_309

      READ TABLE idoc_status INDEX 1.
      IF sy-subrc = 0.
        IF zbal_idoc-werks IS INITIAL AND idoc_status-status = '53'
       AND zbal_idoc-mescod = 'MM'.
          SELECT SINGLE werks INTO zbal_idoc-werks
                              FROM rseg WHERE belnr = idoc_status-msgv1
                                          AND gjahr = sy-datum+0(4).
        ENDIF.

        MOVE idoc_status-status TO zbal_idoc-status.
        MOVE idoc_status-msgty  TO zbal_idoc-statyp.
        MOVE idoc_status-msgid  TO zbal_idoc-stamid.
        MOVE idoc_status-msgno  TO zbal_idoc-stamno.
        MOVE idoc_status-msgv1  TO zbal_idoc-stapa1.
        MOVE idoc_status-msgv2  TO zbal_idoc-stapa2.
        MOVE idoc_status-msgv3  TO zbal_idoc-stapa3.
        MOVE idoc_status-msgv4  TO zbal_idoc-stapa4.
      ENDIF.

      zbal_idoc-upddat = sy-datum.
      zbal_idoc-updtim = sy-uzeit.

      MODIFY zbal_idoc.
    ENDIF.
* END INSERT FI_300_DIA

    wt_new_docdata[] = idoc_data[].         " ITGGH  12.02.2007  CHG_01

*"----------------------------------------------------------------------
* Si l'IDoc n'a pas de poste
*"----------------------------------------------------------------------

  ELSE.

*   Préparer les segments de contrôle
    DATA w_docnum TYPE edidc-docnum.

    READ TABLE idoc_contrl INTO w_new_docctrl INDEX 1.
    w_docnum = w_new_docctrl-docnum.
    CLEAR w_new_docctrl-docnum.

*   Récupérer les postes de la commande
    DATA wt_ekpo TYPE TABLE OF ekpo.
    DATA w_lifnr TYPE lifnr.
    DATA w_xblnr TYPE mkpf-xblnr.
    PERFORM recup_postes TABLES idoc_data wt_ekpo
      CHANGING w_lifnr w_xblnr.

*-----------------------------------------------------------------------*
*   Extension Balance : Si la devise de la commende est différente de la*
*   devise de la facture, bloquer l'idoc                                *
*   Modif du 13/09/2009                                                 *
*   par Louis Roussard                                                  *
*------------------------------------------------------------------------*

* Vérifier la cohérence des devises
    DATA w_devise_comm TYPE ekko-waers.
    DATA w_devise_fact TYPE ekko-waers.
    DATA w_devise TYPE e1eds01.
    DATA w_fact TYPE e1edka1.
    DATA c_commande TYPE ekko-ebeln.
* BEGIN INSERT OHA001 FI_322_DIA
    DATA c_bukrs TYPE ekko-bukrs.
    DATA c_cdbuk TYPE ekko-bukrs.

    CLEAR : c_cdbuk, c_bukrs.
* END INSERT OHA001 FI_322_DIA

    LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
      w_fact = idoc_data-sdata.
      IF w_fact-parvw = 'RE'.
        c_commande = w_fact-name1.
        c_bukrs = w_fact-partn.
      ENDIF.
    ENDLOOP.

* BEGIN INSERT OHA001 FI_322_DIA
    LOOP AT idoc_data WHERE segnam = 'E1EDK14'.
      MOVE idoc_data-sdata TO st_e1edk14.
      IF st_e1edk14-qualf = '011'.
        WRITE st_e1edk14-orgid TO c_bukrs.
        EXIT.
      ENDIF.
    ENDLOOP.
* END INSERT OHA001 FI_322_DIA

* BEGIN REPLACE OHA001 FI_322_DIA OHA001
*    SELECT SINGLE waers FROM ekko INTO w_devise_comm WHERE ebeln = c_commande .
    SELECT SINGLE bukrs waers FROM ekko INTO (c_cdbuk,w_devise_comm) WHERE ebeln = c_commande .
* END INSERT OHA001 FI_322_DIA

*    Si numéro de commande non trouvé dans EKKO, mettre la facture en rapprochement
*    Appia 21.09.06
*
    IF sy-subrc NE 0.
      w_status-docnum = w_docnum.
      w_status-status = '51'.
      w_status-msgty  = 'A'.
      w_status-msgid  ='ZSAPBALANCE'.
      w_status-msgno  = '001'.
      APPEND w_status TO idoc_status.

*      RETURN.
    ENDIF.

* BEGIN INSERT OHA001 FI_322_DIA
* Check purchase order compaby code and IDOC company code
    IF c_cdbuk NE c_bukrs.
      w_status-docnum = w_docnum.
      w_status-status = '51'.
      w_status-msgty  = 'A'.
      w_status-msgid  ='ZSAPBALANCE'.
      w_status-msgno  = '052'.
      w_status-msgv1  = c_cdbuk.
      w_status-msgv2  = c_bukrs.

      APPEND w_status TO idoc_status.
*      RETURN.
    ENDIF.


    IF NOT w_xblnr IS INITIAL
    AND NOT c_commande IS INITIAL.
      READ TABLE wt_ekpo TRANSPORTING NO FIELDS
      WITH KEY ebeln = c_commande.
      IF sy-subrc NE 0.
        w_status-docnum = w_docnum.
        w_status-status = '51'.
        w_status-msgty  = 'A'.
        w_status-msgid  ='ZSAPBALANCE'.
        w_status-msgno  = '053'.
        w_status-msgv1  = w_xblnr.
        w_status-msgv2  = c_commande.
        APPEND w_status TO idoc_status.
*        RETURN.
      ENDIF.
    ENDIF.
* BEGIN INSERT OHA001 FI_322_DIA


    READ TABLE idoc_data WITH KEY segnam = 'E1EDS01'.
    w_devise = idoc_data-sdata.

    w_devise_fact = w_devise-waerq.


*   Si les deux devises diffèrent
    IF w_devise_fact NE w_devise_comm .
      w_status-docnum = w_docnum.
      w_status-status = '51'.
      w_status-msgty  = 'A'.
      w_status-msgid  ='ZSAPBALANCE'.
      w_status-msgno  = '050'.
      APPEND w_status TO idoc_status.

*      RETURN.
    ENDIF.

*            Fin de l'extension
*               LRO, Appia Consulting
*>> RGI005 -
*>> Checking Items against Good Receipt and Invoicing.
    PERFORM delete_not_relevant TABLES wt_ekpo
                                CHANGING w_xblnr.
*>> END RGI005

*   Si aucun poste de commande n'a pu être récupéré
    IF LINES( wt_ekpo ) = 0.

*      DATA w_status TYPE bdidocstat.
      CLEAR  w_status .

      w_status-docnum = w_docnum.
      w_status-status = '51'.
      w_status-msgty  = 'A'.
      w_status-msgid  ='ZSAPBALANCE'.
      w_status-msgno  = '001'.
      APPEND w_status TO idoc_status.

*      RETURN.

    ENDIF.

*   Récupérer le taux TVA
    DATA w_e1edk04 TYPE e1edk04.
    DATA w_idoc_data TYPE edidd.
    READ TABLE idoc_data INTO w_idoc_data WITH KEY segnam = 'E1EDK04'.
    IF sy-subrc = 0.
      w_e1edk04 = w_idoc_data-sdata.
    ENDIF.

*   Transformer les postes sous forme IDoc
    DATA wt_postes TYPE TABLE OF edidd.
    PERFORM trans_ekpo_idoc TABLES wt_ekpo wt_postes idoc_data
      USING w_e1edk04-msatz w_lifnr w_xblnr.

*   Si pas de qté à facturer
    IF LINES( wt_postes ) = 0.

      CLEAR w_status.

      w_status-docnum = w_docnum.
      w_status-status = '51'.
      w_status-msgty  = 'A'.
      w_status-msgid  ='ZSAPBALANCE'.
      w_status-msgno  = '001'.
      APPEND w_status TO idoc_status.

*      RETURN.

    ENDIF.

    IF w_status-status = '51'.
* BEGIN INSERT FI_300_DIA
      READ TABLE idoc_contrl INDEX 1.
      IF sy-subrc = 0 AND
      ( idoc_contrl-mescod = 'FI' OR idoc_contrl-mescod = 'MM' ).

        CLEAR zbal_idoc.

        MOVE-CORRESPONDING idoc_contrl TO zbal_idoc.

        LOOP AT idoc_data WHERE segnam = 'E1EDP02'.
          MOVE idoc_data-sdata TO st_e1edp02.
          CHECK st_e1edp02-qualf = '001'.
          CLEAR: zbal_idoc-bukrs, zbal_idoc-werks.
          SELECT SINGLE bukrs werks
                   FROM ekpo
                   INTO (zbal_idoc-bukrs, zbal_idoc-werks)
                  WHERE ebeln = st_e1edp02-belnr
                    AND ebelp = st_e1edp02-zeile.
          IF sy-subrc = 0.
            EXIT.
          ENDIF.
        ENDLOOP.
        IF sy-subrc NE 0.
          LOOP AT idoc_data WHERE segnam = 'E1EDK14'.
            MOVE idoc_data-sdata TO st_e1edk14.
            IF st_e1edk14-qualf = '011'.
              WRITE st_e1edk14-orgid TO zbal_idoc-bukrs.
              EXIT.
            ENDIF.
          ENDLOOP.
        ENDIF.

        LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
          MOVE idoc_data-sdata TO st_e1edka1.
          IF st_e1edka1-parvw = 'LF'.
            WRITE st_e1edka1-partn TO zbal_idoc-sndprn RIGHT-JUSTIFIED.
            TRANSLATE zbal_idoc-sndprn USING ' 0'.
            EXIT.
          ENDIF.
        ENDLOOP.

        LOOP AT idoc_data WHERE segnam = 'E1EDK02'.
          MOVE idoc_data-sdata TO st_e1edk02.
          CHECK st_e1edk02-qualf = '009'.
          MOVE st_e1edk02-belnr TO zbal_idoc-belnr.
          EXIT.
        ENDLOOP.

*>> RGI008 FI_309
*        DATA xe1eds01 LIKE e1eds01.
        LOOP AT idoc_data WHERE segnam = 'E1EDS01'.
          MOVE idoc_data-sdata TO xe1eds01.
          REPLACE ',' WITH '.' INTO xe1eds01-summe.
          CASE idoc_data-sdata+0(3).
            WHEN '011'. "TTC
              MOVE xe1eds01-summe TO zbal_idoc-betrg.
            WHEN '010'. "HT
              MOVE xe1eds01-summe TO zbal_idoc-wrbtr.
          ENDCASE.
          IF xe1eds01-sunit IS INITIAL.
            MOVE xe1eds01-waerq TO zbal_idoc-waers.
          ELSE.
            MOVE xe1eds01-sunit TO zbal_idoc-waers.
          ENDIF.
        ENDLOOP.
*>> RGI FI_309

        READ TABLE idoc_status INDEX 1.
        IF sy-subrc = 0.
          MOVE idoc_status-status TO zbal_idoc-status.
          MOVE idoc_status-msgty  TO zbal_idoc-statyp.
          MOVE idoc_status-msgid  TO zbal_idoc-stamid.
          MOVE idoc_status-msgno  TO zbal_idoc-stamno.
          MOVE idoc_status-msgv1  TO zbal_idoc-stapa1.
          MOVE idoc_status-msgv2  TO zbal_idoc-stapa2.
          MOVE idoc_status-msgv3  TO zbal_idoc-stapa3.
          MOVE idoc_status-msgv4  TO zbal_idoc-stapa4.
        ENDIF.

        zbal_idoc-upddat = sy-datum.
        zbal_idoc-updtim = sy-uzeit.

        MODIFY zbal_idoc.
      ENDIF.
* END INSERT FI_300_DIA
      RETURN.
    ENDIF.

*   Récuper l'index d'insertion des postes
    DATA w_index_ins TYPE i.
    READ TABLE idoc_data WITH KEY segnam = 'E1EDS01'.
    w_index_ins = sy-tabix.

*   Compléter l'IDoc avec les postes
    wt_new_docdata[] = idoc_data[].
    INSERT LINES OF wt_postes INTO wt_new_docdata INDEX w_index_ins.

*   Créer le nouvel IDoc avec postes
    DATA w_new_docnum TYPE edidc-docnum.

    CALL FUNCTION 'IDOC_INBOUND_WRITE_TO_DB'
      IMPORTING
        pe_idoc_number    = w_new_docnum
      TABLES
        t_data_records    = wt_new_docdata
      CHANGING
        pc_control_record = w_new_docctrl
      EXCEPTIONS
        idoc_not_saved    = 1
        OTHERS            = 2.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
             WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      RETURN.
    ENDIF.

*   Donner un statut de traitement valide (59, 51 ou 62) au nouvel IDoc
    CALL FUNCTION 'ZBALANCE_IDOC_STATUS_CHANGE'
      EXPORTING
        docnum = w_new_docnum
        status = '62'
        msgty  = 'S'
        msgid  = 'ZSAPBALANCE'
        msgno  = '048'
        msgv1  = space
        msgv2  = space
        msgv3  = space
        msgv4  = space.

*   Donner un statut 70 'Original d'un IDOC qui a été édité'
    DATA w_msgv1 TYPE edi_stapa1.
    w_msgv1 = w_new_docnum.

    CLEAR w_status.
    w_status-docnum = w_docnum.
    w_status-status = '70'.
    w_status-msgty  = 'S'.
    w_status-msgid  ='ZSAPBALANCE'.
    w_status-msgno  = '049'.
    w_status-msgv1  = w_msgv1.
    APPEND w_status TO idoc_status.

* BEGIN INSERT FI_300_DIA
    CLEAR zbal_idoc.
    DELETE FROM zbal_idoc WHERE docnum = w_docnum.
* END INSERT FI_300_DIA

*   Traiter le nouvel IDoc
    CALL FUNCTION 'IDOC_MANUAL_INPUT'
      EXPORTING
        idoc_number                  = w_new_docnum
        input_exception              = '0'
        no_dialog                    = 'X'
      EXCEPTIONS
        idoc_not_in_database         = 1
        no_input_function_found      = 2
        no_function_parameters_found = 3
        no_status_record_found       = 4
        no_authorization             = 5
        OTHERS                       = 6.

  ENDIF.

* ITGGH   12.02.2007   CHG_01                               DE5K900049
* save invoice in archive
*  READ TABLE idoc_contrl INTO w_new_docctrl INDEX 1.
*  CALL FUNCTION 'ZBALANCE_ARCHIVE_LINK_MM'
*    EXPORTING
*      idoc_contrl = w_new_docctrl
*    TABLES
*      idoc_data   = wt_new_docdata
*      idoc_status = idoc_status
*    CHANGING
*      e_return    = wt_return.
*  IF wt_return[] IS INITIAL.
*    COMMIT WORK AND WAIT.
*  ELSE.
*    ROLLBACK WORK.
*  ENDIF.

ENDFUNCTION.

*&--------------------------------------------------------------------*
*&      Form  verif_presence_postes
*&--------------------------------------------------------------------*
*       text
*---------------------------------------------------------------------*
*      -->X_IDOC_DATAtext
*      -->X_SANS_POSTtext
*---------------------------------------------------------------------*
FORM verif_presence_postes
  TABLES x_idoc_data STRUCTURE edidd
  CHANGING x_sans_poste TYPE c.

  DATA w_ebeln LIKE ekko-ebeln.
  DATA st_e1edka1 LIKE e1edka1.
  READ TABLE x_idoc_data WITH KEY segnam = 'E1EDP01'.

  IF sy-subrc = 0.
    CLEAR x_sans_poste.
  ELSE.
    x_sans_poste = 'X'.
  ENDIF.

* Add - Jean-Michel BRUNOD - 05.04.2007
* Bug missing good rcpt SGS
*  CLEAR w_ebeln.
*  LOOP AT x_idoc_data WHERE segnam = 'E1EDKA1'.
*    MOVE x_idoc_data-sdata TO st_e1edka1.
*    IF st_e1edka1-parvw = 'RE'.
*      w_ebeln = st_e1edka1-name1.
*    ENDIF.
*  ENDLOOP.
*
*  IF NOT w_ebeln IS INITIAL.
*    SELECT SINGLE ebeln INTO w_ebeln FROM ekko WHERE ebeln = w_ebeln.
*    IF sy-subrc = 0.
*      CLEAR x_sans_poste.
*    ENDIF.
*  ENDIF.
* End add - Jean-Michel BRUNOD - 05.04.2007

ENDFORM.                    "verif_presence_postes

*&--------------------------------------------------------------------*
*&      Form  recup_postes
*&--------------------------------------------------------------------*
*       text
*---------------------------------------------------------------------*
*      -->X_IDOC_DATAtext
*      -->X_POSTES   text
*---------------------------------------------------------------------*
FORM recup_postes
  TABLES x_idoc_data STRUCTURE edidd
         x_ekpo STRUCTURE ekpo
  CHANGING x_lifnr TYPE lifnr
           x_xblnr TYPE c.

  REFRESH x_ekpo.

* Récupérer le segment E1EDKA1
  DATA w_e1edka1 TYPE e1edka1.
  DATA w_e1edka1_re TYPE e1edka1.
  DATA w_e1edka1_lf TYPE e1edka1.
  DATA w_idoc_data TYPE edidd.

  LOOP AT x_idoc_data INTO w_idoc_data WHERE segnam = 'E1EDKA1'.

    w_e1edka1 = w_idoc_data-sdata.

    IF w_e1edka1-parvw = 'RE'.
      w_e1edka1_re = w_idoc_data-sdata.
    ELSEIF w_e1edka1-parvw = 'LF'.
      w_e1edka1_lf = w_idoc_data-sdata.
    ENDIF.

  ENDLOOP.

* Récupérer le code fournisseur
  x_lifnr = w_e1edka1_lf-partn  .
  SHIFT x_lifnr RIGHT DELETING TRAILING space.
  OVERLAY x_lifnr WITH '0000000000'.

* Récupérer le segment E1EDK02
  DATA w_e1edk02 TYPE e1edk02.
  DATA w_e1edk02_063 TYPE e1edk02.

  LOOP AT x_idoc_data INTO w_idoc_data WHERE segnam = 'E1EDK02'.

    w_e1edk02 = w_idoc_data-sdata.

    IF w_e1edk02-qualf = '063'.
      w_e1edk02_063 = w_idoc_data-sdata.
    ENDIF.

  ENDLOOP.

* Récupérer les postes de commande en fonction du Bon de Livraison
  IF w_e1edk02_063-belnr IS NOT INITIAL.

    DATA wt_mseg TYPE TABLE OF mseg.

    SELECT mseg~mblnr mseg~mjahr mseg~ebeln mseg~ebelp
    FROM mseg INNER JOIN mkpf
      ON mseg~mblnr = mkpf~mblnr AND mseg~mjahr = mkpf~mjahr
    INTO CORRESPONDING FIELDS OF TABLE wt_mseg
    WHERE xblnr = w_e1edk02_063-belnr
      AND mseg~lifnr = x_lifnr.

    DATA w_mseg TYPE mseg.
    DATA w_ekpo TYPE ekpo.


* L Roussard , le 18/09/06
* N° de BL fournisseur figurant dans l'idoc inexistant dans SAP ou différent de ce
* qui est dans SAP pour les commandes normales.
*


    DATA nb_lignes TYPE i.
    DESCRIBE TABLE wt_mseg LINES nb_lignes.

* Si la sélection n'a pas trouvé de BL (nbre de lignes = 0)
    IF nb_lignes = 0.

* Faire la sélection sur base de la commande et vérifier le type
*1. Type de document commande
      SELECT SINGLE  * FROM ekko "INTO TABLE x_ekpo
      WHERE ebeln = w_e1edka1_re-name1
      AND bsart IN ('NB', 'I', 'LB', 'M','MAG','NBD','NBE', 'P', 'ZFE', 'ZIN', 'ZCTO').

* Si le type correspond,
      IF sy-subrc = 0.

* BEGIN REPLACE OHA002 FI_321_DIA
*        SELECT * FROM ekpo INTO TABLE x_ekpo
*        WHERE ebeln = w_e1edka1_re-name1.
        SELECT * FROM ekpo INTO TABLE x_ekpo
        WHERE ebeln = w_e1edka1_re-name1
          AND loekz = ' '.
* END   REPLACE OHA002 FI_321_DIA
      ELSE.
*  Point numéro 8 de la DIA : Type de document programme de livraison
        SELECT SINGLE  * FROM ekko "INTO TABLE x_ekpo
        WHERE ebeln = w_e1edka1_re-name1
        AND bsart IN ('LP', 'LPA').

* Si le type correspond,
        IF sy-subrc = 0.
* BEGIN REPLACE OHA002 FI_321_DIA
*          SELECT * FROM ekpo INTO TABLE x_ekpo
*          WHERE ebeln = w_e1edka1_re-name1.
          SELECT * FROM ekpo INTO TABLE x_ekpo
          WHERE ebeln = w_e1edka1_re-name1
            AND loekz = ' '.
* END   REPLACE OHA002 FI_321_DIA
          ws_prenerg = 'X'.
*>> RGI 009 Progr Liv en préenregistrement.
          ws_prgr_liv = 'X'.
        ENDIF.
      ENDIF.
* Si la sélection a trouvé les BL, trouver les postes correspontantes
    ELSE. "ENDIF.
      LOOP AT wt_mseg INTO w_mseg.

        CLEAR w_ekpo.

* BEGIN REPLACE OHA002 FI_321_DIA
*        SELECT SINGLE * FROM ekpo INTO w_ekpo
*          WHERE ebeln = w_mseg-ebeln
*            AND ebelp = w_mseg-ebelp.
        SELECT SINGLE * FROM ekpo INTO w_ekpo
          WHERE ebeln = w_mseg-ebeln
            AND ebelp = w_mseg-ebelp
            AND loekz = ' '.
* END REPLACE OHA002 FI_321_DIA
        IF sy-subrc = 0.
          APPEND w_ekpo TO x_ekpo.
        ENDIF.

      ENDLOOP.

*   Exporter le xblnr
      x_xblnr = w_e1edk02_063-belnr.
    ENDIF.
* Récupérer les postes de commande en fonction du n° de commande
  ELSEIF w_e1edka1_re-name1 IS NOT INITIAL.

* BEGIN REPLACE OHA002 FI_321_DIA
*    SELECT * FROM ekpo INTO TABLE x_ekpo
*    WHERE ebeln = w_e1edka1_re-name1.
    SELECT * FROM ekpo INTO TABLE x_ekpo
    WHERE ebeln = w_e1edka1_re-name1
      AND loekz = ' '.
* END   REPLACE OHA002 FI_321_DIA

  ENDIF.

  SORT x_ekpo.
  DELETE ADJACENT DUPLICATES FROM x_ekpo.

ENDFORM.                    "recup_postes

*&--------------------------------------------------------------------*
*&      Form  trans_ekpo_idoc
*&--------------------------------------------------------------------*
*       text
*---------------------------------------------------------------------*
*      -->X_EKPO     text
*      -->X_POSTES   text
*---------------------------------------------------------------------*
FORM trans_ekpo_idoc
  TABLES x_ekpo STRUCTURE ekpo
         x_postes STRUCTURE edidd
         idoc_data STRUCTURE edidd
  USING x_msatz TYPE c
        x_lifnr TYPE lifnr
        x_xblnr TYPE c.

  DATA w_edidd TYPE edidd.
  DATA w_ekpo TYPE ekpo.
  DATA w_e1edp01 TYPE e1edp01.
  DATA w_e1edp02 TYPE e1edp02.
  DATA w_e1edk02 TYPE e1edk02.
  DATA w_e1edp26 TYPE e1edp26.
  DATA w_e1edp04 TYPE e1edp04.
  DATA w_idoc_data TYPE edidd.
* BEGIN INSERT OHA003 FI_332_DIA
  DATA xcrme TYPE char1.
  DATA w_qte_fact TYPE ekpo-menge.
  DATA w_e1edk01 TYPE e1edk01.
  DATA: w_mtt_em_rest_a_fac TYPE ekbe-dmbtr. "Add JMJ003 - FI_398_DIA

  CLEAR xcrme.
  LOOP AT idoc_data WHERE segnam = 'E1EDK01'.
    w_e1edk01 = idoc_data-sdata.
    IF w_e1edk01-bsart = 'CRME'.
      xcrme = 'X'.
    ENDIF.
  ENDLOOP.
* END INSERT OHA003 FI_332_DIA

  LOOP AT x_ekpo INTO w_ekpo.

*   Récupérer la qté entrée en stock restante à facturer
    DATA w_qte_em_rest_a_fac TYPE ekpo-menge.

    CALL FUNCTION 'ZBALANCE_QTE_EM_REST_A_FAC'
      EXPORTING
        x_ebeln             = w_ekpo-ebeln
        x_ebelp             = w_ekpo-ebelp
        x_xblnr             = x_xblnr
      IMPORTING
        x_qte_em_rest_a_fac = w_qte_em_rest_a_fac
* BEGIN INSERT OHA003 FI_332_DIA
        x_qte_fact          = w_qte_fact
* END INSERT OHA003 FI_332_DIA
      EXCEPTIONS
        no_em_found         = 1.

*   Ne pas prendre en compte les postes dont le restant à facturer < 0
*>> Start RGI003 DEletion
**    IF w_qte_em_rest_a_fac <= 0.
**      CONTINUE.
**    ENDIF.
*>> Start RGI003 Addition
*    IF w_qte_em_rest_a_fac <= 0 AND sy-subrc = 0.
*    IF  sy-subrc = 0.
*      w_qte_em_rest_a_fac = w_ekpo-menge - w_qte_em_rest_a_fac.
**      CONTINUE.
*    ELSEIF sy-subrc NE 0.
*      w_qte_em_rest_a_fac = w_ekpo-menge.
*    ENDIF.

* BEGIN REPLACE OHA003 FI_332_DIA
    IF xcrme = 'X'.
      IF w_qte_fact <= 0.
        CONTINUE.
      ELSE.
        w_qte_em_rest_a_fac = w_qte_fact.
      ENDIF.
    ELSE.
      IF w_qte_em_rest_a_fac <= 0.
        CONTINUE.
      ENDIF.
    ENDIF.
*    IF w_qte_em_rest_a_fac <= 0.
*      CONTINUE.
*    ENDIF.
* END REPLACE OHA003 FI_332_DIA

*>> End Addition
*   segment e1edp01
    w_e1edp01-posex = w_ekpo-ebelp.
    w_e1edp01-menge = w_qte_em_rest_a_fac.
    w_e1edp01-bmng2 = w_qte_em_rest_a_fac.
    w_e1edp01-peinh = w_ekpo-peinh.
*>> RGI008 - Balance V2 Unité Achats
    w_e1edp01-bpumz = w_ekpo-bpumz.
    w_e1edp01-bpumn = w_ekpo-bpumn.
    w_e1edp01-menee = w_ekpo-meins.
    w_e1edp01-pmene = w_ekpo-bprme.

*>> RGI008 - Balance V2
*     w_e1edp26-betrg = ( ( w_qte_em_rest_a_fac / w_ekpo-peinh ) * ( w_ekpo-bpumz / w_ekpo-bpumn ) ) * w_ekpo-netpr.

    w_edidd-segnam = 'E1EDP01'.
    w_edidd-sdata = w_e1edp01.
    APPEND w_edidd TO x_postes.

*   segment e1edp02
    w_e1edp02-qualf = '001'.
    w_e1edp02-belnr = w_ekpo-ebeln.
    w_e1edp02-zeile = w_ekpo-ebelp.

    w_edidd-segnam = 'E1EDP02'.
    w_edidd-sdata = w_e1edp02.
    APPEND w_edidd TO x_postes.

* BEGIN INSERT MM_408_DIA
*   e1edp02 qualifiant 16
* Récupérer les postes de commande en fonction du Bon de Livraison

    IF x_xblnr IS NOT INITIAL.

      w_e1edp02-qualf = '016'.
      w_e1edp02-belnr = x_xblnr.
*    w_e1edp02-zeile = w_ekpo-ebelp.

      w_edidd-segnam = 'E1EDP02'.
      w_edidd-sdata = w_e1edp02.
      APPEND w_edidd TO x_postes.

    ENDIF.
* END INSERT MM_408_DIA

*   segment e1edp26
    w_e1edp26-qualf = '002'.

* Evolution Balance - Appia 21.09.2006
* Mise en commentaire du code original pour la gestion des commandes avec unité d'achat <>1
*    w_e1edp26-betrg = w_qte_em_rest_a_fac * w_ekpo-peinh * w_ekpo-netpr.

* Evolution Balance - Appia 21.09.2006
* Ajout pour gestion Unité Achat Differente =1 ou <> 1

*>> RGI008 - Correction v2 - Unité achat alternatives
*    w_e1edp26-betrg = w_qte_em_rest_a_fac / w_ekpo-peinh * w_ekpo-netpr.
*    w_e1edp26-betrg = ( ( w_qte_em_rest_a_fac / w_ekpo-peinh ) * ( w_ekpo-bpumz / w_ekpo-bpumn ) ) * w_ekpo-netpr.

* >> FI_398_DIA - problem for calcul - must change the decimal number - JMJ003repl
*   w_mtt_em_rest_a_fac = ( ( ( w_qte_em_rest_a_fac / w_ekpo-peinh ) * ( w_ekpo-bpumz / w_ekpo-bpumn ) )    "JMJ003 add
*                     * w_ekpo-netwr ) / w_ekpo-menge.      "JMJ001 - chg calculation w/o using unit price  "JMJ003 add
* FCH001 Added a different calculation for Services
    IF w_ekpo-pstyp = c_pstyp_9.   "Services    FCH001+
*     w_mtt_em_rest_a_fac is already correct
    ELSE.                                                   "FCH001+
      w_mtt_em_rest_a_fac = ( ( ( w_qte_em_rest_a_fac / w_ekpo-peinh ) * ( w_ekpo-bpumz / w_ekpo-bpumn ) )"JMJ003 add
                       * w_ekpo-netwr ) / ( ( w_ekpo-menge * w_ekpo-bpumz ) / ( w_ekpo-peinh * w_ekpo-bpumn ) ).

      "JMJ001 - chg calculation w/o using unit price  "JMJ003 add
    ENDIF.                                                  "FCH001+

    w_e1edp26-betrg = w_mtt_em_rest_a_fac.                  "JMJ003 add
*  w_e1edp26-betrg = ( ( ( w_qte_em_rest_a_fac / w_ekpo-peinh ) * ( w_ekpo-bpumz / w_ekpo-bpumn ) )    "JMJ003 del
*                     * w_ekpo-netwr ) / w_ekpo-menge.      "JMJ001 - chg calculation w/o using unit price "JMJ003 del
*    w_e1edp26-betrg = ( ( w_qte_em_rest_a_fac  ) * ( w_ekpo-bpumz / w_ekpo-bpumn ) ) * w_ekpo-netpr.

* Fin Evolution Balance - Appia 21.09.2006 Unité d'achat.


    w_edidd-segnam = 'E1EDP26'.
    w_edidd-sdata = w_e1edp26.
    APPEND w_edidd TO x_postes.

*   segment e1edp04
    IF x_msatz IS NOT INITIAL.

* Ne pas remplir le code TVA
*      SELECT SINGLE mwskz FROM t076m INTO w_e1edp04-mwskz
*      WHERE parart = 'LI'
*        AND konto = x_lifnr
*        AND mwsatz = x_msatz.

* BEGIN REPLACE OHA001 MM_397_DIA
*      w_e1edp04-mwskz = 'VAT'.
*
*      w_e1edp04-msatz = x_msatz.
*      w_e1edp04-mwsbt = x_msatz * w_e1edp26-betrg / 100.
*
      IF w_ekpo-mwskz IS INITIAL.
        w_e1edp04-mwskz = 'VAT'.
        w_e1edp04-msatz = x_msatz.
        w_e1edp04-mwsbt = x_msatz * w_e1edp26-betrg / 100.
*        w_edidd-segnam = 'E1EDP04'.
*        w_edidd-sdata = w_e1edp04.
*        APPEND w_edidd TO x_postes.
      ELSE.
        w_e1edp04-mwskz = w_ekpo-mwskz.
        w_e1edp04-msatz = x_msatz.
**        w_e1edp04-mwskz = w_ekpo-mwskz.
**        w_e1edp04-msatz = x_msatz.
**>> DIA 302 - TVA.
**        break gilr.
        DATA w_e1edk04 LIKE e1edk04.
        DATA w_e1eds01 LIKE e1eds01.
        DATA w_e1eds10 LIKE e1eds01.
*>> RGI008 -DIA 302
        READ TABLE idoc_data INTO w_idoc_data WITH KEY segnam = 'E1EDK04'.
        IF sy-subrc = 0.
          w_e1edk04 = w_idoc_data-sdata.
          w_e1edk04-mwskz =  w_e1edp04-mwskz.
          w_e1edk04-msatz = w_e1edp04-msatz.
          MOVE w_e1edk04 TO w_idoc_data-sdata.
          MOVE w_idoc_data TO idoc_data.
          MODIFY idoc_data INDEX sy-tabix.
        ENDIF.
**>> RGI008 DIA 302
**>> DIA 302

      ENDIF.
* END REPLACE OHA001 MM_397_DIA

*      w_edidd-segnam = 'E1EDP04'.
*      w_edidd-sdata = w_e1edp04.
*      APPEND w_edidd TO x_postes.
      w_edidd-segnam = 'E1EDP04'.
      w_edidd-sdata = w_e1edp04.
      APPEND w_edidd TO x_postes.

    ENDIF.

  ENDLOOP.

ENDFORM.                    "trans_ekpo_idoc

*&--------------------------------------------------------------------*
*&      Form  f_190_ctrle_em
*&--------------------------------------------------------------------*
*       text
*---------------------------------------------------------------------*
*      -->P_EBELN    text
*      -->P_EBELP    text
*      -->P_DOCNUM   text
*      -->P_BELNR    text
*      -->P_FACT_MENGtext
*      -->P_CMDE_MENGtext
*---------------------------------------------------------------------*
FORM calculer_qte_em_rest_a_fac
  USING    x_ebeln TYPE ekpo-ebeln
           x_ebelp TYPE ekpo-ebelp
  CHANGING
           x_qte_em_rest_a_fac TYPE ekpo-menge

  data w_ekbe TYPE ekbe.
  DATA w_qte_rcpt TYPE menge_d.
  DATA w_qte_fact TYPE menge_d.

  SELECT *  FROM ekbe INTO w_ekbe
    WHERE ebeln = x_ebeln
      AND ebelp = x_ebelp
      AND ( vgabe = '1' OR vgabe = '2' ).

*   Entrée de marchandise
    IF w_ekbe-vgabe = '1'.

*     Annulation de reception
      IF w_ekbe-bwart = '102'.
        w_qte_rcpt  = w_qte_rcpt - w_ekbe-menge.

*     Entrée
      ELSEIF w_ekbe-bwart = '101'.
        w_qte_rcpt  = w_qte_rcpt  + w_ekbe-menge.
      ENDIF.

*   Entrée de facture
    ELSEIF w_ekbe-vgabe = '2'.

*     Débit ( Annulation de facture )
      IF w_ekbe-shkzg = 'S'.
        w_qte_fact  = w_qte_fact  - w_ekbe-menge.

*     Crédit
      ELSE.              " Crédit
        w_qte_fact  = w_qte_fact  + w_ekbe-menge.
      ENDIF.

    ENDIF.

  ENDSELECT.

* Calcul de la qté entrée restant à facturer
  x_qte_em_rest_a_fac = w_qte_rcpt - w_qte_fact.

ENDFORM.                    "calculer_qte_em_rest_a_fac
*"Add JMJ001 -> New step for missing-receipt process
*-----------------------------------------------------------------------*
*   CONTROL_RECEPT
*   In this Form, a check is done for all the items to check if a
*   receipt has been done to avoid the Missing receipt in case of
*   a partial GR
*-----------------------------------------------------------------------*
FORM control_recept
   TABLES x_idoc_data STRUCTURE edidd
          x_idoc_contrl STRUCTURE edidc
          x_idoc_status STRUCTURE bdidocstat
  CHANGING e_new_idoc.
*
  DATA: lw_sans_poste   TYPE c,
        lw_new_docctrl  TYPE edidc,
        lwt_new_docdata TYPE TABLE OF edidd WITH HEADER LINE,
        lwt_return      TYPE bal_t_msg,
        lw_new_docnum TYPE edidc-docnum,
        lw_ebeln LIKE ekko-ebeln,
        lw_msgv1 TYPE edi_stapa1,
        lw_ebelp LIKE ekpo-ebelp,
        lw_status TYPE bdidocstat,
        lwt_ekbe_101 TYPE TABLE OF ekbe WITH HEADER LINE,
        lwt_ekbe_102 TYPE TABLE OF ekbe WITH HEADER LINE,
        lst_e1edp02 LIKE e1edp02,
        lst_e1edk14 LIKE e1edk14,
        lst_e1edka1 LIKE e1edka1,
        lst_e1edk02 LIKE e1edk02,
        lst_e1eds01 LIKE e1eds01,
        lw_docnum TYPE edidd-docnum,
        lnew_idoc,
        lw_psgnum TYPE edidd-psgnum.
*
  READ TABLE x_idoc_contrl INTO lw_new_docctrl INDEX 1.
  CHECK sy-subrc EQ 0.
  CHECK lw_new_docctrl-status = '51'.
*
  lwt_new_docdata[] = x_idoc_data[].
  LOOP AT x_idoc_data WHERE segnam = 'E1EDP02'.
    MOVE x_idoc_data-sdata TO lst_e1edp02.
    lw_docnum = x_idoc_data-docnum.
    CHECK lst_e1edp02-qualf = '001'.
    lw_psgnum = x_idoc_data-psgnum.
    lw_ebeln = lst_e1edp02-belnr.
    lw_ebelp = lst_e1edp02-zeile.
    SELECT  *  FROM ekbe INTO TABLE lwt_ekbe_101
     WHERE ebeln = lw_ebeln
       AND ebelp = lw_ebelp
       AND vgabe = '1'.
*
    lwt_ekbe_102[] = lwt_ekbe_101[].
*
    DELETE lwt_ekbe_102 WHERE bwart = '101'.
    DELETE lwt_ekbe_101 WHERE bwart = '102'.
    LOOP AT lwt_ekbe_102.
      READ TABLE lwt_ekbe_101 WITH KEY belnr = lwt_ekbe_102-lfbnr.
      IF sy-subrc EQ 0.
        DELETE lwt_ekbe_101 INDEX sy-tabix.
      ENDIF.
    ENDLOOP.
* no receipt > delete the item data on the Idoc
    IF lwt_ekbe_101[] IS INITIAL.
      DELETE lwt_new_docdata WHERE psgnum = lw_psgnum.
      DELETE lwt_new_docdata WHERE segnum = lw_psgnum.
    ENDIF.
  ENDLOOP.
* if some items have been deleted, generate a new idoc
  IF lwt_new_docdata[] NE x_idoc_data[].
*
*   Create the new Idoc
    READ TABLE x_idoc_contrl INTO lw_new_docctrl INDEX 1.
    CLEAR lw_new_docctrl-docnum.

    CALL FUNCTION 'IDOC_INBOUND_WRITE_TO_DB'
      IMPORTING
        pe_idoc_number    = lw_new_docnum
      TABLES
        t_data_records    = lwt_new_docdata
      CHANGING
        pc_control_record = lw_new_docctrl
      EXCEPTIONS
        idoc_not_saved    = 1
        OTHERS            = 2.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
             WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      RETURN.
    ENDIF.

*   Donner un statut de traitement valide (59, 51 ou 62) au nouvel IDoc
    CALL FUNCTION 'ZBALANCE_IDOC_STATUS_CHANGE'
      EXPORTING
        docnum = lw_new_docnum
        status = '62'
        msgty  = 'S'
        msgid  = 'ZSAPBALANCE'
        msgno  = '048'
        msgv1  = space
        msgv2  = space
        msgv3  = space
        msgv4  = space.

*   Donner un statut 70 'Original d'un IDOC qui a été édité'
    lw_msgv1 = lw_new_docnum.

    CLEAR lw_status.
    lw_status-docnum = lw_docnum.
    lw_status-status = '70'.
    lw_status-msgty  = 'S'.
    lw_status-msgid  ='ZSAPBALANCE'.
    lw_status-msgno  = '049'.
    lw_status-msgv1  = lw_msgv1.
    APPEND lw_status TO x_idoc_status.

    DELETE FROM zbal_idoc WHERE docnum = lw_docnum.

*  Add the new entry in ZBAL_IDOC
    IF lw_new_docctrl-mescod = 'FI' OR lw_new_docctrl-mescod = 'MM'.

      CLEAR zbal_idoc.

      MOVE-CORRESPONDING lw_new_docctrl TO zbal_idoc.

      LOOP AT x_idoc_data WHERE segnam = 'E1EDP02'.
        MOVE x_idoc_data-sdata TO lst_e1edp02.
        CHECK lst_e1edp02-qualf = '001'.
        CLEAR: zbal_idoc-bukrs, zbal_idoc-werks.
        SELECT SINGLE bukrs werks
                 FROM ekpo
                 INTO (zbal_idoc-bukrs, zbal_idoc-werks)
                WHERE ebeln = lst_e1edp02-belnr
                  AND ebelp = lst_e1edp02-zeile.
        IF sy-subrc = 0.
          EXIT.
        ENDIF.
      ENDLOOP.
      IF sy-subrc NE 0.
        LOOP AT x_idoc_data WHERE segnam = 'E1EDK14'.
          MOVE x_idoc_data-sdata TO lst_e1edk14.
          IF lst_e1edk14-qualf = '011'.
            WRITE lst_e1edk14-orgid TO zbal_idoc-bukrs.
            EXIT.
          ENDIF.
        ENDLOOP.
      ENDIF.

      LOOP AT x_idoc_data WHERE segnam = 'E1EDKA1'.
        MOVE x_idoc_data-sdata TO lst_e1edka1.
        IF lst_e1edka1-parvw = 'LF'.
          WRITE lst_e1edka1-partn TO zbal_idoc-sndprn RIGHT-JUSTIFIED.
          TRANSLATE zbal_idoc-sndprn USING ' 0'.
          EXIT.
        ENDIF.
      ENDLOOP.

      LOOP AT x_idoc_data WHERE segnam = 'E1EDK02'.
        MOVE x_idoc_data-sdata TO lst_e1edk02.
        CHECK lst_e1edk02-qualf = '009'.
        MOVE lst_e1edk02-belnr TO zbal_idoc-belnr.
        EXIT.
      ENDLOOP.

      LOOP AT x_idoc_data WHERE segnam = 'E1EDS01'.
        MOVE x_idoc_data-sdata TO lst_e1eds01.
        REPLACE ',' WITH '.' INTO lst_e1eds01-summe.
        CASE x_idoc_data-sdata+0(3).
          WHEN '011'. "TTC
            MOVE lst_e1eds01-summe TO zbal_idoc-betrg.
          WHEN '010'. "HT
            MOVE lst_e1eds01-summe TO zbal_idoc-wrbtr.
        ENDCASE.
        IF lst_e1eds01-sunit IS INITIAL.
          MOVE lst_e1eds01-waerq TO zbal_idoc-waers.
        ELSE.
          MOVE lst_e1eds01-sunit TO zbal_idoc-waers.
        ENDIF.
      ENDLOOP.

      READ TABLE x_idoc_status INDEX 1.
      IF sy-subrc = 0.
        MOVE x_idoc_status-status TO zbal_idoc-status.
        MOVE x_idoc_status-msgty  TO zbal_idoc-statyp.
        MOVE x_idoc_status-msgid  TO zbal_idoc-stamid.
        MOVE x_idoc_status-msgno  TO zbal_idoc-stamno.
        MOVE x_idoc_status-msgv1  TO zbal_idoc-stapa1.
        MOVE x_idoc_status-msgv2  TO zbal_idoc-stapa2.
        MOVE x_idoc_status-msgv3  TO zbal_idoc-stapa3.
        MOVE x_idoc_status-msgv4  TO zbal_idoc-stapa4.
      ENDIF.

      zbal_idoc-upddat = sy-datum.
      zbal_idoc-updtim = sy-uzeit.

      MODIFY zbal_idoc.
    ENDIF.
*
*   Traiter le nouvel IDoc
    CALL FUNCTION 'IDOC_MANUAL_INPUT'
      EXPORTING
        idoc_number                  = lw_new_docnum
        input_exception              = '0'
        no_dialog                    = 'X'
      EXCEPTIONS
        idoc_not_in_database         = 1
        no_input_function_found      = 2
        no_function_parameters_found = 3
        no_status_record_found       = 4
        no_authorization             = 5
        OTHERS                       = 6.
*
    e_new_idoc = 'X'.
*
  ENDIF.
*
ENDFORM.                    "control_recept
*"Add JMJ001 <- New step for missing-receipt process
