*&---------------------------------------------------------------------*
*&  Include           LZBALANCEF04
*&---------------------------------------------------------------------*

*{   INSERT         MS4K912257                                        1
*&---------------------------------------------------------------------*
*&      Form  delete_not_relevant
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_WT_EKPO  text
*----------------------------------------------------------------------*
FORM delete_not_relevant  TABLES p_wt_ekpo STRUCTURE ekpo
                          changing x_xblnr like mkpf-xblnr.

  DATA : wt_ekbe TYPE TABLE OF ekbe.
  DATA : w_ekbe TYPE ekbe.
  DATA : x_ebeln TYPE ebeln.
  DATA w_qte_rcpt TYPE menge_d.
  DATA w_qte_fact TYPE menge_d.
  DATA w_qte_cmde TYPE menge_d.
  DATA w_qte_em_rest_a_fac TYPE menge_d.
  DATA tabix LIKE sy-tabix.
  DATA x_em_exist_on_po.
  break gilr.
  DATA : BEGIN OF gt_ekpo OCCURS 0,
         ebeln TYPE ebeln,
         ebelp TYPE ebelp,
         menge TYPE menge_d,
         w_qte_rcpt TYPE menge_d,
         w_qte_fact TYPE menge_d,
         END OF gt_ekpo.

  READ TABLE p_wt_ekpo INDEX 1.
  CHECK sy-subrc = 0.
  x_ebeln = p_wt_ekpo-ebeln.

  SELECT * INTO TABLE wt_ekbe FROM ekbe WHERE ebeln = x_ebeln
                                AND ( vgabe = '1' OR vgabe = '2' ).


  LOOP AT p_wt_ekpo.
    MOVE sy-tabix TO tabix.
    LOOP AT wt_ekbe INTO w_ekbe WHERE ebeln = p_wt_ekpo-ebeln
                                  AND ebelp = p_wt_ekpo-ebelp.

* Check sur référence entrée marchandise.
      if x_xblnr is not initial.
        check w_ekbe-xblnr eq x_xblnr.
      endif.

      IF w_ekbe-vgabe = '1'.
*     Annulation de reception
        IF w_ekbe-bwart = '102'.
          w_qte_rcpt  = w_qte_rcpt - w_ekbe-menge.
*     Entrée
        ELSEIF w_ekbe-bwart = '101'.
          w_qte_rcpt  = w_qte_rcpt  + w_ekbe-menge.
        ENDIF.
*   Entrée de facture
      ELSEIF w_ekbe-vgabe = '2'.
*     Débit
        IF w_ekbe-shkzg = 'S'.
          w_qte_fact  = w_qte_fact + w_ekbe-menge.
*     Crédit
        ELSE.              " Crédit
          w_qte_fact  = w_qte_fact - w_ekbe-menge.
        ENDIF.
      ENDIF.
    ENDLOOP.
    IF sy-subrc = 0.
      w_qte_em_rest_a_fac = w_qte_rcpt - w_qte_fact.
      IF w_qte_em_rest_a_fac <= 0.
*        DELETE p_wt_ekpo INDEX tabix.
      ELSE.
        MOVE-CORRESPONDING p_wt_ekpo TO gt_ekpo.
        MOVE  w_qte_rcpt TO gt_ekpo-w_qte_rcpt.
        MOVE   w_qte_fact TO gt_ekpo-w_qte_fact.
        APPEND gt_ekpo.
        CLEAR gt_ekpo.
      ENDIF.
    ELSE.
*>> No entries found in history
      MOVE-CORRESPONDING p_wt_ekpo TO gt_ekpo.
      APPEND gt_ekpo.
      CLEAR gt_ekpo.
    ENDIF.
  ENDLOOP.

  LOOP AT gt_ekpo WHERE w_qte_rcpt NE space.
  ENDLOOP.
  IF sy-subrc EQ 0.
    LOOP AT gt_ekpo WHERE w_qte_rcpt EQ space.
      LOOP AT p_wt_ekpo WHERE ebeln = gt_ekpo-ebeln
                          AND ebelp = gt_ekpo-ebelp.
        DELETE p_wt_ekpo.
      ENDLOOP.
    ENDLOOP.
  else.
  clear x_xblnr.
  ENDIF.


ENDFORM.                    " delete_not_relevant
*}   INSERT
