************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Initialisation des tables des lignes de factures    *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Gaëlle TRIMOREAU (Netinside)                                *
*                                                                      *
* Date   : 13/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport: DE2K900720                                       *
*									         *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_init_table_invoice.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(I_BUKRS) LIKE  BKPF-BUKRS
*"     REFERENCE(I_BELNR) LIKE  BKPF-BELNR
*"     REFERENCE(I_GJAHR) LIKE  BKPF-GJAHR
*"  TABLES
*"      T_SEG STRUCTURE  ZSBALANCE_SEG
*"  EXCEPTIONS
*"      NO_IDOC_LINKED
*"----------------------------------------------------------------------
************************************************************************
*                   DECLARATION DE DONNEES                             *
************************************************************************
***************************** Tables ***********************************
  TABLES: edid4,
          t076s.
  DATA: w_lifnr       LIKE vbsegk-lifnr,
        w_blart       LIKE vbkpf-blart.
***************************** Données **********************************
  DATA: w_sdata   LIKE edid4-sdata,
        w_nb_idoc LIKE srrelroles-objkey,
        w_flag    TYPE i.

***************************** Constantes *******************************
  CONSTANTS: k_objtype LIKE srrelroles-objtype VALUE 'BKPF'.

***************************** Tables internes **************************
  DATA: i_edid4 LIKE edid4 OCCURS 0 WITH HEADER LINE.

***************************** Structures *******************************
  DATA: w_e1edp04 LIKE e1edp04.
  DATA: w_e1edp26 LIKE e1edp26.
  DATA: w_e1edp30 LIKE e1edp30.
  DATA: w_e1edp19 LIKE e1edp19.

  DATA : st_vbsegk LIKE vbsegk.
  DATA : wt_vbsegk LIKE vbsegk OCCURS 0 WITH HEADER LINE.
  DATA : wt_vbsegs LIKE vbsegs OCCURS 0 WITH HEADER LINE.

************************************************************************
*                             TRAITEMENT                               *
************************************************************************
** Recherche du n° de l'idoc
*  CLEAR w_nb_idoc.
*  CALL FUNCTION 'ZBALANCE_SEARCH_IDOC_INVOIC'
*       EXPORTING
*            i_objtype      = k_objtype
*            i_nb_invoic    = i_belnr
*       IMPORTING
*            e_nb_idoc      = w_nb_idoc
*       EXCEPTIONS
*            no_idoc_linked = 1
*            OTHERS         = 2.
*
*  IF sy-subrc <> 0.
** Il n'y a pas d'idoc associé
*    RAISE no_idoc_linked.
*  ELSE.
** Recherche des données présentes dans les différents segments de
*l'idoc
*    CLEAR edid4.
*    SELECT * FROM edid4
*             INTO CORRESPONDING FIELDS OF i_edid4
*             WHERE docnum = w_nb_idoc.
*      IF sy-subrc EQ 0.
*        APPEND i_edid4.
*      ENDIF.
*    ENDSELECT.
*
*    IF sy-subrc NE 0.
*      MESSAGE s533(0u)
*      WITH
*'Aucune entrée correspondante dans la table'(w47)
*'Enregistrements de données d''IDOC pour document: '(w44) w_nb_idoc.
*    ENDIF.
*
*
*    CLEAR i_edid4.
*    CLEAR: w_e1edp26, w_e1edp04.
*    CLEAR w_flag.
*    LOOP AT i_edid4.
*
*      IF i_edid4-segnam = 'E1EDP01'.
*        IF w_flag <> '0'.
*          APPEND t_seg.
*          CLEAR t_seg.
*          w_flag = 1.
*        ELSE.
*          w_flag = 1.
*        ENDIF.
*      ENDIF.
*
*      CASE i_edid4-segnam.
** Poste de document segment montant.
*        WHEN 'E1EDP26'.
*          MOVE i_edid4-sdata TO w_e1edp26.
*          IF w_e1edp26-qualf = '002'.
*            MOVE w_e1edp26-betrg TO t_seg-zwrbtr.
**            APPEND t_seg.
**            CLEAR t_seg.
*            CLEAR w_e1edp26.
*          ELSE.
*            CLEAR w_e1edp26.
*          ENDIF.
** En-tête de document, taxes
*        WHEN 'E1EDP04'.
*          MOVE i_edid4-sdata TO w_e1edp04.
** Code TVA
*
**          MOVE w_e1edp04-mwskz TO t_seg-zmwskz.
*          CLEAR vbsegs.
*          SELECT  mwskz INTO t_seg-zmwskz "ex SELECT SINGLE
*          FROM vbsegs WHERE ausbk = i_bukrs
*                        AND belnr = i_belnr
*                        AND gjahr = i_gjahr.
*          ENDSELECT.
*          IF sy-subrc = 0.
** Montant de la TVA
*            MOVE w_e1edp04-mwsbt TO t_seg-zwmwst.
**          APPEND t_seg.
**          CLEAR t_seg.
*            CLEAR w_e1edp04.
*          ELSE.
*            MESSAGE s208(00)
*        WITH 'Aucune entrée correspondante dans la table VBSEGS'(w45).
*          ENDIF.
*        WHEN 'E1EDP30'.
*          MOVE i_edid4-sdata TO w_e1edp30.
** Le centre de coût
*          IF w_e1edp30-qualf = '045'.
*            MOVE w_e1edp30-ivkon TO t_seg-zkostl.
*            MOVE w_e1edp30-ivkon TO t_seg-zkostl_old.
**            APPEND t_seg.
**            CLEAR t_seg.
*            CLEAR w_e1edp30.
** L'élément d'OTP
*          ELSEIF w_e1edp30-qualf = '047'.
*            MOVE w_e1edp30-ivkon TO t_seg-zprojk.
*            MOVE w_e1edp30-ivkon TO t_seg-zprojk_old.
**            APPEND t_seg.
**            CLEAR t_seg.
*            CLEAR w_e1edp30.
** Domaine d'activité
*          ELSEIF w_e1edp30-qualf = '049'.
*            MOVE w_e1edp30-ivkon TO t_seg-zgsber.
*            MOVE w_e1edp30-ivkon TO t_seg-zgsber_old.
**            APPEND t_seg.
**            CLEAR t_seg.
*            CLEAR w_e1edp30.
** Ordre
*          ELSEIF w_e1edp30-qualf = '0'.
*            MOVE w_e1edp30-ivkon TO t_seg-zaufnr.
*            MOVE w_e1edp30-ivkon TO t_seg-zaufnr_old.
**            APPEND t_seg.
**            CLEAR t_seg.
*            CLEAR w_e1edp30.
*          ELSE.
*            CLEAR w_e1edp30.
*          ENDIF.
** Compte général
*        WHEN 'E1EDP19'.
*          MOVE i_edid4-sdata TO w_e1edp19.
*          IF w_e1edp19-qualf = '002'.
*            MOVE w_e1edp19-idtnr TO t_seg-zhkont.
*            MOVE w_e1edp19-idtnr TO t_seg-zhkont_old.
**            APPEND t_seg.
**            CLEAR t_seg.
*            CLEAR w_e1edp19.
*          ELSE.
*            CLEAR w_e1edp19.
*          ENDIF.
*      ENDCASE.
*    ENDLOOP.
*    APPEND t_seg.
*    CLEAR t_seg.
*  ENDIF.

*BUZEI
*BSCHL
*UMSKZ
*ZHKONT
*ZKOSTL
*ZGSBER
*ZWRBTR
*ZAUFNR
*ZPROJK
*ZMWSKZ
*ZWMWST
*ZHKONT_OLD
*ZKOSTL_OLD
*ZGSBER_OLD
*ZAUFNR_OLD
*ZPROJK_OLD

  SELECT * FROM  vbsegs INTO TABLE wt_vbsegs
         WHERE  ausbk  = i_bukrs
         AND    belnr  = i_belnr
         AND    gjahr  = i_gjahr.

  LOOP AT wt_vbsegs.
    t_seg-buzei = wt_vbsegs-buzei.
    t_seg-bschl = wt_vbsegs-bschl.
    t_seg-zhkont = wt_vbsegs-saknr.
    t_seg-zhkont_old = wt_vbsegs-saknr.
    t_seg-zkostl = wt_vbsegs-kostl.
    t_seg-zkostl_old = wt_vbsegs-kostl.
    t_seg-zgsber = wt_vbsegs-gsber.
    t_seg-zgsber_old = wt_vbsegs-gsber.
    t_seg-zaufnr = wt_vbsegs-aufnr.
    t_seg-zaufnr_old = wt_vbsegs-aufnr.
    t_seg-zprojk = wt_vbsegs-ps_psp_pnr.
    t_seg-zprojk_old = wt_vbsegs-ps_psp_pnr.
    t_seg-zwrbtr = wt_vbsegs-wrbtr.
    t_seg-zmwskz = wt_vbsegs-mwskz.
    APPEND t_seg.
  ENDLOOP.


  SELECT blart INTO (w_blart)
             FROM vbkpf WHERE ausbk = i_bukrs
                          AND belnr = i_belnr
                          AND gjahr = i_gjahr.
  ENDSELECT.

  SELECT * FROM  vbsegk INTO st_vbsegk UP TO 1 ROWS
       WHERE  ausbk  = i_bukrs
       AND    belnr  = i_belnr
       AND    gjahr  = i_gjahr.
  ENDSELECT.

*{   REPLACE        DE5K900049                                        1
*\  SELECT SINGLE * FROM  t076s
*\         WHERE  parart   = 'LI'
*\         AND    konto    = st_vbsegk-lifnr
*\         AND    ktbukrs  = i_bukrs.
* ITGGH   26.02.2007   CHG_01                               DE5K900049
  SELECT SINGLE * FROM  t076s
     WHERE  parart   = 'LS'
     AND    ktbukrs  = i_bukrs.
  IF sy-subrc <> 0.
    SELECT SINGLE * FROM  t076s
           WHERE  parart   = 'LI'
           AND    konto    = st_vbsegk-lifnr
           AND    ktbukrs  = i_bukrs.
  ENDIF.
*}   REPLACE

  IF t076s-cblartkr = w_blart.

    SELECT * FROM  vbsegk INTO TABLE wt_vbsegk
         WHERE  ausbk  = i_bukrs
         AND    belnr  = i_belnr
         AND    gjahr  = i_gjahr
         AND    shkzg  = 'S'.
    LOOP AT wt_vbsegk.
      t_seg-buzei = wt_vbsegk-buzei.
      t_seg-bschl = wt_vbsegk-bschl.
      t_seg-umskz = wt_vbsegk-umskz.
      t_seg-zhkont = wt_vbsegk-lifnr.
      t_seg-zhkont_old = wt_vbsegk-lifnr.
      t_seg-zgsber = wt_vbsegk-gsber.
      t_seg-zgsber_old = wt_vbsegk-gsber.
      t_seg-zwrbtr = wt_vbsegk-wrbtr.
      t_seg-zmwskz = wt_vbsegk-mwskz.
      APPEND t_seg.
    ENDLOOP.
  ENDIF.

  IF t076s-cblartkg = w_blart.

    SELECT * FROM  vbsegk INTO TABLE wt_vbsegk
         WHERE  ausbk  = i_bukrs
         AND    belnr  = i_belnr
         AND    gjahr  = i_gjahr
         AND    shkzg  = 'H'.
    LOOP AT wt_vbsegk.
      t_seg-buzei = wt_vbsegk-buzei.
      t_seg-bschl = wt_vbsegk-bschl.
      t_seg-umskz = wt_vbsegk-umskz.
      t_seg-zhkont = wt_vbsegk-lifnr.
      t_seg-zhkont_old = wt_vbsegk-lifnr.
      t_seg-zgsber = wt_vbsegk-gsber.
      t_seg-zgsber_old = wt_vbsegk-gsber.
      t_seg-zwrbtr = wt_vbsegk-wrbtr.
      t_seg-zmwskz = wt_vbsegk-mwskz.
      APPEND t_seg.
    ENDLOOP.
  ENDIF.



  SORT t_seg BY buzei.


ENDFUNCTION.
