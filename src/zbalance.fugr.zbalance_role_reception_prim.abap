
FUNCTION zbalance_role_reception_prim.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(W_EBELN) TYPE  EKKO-EBELN
*"     VALUE(W_EBELP) TYPE  RSEG-EBELP OPTIONAL
*"  TABLES
*"      AC_CONTAINER STRUCTURE  SWCONT OPTIONAL
*"      ACTOR_TAB STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOBODY_FOUND
*"----------------------------------------------------------------------
***********************************************************************
* C. DUMONT
* 04/10/06
* Création d'un rôle : recherche du user ayant effectué l'entrée de
*                      marchandises.
***********************************************************************
*" APPIA - 25.10.2006
*"
*" Modification pour envoi vers un user en particulier quand le système
*" ne trouve pas d'utilisateur destinataire ou quand il y a un conteneur
*" vide pour le n° commande
***********************************************************************
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Default Role determination                       *
* ----------------- * ------------------------------------------------ *
* Author            : JM.JOURON                                JMJ001  *
* Date              : 22.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : New Role determination                           *
* ----------------- * ------------------------------------------------ *


  DATA w_usnam TYPE mkpf-usnam.
  DATA w_gjahr TYPE ekbe-gjahr.
  DATA w_belnr TYPE ekbe-belnr.
  DATA w_uflag TYPE usr02-uflag.
  DATA w_bname TYPE usr02-bname.
  DATA w_subrc LIKE sy-subrc.
*>> START RGI002
  DATA : l_custom_data LIKE swd_custom.
  DATA w_bukrs TYPE bukrs.
*>> END RGI002
  w_subrc = 0.

  IF w_ebeln IS INITIAL.
*>> START RGI REMARQ - HERE PB WITH WF CONTAINER
    w_subrc = 4.
  ELSE.
*-- Recherche du n° de document article.
    SELECT SINGLE gjahr MAX( belnr )  INTO (w_gjahr, w_belnr) FROM ekbe
             WHERE ebeln = w_ebeln
               AND ebelp = w_ebelp
               AND bwart = '101'
*>> START RGI Optimiation access/
               AND vgabe = '1'
*>> RGI   RGI Optimiation access/
               GROUP BY gjahr belnr.
    IF sy-subrc EQ 0.

      SELECT SINGLE usnam INTO w_usnam FROM mkpf
               WHERE mblnr = w_belnr
                 AND mjahr = w_gjahr.

      IF sy-subrc = 0.

        CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
          EXPORTING
            usnam     = w_usnam
          EXCEPTIONS
            not_exist = 1
            locked    = 2
            not_valid = 3
            OTHERS    = 4.
        IF sy-subrc = 0.
          actor_tab-otype = 'US'.
          actor_tab-objid = w_usnam.
          APPEND actor_tab.
        ELSE.
*>> START RGI - Building logic in error code
*          w_subrc = 4.  "User not active
          w_subrc = 1. "USer not active
        ENDIF.
      ELSE.
*>> START RGI - Building logic in error code
*        w_subrc = 4.    "Select from MKPF failed
        w_subrc = 2.    "Select from MKPF failed
      ENDIF.
    ELSE.
*>> START RGI - Building logic in error code
*      w_subrc = 4.      "Select from EKBE failed
      w_subrc = 3.      "Select from EKBE failed
    ENDIF.
  ENDIF.

*JMJ001 - 19/11/2007 - delete, the default treatment is done in the calling program
 if w_subrc ne 0.
   raise nobody_found.
 endif.
**>> START RGI002
**>> Now we kill Miss BONAVENTURE to replace her by a table
*  IF w_subrc NE 0.
**    w_actor-otype = 'US'.
**    w_actor-objid = 'BONAVENTURE'.
**    APPEND w_actor.
**  ENDIF.
*    w_subrc = 0.
*    IF NOT w_ebeln IS INITIAL.
**>> NOW WE TRY ACCESS BY ZBAL_WFLOGISTIC
*      SELECT SINGLE bukrs INTO w_bukrs FROM ekko WHERE ebeln = w_ebeln.
*      CALL FUNCTION 'Z_BALANCE_ROLE_DEFAULT'
*        EXPORTING
*          i_ebeln = w_ebeln
*          i_ebelp = w_ebelp
*          i_bukrs = w_bukrs
*          i_type  = 'MM'
*        TABLES
*          t_actor = actor_tab.
*      IF actor_tab[] IS INITIAL.
*        CALL FUNCTION 'SWD_GET_CUSTOMIZING'
*          IMPORTING
*            custom_data = l_custom_data.
** default administrator was found in customizing
*        IF NOT l_custom_data-def_admin IS INITIAL.
*          actor_tab      = l_custom_data-def_admin.
*          APPEND actor_tab.
*        ENDIF.
*      ENDIF.
*    ELSE.
**>> NO PO -> PB WITH CONTAINER SO GETTING WF CUSTOMIZING
*      CALL FUNCTION 'SWD_GET_CUSTOMIZING'
*        IMPORTING
*          custom_data = l_custom_data.
** default administrator was found in customizing
*      IF NOT l_custom_data-def_admin IS INITIAL.
*        actor_tab      = l_custom_data-def_admin.
*        APPEND actor_tab.
*      ENDIF.
*    ENDIF.
*  ENDIF.
*  IF actor_tab[] IS INITIAL.
*    CALL FUNCTION 'SWD_GET_CUSTOMIZING'
*      IMPORTING
*        custom_data = l_custom_data.
** default administrator was found in customizing
*    IF NOT l_custom_data-def_admin IS INITIAL.
*      actor_tab      = l_custom_data-def_admin.
*      APPEND actor_tab.
*    ENDIF.
*  ENDIF.
*JMJ001 - 19/11/2007 - end delete
  CLEAR actor_tab.
ENDFUNCTION.
