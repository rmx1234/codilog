FUNCTION ZBALANCE_POST_AND_BLOCK.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(BUKRS) TYPE  BUKRS
*"     REFERENCE(BELNR) TYPE  CHAR10
*"     REFERENCE(GJAHR) TYPE  GJAHR
*"     REFERENCE(KOSTL) TYPE  KOSTL
*"     REFERENCE(AUFNR) TYPE  AUFNR
*"  EXPORTING
*"     REFERENCE(P_SUBRC) TYPE  SUBRC
*"----------------------------------------------------------------------

*{   INSERT         MS4K915548                                        1
*"----------------------------------------------------------------------
* HAVYO 13/05/2008 : Send XML file only for company code in ZF144
* DIA : FI_00184/ OHA002
*"----------------------------------------------------------------------
*Définition des noms des champs
  data : w_subrc type sy-subrc,
         w_mwskz type mwskz,
         pl_fourn type vbsegk-buzei,
         pl_poste type vbsegk-buzei,
         field_poste(16),
         field_fourn(16).


*Récupere le poste fournisseur
  select single buzei
           from vbsegk
           into pl_fourn
          where bukrs = bukrs
            and belnr = belnr
            and gjahr = gjahr.

*Récupere le poste compte général
  select single buzei mwskz
           from vbsegs
           into (pl_poste , w_mwskz)
          where ausbk = bukrs
            and belnr = belnr
            and gjahr = gjahr.

refresh w_bdcdata.
clear w_bdcdata.

* First Screen
  perform bdc_dynpro      using 'SAPMF05V' '0100'.
  perform bdc_field       using 'BDC_CURSOR'
                                'RF05V-BUKRS'.
  perform bdc_field       using 'BDC_OKCODE'
                                '/00'.
  perform bdc_field       using 'RF05V-BUKRS'
                                bukrs.
  perform bdc_field       using 'RF05V-BELNR'
                                belnr.
  perform bdc_field       using 'RF05V-GJAHR'
                                gjahr.

* Second screen
  perform bdc_dynpro      using 'SAPLF040' '0700'.

  concatenate 'RF05V-ANZDT('  pl_poste+2 ')' into field_poste.
  concatenate 'RF05V-ANZDT('  pl_fourn+2 ')' into field_fourn.

  perform bdc_field       using 'BDC_CURSOR'
                                field_fourn.

  perform bdc_field       using 'BDC_OKCODE'
                                '=PI'.

  perform bdc_dynpro      using 'SAPLF040' '0302'.
  perform bdc_field       using 'BDC_CURSOR'
                                'BSEG-ZLSPR'.
  perform bdc_field       using 'BDC_OKCODE'
                                '=SB'.
* BEGIN INSERT OHA002 FI_00184
  clear zf144.
  select single * from zf144 where bukrs = bukrs.

  if zf144-zzxml = 'X'.
  perform bdc_field       using 'BSEG-ZLSPR'
                                'S'.
else.
  perform bdc_field       using 'BSEG-ZLSPR'
                                ' '.
endif.
* END INSERT OHA002.

  perform bdc_dynpro      using 'SAPLF040' '0310'.
  perform bdc_field       using 'BDC_CURSOR'
                                'BSEG-KOSTL(01)'.
  perform bdc_field       using 'BDC_OKCODE'
                                '=BU'.
  perform bdc_field       using 'BSEG-KOSTL(01)'
                                kostl.
  perform bdc_field       using 'BSEG-AUFNR(01)'
                                aufnr.

  perform bdc_transaction using 'FBV0' 'LOCK' ' '
                          changing p_subrc.
*
*}   INSERT




ENDFUNCTION.
