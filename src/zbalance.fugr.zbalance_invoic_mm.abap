************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Module d'intégration des factures MM                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : Balance For MySAP Business Suite                            *
*                                                                      *
* Auteur :                                                             *
*                                                                      *
* Date   :                                                             *
*                                                                      *
* Ordre de transport:                                                  *
*                              *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*  02.2007 ! ITGGH        ! CHG_01                          DE5K900049 *
*          !              ! Save invoice in archive                    *
*----------!--------------!--------------------------------------------*
*  02.2007 ! ITGGH        ! CHG_02                          DE5K900049 *
*          !              ! Change EDI partner type LI -> LS           *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION ZBALANCE_INVOIC_MM.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(I_DOCNUM) TYPE  EDI_DOCNUM
*"     VALUE(I_VER) TYPE  INT4 OPTIONAL
*"     VALUE(I_LASTSTATUS) TYPE  EDIDS
*"     VALUE(I_IDOC_CONTROL) TYPE  EDIDC
*"     VALUE(I_ARCHIV_ID) TYPE  TOAAR-ARCHIV_ID
*"     REFERENCE(SCREENTYPE) TYPE  INT4
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD
*"----------------------------------------------------------------------

  DATA: WS_FACT TYPE T_FACT.
  DATA: WS_CMDE TYPE T_CMDE.
  DATA: WS_CADD TYPE T_CADD.
  DATA: WT_FACTURES      TYPE STANDARD TABLE OF ZBALANCE_01
                         WITH HEADER LINE.
  DATA: WT_FACTURES_ORIG TYPE STANDARD TABLE OF ZBALANCE_01
                         WITH HEADER LINE.
  DATA: W_BUKRS_X TYPE FLAG.
  DATA: W_CHANGED TYPE FLAG.
  DATA: WT_RETURN TYPE BAL_T_MSG.
  DATA: W_RET.
  DATA : W_COUNT_BEFORE TYPE I.
  DATA : W_COUNT_NOW TYPE I.
* Table des postes de(s) commandes
  DATA : WT_POSTES   TYPE STANDARD TABLE OF ZBALANCE_02.
  DATA : WS_POSTE    TYPE ZBALANCE_02.
  DATA : WS_FACTURE     TYPE ZBALANCE_01.
  DATA : WA_TABIX LIKE SY-TABIX.
  DATA : W_NEW_DATECOMP LIKE SY-DATUM.
  DATA : W_DOCNUM LIKE EDIDC-DOCNUM.
  DATA : WA_DEBUT LIKE WT_FACTURES-DEBUT, WA_ECART TYPE I.

  DATA : W_TAUX TYPE P DECIMALS 4.
  DATA : W_TOTALHT TYPE P DECIMALS 4.
  DATA : W_TOTALTTC TYPE P DECIMALS 4.

  DATA : BEGIN OF WT_FACT_TVARECAP OCCURS 0,
         CODETVA  TYPE T007A-MWSKZ,
         TOTALHT  TYPE EKPO-NETWR,
         TVA      TYPE EKPO-BRTWR,
         TAUX     TYPE T076M-MWSATZ,
         TOTALTTC TYPE EKPO-NETWR,
       END OF WT_FACT_TVARECAP.

  CALL FUNCTION 'ZBALANCE_INVOIC_MM_DETAILS'
    EXPORTING
      I_DOCNUM    = I_DOCNUM
    IMPORTING
      E_HEADER    = ZBALANCE_04
    TABLES
      T_IDOC_DATA = T_IDOC_DATA
      T_FACTURES  = WT_FACTURES.


  CHECK NOT ( ZBALANCE_04 IS INITIAL ).

  CLEAR: W_OBJECT_ID, W_MODIF_POSS.
  W_9210_INIT = 'X'.

   *ZBALANCE_04 = ZBALANCE_04.
  CLEAR: WT_ALV_FACT[], WT_FACTURES_ORIG[].
  LOOP AT WT_FACTURES.
    WS_FACT-POSTEFACT = WT_FACTURES-POSEX.
    WS_FACT-BDL       = WT_FACTURES-VERUR.
    WS_FACT-POSTECMDE = WT_FACTURES-ZEILE.
    WS_FACT-NUMCMDE   = WT_FACTURES-BELNR.
    WS_FACT-ARTICLE   = WT_FACTURES-MATNR.

**rajout ID53
    SELECT SINGLE MATNR INTO WS_FACT-ARTICLE
        FROM EKPO
        WHERE EBELN = WT_FACTURES-BELNR
        AND   EBELP = WT_FACTURES-POSEX.
**fin rajout ID53

    WS_FACT-MAKTX     = WT_FACTURES-MAKTX.
    MOVE WT_FACTURES-MENGE TO WS_FACT-QUANTITE.
    IF WT_FACTURES-PEINH IS INITIAL.
      MOVE 1 TO WS_FACT-CONDI.
    ELSE.
      MOVE WT_FACTURES-PEINH TO WS_FACT-CONDI.
    ENDIF.
    MOVE WT_FACTURES-NETPR TO WS_FACT-PRIXUNIT.
    MOVE WT_FACTURES-BETRG TO WS_FACT-TOTALHT.
    MOVE WT_FACTURES-MWSBT TO WS_FACT-TVA.

    WS_FACT-CODETVA   = WT_FACTURES-MWSKZ.
    MOVE WT_FACTURES-TTC TO WS_FACT-TOTALTTC.

*&--------------------- Modifications v1 TBR 030804 ------------------&*
*                  Rappel des specs techniques                         *
*La zone IDENT spécifie si l’identification de la facture est réussie ou
* non, et la raison selon les règles suivantes :
* '1'  - présent dans l'idoc
* '2'  - impossible à déterminer
* '3'  - commande inexistante
* '4'  - bon de livraison inexistant
* '5'  - bon de livraison - pas de l'entrée de marchandise
* 'X'  - rapprochement effectué
* vide  - rapprochement échoué de raison : traitement sans succès
*
    IF WT_FACTURES-IDENT EQ 'X'.
      MOVE ICON_CHECKED TO WS_FACT-STATUS.
    ELSEIF  WT_FACTURES-IDENT EQ '1'.
      PERFORM F_210_LIRE_COMMANDE TABLES WT_POSTES
                            USING  WT_FACTURES.
      READ TABLE WT_POSTES WITH KEY BELNR = WT_FACTURES-BELNR
                                    ZEILE = WT_FACTURES-ZEILE
                                    MENGE = WT_FACTURES-MENGE
      INTO WS_POSTE.
      IF SY-SUBRC EQ 0.
        MOVE ICON_CHECKED TO WS_FACT-STATUS.
      ELSE.
        MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
      ENDIF.
    ELSE.
      MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
    ENDIF.
*&------------------Fin Modifications v1 TBR 030804 ------------------&*
    APPEND WS_FACT TO WT_ALV_FACT.
  ENDLOOP.
  APPEND LINES OF WT_FACTURES TO WT_FACTURES_ORIG.


*** Récupération des données sur les coûts additionnels

  REFRESH WT_ALV_CADD. CLEAR WT_ALV_CADD.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
    EXPORTING
      INPUT  = *ZBALANCE_04-LIFNR
    IMPORTING
      OUTPUT = *ZBALANCE_04-LIFNR.

  SELECT SINGLE * FROM  ZBAL_CADD
         WHERE  PARTN  = *ZBALANCE_04-LIFNR.
  IF SY-SUBRC NE 0.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        INPUT  = *ZBALANCE_04-LIFNR
      IMPORTING
        OUTPUT = *ZBALANCE_04-LIFNR.
    SELECT SINGLE * FROM  ZBAL_CADD
             WHERE  PARTN  = *ZBALANCE_04-LIFNR.
  ENDIF.


  LOOP AT T_IDOC_DATA WHERE SEGNAM = 'E1EDK05'.
    MOVE T_IDOC_DATA-SDATA TO E1EDK05.
    WS_CADD-HKONT = ZBAL_CADD-HKONT.
    WS_CADD-ALCKZ = E1EDK05-ALCKZ.
    WS_CADD-BETRG = E1EDK05-BETRG.
    WS_CADD-MWSKZ = E1EDK05-MWSKZ.
    WS_CADD-KOSTL = ZBAL_CADD-KOSTL.
    WS_CADD-PSPNR = ZBAL_CADD-PSPNR.
    WS_CADD-AUFNR = ZBAL_CADD-AUFNR.

    APPEND WS_CADD TO WT_ALV_CADD.

*    zbalance_04-ttc = zbalance_04-ttc + e1edk05-betrg.
*    zbalance_04-tva = zbalance_04-tva + e1edk05-kobtr - e1edk05-betrg.

    IF E1EDK05-MSATZ IS INITIAL.
*-----------------------------------------------------------------------
* ITGGH   23.02.2007  CHG_02                                 DE5K900049
* Extension for logical system
      SELECT * FROM  T076M
        WHERE  PARART  = 'LS'
        AND    MWART   = E1EDK05-MWSKZ
        AND    MWSKZ   = E1EDK05-MWSKZ.
      ENDSELECT.
      IF SY-SUBRC <> 0.
*-----------------------------------------------------------------------
        CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
          EXPORTING
            INPUT  = *ZBALANCE_04-LIFNR
          IMPORTING
            OUTPUT = *ZBALANCE_04-LIFNR.

        SELECT * FROM  T076M
                 WHERE  PARART  = 'LI'
                 AND    KONTO   = *ZBALANCE_04-LIFNR
                 AND    MWART   = E1EDK05-MWSKZ
                 AND    MWSKZ   = E1EDK05-MWSKZ.
        ENDSELECT.
      ENDIF.                                  "* ITGGH 23.02.2007 CHG_02

      IF SY-SUBRC EQ 0.
        E1EDK05-MSATZ = T076M-MWSATZ.
      ENDIF.
    ENDIF.

    W_TAUX = E1EDK05-MSATZ.
    W_TAUX = W_TAUX / 100.
    W_TAUX = W_TAUX + 1.
    W_TOTALHT = E1EDK05-BETRG.
    W_TOTALTTC = W_TOTALHT * W_TAUX.
    CASE E1EDK05-ALCKZ.
      WHEN '+'.
        ZBALANCE_04-TVA = ZBALANCE_04-TVA + ( W_TOTALTTC - W_TOTALHT ).
      WHEN '-'.
        ZBALANCE_04-TVA = ZBALANCE_04-TVA - ( W_TOTALTTC - W_TOTALHT ).
    ENDCASE.

  ENDLOOP.

  WT_ALV_CADD_SAVE[] = WT_ALV_CADD[].


***

  DO.
    CLEAR: W_MODIF_POSS.
    IF I_LASTSTATUS-STATUS NE '53'.
      IF SCREENTYPE EQ 2.
        W_MODIF_POSS = 'X'.
      ENDIF.
    ELSE.
      IF NOT ( I_ARCHIV_ID IS INITIAL ).
        CONCATENATE I_LASTSTATUS-STAPA2
                    I_LASTSTATUS-STAPA3
                    I_LASTSTATUS-STAPA1
        INTO W_OBJECT_ID.
        W_ARCHIV_DOC_ID = I_IDOC_CONTROL-ARCKEY.
        W_ARCHIV_ID = I_ARCHIV_ID.
      ENDIF.
    ENDIF.

    W_REPID = SY-REPID.
    CASE I_VER.
      WHEN OTHERS.
        IF I_IDOC_CONTROL-STATUS <> '53'.
          W_DYNNR = '9210'.
          EXPORT I_DOCNUM TO MEMORY ID 'DOCNUM'.
*          w_docnum = i_docnum. "passage du num d'iDoc - affich facture
          CALL SCREEN 9210.
        ELSE.
          EXIT.
        ENDIF.
    ENDCASE.

*   Vérification des données modifiées
*   Société
    CLEAR W_CHANGED.
    IF ZBALANCE_04-BUKRS NE *ZBALANCE_04-BUKRS.
      W_BUKRS_X = 'X'.
      W_CHANGED = 'X'.
    ELSE.
*     Vérification des données modifiées
*     Postes
      W_BUKRS_X = SPACE.
      LOOP AT WT_ALV_FACT INTO WS_FACT.
        READ TABLE WT_FACTURES INDEX SY-TABIX..
        IF WT_FACTURES-ZEILE NE WS_FACT-POSTECMDE OR
           WT_FACTURES-BELNR NE WS_FACT-NUMCMDE OR
           WT_FACTURES-MWSKZ NE WS_FACT-CODETVA OR
           NOT W_DECOMPOSE IS INITIAL OR
           NOT W_CONSOLIDATION IS INITIAL OR
           NOT W_SUPPR IS INITIAL.
          W_CHANGED = 'X'.
          EXIT.
        ENDIF.
      ENDLOOP.
    ENDIF.
*   Vérification des données modifiées
*   Date comptable
    IF ZBALANCE_04-DATUM NE *ZBALANCE_04-DATUM.
      W_CHANGED = 'X'.
      W_NEW_DATECOMP = ZBALANCE_04-DATUM.
    ELSE.
      CLEAR W_NEW_DATECOMP.
    ENDIF.

    IF WT_ALV_CADD[] NE WT_ALV_CADD_SAVE[].
      W_CHANGED = 'X'.
    ENDIF.

*** Vérification des données modifiées
*   Changement de la structure des postes
    DESCRIBE TABLE WT_ALV_FACT LINES W_COUNT_NOW.
    DESCRIBE TABLE WT_FACTURES LINES W_COUNT_BEFORE.

    IF W_COUNT_BEFORE NE W_COUNT_NOW.
      W_CHANGED = 'X'.
    ENDIF.



    IF W_CHANGED = 'X' AND W_9210_OKCODE NE 'EXIT'.
      W_SUPPR = 'X'.
      IF W_9210_OKCODE EQ 'SAVE'.
        W_RET = 'J'.
      ELSE.

** Panaya 2010-11-13 - Replaced call to obsolete function module
** Panaya 2010-11-13 - 'POPUP_TO_CONFIRM_STEP'(task: 542)
                                                      "Panaya 2010-11-13
        CALL FUNCTION 'POPUP_TO_CONFIRM'              "Panaya 2010-11-13
            EXPORTING                                 "Panaya 2010-11-13
                TITLEBAR = TEXT-T01                   "Panaya 2010-11-13
                TEXT_QUESTION = TEXT-Q03              "Panaya 2010-11-13
                TEXT_BUTTON_1 = 'Yes'                 "Panaya 2010-11-13
                TEXT_BUTTON_2 = 'No'                  "Panaya 2010-11-13
                DEFAULT_BUTTON = '2'                  "Panaya 2010-11-13
            IMPORTING                                 "Panaya 2010-11-13
                ANSWER = W_RET.                       "Panaya 2010-11-13
                                                      "Panaya 2010-11-13
        IF W_RET = '1'.                               "Panaya 2010-11-13
            W_RET = 'J'.                              "Panaya 2010-11-13
        ELSEIF W_RET = '2'.                           "Panaya 2010-11-13
            W_RET = 'N'.                              "Panaya 2010-11-13
        ENDIF.                                        "Panaya 2010-11-13
      ENDIF.
      CASE W_RET.
*       Oui on sauvegarde la facture
        WHEN 'J'.
          IF  NOT W_SUPPR IS INITIAL OR
              NOT W_CONSOLIDATION IS INITIAL.

            LOOP AT WT_FACTURES.
              WA_TABIX = SY-TABIX.
              READ TABLE WT_ALV_FACT INTO WS_FACT INDEX WA_TABIX.
              IF SY-SUBRC NE 0.
                DELETE WT_FACTURES  INDEX WA_TABIX.
              ENDIF.
            ENDLOOP.

          ELSEIF NOT W_DECOMPOSE IS INITIAL.
*           Passage sur wt_factures pour supprimer les segments
*           non présents et les informations de début et de fin
*           de segment
            CLEAR: WA_DEBUT, WA_ECART.
            LOOP AT WT_ALV_FACT INTO WS_FACT.
              WA_TABIX = SY-TABIX.
              READ TABLE WT_FACTURES INDEX WA_TABIX.
              CHECK SY-SUBRC = 0.
              IF WS_FACT-MAKTX = 'POSTE DECOMPOSE'.
                IF WA_DEBUT IS INITIAL. "Il s'agit du premier segment
                  MOVE : WT_FACTURES-DEBUT TO WA_DEBUT.
                  WA_ECART = WT_FACTURES-FIN - WT_FACTURES-DEBUT.
                ENDIF.
                DELETE WT_FACTURES INDEX WA_TABIX.
              ENDIF.
            ENDLOOP.
*           Les segments restants sont modifiés quant à leur
*           position debut et fin
            LOOP AT WT_FACTURES.
              WA_TABIX = SY-TABIX.
              UNPACK WA_TABIX TO WT_FACTURES-POSEX.
              SHIFT WT_FACTURES-POSEX LEFT DELETING LEADING '0'.
*              MOVE wa_tabix TO wt_factures-posex.
              MOVE WA_DEBUT TO WT_FACTURES-DEBUT.
              WT_FACTURES-FIN = WA_DEBUT + WA_ECART.
              WT_FACTURES-POS_1 = WT_FACTURES-DEBUT + 1.
              WT_FACTURES-POS_2 = 0.
              WA_DEBUT = WT_FACTURES-FIN.
              MODIFY WT_FACTURES INDEX WA_TABIX.
            ENDLOOP.

            LOOP AT WT_ALV_FACT INTO WS_FACT.
              WA_TABIX = SY-TABIX.

              READ TABLE WT_FACTURES INDEX WA_TABIX.

              IF SY-SUBRC NE 0. "Il s'agit de segments supplémentaires
                WT_FACTURES-ZEILE = WS_FACT-POSTECMDE.
                WT_FACTURES-BELNR = WS_FACT-NUMCMDE.
                WT_FACTURES-MWSKZ = WS_FACT-CODETVA.
                UNPACK WA_TABIX TO WT_FACTURES-POSEX.
                SHIFT WT_FACTURES-POSEX LEFT DELETING LEADING '0'.
*                MOVE wa_tabix TO wt_factures-posex.
                WT_FACTURES-DEBUT = WA_DEBUT.
                WT_FACTURES-FIN   = WA_DEBUT + WA_ECART.
                WT_FACTURES-POS_1 = WT_FACTURES-DEBUT + 1.
                WT_FACTURES-POS_2 = 0.
                WA_DEBUT = WT_FACTURES-FIN.
                APPEND WT_FACTURES.
              ELSE.
                WT_FACTURES-ZEILE = WS_FACT-POSTECMDE.
                WT_FACTURES-BELNR = WS_FACT-NUMCMDE.
                WT_FACTURES-MWSKZ = WS_FACT-CODETVA.
                MODIFY WT_FACTURES INDEX WA_TABIX.
                WA_DEBUT = WT_FACTURES-FIN.
                WA_ECART = WT_FACTURES-FIN - WT_FACTURES-DEBUT.
              ENDIF.

            ENDLOOP.

          ENDIF.

          LOOP AT WT_ALV_FACT INTO WS_FACT.
            WA_TABIX = SY-TABIX.
            READ TABLE WT_FACTURES INDEX WA_TABIX.
            IF WT_FACTURES-ZEILE NE WS_FACT-POSTECMDE OR
               WT_FACTURES-BELNR NE WS_FACT-NUMCMDE OR
               WT_FACTURES-MWSKZ NE WS_FACT-CODETVA OR
               NOT W_DECOMPOSE IS INITIAL OR
               NOT W_CONSOLIDATION IS INITIAL OR
               NOT W_SUPPR IS INITIAL.

              WT_FACTURES-MENGE = WS_FACT-QUANTITE.
              WT_FACTURES-NETPR = WS_FACT-PRIXUNIT.
              WT_FACTURES-BETRG = WS_FACT-TOTALHT.
              WT_FACTURES-KTEXT = WS_FACT-MAKTX.
              WT_FACTURES-MATNR = WS_FACT-ARTICLE.
              WT_FACTURES-ZEILE = WS_FACT-POSTECMDE.
              WT_FACTURES-BELNR = WS_FACT-NUMCMDE.
              WT_FACTURES-MWSKZ = WS_FACT-CODETVA.
              WT_FACTURES-MWSBT = WS_FACT-TVA.
              WT_FACTURES-TTC   = WS_FACT-TOTALTTC.

              OVERLAY WT_FACTURES-MENGE WITH '0000000000.000'.

              MODIFY WT_FACTURES INDEX WA_TABIX.

*             T. NGUYEN 25072006
*             Le MODIFY ne marche pas lors d'un ajout de postes
              IF SY-SUBRC = 4.
                APPEND WT_FACTURES.
              ENDIF.
*             T. NGUYEN 25072006


            ENDIF.
          ENDLOOP.
          CLEAR WT_RETURN[].

          CLEAR ZBALANCE_04-TTC.
          CLEAR ZBALANCE_04-TVA.

*         T. NGUYEN 26072006
*         Correction d'un bug dans le calcul TVA
          REFRESH WT_FACT_TVARECAP. CLEAR WT_FACT_TVARECAP.
*         T. NGUYEN 26072006

*         Cumul des montant HT des poste par code TVA
          LOOP AT WT_ALV_FACT INTO WA_ALV_FACT.
            READ TABLE WT_FACT_TVARECAP
            WITH KEY CODETVA = WA_ALV_FACT-CODETVA BINARY SEARCH.
            IF SY-SUBRC NE 0.
              WT_FACT_TVARECAP-CODETVA = WA_ALV_FACT-CODETVA.
              WT_FACT_TVARECAP-TOTALHT = WA_ALV_FACT-TOTALHT.
*>> RGI008 - Balance V2
              IF CALCTVA = SPACE.
                WT_FACT_TVARECAP-TVA = WA_ALV_FACT-TVA.
              ENDIF.
              APPEND WT_FACT_TVARECAP.
            ELSE.
              ADD WA_ALV_FACT-TOTALHT TO WT_FACT_TVARECAP-TOTALHT.
              MODIFY WT_FACT_TVARECAP INDEX SY-TABIX.
            ENDIF.
          ENDLOOP.

          BREAK GILR.
*         Récupération du taux de TVA pour chaque code et calcul des
*         montants de TVA
          LOOP AT WT_FACT_TVARECAP.
*-----------------------------------------------------------------------
* ITGGH   23.02.2007  CHG_02                                 DE5K900049
* Extension for logical system
            SELECT * FROM  T076M UP TO 1 ROWS
              WHERE  PARART  = 'LS'
              AND    MWART   = WT_FACT_TVARECAP-CODETVA
              AND    MWSKZ   = WT_FACT_TVARECAP-CODETVA.
            ENDSELECT.
            IF SY-SUBRC <> 0.
*-----------------------------------------------------------------------
              SELECT * FROM  T076M UP TO 1 ROWS
                WHERE  PARART  = 'LI'
                AND    KONTO   = ZBALANCE_04-LIFNR
                AND    MWART   = WT_FACT_TVARECAP-CODETVA
                AND    MWSKZ   = WT_FACT_TVARECAP-CODETVA.
              ENDSELECT.
            ENDIF.                            "* ITGGH 23.02.2007 CHG_02
            IF SY-SUBRC EQ 0.
              WT_FACT_TVARECAP-TAUX = T076M-MWSATZ.
*>> Ajout RGI008 - Balance V2 - Arrondi
              IF CALCTVA = 'X'.
                WT_FACT_TVARECAP-TVA = WT_FACT_TVARECAP-TOTALHT *
                                       ( WT_FACT_TVARECAP-TAUX / 100 ).
              ENDIF.
*>> FIN RGI008
              WT_FACT_TVARECAP-TOTALTTC = WT_FACT_TVARECAP-TOTALHT +
                                          WT_FACT_TVARECAP-TVA.
              MODIFY WT_FACT_TVARECAP.
            ENDIF.
          ENDLOOP.

*         Cacul des montant globaux de la facture avec les données de
*         récap de TVA
          LOOP AT WT_FACT_TVARECAP.
            ADD WT_FACT_TVARECAP-TOTALTTC TO ZBALANCE_04-TTC.
            ADD WT_FACT_TVARECAP-TVA      TO ZBALANCE_04-TVA.
          ENDLOOP.


*         Prise en compte des coûts additionnels/remises
          LOOP AT WT_ALV_CADD INTO WA_ALV_CADD.
            IF WA_ALV_CADD-MSATZ IS INITIAL.
              PERFORM GET_VATRATE_CADD.
            ENDIF.
            IF WA_ALV_CADD-KOBTR IS INITIAL.
              WA_ALV_CADD-KOBTR = WA_ALV_CADD-BETRG *
                                  ( WA_ALV_CADD-MSATZ / 100 ).
              WA_ALV_CADD-KOBTR = WA_ALV_CADD-KOBTR + WA_ALV_CADD-BETRG
      .
            ENDIF.
            CASE WA_ALV_CADD-ALCKZ.
              WHEN '+'.
                ZBALANCE_04-TTC = ZBALANCE_04-TTC + WA_ALV_CADD-KOBTR.
                ZBALANCE_04-TVA = ZBALANCE_04-TVA +
                         ( WA_ALV_CADD-KOBTR - WA_ALV_CADD-BETRG ).
              WHEN '-'.
                ZBALANCE_04-TTC = ZBALANCE_04-TTC - WA_ALV_CADD-KOBTR.
                ZBALANCE_04-TVA = ZBALANCE_04-TVA -
                         ( WA_ALV_CADD-KOBTR - WA_ALV_CADD-BETRG ).

            ENDCASE.
          ENDLOOP.
***
          CALL FUNCTION 'ZBALANCE_UPDATE_INVOIC_MM'
            EXPORTING
              I_IDOC_CONTRL   = I_IDOC_CONTROL
              I_BUKRS         = ZBALANCE_04-BUKRS
              I_BUKRS_X       = W_BUKRS_X
              I_DATECOMP      = W_NEW_DATECOMP
              I_TTC           = ZBALANCE_04-TTC
              I_TVA           = ZBALANCE_04-TVA
            TABLES
              T_IDOC_DATA     = T_IDOC_DATA
              T_CADD          = WT_ALV_CADD
              T_FACTURES      = WT_FACTURES
              T_FACTURES_ORIG = WT_FACTURES_ORIG
            CHANGING
              E_RETURN        = WT_RETURN.
          IF WT_RETURN[] IS INITIAL.
            COMMIT WORK AND WAIT.

*-----------------------------------------------------------------------
* ITGGH   12.02.2007   CHG_01                               DE5K900049
* save invoice in archive
            CALL FUNCTION 'ZBALANCE_ARCHIVE_LINK_MM'
              EXPORTING
                IDOC_CONTRL = I_IDOC_CONTROL
              TABLES
                IDOC_DATA   = T_IDOC_DATA
              CHANGING
                E_RETURN    = WT_RETURN.
            IF WT_RETURN[] IS INITIAL.
              COMMIT WORK AND WAIT.
            ELSE.
              PERFORM F_AFF_RAPPORT TABLES WT_RETURN.
              ROLLBACK WORK.
            ENDIF.
*-----------------------------------------------------------------------

          ELSE.
            PERFORM F_AFF_RAPPORT TABLES WT_RETURN.
            CLEAR W_CHANGED.
            CLEAR W_9210_OKCODE.
            ROLLBACK WORK.
          ENDIF.

*         Mise à jour du numéro de l'idoc au cas où un nouvel
*         idoc aurait été créé
          I_DOCNUM = T_IDOC_DATA-DOCNUM.


          SELECT SINGLE * FROM EDIDC INTO I_IDOC_CONTROL
            WHERE DOCNUM = I_DOCNUM.
          CLEAR : ZBALANCE_04,  WT_FACTURES[], T_IDOC_DATA[].

          CALL FUNCTION 'ZBALANCE_INVOIC_MM_DETAILS'
            EXPORTING
              I_DOCNUM    = I_DOCNUM
            IMPORTING
              E_HEADER    = ZBALANCE_04
            TABLES
              T_IDOC_DATA = T_IDOC_DATA
              T_FACTURES  = WT_FACTURES.
           *ZBALANCE_04 = ZBALANCE_04.
          CLEAR: WT_ALV_FACT[], WT_FACTURES_ORIG[].
          LOOP AT WT_FACTURES.
            WS_FACT-POSTEFACT = WT_FACTURES-POSEX.
            WS_FACT-BDL       = WT_FACTURES-VERUR.
            WS_FACT-POSTECMDE = WT_FACTURES-ZEILE.
            WS_FACT-NUMCMDE   = WT_FACTURES-BELNR.
            WS_FACT-ARTICLE   = WT_FACTURES-MATNR.
            WS_FACT-MAKTX     = WT_FACTURES-MAKTX.
            WS_FACT-QUANTITE  = WT_FACTURES-MENGE.
            WS_FACT-PRIXUNIT  = WT_FACTURES-NETPR.
            WS_FACT-TOTALHT   = WT_FACTURES-BETRG.
            WS_FACT-TVA       = WT_FACTURES-MWSBT.
            WS_FACT-CODETVA   = WT_FACTURES-MWSKZ.
            WS_FACT-TOTALTTC  = WT_FACTURES-TTC.
            CLEAR WS_FACT-STATUS.
*&--------------------- Modifications v1 TBR 080904 ------------------&*
            IF WT_FACTURES-IDENT EQ 'X'.
              MOVE ICON_CHECKED TO WS_FACT-STATUS.
            ELSEIF  WT_FACTURES-IDENT EQ '1'.
              PERFORM F_210_LIRE_COMMANDE TABLES WT_POSTES
                                    USING  WT_FACTURES.
              READ TABLE WT_POSTES WITH KEY BELNR = WT_FACTURES-BELNR
                                            ZEILE = WT_FACTURES-ZEILE
                                            MENGE = WT_FACTURES-MENGE
                INTO WS_POSTE.
              IF SY-SUBRC EQ 0.
                MOVE ICON_CHECKED TO WS_FACT-STATUS.
              ELSE.
                MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
              ENDIF.
            ELSE.
              MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
            ENDIF.
*&------------------Fin Modifications v1 TBR 080904 ------------------&*
            APPEND WS_FACT TO WT_ALV_FACT.
          ENDLOOP.
          APPEND LINES OF WT_FACTURES TO WT_FACTURES_ORIG.

        WHEN 'N'.
          ZBALANCE_04 = *ZBALANCE_04.
          CLEAR: WT_ALV_FACT[].
          LOOP AT WT_FACTURES.
            WS_FACT-POSTEFACT = WT_FACTURES-POSEX.
            WS_FACT-BDL       = WT_FACTURES-VERUR.
            WS_FACT-POSTECMDE = WT_FACTURES-ZEILE.
            WS_FACT-NUMCMDE   = WT_FACTURES-BELNR.
            WS_FACT-ARTICLE   = WT_FACTURES-MATNR.
            WS_FACT-QUANTITE  = WT_FACTURES-MENGE.
            WS_FACT-PRIXUNIT  = WT_FACTURES-NETPR.
            WS_FACT-TOTALHT   = WT_FACTURES-BETRG.
            WS_FACT-TVA       = WT_FACTURES-MWSBT.
            WS_FACT-CODETVA   = WT_FACTURES-MWSKZ.
            WS_FACT-TOTALTTC  = WT_FACTURES-TTC.
            CLEAR WS_FACT-STATUS.
*&--------------------- Modifications v1 TBR 080904 ------------------&*
            IF WT_FACTURES-IDENT EQ 'X'.
              MOVE ICON_CHECKED TO WS_FACT-STATUS.
            ELSEIF  WT_FACTURES-IDENT EQ '1'.
              PERFORM F_210_LIRE_COMMANDE TABLES WT_POSTES
                                    USING  WT_FACTURES.
              READ TABLE WT_POSTES WITH KEY BELNR = WT_FACTURES-BELNR
                                            ZEILE = WT_FACTURES-ZEILE
                                            MENGE = WT_FACTURES-MENGE
                INTO WS_POSTE.
              IF SY-SUBRC EQ 0.
                MOVE ICON_CHECKED TO WS_FACT-STATUS.
              ELSE.
                MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
              ENDIF.
            ELSE.
              MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
            ENDIF.
*&------------------Fin Modifications v1 TBR 080904 ------------------&*

            APPEND WS_FACT TO WT_ALV_FACT.
          ENDLOOP.
          CLEAR W_CHANGED.

        WHEN 'A'.
          CLEAR W_CHANGED.
          CLEAR W_9210_OKCODE.
      ENDCASE.
    ENDIF.

* T. NGUYEN (Appia Consulting) - 23.08.2006
* Correction du bug 'Erreur traitement d'Idoc'
*   lors du retraitement en Reception Manquante
    PERFORM DETEC_RECEPT_MANQ
      USING WT_FACTURES-BELNR
      CHANGING W_9210_OKCODE.

    CASE W_9210_OKCODE.
      WHEN 'RAPPR'.
        IF I_IDOC_CONTROL-STATUS = '69' OR W_CHANGED = 'X'.
          PERFORM F_CTRL_FACT TABLES WT_RETURN USING SPACE.
          IF NOT ( WT_RETURN[] IS INITIAL ).

** Panaya 2010-11-13 - Replaced call to obsolete function module
** Panaya 2010-11-13 - 'POPUP_TO_CONFIRM_STEP'(task: 542)
                                                      "Panaya 2010-11-13
            data: pny_text_question2 type string.     "Panaya 2010-11-13
            concatenate 'La facture comprend encore des erreurs'(Q01)
                        'Voulez vous afficher le résultat des contrôles?'(Q02)
 into pny_text_question2 separated by space.          "Panaya 2010-11-13
                                                      "Panaya 2010-11-13
            CALL FUNCTION 'POPUP_TO_CONFIRM'          "Panaya 2010-11-13
                EXPORTING                             "Panaya 2010-11-13
                    TITLEBAR = 'Rapprochement manuel des iDocs'(T01)
                    TEXT_QUESTION = pny_text_question2
                    TEXT_BUTTON_1 = 'Yes'             "Panaya 2010-11-13
                    TEXT_BUTTON_2 = 'No'              "Panaya 2010-11-13
                    DEFAULT_BUTTON = '2'              "Panaya 2010-11-13
                IMPORTING                             "Panaya 2010-11-13
                    ANSWER = W_RET.                   "Panaya 2010-11-13
                                                      "Panaya 2010-11-13
            IF W_RET = '1'.                           "Panaya 2010-11-13
                W_RET = 'J'.                          "Panaya 2010-11-13
            ELSEIF W_RET = '2'.                       "Panaya 2010-11-13
                W_RET = 'N'.                          "Panaya 2010-11-13
            ENDIF.                                    "Panaya 2010-11-13
            IF W_RET EQ 'J'.
              PERFORM F_AFF_RAPPORT TABLES WT_RETURN.
            ENDIF.
            IF W_RET NE 'N'.
              PERFORM IDOC_UNBLOCK USING I_DOCNUM.
              CALL METHOD O_FACT_GRID->REFRESH_TABLE_DISPLAY.
              CONTINUE.
            ENDIF.
          ENDIF.

          IF NOT W_DECOMPOSE IS INITIAL
          OR NOT W_CONSOLIDATION IS INITIAL
          OR NOT W_SUPPR IS INITIAL
          OR NOT W_NEW_DATECOMP IS INITIAL.
            SELECT SINGLE * FROM EDIDC INTO I_IDOC_CONTROL
              WHERE DOCNUM = I_DOCNUM.
            IF I_IDOC_CONTROL-STATUS EQ '69'.
              CALL FUNCTION 'IDOC_MANUAL_INPUT'
                EXPORTING
                  IDOC_NUMBER                  = I_DOCNUM
                  INPUT_EXCEPTION              = '00'
                  NO_DIALOG                    = 'X'
                EXCEPTIONS
                  IDOC_NOT_IN_DATABASE         = 1
                  NO_INPUT_FUNCTION_FOUND      = 2
                  NO_FUNCTION_PARAMETERS_FOUND = 3
                  NO_STATUS_RECORD_FOUND       = 4
                  NO_AUTHORIZATION             = 5
                  OTHERS                       = 6.
              IF SY-SUBRC NE 0.
                CASE SY-SUBRC.
                  WHEN 1. MESSAGE S251(B1) WITH I_DOCNUM.
                  WHEN 2. MESSAGE S208(00) WITH TEXT-W49.
                ENDCASE.

                MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
                        WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
              ELSE.

                EXIT.
              ENDIF.
            ELSEIF I_IDOC_CONTROL-STATUS NE '53'.
              MESSAGE S042 WITH I_DOCNUM.
            ENDIF.
          ELSE.
            CALL FUNCTION 'IDOC_MANUAL_INPUT'
              EXPORTING
                IDOC_NUMBER                  = I_DOCNUM
                INPUT_EXCEPTION              = '00'
                NO_DIALOG                    = 'X'
              EXCEPTIONS
                IDOC_NOT_IN_DATABASE         = 1
                NO_INPUT_FUNCTION_FOUND      = 2
                NO_FUNCTION_PARAMETERS_FOUND = 3
                NO_STATUS_RECORD_FOUND       = 4
                NO_AUTHORIZATION             = 5
                OTHERS                       = 6.
            IF SY-SUBRC NE 0.
              CASE SY-SUBRC.
                WHEN 1. MESSAGE S251(B1) WITH I_DOCNUM.
                WHEN 2. MESSAGE S208(00) WITH TEXT-W49.
              ENDCASE.

              MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
                      WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
            ENDIF.
          ENDIF.

          CLEAR : ZBALANCE_04,  WT_FACTURES[].
          CALL FUNCTION 'ZBALANCE_INVOIC_MM_DETAILS'
            EXPORTING
              I_DOCNUM    = I_DOCNUM
            IMPORTING
              E_HEADER    = ZBALANCE_04
            TABLES
              T_IDOC_DATA = T_IDOC_DATA
              T_FACTURES  = WT_FACTURES.
           *ZBALANCE_04 = ZBALANCE_04.
          CLEAR: WT_ALV_FACT[], WT_FACTURES_ORIG[].
          LOOP AT WT_FACTURES.
            WS_FACT-POSTEFACT = WT_FACTURES-POSEX.
            WS_FACT-BDL       = WT_FACTURES-VERUR.
            WS_FACT-POSTECMDE = WT_FACTURES-ZEILE.
            WS_FACT-NUMCMDE   = WT_FACTURES-BELNR.
            WS_FACT-ARTICLE   = WT_FACTURES-MATNR.
            WS_FACT-MAKTX     = WT_FACTURES-MAKTX.
            WS_FACT-QUANTITE  = WT_FACTURES-MENGE.
            WS_FACT-PRIXUNIT  = WT_FACTURES-NETPR.
            WS_FACT-TOTALHT   = WT_FACTURES-BETRG.
            WS_FACT-TVA       = WT_FACTURES-MWSBT.
            WS_FACT-CODETVA   = WT_FACTURES-MWSKZ.
            WS_FACT-TOTALTTC  = WT_FACTURES-TTC.
            CLEAR WS_FACT-STATUS.
*&--------------------- Modifications v1 TBR 080904 ------------------&*
            IF WT_FACTURES-IDENT EQ 'X'.
              MOVE ICON_CHECKED TO WS_FACT-STATUS.
            ELSEIF  WT_FACTURES-IDENT EQ '1'.
              PERFORM F_210_LIRE_COMMANDE TABLES WT_POSTES
                                    USING  WT_FACTURES.
              READ TABLE WT_POSTES WITH KEY BELNR = WT_FACTURES-BELNR
                                            ZEILE = WT_FACTURES-ZEILE
                                            MENGE = WT_FACTURES-MENGE
                            INTO WS_POSTE.
              IF SY-SUBRC EQ 0.
                MOVE ICON_CHECKED TO WS_FACT-STATUS.
              ELSE.
                MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
              ENDIF.
            ELSE.
              MOVE ICON_INCOMPLETE TO WS_FACT-STATUS.
            ENDIF.
*&------------------Fin Modifications v1 TBR 080904 ------------------&*

            APPEND WS_FACT TO WT_ALV_FACT.
          ENDLOOP.
          APPEND LINES OF WT_FACTURES TO WT_FACTURES_ORIG.

        ELSE.
          MESSAGE S023.
        ENDIF.

        SELECT SINGLE * FROM EDIDC INTO I_IDOC_CONTROL
                        WHERE DOCNUM = I_DOCNUM.
        IF I_IDOC_CONTROL-STATUS EQ '53'.
          PERFORM IDOC_UNBLOCK USING I_DOCNUM.
          EXIT.
        ENDIF.

      WHEN 'BACK' OR 'EXIT'.
        PERFORM IDOC_UNBLOCK USING I_DOCNUM.
        EXIT.
    ENDCASE.
    CALL METHOD O_FACT_GRID->REFRESH_TABLE_DISPLAY.
  ENDDO.

  CALL METHOD O_FACT_CONTAINER->FREE
    EXCEPTIONS
      CNTL_SYSTEM_ERROR = 1
      CNTL_ERROR        = 2.
  CALL METHOD O_CMDE_CONTAINER->FREE
    EXCEPTIONS
      CNTL_SYSTEM_ERROR = 1
      CNTL_ERROR        = 2.
  CALL METHOD O_CADD_CONTAINER->FREE
    EXCEPTIONS
      CNTL_SYSTEM_ERROR = 1
      CNTL_ERROR        = 2.
  CLEAR: O_FACT_CONTAINER,  O_CMDE_CONTAINER,  O_CADD_CONTAINER,
         WT_ALV_FACT[],     WT_ALV_CMDE[],     WT_ALV_CADD[],
         WT_SORT_FACT[],    WT_SORT_CMDE[],    WT_SORT_CADD[],
         WT_CATALOG_FACT[], WT_CATALOG_CMDE[], WT_CATALOG_CADD[].

ENDFUNCTION.

*&--------------------------------------------------------------------*
*&      Form  detec_recept_manq
*&--------------------------------------------------------------------*
*       T. NGUYEN (Appia Consulting) - 23.08.2006
*       Détecte si Reception manquante.
*       Si OK, change le ok_code pour modifier le traitement
*---------------------------------------------------------------------*
*      -->X_BELNR    text
*      -->X_OKCODE   text
*---------------------------------------------------------------------*
FORM DETEC_RECEPT_MANQ
  USING X_BELNR TYPE C
  CHANGING X_OKCODE TYPE SY-UCOMM.

  DATA WT_EKBE TYPE TABLE OF EKBE.

  SELECT * FROM EKBE INTO TABLE WT_EKBE
  WHERE EBELN = X_BELNR
    AND VGABE = '1'.

  IF LINES( WT_EKBE ) = 0.
    X_OKCODE = 'BACK'.
  ENDIF.

ENDFORM.                    "detec_recept_manq

