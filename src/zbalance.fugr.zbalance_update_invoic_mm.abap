************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : MAJ des iDocs INVOIC MM après rapprochement         *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Attila Kovacs (Netinside)                                   *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*		                                                         *
* Ordre de transport: DE2K900720                                       *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*  09.2004 ! T.BREVIGNON  ! Création d'un nouvel iDoc si un ajout ou   *
*          !              ! une suppression de lignes a été effectué   *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_update_invoic_mm.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(I_IDOC_CONTRL) LIKE  EDIDC STRUCTURE  EDIDC
*"     VALUE(I_BUKRS) TYPE  BUKRS OPTIONAL
*"     VALUE(I_BUKRS_X) TYPE  FLAG OPTIONAL
*"     VALUE(I_DATECOMP) TYPE  DATUM OPTIONAL
*"     VALUE(I_TTC) LIKE  ZBALANCE_04-TTC OPTIONAL
*"     VALUE(I_TVA) LIKE  ZBALANCE_04-TVA OPTIONAL
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD
*"      T_FACTURES STRUCTURE  ZBALANCE_01
*"      T_FACTURES_ORIG STRUCTURE  ZBALANCE_01 OPTIONAL
*"      T_CADD STRUCTURE  ZBALANCE_05 OPTIONAL
*"  CHANGING
*"     REFERENCE(E_RETURN) TYPE  BAL_T_MSG OPTIONAL
*"----------------------------------------------------------------------

* MAJ de l'iDoc:
*
* SI I_BUKRS_X = 'X': MAJ du segment E1EDK14-ORGID = I_BUKRS (QUALF=011)
*
* SI T_FACTURES_orig est transmis: MAJ des champs: BELNR, ZEILE, MWSKZ
* si différents des champs correspondants de T_FACTURES
*
* SI T_FACTURES_orig n'est pas transmis: MAJ des zones BELNR, ZEILE pour
* IDENT = '1' ou IDENT = 'X'

* Table et zone de travail pour les postes de la commande
  DATA: ws_facture    TYPE zbalance_01,
        ws_orig       TYPE zbalance_01.

* Variables temporaires
  DATA: wl_msg        TYPE bal_s_msg,
        wt_idoc_data  TYPE edidd_tt,
        wl_idx        TYPE sy-tabix,
        ws_edidd      TYPE edidd,
        ws_status     LIKE edi_ds.


  DATA: BEGIN OF wt_idoc_contrl OCCURS 0.
          INCLUDE STRUCTURE edidc.
  DATA: END OF wt_idoc_contrl.

  DATA: BEGIN OF int_edidd OCCURS 30.
          INCLUDE STRUCTURE edidd.
  DATA: END OF int_edidd.

  DATA syn_rc LIKE sy-subrc.

  DATA st_e1edk03 LIKE e1edk03.

  CONSTANTS:
*   direction
    w_direct_incoming                 LIKE edidc-direct VALUE '2'.

  DATA  wa_docnum LIKE edidc-docnum.

* Données pour création iDoc - Dvt v1 TBR 07092004
  DATA: w_factlines TYPE i,
        w_idxfact LIKE sy-tabix,
        w_idxidc LIKE sy-tabix,
        wa_debut(6),
        wa_fin(6),
        wa_psgnum(6).

  DATA: wa_segnum TYPE i,
        wa_oldsegnum TYPE i VALUE 1,
        wa_soustr TYPE i,
        wa_segfather LIKE edidd-psgnum.

  DATA: wa_cpt TYPE i.

  DATA : l_segnum LIKE edidd-segnum.

  DATA : BEGIN OF w_e1eds01 OCCURS 01.
          INCLUDE STRUCTURE  edidd.
  DATA: END OF w_e1eds01.

  DATA : st_e1eds01 LIKE e1eds01.

  DATA : w_tabix LIKE sy-tabix.
  DATA : w_flag_datecomp(1) TYPE c.

* Ouverture de l'iDoc en mode d'édition
  CLEAR: sy-msgid, sy-msgty, sy-msgno.

  DATA : wt_seltab LIKE rsparams OCCURS 0 WITH HEADER LINE.

  data:  wl_idx_t  TYPE sy-tabix.   "JMJ001 - add

*----------------------------------------------------------------------*
* Dvt v1 - Création d'un nouvel iDoc si un ajout ou une suppression
* a été fait  - TBR 02092004
*----------------------------------------------------------------------*


  IF NOT w_decompose IS INITIAL OR
     NOT w_consolidation IS INITIAL OR
     NOT w_suppr IS INITIAL OR
     NOT t_cadd[] IS INITIAL.
*     NOT I_DATECOMP is initial.


*  Alimentation de la table interne wt_idoc_contrl dans le cas d'une
*  création d'un nouvel iDoc après ajout/suppression de lignes
    MOVE-CORRESPONDING i_idoc_contrl TO wt_idoc_contrl.

    wt_idoc_contrl-direct = w_direct_incoming.
    wt_idoc_contrl-credat = sy-datum.
    wt_idoc_contrl-cretim = sy-uzeit.
    wt_idoc_contrl-upddat = sy-datum.
    wt_idoc_contrl-updtim = sy-uzeit.

    IF wt_idoc_contrl-mandt IS INITIAL.
      wt_idoc_contrl-mandt = sy-mandt.
    ELSE.
      IF wt_idoc_contrl-mandt <> sy-mandt.
        MESSAGE ID       'E0'
                TYPE     'A'
                NUMBER   '051'
                WITH     'MANDT' wt_idoc_contrl-mandt.
      ENDIF.
    ENDIF.

    IF wt_idoc_contrl-docrel IS INITIAL.
      wt_idoc_contrl-docrel = sy-saprl.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_EDIT'
      EXPORTING
        document_number               = i_idoc_contrl-docnum
      TABLES
        idoc_data                     = t_idoc_data
      EXCEPTIONS
        document_foreign_lock         = 1
        document_not_exist            = 2
        document_not_open             = 3
        status_is_unable_for_changing = 4
        OTHERS                        = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.
      MOVE wt_idoc_contrl-docnum TO wa_docnum.
    ENDIF.
************************************************************************
* Ajout et modification des segments de l'Idoc manquant                *
************************************************************************

*  Suppression des postes de l'actuel Idoc
    PERFORM f_delete_postes_actuels_idoc TABLES t_idoc_data.

* 20070214 Hans-Peter Schulz IT_INFORMATIK Beginn Buchungskreis ergänzen
* wenn nicht vorhanden
    DATA: v_bukrs LIKE i_bukrs.
    DATA : st_e1edk14 LIKE e1edk14.
      v_bukrs = i_bukrs.

    IF i_bukrs IS INITIAL.
      LOOP AT t_idoc_data WHERE segnam = 'E1EDK14'.
        MOVE t_idoc_data-sdata TO st_e1edk14.
        CASE st_e1edk14-qualf.
          WHEN '011'.
            v_bukrs = st_e1edk14-orgid.
            EXIT.
        ENDCASE.
      ENDLOOP.
      if sy-subrc <> 0.

      endif.
    ENDIF.
* Ajout des nouveaux postes "décomposés" "suppprimés" ou "consolidés"
    PERFORM f_inserer_nouveaux_postes TABLES t_idoc_data
                                             t_factures
                                       USING v_bukrs.

* 20070214 Hans-Peter Schulz IT_INFORMATIK alt Buchungskreis ergänzen

** Ajout des nouveaux postes "décomposés" "suppprimés" ou "consolidés"
*    PERFORM f_inserer_nouveaux_postes TABLES t_idoc_data
*                                             t_factures
*                                       USING i_bukrs.
* 20070214 Hans-Peter Schulz IT_INFORMATIK ENDE Buchungskreis ergänzen

*   Ajout des coûts additionnels
    PERFORM t_inserer_cadd TABLES t_idoc_data
                                  t_cadd.

*   Regénération des segment de TVA
    PERFORM t_inserer_tva TABLES t_idoc_data
                          using i_tva.


* Replacer le numéro de nouvel iDoc  TBR 07092004
    LOOP AT t_idoc_data .
      MOVE wt_idoc_contrl-docnum TO t_idoc_data-docnum.
      MODIFY t_idoc_data.
    ENDLOOP.

* Mise à jour de la date comptable

    IF NOT i_datecomp IS INITIAL.

      CLEAR w_flag_datecomp.

      LOOP AT t_idoc_data INTO int_edidd WHERE segnam = 'E1EDK03'.
        MOVE int_edidd-sdata TO st_e1edk03.
        IF st_e1edk03-iddat = '016'.
          st_e1edk03-datum = i_datecomp.
          int_edidd-sdata = st_e1edk03.
          t_idoc_data = int_edidd.
          MODIFY t_idoc_data.
          w_flag_datecomp = 'X'.
        ENDIF.
      ENDLOOP.
    ENDIF.


*Les segments E1EDS01 de fin sont extraits avant renumérotation
    LOOP AT t_idoc_data INTO int_edidd WHERE segnam = 'E1EDS01'.
      MOVE int_edidd-sdata TO st_e1eds01.
      IF st_e1eds01-sumid = '010' AND NOT i_tva IS INITIAL.
        st_e1eds01-summe = i_ttc - i_tva.
      ENDIF.
      IF st_e1eds01-sumid = '011' AND NOT i_ttc IS INITIAL.
        st_e1eds01-summe = i_ttc.
      ENDIF.
      MOVE st_e1eds01 TO int_edidd-sdata.
      APPEND int_edidd  TO w_e1eds01.
      DELETE t_idoc_data.
    ENDLOOP.

* Renumérotation des segments après suppression / ajout
    LOOP AT t_idoc_data.
      wl_idx = sy-tabix.
      wa_segnum = t_idoc_data-segnum.
      IF wa_segnum GT 1.
        wa_soustr = wa_segnum - wa_oldsegnum.
        wa_oldsegnum = wa_oldsegnum + 1.
        IF wa_soustr NE 1.
          t_idoc_data-segnum = wa_oldsegnum.
          IF t_idoc_data-hlevel = '02'.
            wa_segfather = t_idoc_data-segnum .
          ELSEIF t_idoc_data-hlevel = '03'.
            t_idoc_data-psgnum =  wa_segfather.
          ENDIF.
          MODIFY t_idoc_data INDEX wl_idx.
        ENDIF.
      ENDIF.
    ENDLOOP.

**
    DESCRIBE TABLE t_idoc_data LINES w_idxidc.

*   Les segments E1EDS01 de fin sont ajoutés après renumérotation
    LOOP AT w_e1eds01 INTO int_edidd.
      w_idxidc = w_idxidc + 1.
      MOVE w_idxidc TO int_edidd-segnum.
      APPEND int_edidd  TO t_idoc_data.
    ENDLOOP.

    SORT t_idoc_data BY segnum ASCENDING.

    CLEAR wt_idoc_contrl-docnum.


    CALL FUNCTION 'IDOC_INBOUND_WRITE_TO_DB'
      TABLES
        t_data_records    = t_idoc_data
      CHANGING
        pc_control_record = wt_idoc_contrl
      EXCEPTIONS
        idoc_not_saved    = 1
        OTHERS            = 2.


    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.

    ELSE.
      EXPORT wt_idoc_contrl-docnum TO MEMORY ID 'IDOCNUM'.

      REFRESH wt_seltab. CLEAR wt_seltab.
      MOVE 'DOCNUM'              TO wt_seltab-selname.
      MOVE 'S'                   TO wt_seltab-kind.
      MOVE 'I'                   TO wt_seltab-sign.
      MOVE 'EQ'                  TO wt_seltab-option.
      MOVE wt_idoc_contrl-docnum TO wt_seltab-low.
      APPEND wt_seltab.

      SUBMIT rbdapp01 WITH SELECTION-TABLE wt_seltab
                      EXPORTING LIST TO MEMORY
                      AND RETURN.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_EDIT'
      EXPORTING
        document_number = i_idoc_contrl-docnum
      EXCEPTIONS
        idoc_not_open   = 1
        db_error        = 2
        OTHERS          = 3.

    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_PROCESS'
      EXPORTING
        document_number = i_idoc_contrl-docnum.

    CLEAR ws_status.
    MOVE '70' TO ws_status-status.
    MOVE sy-datum TO ws_status-logdat.
    MOVE sy-uzeit TO ws_status-logtim.
    MOVE 'ZSAPBALANCE' TO ws_status-repid.
    MOVE space TO ws_status-statxt.

    CALL FUNCTION 'EDI_DOCUMENT_STATUS_SET'
      EXPORTING
        document_number = i_idoc_contrl-docnum
        idoc_status     = ws_status.

    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_PROCESS'
      EXPORTING
        document_number = i_idoc_contrl-docnum.

*----------------------------------------------------------------------*
* Fin Dvt v1 - Création d'un nouvel iDoc sur un ajout ou une suppression
*  TBR 02092004
*----------------------------------------------------------------------*

  ELSE.
    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_EDIT'
      EXPORTING
        document_number                     = i_idoc_contrl-docnum
*     ALREADY_OPEN                        = 'N'
*   IMPORTING
*     IDOC_CONTROL                        =
     TABLES
        idoc_data                           = wt_idoc_data
     EXCEPTIONS
       document_foreign_lock               = 1
       document_not_exist                  = 2
       document_not_open                   = 3
       status_is_unable_for_changing       = 4
       OTHERS                              = 5.
    IF sy-subrc <> 0.
      IF NOT ( sy-msgid IS INITIAL ) AND
         NOT ( sy-msgty IS INITIAL ) AND
         NOT ( sy-msgno IS INITIAL ).
        PERFORM f_130_message
           TABLES e_return
           USING '006' 'E' i_idoc_contrl-docnum
                 'Raison: dans le message suivant'(003) '' ''.
        CLEAR wl_msg.
        MOVE-CORRESPONDING syst TO wl_msg.
        APPEND wl_msg TO e_return.
        EXIT.
      ELSE.
        PERFORM f_130_message
          TABLES e_return
          USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
        EXIT.
      ENDIF.
    ENDIF.

    CLEAR ws_edidd.
    READ TABLE wt_idoc_data INTO ws_edidd INDEX 1.
    CALL FUNCTION 'EDI_CHANGE_DATA_SEGMENT'
      EXPORTING
        idoc_changed_data_record = ws_edidd
      EXCEPTIONS
        OTHERS                   = 0.


* Mise à jour de l'iDoc
    IF NOT i_datecomp IS INITIAL.
      LOOP AT t_idoc_data INTO edidd WHERE segnam = 'E1EDK03'.
        MOVE: edidd-sdata TO st_e1edk03.
        CHECK st_e1edk03-iddat = '016'.
        CLEAR *edidd.
        READ TABLE wt_idoc_data INDEX sy-tabix INTO *edidd.
        CHECK edidd EQ *edidd.
        st_e1edk03-datum = i_datecomp.
        MOVE: st_e1edk03 TO edidd-sdata.

        CLEAR: sy-msgid, sy-msgty, sy-msgno.
        CALL FUNCTION 'EDI_CHANGE_DATA_SEGMENT'
          EXPORTING
            idoc_changed_data_record = edidd
          EXCEPTIONS
            idoc_not_open            = 1
            data_record_not_exist    = 2
            OTHERS                   = 3.
        IF sy-subrc <> 0.
          IF NOT ( sy-msgid IS INITIAL ) AND
             NOT ( sy-msgty IS INITIAL ) AND
             NOT ( sy-msgno IS INITIAL ).
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum
                    'Raison: dans le message suivant'(003) '' ''.
            CLEAR wl_msg.
            MOVE-CORRESPONDING syst TO wl_msg.
            APPEND wl_msg TO e_return.
          ELSE.
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
          ENDIF.
          EXIT.
        ENDIF.

        MODIFY wt_idoc_data  INDEX sy-tabix  FROM edidd.
        MODIFY t_idoc_data INDEX sy-tabix  FROM edidd.
      ENDLOOP.
      IF sy-subrc NE 0.
        PERFORM f_130_message
          TABLES e_return
          USING '002' 'E' i_idoc_contrl-docnum 'E1EDK14'
                          'Société'(t02) ''.
      ENDIF.
    ENDIF.

* Mise à jour de la société
    IF i_bukrs_x = 'X'.
      LOOP AT t_idoc_data INTO edidd WHERE segnam = 'E1EDK14'.
        MOVE: edidd-sdata TO e1edk14.
        CHECK e1edk14-qualf = '011'.
        CLEAR *edidd.
        READ TABLE wt_idoc_data INDEX sy-tabix INTO *edidd.
        CHECK edidd EQ *edidd.
        e1edk14-orgid = i_bukrs.
        MOVE: e1edk14 TO edidd-sdata.

        CLEAR: sy-msgid, sy-msgty, sy-msgno.
        CALL FUNCTION 'EDI_CHANGE_DATA_SEGMENT'
          EXPORTING
            idoc_changed_data_record = edidd
          EXCEPTIONS
            idoc_not_open            = 1
            data_record_not_exist    = 2
            OTHERS                   = 3.
        IF sy-subrc <> 0.
          IF NOT ( sy-msgid IS INITIAL ) AND
             NOT ( sy-msgty IS INITIAL ) AND
             NOT ( sy-msgno IS INITIAL ).
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum
                    'Raison: dans le message suivant'(003) '' ''.
            CLEAR wl_msg.
            MOVE-CORRESPONDING syst TO wl_msg.
            APPEND wl_msg TO e_return.
          ELSE.
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
          ENDIF.
          EXIT.
        ENDIF.

        MODIFY wt_idoc_data  INDEX sy-tabix  FROM edidd.
        MODIFY t_idoc_data INDEX sy-tabix  FROM edidd.
      ENDLOOP.
      IF sy-subrc NE 0.
        PERFORM f_130_message
          TABLES e_return
          USING '002' 'E' i_idoc_contrl-docnum 'E1EDK14'
                          'Société'(t02) ''.
      ENDIF.
    ELSE. "i_bukrs_x = 'X'.
* 20070214 Hans-Peter Schulz Logischen System auswerten f. Kunden Beginn
* neu wenn der Sender ein logisches system ist:
      IF i_idoc_contrl-sndprt = 'LS'.
        LOOP AT t_idoc_data INTO edidd WHERE segnam = 'E1EDK14'.
          MOVE: edidd-sdata TO e1edk14.
          CHECK e1edk14-qualf = '011'.
          CLEAR *edidd.
          READ TABLE wt_idoc_data INDEX sy-tabix INTO *edidd.
          CHECK edidd EQ *edidd.
          i_bukrs = e1edk14-orgid.
          v_bukrs = e1edk14-orgid.
          MOVE: e1edk14 TO edidd-sdata.

          MODIFY wt_idoc_data  INDEX sy-tabix  FROM edidd.
          MODIFY t_idoc_data INDEX sy-tabix  FROM edidd.

        ENDLOOP.
        IF sy-subrc <> 0.
          IF NOT ( sy-msgid IS INITIAL ) AND
             NOT ( sy-msgty IS INITIAL ) AND
             NOT ( sy-msgno IS INITIAL ).
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum
                    'Raison: dans le message suivant'(003) '' ''.
            CLEAR wl_msg.
            MOVE-CORRESPONDING syst TO wl_msg.
            APPEND wl_msg TO e_return.
          ELSE.
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
          ENDIF.
          EXIT.
        ENDIF. "sy-subrc <> 0.
      ENDIF. "I_IDOC_CONTRL-SNDPRT = 'LS'
* 20070214 Hans-Peter Schulz Logischen System auswerten f. Kunden Ende
    ENDIF. "i_bukrs_x = 'X'.

    IF e_return[] IS INITIAL.
*   Mise à jour des postes
      LOOP AT t_factures INTO ws_facture.
        IF NOT ( t_factures_orig IS REQUESTED ).
          CHECK ws_facture-ident = 'X' OR ws_facture-ident = '1'.
          CLEAR ws_orig.
        ELSE.
          READ TABLE t_factures_orig INTO ws_orig
            WITH KEY posex = ws_facture-posex.
          CHECK sy-subrc EQ 0.
        ENDIF.

*   Mise à jour du segment E1EDP02, QUALF = '001'
        IF ws_orig-belnr NE ws_facture-belnr OR
           ws_orig-zeile NE ws_facture-zeile.
*JMJ001 - change acces - acces by segnum instead of index
*          READ TABLE t_idoc_data INDEX ws_facture-pos_1 INTO edidd.
          READ TABLE t_idoc_data with key segnum = ws_facture-pos_1 INTO edidd.
          READ TABLE wt_idoc_data INDEX ws_facture-pos_1 INTO *edidd.

*          IF edidd-segnam NE 'E1EDP02' OR
*             edidd NE *edidd.
*            PERFORM f_130_message
*              TABLES e_return
*              USING '006' 'E' i_idoc_contrl-docnum
*               'Raison: différences entre les données éditées'(001)
*               'et présentes dans la base de données'(002) ''.
*            EXIT.
*          ENDIF.
          MOVE: edidd-sdata TO e1edp02.
          e1edp02-belnr = ws_facture-belnr.
          e1edp02-zeile = ws_facture-zeile.
          MOVE: e1edp02 TO edidd-sdata.
          MODIFY wt_idoc_data INDEX ws_facture-pos_1 FROM edidd.

*     Mise à jour du document
          CLEAR: sy-msgid, sy-msgty, sy-msgno.
          CALL FUNCTION 'EDI_CHANGE_DATA_SEGMENT'
            EXPORTING
              idoc_changed_data_record = edidd
            EXCEPTIONS
              idoc_not_open            = 1
              data_record_not_exist    = 2
              OTHERS                   = 3.
          IF sy-subrc <> 0.
            IF NOT ( sy-msgid IS INITIAL ) AND
               NOT ( sy-msgty IS INITIAL ) AND
               NOT ( sy-msgno IS INITIAL ).
              PERFORM f_130_message
                TABLES e_return
                USING '006' 'E' i_idoc_contrl-docnum
                      'Raison: dans le message suivant'(003) '' ''.
              CLEAR wl_msg.
              MOVE-CORRESPONDING syst TO wl_msg.
              APPEND wl_msg TO e_return.
              EXIT.
            ELSE.
              PERFORM f_130_message
                TABLES e_return
                USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
              EXIT.
            ENDIF.
          ENDIF.
        ENDIF.


*       Mise à jour du segment E1EDP04 QUALF = '011'
        IF t_factures_orig IS REQUESTED AND
           ws_orig-mwskz NE ws_facture-mwskz.
          wl_idx = ws_facture-debut.
          PERFORM f_120_recherche_segment TABLES t_idoc_data
            USING  'E1EDP04' wl_idx ws_facture-fin wl_idx e1edp04.
          IF wl_idx = 0 OR
             ( ws_facture-fin NE 0 AND
               wl_idx GE ws_facture-fin ).
          ELSE.
*JMJ001 - change acces - acces by segnum instead of index
*         READ TABLE t_idoc_data  INDEX wl_idx INTO edidd.
            READ TABLE t_idoc_data with key segnum = wl_idx INTO edidd.
            wl_idx_t = sy-tabix.
            READ TABLE wt_idoc_data INDEX wl_idx INTO *edidd.

            IF edidd-segnam NE 'E1EDP04' OR
               edidd NE *edidd.
              PERFORM f_130_message
                TABLES e_return
                USING '006' 'E' i_idoc_contrl-docnum
               'Raison: différences entre les données éditées'(001)
                 'et présentes dans la base de données'(002) ''.
              EXIT.
            ENDIF.
            MOVE: edidd-sdata TO e1edp04.
            e1edp04-mwskz = ws_facture-mwskz.
            MOVE: e1edp04 TO edidd-sdata.
            MODIFY wt_idoc_data INDEX wl_idx FROM edidd.
*JMJ001 - change acces - acces by segnum instead of index
            modify t_idoc_data index wl_idx_t from edidd.
*            MODIFY  t_idoc_data INDEX wl_idx FROM edidd.

*           Mise à jour de l'idoc
            CLEAR: sy-msgid, sy-msgty, sy-msgno.
            CALL FUNCTION 'EDI_CHANGE_DATA_SEGMENT'
              EXPORTING
                idoc_changed_data_record = edidd
              EXCEPTIONS
                idoc_not_open            = 1
                data_record_not_exist    = 2
                OTHERS                   = 3.
            IF sy-subrc <> 0.
              IF NOT ( sy-msgid IS INITIAL ) AND
                 NOT ( sy-msgty IS INITIAL ) AND
                 NOT ( sy-msgno IS INITIAL ).
                PERFORM f_130_message
                  TABLES e_return
                  USING '006' 'E' i_idoc_contrl-docnum
                       'Raison: dans le message suivant'(003) '' ''.
                CLEAR wl_msg.
                MOVE-CORRESPONDING syst TO wl_msg.
                APPEND wl_msg TO e_return.
                EXIT.
              ELSE.
                PERFORM f_130_message
                  TABLES e_return
                  USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
                EXIT.
              ENDIF.
            ENDIF.

          ENDIF.
        ENDIF.

*       Mise à jour du segment E1EDP01
        wl_idx = ws_facture-debut - 1.
        PERFORM f_120_recherche_segment TABLES t_idoc_data
          USING  'E1EDP01' wl_idx ws_facture-fin wl_idx e1edp01.
        IF wl_idx = 0 OR
           ( ws_facture-fin NE 0 AND
             wl_idx GE ws_facture-fin ).
        ELSE.
*JMJ001 - change access by segnum instead of index
*          READ TABLE t_idoc_data  INDEX wl_idx INTO edidd.
          READ TABLE t_idoc_data with key segnum = wl_idx INTO edidd.
          wl_idx_t = sy-tabix.
          READ TABLE wt_idoc_data INDEX wl_idx INTO *edidd.

          IF edidd-segnam NE 'E1EDP01'.
            PERFORM f_130_message
              TABLES e_return
              USING '006' 'E' i_idoc_contrl-docnum
             'Raison: différences entre les données éditées'(001)
               'et présentes dans la base de données'(002) ''.
            EXIT.
          ENDIF.
          MOVE: edidd-sdata TO e1edp01.
          e1edp01-menge = ws_facture-menge.
          MOVE: e1edp01 TO edidd-sdata.
          MODIFY wt_idoc_data INDEX wl_idx FROM edidd.
*JMJ001 - change acces by segnum instead of index
          modify t_idoc_data index wl_idx_t from edidd.
*          MODIFY  t_idoc_data INDEX wl_idx FROM edidd.

*           Mise à jour de l'idoc
          CLEAR: sy-msgid, sy-msgty, sy-msgno.
          CALL FUNCTION 'EDI_CHANGE_DATA_SEGMENT'
            EXPORTING
              idoc_changed_data_record = edidd
            EXCEPTIONS
              idoc_not_open            = 1
              data_record_not_exist    = 2
              OTHERS                   = 3.
          IF sy-subrc <> 0.
            IF NOT ( sy-msgid IS INITIAL ) AND
               NOT ( sy-msgty IS INITIAL ) AND
               NOT ( sy-msgno IS INITIAL ).
              PERFORM f_130_message
                TABLES e_return
                USING '006' 'E' i_idoc_contrl-docnum
                     'Raison: dans le message suivant'(003) '' ''.
              CLEAR wl_msg.
              MOVE-CORRESPONDING syst TO wl_msg.
              APPEND wl_msg TO e_return.
              EXIT.
            ELSE.
              PERFORM f_130_message
                TABLES e_return
                USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
              EXIT.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDLOOP.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_EDIT'
      EXPORTING
        document_number        = i_idoc_contrl-docnum
*           do_commit              = space
*           DO_UPDATE              = 'X'
*           WRITE_ALL_STATUS       = 'X'
*         TABLES
*           STATUS_RECORDS         =
      EXCEPTIONS
        idoc_not_open          = 1
        db_error               = 2
        OTHERS                 = 3.
    IF sy-subrc <> 0.
      IF NOT ( sy-msgid IS INITIAL ) AND
         NOT ( sy-msgty IS INITIAL ) AND
         NOT ( sy-msgno IS INITIAL ).
        PERFORM f_130_message
          TABLES e_return
          USING '006' 'E' i_idoc_contrl-docnum
                'Raison: dans le message suivant'(003) '' ''.
        CLEAR wl_msg.
        MOVE-CORRESPONDING syst TO wl_msg.
        APPEND wl_msg TO e_return.
        EXIT.
      ELSE.
        PERFORM f_130_message
          TABLES e_return
          USING '006' 'E' i_idoc_contrl-docnum '' '' ''.
        EXIT.
      ENDIF.
    ENDIF.

    CHECK e_return[] IS INITIAL.
* Mise à jour des tables internes
    LOOP AT t_factures INTO ws_facture WHERE ident = 'X'.
      READ TABLE wt_idoc_data INDEX ws_facture-pos_1 INTO edidd.
      MODIFY t_idoc_data INDEX ws_facture-pos_1 FROM edidd.
    ENDLOOP.

  ENDIF.

ENDFUNCTION.
