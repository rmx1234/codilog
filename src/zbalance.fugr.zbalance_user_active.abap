FUNCTION ZBALANCE_USER_ACTIVE.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(USNAM) TYPE  USNAM
*"  EXCEPTIONS
*"      NOT_EXIST
*"      LOCKED
*"      NOT_VALID
*"----------------------------------------------------------------------

*{   INSERT         MS4K912255                                        1
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Correction for Generique User                    *
* ----------------- * ------------------------------------------------ *
* Author            : Jean-Marc JOURON                           JMJ001*
* Date              : 27.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : Check the validity of mail address               *
* ----------------- * ------------------------------------------------ *

  DATA :
  BEGIN OF ls_usr02 ,
  gltgv TYPE xugltgv,
  gltgb TYPE xugltgb,
  uflag TYPE xuuflag,
  END OF ls_usr02,
  lt_return TYPE TABLE OF bapiret2,     "JMJ001 +
  lt_addsmtp TYPE TABLE OF bapiadsmtp.      "JMJ001 +

  SELECT SINGLE gltgv gltgb uflag FROM usr02 INTO ls_usr02
  WHERE bname = usnam
*>> START RGI002 - Correction for Generic User as MM-BTACH....
    and ustyp = 'A'. "Only Dialog User
*>> START RGI002 - Correction for Generic User as MM-BTACH....
  IF sy-subrc NE 0.
    RAISE not_exist.
  ELSEIF ls_usr02-uflag NE 0.
    RAISE locked.
  ELSE.
    IF ( NOT ls_usr02-gltgv IS INITIAL AND ls_usr02-gltgv GT sy-datum )
    OR ( NOT ls_usr02-gltgb IS INITIAL AND ls_usr02-gltgb LT sy-datum ).
      RAISE not_valid.
    ENDIF.
  ENDIF.
*JMJ001 - add control on the user e-mal adress
  CALL FUNCTION 'BAPI_USER_GET_DETAIL'
    EXPORTING
      username             = usnam
   TABLES
     return   = lt_return
     addsmtp  = lt_addsmtp.

  if lt_addsmtp[] IS INITIAL.
    raise not_valid.
  endif.
*}   INSERT




ENDFUNCTION.
