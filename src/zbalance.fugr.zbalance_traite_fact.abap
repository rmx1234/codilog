FUNCTION zbalance_traite_fact.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(I_SEL) TYPE  CHAR2 DEFAULT 'MM'
*"     VALUE(I_VER) TYPE  INT4 OPTIONAL
*"     REFERENCE(I_DOCNUM) TYPE  EDI_DOCNUM
*"     VALUE(I_LASTSTATUS) TYPE  EDIDS OPTIONAL
*"     VALUE(I_IDOC_CONTROL) LIKE  EDIDC STRUCTURE  EDIDC OPTIONAL
*"     VALUE(I_ARCHIV_ID) TYPE  TOAAR-ARCHIV_ID OPTIONAL
*"     REFERENCE(SCREENTYPE) TYPE  INT4
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD OPTIONAL
*"----------------------------------------------------------------------
***********************************************************************
* Auteur                : R.GIL                                "RGI001
* Date                  : 30.01.2007
* MODIFICATIONS         : FI_301_DIA - RGI001
* Description           : Ajout branchement MIRO pour statut
*                       : a rapprocher manuellement dans ZRM03.
***********************************************************************
***********************************************************************
* Auteur                : R.GIL                                "RGI002
* Date                  : 30.01.2007
* MODIFICATIONS         : FI_303_DIA - RGI002
* Description           : Ajout archive
***********************************************************************
***********************************************************************
* Auteur                : R.GIL                                "RGI003
* Date                  : 30.01.2007
* MODIFICATIONS         : FI_307_DIA - RGI002
* Description           : Ajout archive
***********************************************************************
***********************************************************************
* Auteur                : R.GIL                                "RGI004
* Date                  : 10.02.2007
* MODIFICATIONS         : FI_3DIA_595 - RGI004
* Description           : Add Manual treatment for FI Italy and poland
***********************************************************************
***********************************************************************
* Auteur                : JM.JOURON                            "JMJ001
* Date                  : 15.02.2008
* MODIFICATIONS         : FI_382_DIA - JMJ001
* Description           : Add Message number for saved documents
***********************************************************************
***********************************************************************
* Auteur                : JM.JOURON                            "JMJ002
* Date                  : 10.03.2008
* MODIFICATIONS         : FI_393_DIA - JMJ002
* Description           : Add PO Number/REF.BELNR and get document type
*                        from ZM207 for MIRO calling.
***********************************************************************
***********************************************************************
* Auteur                : JM.JOURON                            "JMJ003
* Date                  : 10.03.2008
* MODIFICATIONS         : FI_595_DIA - JMJ003
* Description           : Do not take the supplier item in BSIS
***********************************************************************

  DATA: wt_edids  TYPE STANDARD TABLE OF edids.
  DATA: invoicedocnumber LIKE rbkp-belnr,
        fiscalyear LIKE rbkp-gjahr,
        companycode LIKE bkpf-bukrs.
  DATA invoice_mess LIKE rbkp-belnr.


  break gilr.
  CHECK NOT ( i_docnum IS INITIAL ).

  IF t_idoc_data[]  IS INITIAL OR
     i_idoc_control IS INITIAL OR
     i_laststatus   IS INITIAL.

    IF t_idoc_data[] IS INITIAL.
      CALL FUNCTION 'IDOC_READ_COMPLETELY'
        EXPORTING
          document_number         = i_docnum
        IMPORTING
          idoc_control            = i_idoc_control
        TABLES
          int_edidd               = t_idoc_data
          int_edids               = wt_edids
        EXCEPTIONS
          document_not_exist      = 1
          document_number_invalid = 2
          OTHERS                  = 3.
    ELSE.
      CALL FUNCTION 'IDOC_READ_COMPLETELY'
        EXPORTING
          document_number         = i_docnum
        IMPORTING
          idoc_control            = i_idoc_control
        TABLES
          int_edids               = wt_edids
        EXCEPTIONS
          document_not_exist      = 1
          document_number_invalid = 2
          OTHERS                  = 3.
    ENDIF.
    CHECK sy-subrc EQ 0.
    READ TABLE wt_edids INTO i_laststatus INDEX 1.
  ENDIF.

  CASE i_sel.
    WHEN 'MM'.
*>> RGI001 DELETION
*      CALL FUNCTION 'ZBALANCE_INVOIC_MM'
*           EXPORTING
*                i_docnum       = i_docnum
*                i_ver          = i_ver
*                i_laststatus   = i_laststatus
*                i_idoc_control = i_idoc_control
*                i_archiv_id      = i_archiv_id
*                screentype     = screentype
*           TABLES
*                t_idoc_data    = t_idoc_data.
*>> RGI001 ADDITION
      DATA messages LIKE bdcmsgcoll OCCURS 0 WITH HEADER LINE.
      DATA msgv1 TYPE edi_stapa1.
      DATA msgv2 TYPE edi_stapa2.
*>> RGI003 Addition to manage popup screen for Company code
      DATA bukrs LIKE t001-bukrs.
      DATA ebeln LIKE ekko-ebeln.
      DATA x_e1edka1 LIKE e1edka1.
*>JMJ002 - add - begin
      DATA: ls_idoc_ctrl TYPE edidc,
            l_blart TYPE blart,
            l_lifnr TYPE lifnr,
            l_xblnr TYPE xblnr,
            l_e1edk01 TYPE e1edk01,
            l_e1edk03 TYPE e1edk03,
            l_e1edk04 TYPE e1edk04,
            l_e1eds01 TYPE e1eds01.
      DATA idoc_bukrs TYPE bukrs.
*<JMJ002 - add - end
*      GET PARAMETER ID 'BUK' FIELD bukrs.
*      IF bukrs IS INITIAL.
      LOOP AT t_idoc_data WHERE segnam = 'E1EDKA1'.
        MOVE t_idoc_data-sdata TO x_e1edka1.
*JMJ002 - get supplier number to access ZM207 for document type
*        CHECK x_e1edka1-parvw = 'RE'.    "JMJ002 - del
        IF x_e1edka1-parvw = 'RE'.        "JMJ002 - add
          MOVE x_e1edka1-name1 TO ebeln.     "JMJ002 - chg
*§..RGI011 Addition FI_419_DIA
*§Getting C.Code form Idoc first
          MOVE x_e1edka1-partn TO idoc_bukrs.
        ELSEIF x_e1edka1-parvw = 'LF'.    "JMJ002 - add
          MOVE x_e1edka1-partn TO l_lifnr. "JMJ002 - add
        ENDIF.                            "JMJ002 - add
*        EXIT.
      ENDLOOP.
*>JMJ002 - add access for the external number
      LOOP AT t_idoc_data WHERE segnam = 'E1EDK01'.
        MOVE t_idoc_data-sdata TO l_e1edk01.
        MOVE l_e1edk01-belnr TO l_xblnr.
      ENDLOOP.
*§..RGI010 Adddition FI_419_DIA
*§..TVA
      DATA v_wmwst(10).
      DATA l_mwskz TYPE mwskz.
      LOOP AT t_idoc_data WHERE segnam = 'E1EDK04'.
        MOVE t_idoc_data-sdata TO l_e1edk04.
        MOVE l_e1edk04-mwskz TO l_mwskz.
        MOVE l_e1edk04-mwsbt TO v_wmwst.
      ENDLOOP.

*§..Billing Date
      DATA l_bldat TYPE bldat.
      LOOP AT t_idoc_data WHERE segnam = 'E1EDK03'.
        MOVE t_idoc_data-sdata TO l_e1edk03.
        MOVE l_e1edk03-datum TO l_bldat.
      ENDLOOP.

*§..
      DATA v_wrbtr(10).

      LOOP AT t_idoc_data WHERE segnam = 'E1EDS01'.
        MOVE t_idoc_data-sdata TO l_e1eds01.
        IF l_e1eds01-sumid = '011'.
          MOVE l_e1eds01-summe TO v_wrbtr.
        ELSEIF l_e1eds01-sumid = '010'.
*          MOVE l_e1eds01-summe TO v_wmwst.
        ENDIF.
      ENDLOOP.
*      IF sy-subrc = 0.
*        v_tva = v_wrbtr - v_wmwst.
*      ENDIF.
      DATA v_refint TYPE idoccrfint.
      MOVE i_idoc_control-refint TO v_refint.
*§..RGI010 End Adddition FI_419_DIA

      IF NOT ebeln IS INITIAL.
        SELECT SINGLE bukrs INTO bukrs FROM ekko WHERE ebeln = ebeln.
*§..RGI Addtion for Company Code
      ELSE.
        bukrs = idoc_bukrs.
      ENDIF.

*> JMJ002 - Add access to ZM207 to get the document type depending on company code and supplier number
      MOVE i_idoc_control TO ls_idoc_ctrl.
      SELECT SINGLE cblartkr INTO l_blart FROM zm207 WHERE mescod = ls_idoc_ctrl-mescod
                                                   AND mesfct = ls_idoc_ctrl-mesfct
                                                   AND bukrs  = bukrs
                                                   AND lifnr  = l_lifnr.
*                                                   AND zidoc51 = 'X'.
      IF sy-subrc NE 0.
        SELECT SINGLE cblartkr INTO l_blart FROM zm207 WHERE mescod = ls_idoc_ctrl-mescod
                                                             AND mesfct = ls_idoc_ctrl-mesfct
                                                             AND bukrs  = bukrs
                                                             AND lifnr  = space.
*                                                               AND zidoc51 = 'X'.

      ENDIF.
*      ENDIF.
      SET PARAMETER ID 'BUK' FIELD bukrs.
*>> END RGI003 Addition

      DATA datum(10) .
*      WRITE sy-datum DD/MM/YYYY TO datum.
      WRITE l_bldat DD/MM/YYYY TO datum.
      DATA v_dcpfm LIKE usr01-dcpfm.
      SELECT SINGLE dcpfm INTO v_dcpfm FROM usr01 WHERE bname = sy-uname.
*                                                    AND dcpfm NE 'X'.
      IF sy-subrc EQ 0 AND ( v_dcpfm = ' ' OR v_dcpfm = 'Y' ).
        REPLACE '.' WITH ',' INTO v_wrbtr.
        REPLACE '.' WITH ',' INTO v_wmwst.
      ENDIF.
      REFRESH w_bdcdata.
      PERFORM bdc_dynpro      USING 'SAPLMR1M'   '6000'.
      PERFORM bdc_field       USING 'INVFO-BLDAT' datum.
      PERFORM bdc_field       USING 'INVFO-XBLNR' l_xblnr.   "JMJ002 - add ref number
      PERFORM bdc_field       USING 'INVFO-WRBTR' v_wrbtr.   "RGI010 Addition
      PERFORM bdc_field       USING 'INVFO-WMWST' v_wmwst.   "RGI010 Addition
*      PERFORM bdc_field       USING 'INVFO-MWSKZ' l_mwskz.   "RGI010 Addition
      PERFORM bdc_field       USING 'BDC_OKCODE' 'HEADER_FI'.
      PERFORM bdc_dynpro      USING 'SAPLMR1M'   '6000'.
      IF NOT l_blart IS INITIAL.       "JMJ002 - add blart from ZM207
        PERFORM bdc_field       USING 'INVFO-BLART' l_blart. "JMJ002 - add blart from ZM207
      ELSE.                            "JMJ002 - add blart from ZM207
        PERFORM bdc_field       USING 'INVFO-BLART' 'RB'.
      ENDIF.                           "JMJ002 - add blart from ZM207
      PERFORM bdc_field       USING 'INVFO-BKTXT' v_refint. "RGI010 Addition
      PERFORM bdc_field       USING 'RM08M-EBELN' ebeln.    "JMJ002 - Add
      PERFORM bdc_field       USING 'BDC_OKCODE' 'HEADER_TOTAL'.
      PERFORM bdc_field       USING 'RM08M-EBELN' ebeln.

      CALL TRANSACTION 'MIRO' USING w_bdcdata
                   MODE 'E'
                   UPDATE 'S'
                   MESSAGES INTO messages.
      break majgilr.
      GET PARAMETER ID 'RBN' FIELD invoicedocnumber.
      GET PARAMETER ID 'GJR' FIELD fiscalyear.
      LOOP AT messages WHERE tcode = 'MIRO'
                         AND msgtyp = 'S'
                         AND msgid = 'M8'
                         AND ( msgnr = '075' OR msgnr = '060'
                               OR msgnr = '661' OR msgnr = '698'  "). JMJ001 - repl DIA_382
                               OR msgnr = '391' OR msgnr = '399'
                               OR msgnr = '697' ).    "JMJ001 - add DIA_382
        MOVE messages-msgv1 TO invoice_mess.
        MOVE messages-msgv1 TO msgv1.
*        MOVE messages-msgv2 TO msgv2.
        EXIT.
      ENDLOOP.
      IF sy-subrc = 0." AND invoice_mess = invoicedocnumber.
        CALL FUNCTION 'ZBALANCE_IDOC_STATUS_CHANGE'
          EXPORTING
            docnum                    = i_docnum
            status                    = '53'
            msgty                     = 'S'
            msgid                     = 'M8'
            msgno                     = '075'
            msgv1                     = msgv1
            msgv2                     = ' '
            msgv3                     = ' '
            msgv4                     = ' '
          EXCEPTIONS
            idoc_foreign_lock         = 1
            idoc_not_found            = 2
            idoc_status_records_empty = 3
            idoc_status_invalid       = 4
            db_error                  = 5
            OTHERS                    = 6.
        IF sy-subrc <> 0.
          PERFORM message_box USING text-m04 text-m04 'A'.
        ELSE.
          COMMIT WORK.
*>> ARCHIVAGE
*>> RGI002 ADDITION - Archive Link after saving Call Transaction MIRO

          IF NOT invoicedocnumber IS INITIAL.
            PERFORM archive_link USING 'BUS2081'
                                       'ZMMINVOICE'
                                       i_idoc_control
                                       invoicedocnumber
                                       fiscalyear
                                       space.

          ENDIF.
        ENDIF.
      ENDIF.
    WHEN 'FI'.
*§..RGI004 Addition DIA_FI_595
      DATA ls_idoc_data LIKE LINE OF t_idoc_data.
      DATA ls_e1edka1 TYPE e1edka1.
      LOOP AT wt_edids INTO i_laststatus WHERE status EQ '53'.
      ENDLOOP.
      IF sy-subrc NE 0.
        DATA: workflow_result LIKE  bdwfap_par-result,
              application_variable LIKE  bdwfap_par-appl_var,
              in_update_task LIKE  bdwfap_par-updatetask,
              call_transaction_done LIKE  bdwfap_par-calltrans,
              x_idoc_control LIKE i_idoc_control OCCURS 0 WITH HEADER LINE,
              x_idoc_data   LIKE t_idoc_data     OCCURS 0 WITH HEADER LINE,
              x_idoc_status LIKE bdidocstat      OCCURS 0 WITH HEADER LINE,
              x_return_variable LIKE bdwfretvar   OCCURS 0 WITH HEADER LINE,
              x_serialization_info LIKE bdi_ser   OCCURS 0 WITH HEADER LINE.
        DATA w_bukrs TYPE bukrs.
        DATA w_lifnr(10) TYPE n.
        DATA ls_zm207 TYPE zm207.
        APPEND i_idoc_control TO x_idoc_control.
        x_idoc_data[] = t_idoc_data[].
        LOOP AT t_idoc_data INTO ls_idoc_data WHERE segnam = 'E1EDKA1'.
          MOVE ls_idoc_data-sdata TO ls_e1edka1.
          IF ls_e1edka1-parvw = 'RE'.
            MOVE ls_e1edka1-partn TO w_bukrs.
          ELSEIF ls_e1edka1-parvw = 'LF'.
            MOVE ls_e1edka1-partn TO w_lifnr.
          ENDIF.
        ENDLOOP.
        IF sy-subrc = 0.
          CLEAR t_idoc_data.
          READ TABLE x_idoc_control INDEX 1.
          SELECT SINGLE * INTO ls_zm207 FROM zm207 WHERE mescod = x_idoc_control-mescod
                                                     AND mesfct = x_idoc_control-mesfct
                                                     AND bukrs  = w_bukrs
                                                     AND lifnr  = w_lifnr
                                                     AND zidoc51 = 'X'.
          IF sy-subrc NE 0.
            SELECT SINGLE * INTO ls_zm207 FROM zm207 WHERE mescod = x_idoc_control-mescod
                                                                 AND mesfct = x_idoc_control-mesfct
                                                                 AND bukrs  = w_bukrs
                                                                 AND lifnr  = space
                                                                 AND zidoc51 = 'X'.

          ENDIF.
          CHECK sy-subrc = 0.
        ENDIF.
*§.. We run here the process in BTCI call dialog mode !!!!
        CALL FUNCTION 'IDOC_INPUT_INVOIC_FI'
          EXPORTING
            input_method          = 'A'
            mass_processing       = ' '
          IMPORTING
            workflow_result       = workflow_result
            application_variable  = application_variable
            in_update_task        = in_update_task
            call_transaction_done = call_transaction_done
          TABLES
            idoc_contrl           = x_idoc_control
            idoc_data             = x_idoc_data
            idoc_status           = x_idoc_status
            return_variables      = x_return_variable
            serialization_info    = x_serialization_info
          EXCEPTIONS
            wrong_function_called = 1
            OTHERS                = 2.
        IF sy-subrc NE 0.
          break e2gilr.
        ELSE.
          DATA no TYPE msgno.
          DATA id TYPE msgid.
          DATA type TYPE symsgty.
          READ TABLE x_idoc_status WITH KEY status = '53'.
          IF sy-subrc NE 0.
            MOVE x_idoc_status-msgty TO type.
            MOVE x_idoc_status-msgid TO id.
            MOVE x_idoc_status-msgno TO no.
            CALL FUNCTION 'ZBALANCE_IDOC_STATUS_CHANGE'
              EXPORTING
                docnum                    = i_docnum
                status                    = '51'
                msgty                     = type
                msgid                     = id
                msgno                     = no
                msgv1                     = x_idoc_status-msgv1
                msgv2                     = x_idoc_status-msgv2
                msgv3                     = x_idoc_status-msgv3
                msgv4                     = x_idoc_status-msgv4
              EXCEPTIONS
                idoc_foreign_lock         = 1
                idoc_not_found            = 2
                idoc_status_records_empty = 3
                idoc_status_invalid       = 4
                db_error                  = 5
                OTHERS                    = 6.
            IF sy-subrc <> 0.
              PERFORM message_box USING text-m04 text-m04 'A'.
            ELSE.
              COMMIT WORK AND WAIT.
            ENDIF.
          ELSE.
            CALL FUNCTION 'ZBALANCE_IDOC_STATUS_CHANGE'
              EXPORTING
                docnum                    = i_docnum
                status                    = '53'
                msgty                     = 'S'
                msgid                     = 'F5'
                msgno                     = '312'
                msgv1                     = x_idoc_status-msgv1
                msgv2                     = ' '
                msgv3                     = ' '
                msgv4                     = ' '
              EXCEPTIONS
                idoc_foreign_lock         = 1
                idoc_not_found            = 2
                idoc_status_records_empty = 3
                idoc_status_invalid       = 4
                db_error                  = 5
                OTHERS                    = 6.
            IF sy-subrc <> 0.
              PERFORM message_box USING text-m04 text-m04 'A'.
            ELSE.
              COMMIT WORK.
              GET PARAMETER ID 'BLN' FIELD invoicedocnumber.
              GET PARAMETER ID 'GJR' FIELD fiscalyear.
              GET PARAMETER ID 'BUK' FIELD companycode.
              MOVE invoicedocnumber TO bkpf-belnr.
              MOVE fiscalyear       TO bkpf-gjahr.
              MOVE companycode      TO bkpf-bukrs.
              IF NOT invoicedocnumber IS INITIAL.
                PERFORM archive_link USING 'BKPF'
                                           'ZFIINVOICE'
                                           i_idoc_control
                                           invoicedocnumber
                                           fiscalyear
                                           companycode.
                SELECT SINGLE * FROM zbal_idoc WHERE docnum = i_docnum.
                IF sy-subrc = 0.
                  zbal_idoc-upddat = sy-datum.
                  zbal_idoc-updtim = sy-uzeit.
                ENDIF.
                SELECT SINGLE * FROM  bkpf
                                WHERE bukrs EQ bkpf-bukrs
                                  AND belnr EQ bkpf-belnr
                                  AND gjahr EQ bkpf-gjahr.
                IF sy-subrc = 0.
                  SELECT SINGLE hkont aufnr kostl
                           INTO (zbal_idoc-hkont, zbal_idoc-aufnr,
                                 zbal_idoc-kostl)
                           FROM bsis
                          WHERE bukrs EQ bkpf-bukrs
                            AND gjahr EQ bkpf-gjahr
                            AND belnr EQ bkpf-belnr
                            AND ( kostl NE space   "JMJ003 - add
                             OR  aufnr NE space ). "JMJ003 - add
*                            AND buzid EQ space.  "JMJ003 - del
                  IF  zbal_idoc-kostl NP c_scan
                  AND zbal_idoc-kostl NP c_cont
                  AND zbal_idoc-hkont NS c_699999.

                    SELECT SINGLE zzuswin INTO zbal_idoc-validor
                                          FROM zbal_validor
                                         WHERE bukrs = bkpf-bukrs
                                           AND lifnr = zbal_idoc-sndprn.
                    IF sy-subrc NE 0.
                      SELECT SINGLE zzuswin INTO zbal_idoc-validor
                                              FROM zbal_validor
                                             WHERE bukrs = bkpf-bukrs
                                               AND ( ( aufnr = zbal_idoc-aufnr
                                                   AND aufnr NE space )
                                                  OR ( kostl = zbal_idoc-kostl
                                                   AND kostl NE space ) ).
                    ENDIF.
                    IF NOT zbal_idoc-validor IS INITIAL.
                      UPDATE zbal_idoc.
                      CALL FUNCTION 'ZBALANCE_CREATE_SHARE_FILE'
                        EXPORTING
                          belnr     = bkpf-belnr
                          bukrs     = bkpf-bukrs
                          gjahr     = bkpf-gjahr
                          xbal_idoc = zbal_idoc.
                    ENDIF.
                  ENDIF.
                ENDIF.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
*§..RGI004 End Addition DIA_FI_595
  ENDCASE.
  CLEAR : bkpf,zbal_idoc.
ENDFUNCTION.
