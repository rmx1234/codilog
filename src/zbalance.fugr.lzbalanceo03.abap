*----------------------------------------------------------------------*
*   INCLUDE LZBALANCEO03                                               *
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Module  STATUS_9210  OUTPUT
*&---------------------------------------------------------------------*
*       Affichage de l'édition des factures MM
*----------------------------------------------------------------------*
  MODULE status_9210 OUTPUT.

    IF w_9210_init = 'X'.
      w_lib1(15) = text-d01.
      w_lib2(16) = text-d02.

      CLEAR w_9210_init.

      gs_test-dragdrop_type = con_dragdrop_row.
      APPEND 'POSTE' TO gs_test-dragdrop_fields.
      gs_test-dragdrop_effect = con_dragdrop_effect_copy.
      gs_test-edit_type     = con_edit_cell.
      gs_test-edit_mode     = con_edit_mode_display.
      gs_test-edit_event    = con_edit_event_modify.


*     Création objet liste AVL facture
      IF o_fact_container IS INITIAL.
        CREATE OBJECT o_fact_container
          EXPORTING container_name = 'ALV_FACT'.

        CREATE OBJECT o_fact_grid
          EXPORTING
            i_parent = o_fact_container.

        PERFORM load_data_fact.
      ENDIF.

*     Création objet liste AVL commande
      IF o_cmde_container IS INITIAL.
        CREATE OBJECT o_cmde_container
          EXPORTING container_name = 'ALV_CMDE'.

        CREATE OBJECT o_cmde_grid
          EXPORTING
            i_parent = o_cmde_container.

        PERFORM load_data_cde.
      ENDIF.

*     Création objet liste AVL Coûts additionnels
      IF o_cadd_container IS INITIAL.
        CREATE OBJECT o_cadd_container
          EXPORTING container_name = 'ALV_CADD'.

        CREATE OBJECT o_cadd_grid
          EXPORTING
            i_parent = o_cadd_container.

        PERFORM load_data_cadd.
      ENDIF.

      CLEAR : w_consolidation,w_decompose,w_suppr.
      IMPORT i_docnum FROM MEMORY ID 'DOCNUM'.
      MOVE i_docnum TO w_docnum.
    ENDIF.


    SET TITLEBAR  'TITRE_MM_DYNP_9210'.
    SET PF-STATUS 'MAIN_9210'.


  ENDMODULE.                 " STATUS_9210  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_9101  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
  MODULE status_9101 OUTPUT.
    SET PF-STATUS '9101'.
*>> RGI008- BAMANCE V2 - ARRONDI
*    LOOP AT SCREEN.
*      CASE screen-group1.
*        WHEN 'RGI'.
*          IF wa_alv_fact -bukrs NE 'LW01'.
*            screen-input = 0.
*            screen-invisible = 1.
*        ENDCASE.
*      ENDLOOP.
    ENDMODULE.                 " STATUS_9101  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  TABS_ACTIVE_TAB_SET  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
  MODULE tabs_active_tab_set OUTPUT.

    tabs-activetab = g_tabs-pressed_tab.
    CASE g_tabs-pressed_tab.
      WHEN c_tabs-tab1.
        g_tabs-subscreen = '9211'.
      WHEN c_tabs-tab2.
        g_tabs-subscreen = '9212'.
      WHEN OTHERS.
*      DO NOTHING
    ENDCASE.

  ENDMODULE.                 " TABS_ACTIVE_TAB_SET  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_0200  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
  MODULE status_0200 OUTPUT.

    DATA: uris LIKE bapiuri OCCURS 0 WITH HEADER LINE.
    DATA: url TYPE cndp_url.
    DATA: picture TYPE REF TO cl_gui_picture.
    DATA: picture_container TYPE REF TO cl_gui_custom_container.


*Get the picture from the server
    CALL FUNCTION 'BDS_BUSINESSDOCUMENT_GET_URL'
      EXPORTING
        classname  = 'PICTURES'
        classtype  = 'OT'
        object_key = 'IE'
      TABLES
        uris       = uris.

    LOOP AT uris.
      SEARCH uris-uri FOR '.TIF'.
      IF sy-subrc = 0.
        url = uris-uri.
        EXIT.
      ENDIF.
    ENDLOOP.

*create container and place picture in it
    IF picture IS INITIAL.
      CREATE OBJECT picture_container
      EXPORTING container_name = 'PICTURE'.

      CREATE OBJECT picture
      EXPORTING parent = picture_container.

      CALL METHOD:

      picture->load_picture_from_url
      EXPORTING url = url,

      picture->set_display_mode
      EXPORTING display_mode = picture->display_mode_normal_center.
    ENDIF.

  ENDMODULE.                 " STATUS_0200  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_9102  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
  MODULE status_9102 OUTPUT.
    SET PF-STATUS '9102'.
*  SET TITLEBAR 'xxx'.

  ENDMODULE.                 " STATUS_9102  OUTPUT
