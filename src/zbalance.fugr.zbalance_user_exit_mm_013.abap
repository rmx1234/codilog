FUNCTION ZBALANCE_USER_EXIT_MM_013.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_RBKPV) TYPE  MRM_RBKPV
*"     VALUE(I_ITEMDATA) TYPE  MMCR_F_ITEMDATA
*"  EXPORTING
*"     VALUE(E_EBELN) TYPE  DRSEG-EBELN
*"     VALUE(E_EBELP) TYPE  DRSEG-EBELP
*"     VALUE(E_LFSNR) TYPE  RM08M-LFSNR
*"     VALUE(E_CHANGE) TYPE  C
*"----------------------------------------------------------------------

call function 'ZBALANCE_CHOIX_FACT'
     exporting
          i_itemdata = i_itemdata
     importing
          e_ebeln    = e_ebeln
          e_ebelp    = e_ebelp
          e_lfsnr    = e_lfsnr
          e_change   = e_change.

ENDFUNCTION.
