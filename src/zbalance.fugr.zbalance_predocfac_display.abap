FUNCTION ZBALANCE_PREDOCFAC_DISPLAY.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(BELNR) LIKE  RBKP-BELNR
*"     REFERENCE(GJAHR) LIKE  RBKP-GJAHR
*"     REFERENCE(BUKRS) LIKE  RF05V-BUKRS
*"  EXCEPTIONS
*"      DOC_NOT_FOUND
*"----------------------------------------------------------------------

*  SELECT SINGLE * FROM ekko WHERE ebeln = ordernum.

  IF sy-subrc NE 0.
    RAISE doc_not_found.
  ELSE.
        SET PARAMETER ID 'BUK' field bukrs.
        SET PARAMETER ID 'BLP' FIELD belnr.
        SET PARAMETER ID 'GJR' FIELD gjahr.
        CALL TRANSACTION 'FBV3' AND SKIP FIRST SCREEN.
  ENDIF.

ENDFUNCTION.
