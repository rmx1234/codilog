*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZBALANCETOP.        " Global Data

  INCLUDE zbalance_lcl.        "local Class, Serco

  INCLUDE LZBALANCEUXX.        " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
  INCLUDE LZBALANCEF01.        " Utilitaires              (f_1??_*)
  INCLUDE LZBALANCEF02.        " Algorythme rapprochement (f_2??_*)

* Facture MM
  INCLUDE LZBALANCEF03.        " Routines pour facture MM
  INCLUDE LZBALANCEO03.        " PBO-Modules - facture MM
  INCLUDE LZBALANCEI03.        " PAI-Modules - facture MM

INCLUDE LZBALANCEI34.

INCLUDE LZBALANCEI37.

INCLUDE LZBALANCEF04.

INCLUDE LZBALANCEF05.
*{   INSERT         MS4K915548                                        1
*
INCLUDE LZBALANCEF06.
*}   INSERT
