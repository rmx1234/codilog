FUNCTION zbalance_image_display.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(DOCNUM) LIKE  EDIDC-DOCNUM
*"  EXCEPTIONS
*"      ERROR_ARCHIV
*"      ERROR_COMMUNICATIONTABLE
*"      ERROR_KERNEL
*"      BALANCE_CONFIG
*"      OTHERS
*"----------------------------------------------------------------------
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*18.09.2006! ITVLA        ! CHG_02:                         GEDK900124 *
*                         ! Balance v1.7 (ECC5.0) high_priority_1:     *
*                         ! In screen « Invoice list », if there is the*
*                         ! picture of the item is not available (or if*
*                         ! there is no picture), an error message is  *
*                         ! displayed when the user press the button   *
*                         ! “Image”and the transaction ZRM03 is killed.*
*                         !                                            *
*                         ! ToDo:                                      *
*                         !
*                         ! Balance has to test the availability of the*
*                         ! picture and to display a warning message to*
*                         ! inform the user (without trying to open the*
*                         ! document) if it is unavailable.            *
************************************************************************
*09.01.2008! JMJOURON     ! Problem when accessing a deleted parked
*                         ! document MM_375_DIA - JMJ001
*-------------------------!--------------------------------------------*
*
  DATA : w_url LIKE edidc-arckey.

  SELECT SINGLE * FROM edidc WHERE docnum = docnum.
  IF sy-subrc EQ 0.
    PERFORM open_idoc USING edidc-docnum.
    PERFORM read_last_status_idoc USING edidc-docnum
                                  CHANGING int_edids.
    PERFORM close_idoc USING edidc-docnum.

    SELECT SINGLE * FROM  zbal_params CLIENT SPECIFIED
           WHERE  mandt  = sy-mandt
           AND    param  = 'IMAGEACCESSMODE'.
    IF sy-subrc EQ 0.

      CASE zbal_params-value.
        WHEN 'URL'.
          w_url = edidc-arckey.
*\--> CHG_02+ #ITVLA
          perform get_file_info using w_url.
          check not w_url is initial.
*\--> CHG_02+
          PERFORM display_image_url USING w_url.
        WHEN 'URLSERVER'.
          PERFORM display_image_urlsrv.
        WHEN 'ARCHIVLINK'.
          PERFORM display_image_archivelink.
        WHEN OTHERS.
          RAISE balance_config.
      ENDCASE.
    ELSE.
      RAISE balance_config.
    ENDIF.
  ENDIF.
ENDFUNCTION.
