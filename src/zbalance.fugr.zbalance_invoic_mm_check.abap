************************************************************************
* Description    : Contrôle sur facture MM                             *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : Balance For MySAP Business Suite                            *
* Auteur : Jean-Michel BRUNOD (Netinside)                              *
* Date   : 27.10.2005                                                  *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
* 01.2008  ! JM.JOURON    !  FI_365_DIA new logic for missing receipt  *
*          !              !  JMJ001                                    *
*----------!--------------!--------------------------------------------*
* 06.2008  ! F. Chartier  !  DIA 147 Consider cancelled GR             *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
* 09.2008  ! JM.JOURON    !  DIA 611 Missing GR determination          *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_invoic_mm_check.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(W_DOCNUM) TYPE  EDIDC-DOCNUM OPTIONAL
*"  EXPORTING
*"     REFERENCE(EM_MANQ) TYPE  FLAG
*"     REFERENCE(EP_DTCT) TYPE  FLAG
*"     REFERENCE(EQ_DTCT) TYPE  FLAG
*"     REFERENCE(E_FULLY_INVOICED) TYPE  FLAG
*"  TABLES
*"      T_FACTURES STRUCTURE  ZBALANCE_01 OPTIONAL
*"      T_DOCNUM STRUCTURE  ZBALANCE_03 OPTIONAL
*"----------------------------------------------------------------------

* Table des postes de la facture
  DATA:  ws_facture     TYPE zbalance_01.
* Table des postes de(s) commandes
  DATA:  wt_postes   TYPE STANDARD TABLE OF zbalance_02.
  DATA:  ws_poste    TYPE zbalance_02.
  data: one_gr_done.   "JMJ001 - Add
  data: lw_gr.         "JMJ002 - Add
  CLEAR : em_manq,
          ep_dtct,
          eq_dtct,
          one_gr_done.  "JMJ001 - Add

  clear lw_gr.         "JMJ002 - Add


  LOOP AT t_factures INTO ws_facture.
    IF ws_facture-pos_2 IS INITIAL.
*     Sélection: Commande
      PERFORM f_210_lire_commande TABLES wt_postes USING ws_facture.
    ELSE.
*     Sélection: Bon de livraison
      PERFORM f_210_lire_bdl TABLES wt_postes USING ws_facture.
    ENDIF.

    READ TABLE wt_postes WITH KEY belnr = ws_facture-belnr
                                  zeile = ws_facture-zeile
          INTO ws_poste.
    IF sy-subrc EQ 0.

*     Contrôle Entrée de marchandise manquante
      IF ws_poste-webre = 'X'.

*----------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 22.08.2006
*
*        PERFORM f_190_ctrle_em USING
*          ws_poste-ebeln
*          ws_poste-ebelp
*          w_docnum
*          ws_poste-belnr
*          ws_facture-menge
*          ws_poste-menge.

* DIA 148 FCH001: Now we take cancelled GR into account
*        PERFORM f_190_ctrle_em_2 USING
*          ws_poste-ebeln
*          ws_poste-ebelp
*          w_docnum
*          ws_poste-belnr
*          ws_facture-menge
*          ws_poste-menge.

* DIA 148 FCH001: Now we take cancelled GR into account
    DATA: w_qte_fact TYPE ekpo-menge,
          w_qte_em   TYPE ekpo-menge.
    clear: w_qte_fact,
           w_qte_em.
    CALL FUNCTION 'ZBALANCE_QTE_EM_REST_A_FAC'
      EXPORTING
        x_ebeln             = ws_poste-ebeln
        x_ebelp             = ws_poste-ebelp
*        x_xblnr             =
      IMPORTING
*        x_qte_em_rest_a_fac =
        x_qte_fact         = w_qte_fact
*        x_mtt_rest_a_fac   =
        x_qte_em            = w_qte_em
      EXCEPTIONS
        no_em_found         = 1.

    if sy-subrc = 1 or ( w_qte_fact ge w_qte_em
                        and w_qte_em lt ws_poste-menge ). "JMJ002 - Add
      w_em_manq = 'X'.
      w_em_ebeln = ws_poste-ebeln.
      w_em_belnr = ws_poste-ebelp.
    endif.
* DIA 148 FCH001 End
* JMJ002 - begin DIA 611 - check still something to deliver or to invoice
    if w_qte_fact ge w_qte_em and
       w_qte_em ge ws_poste-menge.
       lw_gr = 'X'.
    else.
       clear lw_gr.
    endif.
* T. NGUYEN (Appia Consulting) - 22.08.2006
*----------------------------------------------------------------------*

        IF  w_em_manq = 'X' and one_gr_done ne 'X'. "JMJ001 - change
          em_manq = 'X'.
        elseif w_em_manq ne 'X'.                    "JMJ001 - add
          clear em_manq.                            "JMJ001 - add
          move 'X' to one_gr_done.                  "JMJ001 - add
        elseif one_gr_done eq 'X'.                  "JMJ001 - add
           clear em_manq.                           "JMJ001 - add
*          EXIT.
        ENDIF.
      ENDIF.

*     Contrôle écart de prix
      IF ws_facture-netpr NE ws_poste-netpr.
        ep_dtct = 'X'.
      ENDIF.

*     Contrôle écart de quantité
      IF ws_facture-menge NE ws_poste-menge.
        eq_dtct = 'X'.
      ENDIF.

    ENDIF.
*>> JMJ001 - del
*    IF em_manq = 'X' OR
*       ep_dtct = 'X' OR
*       eq_dtct = 'X'.
*      EXIT.
*    ENDIF.
*<< JMJ001 - del
  ENDLOOP.

 move lw_gr to e_fully_invoiced.   "JMJ002 - Add

ENDFUNCTION.
