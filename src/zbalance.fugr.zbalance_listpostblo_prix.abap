FUNCTION zbalance_listpostblo_prix.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_BELNR) LIKE  RBKP-BELNR
*"     VALUE(I_GJAHR) LIKE  RBKP-GJAHR
*"     REFERENCE(I_LISTMODE) LIKE  SY-INDEX DEFAULT 1
*"  EXPORTING
*"     REFERENCE(E_DECISION) LIKE  SY-UCOMM
*"     REFERENCE(E_DWERT) LIKE  ARSEG-DWERT
*"  TABLES
*"      TE_SELECTED_INVOICE_ITEMS STRUCTURE  RSEG_BUZEI
*"----------------------------------------------------------------------
************************************************************************
* Identification : ZBALANCE_LISTPOSTBLO_PRIX		         *
*                                                                      *
*                                                                      *
*  Description de la fonction :                                        *
*  Cette fonction permet de rechercher l'écart de prix entre le poste  *
*  facture et le poste commande                                        *
*  En entrée: I_BELNR : Numéro de document de facturation              *
*             I_GJAHR : Exercice comptable                             *
*             I_LISTMODE : Type d'affichage                            *
*  En sortie: E_DECISION : Décision                                    *
*                                                                      *
**----------------------------------------------------------------------
*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur :  Frédérick HUYNH   (Netinside)                              *
*                                                                      *
* Date : 27/01/2004                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport:                                                  *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
* 27/05/08 ! F. Chartier  ! DIA 208: If there are no payment blocks    *
*          !              ! found, continue workflow.         FCH001   *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
*                                                                      *
* Auteur                : R.GIL                                "RGI001
* Date                  : 30.08.2007
* MODIFICATIONS         : FI_xxx_DIA - RGI001
* Description           : Correction dans le cas ou le pb de prix a
* déjà été corrigé.
*----------------------------------------------------------------------*

* si I_LISTMODE = 1 => Affichage liste poste
* si I_LISTMODE = 2 => Validation Acheteur
* si I_LISTMODE = 3 => Validation Compta

  CONSTANTS: c_dec_noblock type SY-UCOMM value 'NOBLOCK'.   "FCH001+

  CHECK i_listmode EQ '1' OR i_listmode EQ '2' OR i_listmode EQ '3'.

  w_belnr = i_belnr.
  w_gjahr = i_gjahr.
  w_listmode = i_listmode.
  CLEAR rseg.
  CLEAR e_dwert .
  CLEAR t_item_prix.
  REFRESH t_item_prix.


  SELECT * APPENDING CORRESPONDING FIELDS OF TABLE t_item_prix
    FROM rseg
    WHERE belnr = i_belnr
      AND gjahr = i_gjahr
      AND spgrp = 'X'.
*  SELECT * FROM rseg WHERE belnr = i_belnr
*                       AND gjahr = i_gjahr
*                       AND spgrp = 'X'.
*
*    MOVE-CORRESPONDING rseg TO t_item_prix.
*    APPEND t_item_prix.
*  ENDSELECT.


  LOOP AT t_item_prix.

    CALL FUNCTION 'ZBALANCE_RECHERCHE_ECARTPRIX'
      EXPORTING
        i_belnr  = i_belnr
        i_gjahr  = i_gjahr
        i_buzei  = t_item_prix-buzei
      IMPORTING
        e_ebeln  = t_item_prix-ebeln
        e_ebelp  = t_item_prix-ebelp
        e_netpr  = t_item_prix-netpr
        e_netpr2 = t_item_prix-netpr2
        e_dwert  = t_item_prix-dwert
        e_waers  = t_item_prix-waers
      EXCEPTIONS
        no_found = 1
        OTHERS   = 2.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE 'I' NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.
      e_dwert = t_item_prix-dwert.
      MODIFY t_item_prix.
    ENDIF.

  ENDLOOP.
  IF NOT t_item_prix IS INITIAL.
    CALL SCREEN 9200.
    e_decision = w_decision.
    REFRESH te_selected_invoice_items. CLEAR te_selected_invoice_items.
    LOOP AT t_item_prix WHERE sel = 'X'.
      te_selected_invoice_items-buzei = t_item_prix-buzei.
      APPEND  te_selected_invoice_items.
    ENDLOOP.
*>> Ajout RGI001.
  ELSE.
    e_decision = c_dec_noblock.                               "FCH001+
    MESSAGE i099(zsapbalance).
  ENDIF.
  DELETE ADJACENT DUPLICATES FROM te_selected_invoice_items.

ENDFUNCTION.
