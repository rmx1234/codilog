*----------------------------------------------------------------------*
*   INCLUDE LZBALANCEI03                                               *
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9210  INPUT
*&---------------------------------------------------------------------*
*       PAI pour l'affichage des factures MM
*----------------------------------------------------------------------*
module user_command_9210 input.

data : w_date_text(10) type c.

CALL FUNCTION 'GET_DYNP_VALUE'
  EXPORTING
    i_field             = 'ZBALANCE_04-DATUM'
    i_repid             = 'SAPLZBALANCE'
    i_dynnr             = '9211'
*   I_CONV_INPUT        = ' '
*   I_CONV_OUTPUT       = ' '
  changing
    o_value             = w_date_text.

    concatenate w_date_text+6(4)
                w_date_text+3(2)
                w_date_text(2)
           into
                ZBALANCE_04-DATUM.



  perform f_2xxx_user_command using w_9210_okcode.
  clear w_9210_okcode.

endmodule.                 " USER_COMMAND_9210  INPUT
*&---------------------------------------------------------------------*
*&      Module  tabs_active_tab_get  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
module tabs_active_tab_get input.

  ok_code = sy-ucomm.
  case ok_code.
    when c_tabs-tab1.
      g_tabs-pressed_tab = c_tabs-tab1.
    when c_tabs-tab2.
      g_tabs-pressed_tab = c_tabs-tab2.
    when others.
*      DO NOTHING
  endcase.

endmodule.                 " tabs_active_tab_get  INPUT
