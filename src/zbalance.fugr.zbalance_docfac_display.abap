FUNCTION ZBALANCE_DOCFAC_DISPLAY.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(BELNR) LIKE  RBKP-BELNR
*"     REFERENCE(GJAHR) LIKE  RBKP-GJAHR
*"  EXCEPTIONS
*"      DOC_NOT_FOUND
*"----------------------------------------------------------------------

*  SELECT SINGLE * FROM ekko WHERE ebeln = ordernum.

  IF sy-subrc NE 0.
    RAISE doc_not_found.
  ELSE.
        SET PARAMETER ID 'RBN' FIELD belnr.
        SET PARAMETER ID 'GJR' FIELD gjahr.
        CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.
  ENDIF.

ENDFUNCTION.
