FUNCTION zbalance_invoic_mm_details.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_DOCNUM) TYPE  EDI_DOCNUM OPTIONAL
*"  EXPORTING
*"     VALUE(E_HEADER) TYPE  ZBALANCE_04
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD OPTIONAL
*"      T_FACTURES STRUCTURE  ZBALANCE_01 OPTIONAL
*"----------------------------------------------------------------------

  DATA:
    wl_ttc       TYPE edi_summe,
    wl_tva       TYPE edi_summe,
    wl_datum     TYPE edidat8,
    wl_societe   TYPE edi_orgid,
    wl_devise    TYPE char3,
    wl_reference TYPE edi_belnr.

  CHECK e_header IS REQUESTED.
  CLEAR: e_header, t_factures[].
  CLEAR calctva.
  IF t_idoc_data[] IS INITIAL.
    CHECK NOT ( i_docnum IS INITIAL ).
    CALL FUNCTION 'IDOC_READ_COMPLETELY'
      EXPORTING
        document_number         = i_docnum
      TABLES
        int_edidd               = t_idoc_data
      EXCEPTIONS
        document_not_exist      = 1
        document_number_invalid = 2
        OTHERS                  = 3.
    CHECK sy-subrc EQ 0.
  ENDIF.

*"----------------------------------------------------------------------
* T. NGUYEN - 20/07/2006
* Rendre le rapprochement de factures sans postes possible
  DATA w_lifnr TYPE zbalance_01-lifnr.
*"----------------------------------------------------------------------

  CALL FUNCTION 'ZBALANCE_INVOIC_GET'
    EXPORTING
      x_rapp      = 'X'
    IMPORTING
      e_ttc       = wl_ttc
      e_tva       = wl_tva
      e_datum     = wl_datum
      e_societe   = wl_societe
      e_devise    = wl_devise
      e_reference = wl_reference
      e_lifnr     = w_lifnr
    TABLES
      t_idoc_data = t_idoc_data
      t_factures  = t_factures.
  .
*CALL FUNCTION 'ZBALANCE_RAPPR_AUTO'
*  TABLES
*    t_factures       = T_FACTURES.          .

*"----------------------------------------------------------------------
* T. NGUYEN - 20/07/2006
* Rendre le rapprochement de factures sans postes possible
*  READ TABLE t_factures INDEX 1.
*  PERFORM f_170_ctrl_lifnr USING t_factures-lifnr.
*  e_header-lifnr = t_factures-lifnr.
*
  READ TABLE t_factures INDEX 1.
  PERFORM f_170_ctrl_lifnr USING w_lifnr.
  e_header-lifnr = w_lifnr.
*"----------------------------------------------------------------------

  e_header-name1 = lfa1-name1.
  e_header-belnr = wl_reference.
  SHIFT wl_devise LEFT DELETING LEADING space.
  SELECT SINGLE waers FROM tcurc
                      INTO e_header-waers
                     WHERE waers  = wl_devise
                     AND   isocd  = wl_devise.
  CALL FUNCTION 'RP_CHECK_DATE'
    EXPORTING
      date         = wl_datum
    EXCEPTIONS
      date_invalid = 1
      OTHERS       = 2.
  IF sy-subrc EQ 0.
    e_header-datum = wl_datum.
  ENDIF.
  SHIFT wl_societe LEFT DELETING LEADING space.
  SELECT SINGLE bukrs
    INTO e_header-bukrs
    FROM t001 WHERE bukrs = wl_societe.
  e_header-ttc = wl_ttc.
  e_header-tva = wl_tva.
*>> Start RGI Ajout Zone TTC de L'idoc - RGI001
  e_header-ttc_idoc = wl_ttc.
*>> End   RGI Ajout Zone TTC de L'idoc - RGI001
  IF NOT ( lfa1-lnrza IS INITIAL ).
* ex SELECT SINGLE
    SELECT  * FROM lfa1 WHERE lnrza = lfa1-lnrza.
    ENDSELECT.
  ENDIF.
  e_header-lifnr_dp = lfa1-lifnr.
  e_header-name1_dp = lfa1-name1.
  e_header-stceg_dp = lfa1-stceg.
  e_header-land1_dp = lfa1-land1.
  e_header-pstlz_dp = lfa1-pstlz.
  e_header-ort01_dp = lfa1-ort01.
  SELECT bankl FROM lfbk "ex SELECT SINGLE
                      INTO e_header-bankl_dp
                     WHERE lifnr = lfa1-lifnr
                                  AND banks = lfa1-land1.
  ENDSELECT.
ENDFUNCTION.
