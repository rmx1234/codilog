FUNCTION z_balance_user_determine.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(WF_NUMBER) TYPE  ZBAL_WF_NUMBER_D
*"     VALUE(EBELN) TYPE  EBELN
*"  EXPORTING
*"     REFERENCE(DEFAULT_USER) TYPE  ZBAL_DFT_USER
*"     REFERENCE(SPEC_RULE_USER) TYPE  USNAM
*"  EXCEPTIONS
*"      INVALID_DOCUMENT
*"----------------------------------------------------------------------
* Author            : JM.JOURON                                JMJ001  *
* Date              : 22.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : New Role determination  with ZBAL_WFROLES        *
* ----------------- * ------------------------------------------------ *
* Modification :  FI_371_DIA > JMJ001 - 03/01/2008                     *
*                 Add access by plant for the price discrepancy WF     *
* Modification :  FI_400_DIA > JMJ002 - 28/02/2008                     *
*                 Add access by material group and item category       *
* Modification :                                                       *
*----------------------------------------------------------------------*

  CONSTANTS: c_dft(10) VALUE 'DFTNAME',
             c_spc(10) VALUE 'USNAM',
             c_dft_r(15) VALUE 'L_TMP_DFT',
             c_spc_r(15) VALUE 'L_TMP_SPC',
             c_ecart_prix TYPE zbal_wf_number_d VALUE 1,
             c_ecart_qte  type zbal_wf_number_d value 2,    "JMJ002 - Add wf number for qty disc.
             c_recep_mqt  type zbal_wf_number_d value 3,    "JMJ002 - Add wf number for missing GR
             c_gr         type ekbe-vgabe value 1,          "JMJ002 - Add GR document type in EKBE
             c_inv        type ekbe-vgabe value 2,          "JMJ002 - Add invoice document type in EKBE
             c_park       type ekbe-vgabe value 'P',        "JMJ002 - Add park document type in EKBE
             c_lg         type i value 9.                   "JMJ002 - add length of material group field

  DATA: l_ekgrp TYPE ekko-ekgrp,
        l_lgort TYPE ekpo-lgort,
        l_tmp_lgort TYPE ekpo-lgort,
        l_bukrs TYPE ekko-bukrs,
        l_lg type i,
        l_werks TYPE ekpo-werks,
        l_tmp_werks TYPE ekpo-werks,
        l_afnam type ekpo-afnam,             "JMJ002 - new fields for selection
        l_matkl type ekpo-matkl,             "JMJ002 - new fields for selection
        l_pstyp type ekpo-pstyp,             "JMJ002 - new fields for selection
        l_tmp_afnam type ekpo-afnam,         "JMJ002 - new fields for selection
        l_tmp_matkl type ekpo-matkl,         "JMJ002 - new fields for selection
        l_tmp_ebelp type ekpo-ebelp,         "JMJ002 - new fields for selection
        BEGIN OF ls_tmp,
          dftname TYPE zbal_dft_user,
          usnam TYPE zbal_usnam,
        END OF ls_tmp,
        BEGIN OF ls_tmp2,
          dftname TYPE zbal_dft_user,
          usnam TYPE zbal_usnam,
        END OF ls_tmp2,
        l_data_to_read TYPE char50,
        l_data_read TYPE char50,
        l_qte TYPE ekpo-menge,
        l_ebelp type ekpo-ebelp,                  "JMJ002 - new fields for selection
        l_spec_usr type usnam,                    "JMJ002 - new fields for selection
        lt_ekbe type table of ekbe with header line,    "JMJ002 - new fields for selection
        l_spgr type spgrp.                        "JMJ002 - new fields for selection
*
   data: begin of lt_ekpo occurs 0,
          ebeln type ekpo-ebeln,
          ebelp type ekpo-ebelp,
         end of lt_ekpo.

*get parameters to access the specific table
  SELECT SINGLE bukrs ekgrp FROM ekko
                   INTO (l_bukrs, l_ekgrp)
                  WHERE ebeln = ebeln.
  IF sy-subrc NE 0.
    RAISE invalid_document.
  ENDIF.
* JMJ002 > get the blocked item
      case wf_number.
*
         when c_ecart_prix.
*  price discrepancy blocking reason
           select * from ekbe into table lt_ekbe
                        where ebeln = ebeln
                          and ( vgabe = c_inv
                           or vgabe = c_park ).
           loop at lt_ekbe.
             select single spgrp from rseg into l_spgr
                           where belnr = lt_ekbe-belnr
                             and gjahr = lt_ekbe-gjahr
                             and buzei = lt_ekbe-buzei.
*
             if l_spgr = 'X'.
                move lt_ekbe-ebelp to l_ebelp.
                exit.
             endif.
*
           endloop.
*
         when c_ecart_qte.
*  qty discrepancy blocking reason
           select * from ekbe into table lt_ekbe
                        where ebeln = ebeln
                          and ( vgabe = c_inv
                           or vgabe = c_park ).
           loop at lt_ekbe.
             select single spgrm from rseg into l_spgr
                           where belnr = lt_ekbe-belnr
                             and gjahr = lt_ekbe-gjahr
                             and buzei = lt_ekbe-buzei.
*
             if l_spgr = 'X'.
                move lt_ekbe-ebelp to l_ebelp.
                exit.
             endif.
*
           endloop.
*
       when c_recep_mqt.
* missing GR blocking reason
           select * from ekpo into corresponding fields of table lt_ekpo
                             where ebeln = ebeln
                               and loekz eq space.
*
           loop at lt_ekpo.

             select * from ekbe into table lt_ekbe
                          where ebeln = lt_ekpo-ebeln
                            and ebelp = lt_ekpo-ebelp
                            and vgabe = c_gr.
*
              if lt_ekbe[] is initial.

                move lt_ekpo-ebelp to l_ebelp.
                exit.

              else.
*
               CALL FUNCTION 'ZBALANCE_QTE_EM_REST_A_FAC'
                 EXPORTING
                   x_ebeln                   = lt_ekpo-ebeln
                   x_ebelp                   = lt_ekpo-ebelp
*                  X_XBLNR                   =
                IMPORTING
                  X_QTE_EM_REST_A_FAC       =  l_qte
*                  X_QTE_FACT               =
                EXCEPTIONS
                  NO_EM_FOUND               = 1
                  OTHERS                    = 2
                         .
               IF sy-subrc <> 0.
*                 MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*                         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
               else.
                 if l_qte <= 0.
                    move lt_ekpo-ebelp to l_ebelp.
                    exit.
                 endif.
               ENDIF.
*
               endif.
*
           endloop.
*
     endcase.
* JMJ002 > direct access to the item
      SELECT single lgort werks afnam matkl pstyp FROM ekpo      "JMJ002 add pstyp/matkl/afnam
                     INTO (l_lgort, l_werks, l_afnam,
                           l_matkl, l_pstyp )
                    WHERE ebeln = ebeln
                      and ebelp = l_ebelp.
* JMJ002 > del
*    IF l_lgort EQ space.
*      MOVE l_tmp_lgort TO l_lgort.
*    ENDIF.
*    IF l_werks EQ space.
*      MOVE l_tmp_werks TO l_werks.
*    ENDIF.
*    IF l_matkl EQ space.
*      MOVE l_tmp_matkl TO l_matkl.
*    ENDIF.
*    IF l_lgort NE space AND l_werks NE space.
*      EXIT.
*    ENDIF.
*  ENDSELECT.
* JMJ002 < del
*access the specific table.
  CONCATENATE c_spc c_dft INTO l_data_to_read SEPARATED BY space.
*
  CASE wf_number.
    WHEN c_ecart_prix.
*access with BUKRS / EKGRP
      SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                              INTO CORRESPONDING FIELDS OF ls_tmp
                             WHERE wf_number = wf_number
                               AND bukrs = l_bukrs
                               AND werks = l_werks
                               AND ekgrp = l_ekgrp.
      IF sy-subrc NE 0.
      ELSE.
        CLEAR: l_data_to_read.
        IF ls_tmp-usnam EQ space.
          MOVE c_spc TO l_data_to_read.
        ENDIF.
        IF ls_tmp-dftname EQ space.
          CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
        ENDIF.
      ENDIF.
    WHEN OTHERS.
*access with BUKRS / WERKS / LGORT
      SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                              INTO CORRESPONDING FIELDS OF ls_tmp
                             WHERE wf_number = wf_number
                               AND bukrs = l_bukrs
                               AND werks = l_werks
                               AND lgort = l_lgort
                               and pstyp = space
                               and matkl = space.
*                               and ekgrp = space.
      IF sy-subrc NE 0.
      ELSE.
        CLEAR: l_data_to_read.
        IF ls_tmp-usnam EQ space.
          MOVE c_spc TO l_data_to_read.
        ENDIF.
        IF ls_tmp-dftname EQ space.
          CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
        ENDIF.
      ENDIF.
  ENDCASE.
* JMJ002 > add access by MATKL
  IF l_data_to_read NE space.
    CASE wf_number.
      WHEN c_ecart_qte or c_recep_mqt.
*access with BUKRS / WERKS / MATKL -> use a wildcard for MATKL access
       move l_matkl to l_tmp_matkl.
       do.
        SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                                INTO CORRESPONDING FIELDS OF ls_tmp2
                               WHERE wf_number = wf_number
                                 AND bukrs = l_bukrs
                                 AND werks = l_werks
                                 and matkl = l_tmp_matkl
                                 AND lgort = space
                                 and pstyp = space.

        IF sy-subrc NE 0.
          compute l_lg = strlen( l_tmp_matkl ).
          if l_lg = c_lg.
            subtract 1 from l_lg.
            move '*' to l_tmp_matkl+l_lg(1).
          else.
            translate l_tmp_matkl using '* '.
            compute l_lg = strlen( l_tmp_matkl ).
            subtract 1 from l_lg.
            if l_lg = 0.
              exit.
            else.
             move '*' to l_tmp_matkl+l_lg(1).
            endif.
          endif.
        ELSE.
          CLEAR: l_data_to_read.
          if ls_tmp-usnam eq space.
             move ls_tmp2-usnam to ls_tmp-usnam.
          endif.
          if ls_tmp-dftname eq space.
             move ls_tmp2-dftname to ls_tmp-dftname.
          endif.
          IF ls_tmp-usnam EQ space.
            MOVE c_spc TO l_data_to_read.
          ENDIF.
          IF ls_tmp-dftname EQ space.
            CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
          ENDIF.
        ENDIF.
        if l_data_to_read is initial.
          exit.
        else.
          compute l_lg = strlen( l_tmp_matkl ).
          if l_lg = c_lg.
            subtract 1 from l_lg.
            move '*' to l_tmp_matkl+l_lg(1).
          else.
            translate l_tmp_matkl using '* '.
            compute l_lg = strlen( l_tmp_matkl ).
            subtract 1 from l_lg.
            if l_lg = 0.
              exit.
            else.
             move '*' to l_tmp_matkl+l_lg(1).
            endif.
          endif.
        endif.
      enddo.
    ENDCASE.
  ENDIF.
* JMJ002 >> add access by Item category
  IF l_data_to_read NE space.
    CASE wf_number.
      WHEN c_ecart_qte or c_recep_mqt.
        SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                                INTO CORRESPONDING FIELDS OF ls_tmp
                               WHERE wf_number = wf_number
                                 AND bukrs = l_bukrs
                                 AND werks = l_werks
                                 and pstyp = l_pstyp
                                 AND lgort = space
                                 and matkl = space.
*                                 and ekgrp = space.

        IF sy-subrc NE 0.
        ELSE.
          CLEAR: l_data_to_read.
          IF ls_tmp-usnam EQ space.
            MOVE c_spc TO l_data_to_read.
          ENDIF.
          IF ls_tmp-dftname EQ space.
            CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
          ENDIF.
        ENDIF.
    ENDCASE.
  ENDIF.
*
  IF l_data_to_read NE space.
    CASE wf_number.
      WHEN c_ecart_prix.
*access with BUKRS / EKGRP
        SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                                INTO CORRESPONDING FIELDS OF ls_tmp
                               WHERE wf_number = wf_number
                                 AND bukrs = l_bukrs
*                                 AND werks = l_werks
                                 AND ekgrp = l_ekgrp
                                 AND werks = space.
        IF sy-subrc NE 0.
        ELSE.
          CLEAR: l_data_to_read.
          IF ls_tmp-usnam EQ space.
            MOVE c_spc TO l_data_to_read.
          ENDIF.
          IF ls_tmp-dftname EQ space.
            CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
          ENDIF.
        ENDIF.
      WHEN OTHERS.
*access with BUKRS / WERKS
        SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                                INTO CORRESPONDING FIELDS OF ls_tmp
                               WHERE wf_number = wf_number
                                 AND bukrs = l_bukrs
                                 AND werks = l_werks
                                 AND lgort = space
                                 and pstyp = space
                                 and matkl = space.
*                                 and ekgrp = space.

        IF sy-subrc NE 0.
        ELSE.
          CLEAR: l_data_to_read.
          IF ls_tmp-usnam EQ space.
            MOVE c_spc TO l_data_to_read.
          ENDIF.
          IF ls_tmp-dftname EQ space.
            CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
          ENDIF.
        ENDIF.
    ENDCASE.
  ENDIF.
* Price discrepancy > access by EKGRP
  IF l_data_to_read NE space.
    CASE wf_number.
      WHEN c_ecart_prix.
*access with BUKRS / WERKS
        SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                                INTO CORRESPONDING FIELDS OF ls_tmp
                               WHERE wf_number = wf_number
                                 AND bukrs = l_bukrs
                                 AND ekgrp = space
                                 AND werks = l_werks.
        IF sy-subrc NE 0.
        ELSE.
          CLEAR: l_data_to_read.
          IF ls_tmp-usnam EQ space.
            MOVE c_spc TO l_data_to_read.
          ENDIF.
          IF ls_tmp-dftname EQ space.
            CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
          ENDIF.
        ENDIF.
       when others.
     endcase.
   endif.
*
  IF l_data_to_read NE space.
*
    SELECT SINGLE (l_data_to_read) FROM zbal_wfrole
                            INTO CORRESPONDING FIELDS OF ls_tmp
                           WHERE wf_number = wf_number
                             AND bukrs = l_bukrs
                             AND werks = space
                             AND lgort = space
                             and pstyp = space
                             and matkl = space.
*                             and ekgrp = space.
      IF sy-subrc NE 0.
      ELSE.
        CLEAR: l_data_to_read.
        IF ls_tmp-usnam EQ space.
          MOVE c_spc TO l_data_to_read.
        ENDIF.
        IF ls_tmp-dftname EQ space.
          CONCATENATE l_data_to_read c_dft INTO l_data_to_read SEPARATED BY space.
        ENDIF.
      ENDIF.
*
  ENDIF.
* JMJ002 > Add control for AFNAM -> Portugal treatment
  if l_data_to_read ne space.
    CASE wf_number.
      WHEN c_recep_mqt.
         if l_afnam ne space.
         move l_afnam to l_spec_usr.
          CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
            EXPORTING
              usnam           = l_spec_usr
           EXCEPTIONS
             NOT_EXIST       = 1
             LOCKED          = 2
             NOT_VALID       = 3
             OTHERS          = 4.

          IF sy-subrc <> 0.
          else.
            move l_spec_usr to ls_tmp-usnam.
          ENDIF.
         endif.
      when others.
    endcase.
  endif.

*transfer the values found in export parameter
  MOVE ls_tmp-usnam TO spec_rule_user.
  MOVE ls_tmp-dftname TO default_user.
*
ENDFUNCTION.
