FUNCTION z_balance_role_default.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(I_EBELN) TYPE  EBELN OPTIONAL
*"     REFERENCE(I_EBELP) TYPE  EBELP OPTIONAL
*"     REFERENCE(I_BUKRS) TYPE  BUKRS
*"     REFERENCE(I_WERKS) TYPE  WERKS_D OPTIONAL
*"     REFERENCE(I_TYPE) TYPE  CHAR_02 DEFAULT 'MM'
*"  TABLES
*"      T_ACTOR STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOT_FOUND
*"----------------------------------------------------------------------
*{   INSERT         MS4K910456                                        1
* ----------------- * ------------------------------------------------ *
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Default Role determination                       *
* ----------------- * ------------------------------------------------ *
*>> START RGI002  - Adding organisation local variable
  DATA : w_werks TYPE werks_d,
         w_bukrs TYPE bukrs.
  DATA : l_custom_data LIKE swd_custom.
  DATA : w_usnam TYPE usnam.
*>> END   RGI002  - Adding organisation local variable

  CASE i_type.
    WHEN 'MM'.
      IF NOT i_bukrs IS INITIAL.
        MOVE i_bukrs TO w_bukrs.
      ELSE.
        SELECT SINGLE bukrs INTO w_bukrs FROM ekko WHERE ebeln = i_ebeln.
      ENDIF.
*>> Getting plant organizational data if empty
      IF i_werks IS INITIAL.
        IF i_ebelp IS INITIAL.
          SELECT SINGLE werks INTO w_werks FROM ekpo WHERE ebeln = i_ebeln.
        ELSE.
          SELECT SINGLE werks INTO w_werks FROM ekpo WHERE ebeln = i_ebeln
                                                       AND ebelp = i_ebelp.
        ENDIF.
      ENDIF.
*>> Accessing exception logistic table
      SELECT SINGLE usnam INTO t_actor-objid FROM zbal_wflogistics
                          WHERE bukrs = w_bukrs
                            AND werks = w_werks.
      IF sy-subrc = 0.
        MOVE t_actor-objid TO w_usnam.
        CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
          EXPORTING
            usnam     = w_usnam
          EXCEPTIONS
            not_exist = 1
            locked    = 2
            not_valid = 3
            OTHERS    = 4.
        IF sy-subrc EQ 0.
          t_actor-otype = 'US'.
          APPEND t_actor.
        ELSE.
          CALL FUNCTION 'SWD_GET_CUSTOMIZING'
            IMPORTING
              custom_data = l_custom_data.
* default administrator was found in customizing
          IF NOT l_custom_data-def_admin IS INITIAL.
            t_actor      = l_custom_data-def_admin.
            APPEND t_actor.
          ENDIF.
        ENDIF.
      ELSE.
        SELECT SINGLE usnam INTO t_actor-objid FROM zbal_wflogistics
                            WHERE bukrs = w_bukrs.
        IF sy-subrc = 0.
          MOVE t_actor-objid TO w_usnam.
          CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
            EXPORTING
              usnam     = w_usnam
            EXCEPTIONS
              not_exist = 1
              locked    = 2
              not_valid = 3
              OTHERS    = 4.
          IF sy-subrc EQ 0.
            t_actor-otype = 'US'.
            APPEND t_actor.
          ELSE.
            CALL FUNCTION 'SWD_GET_CUSTOMIZING'
              IMPORTING
                custom_data = l_custom_data.
* default administrator was found in customizing
            IF NOT l_custom_data-def_admin IS INITIAL.
              t_actor      = l_custom_data-def_admin.
              APPEND t_actor.
            ENDIF.
          ENDIF.
        ELSE.
*>>      Now we get Administator of System from WF point of view
*        get customizing information
          CALL FUNCTION 'SWD_GET_CUSTOMIZING'
            IMPORTING
              custom_data = l_custom_data.
* default administrator was found in customizing
          IF NOT l_custom_data-def_admin IS INITIAL.
            t_actor      = l_custom_data-def_admin.
            APPEND t_actor.
          ENDIF.
        ENDIF.
      ENDIF.
    WHEN 'FI'.
*>> EVEN IF THIS FUNCTION IF FOR LOGISTIC MAYBE IT COULD CHANGE (FI)
  ENDCASE.
  CLEAR t_actor.
  IF t_actor[] IS INITIAL.
    RAISE not_found.
  ENDIF.
*}   INSERT





ENDFUNCTION.
