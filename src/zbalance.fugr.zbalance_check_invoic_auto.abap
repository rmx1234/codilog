************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Permet de contrôler que les factures sont intégrées *
*                  en automatique 						  *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Gaëlle TRIMOREAU (Netinside)                                *
*                                                                      *
* Date   : 26/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport: DE2K900720                                       *
*									         *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_check_invoic_auto.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(OBJTYPE) LIKE  SWETYPECOU-OBJTYPE
*"     REFERENCE(OBJKEY) LIKE  SWEINSTCOU-OBJKEY
*"     REFERENCE(EVENT) LIKE  SWEINSTCOU-EVENT
*"     REFERENCE(RECTYPE) LIKE  SWETYPECOU-RECTYPE
*"  EXPORTING
*"     REFERENCE(EVENT_CONTAINER) LIKE  SWCONT STRUCTURE  SWCONT
*"  EXCEPTIONS
*"      NO_IDOC_LINKED
*"----------------------------------------------------------------------
************************************************************************
*                   DECLARATION DE DONNEES                             *
************************************************************************
*  TABLES: srrelroles, idocrel, vbkpf.

***************************** Données **********************************
  DATA: w_role_a LIKE idocrel-role_a,
        w_objkey LIKE srrelroles-objkey,
        w_objkey2 LIKE srrelroles-objkey,
        w_bukrs  LIKE vbkpf-bukrs,
        w_gjahr  LIKE vbkpf-gjahr.

***************************** Constantes *******************************
* Constante pour trouver l'id associé à la facture
  CONSTANTS: k_inbeleg LIKE srrelroles-roletype VALUE 'INBELEG',
* Constante pour trouver le n° de l'idoc
             k_inidoc  LIKE srrelroles-roletype VALUE 'INIDOC',
             k_objtype LIKE srrelroles-objtype  VALUE 'IDOC',
             k_bkpf    LIKE srrelroles-objtype VALUE 'BKPF',
             k_bus2081 LIKE srrelroles-objtype VALUE 'BUS2081'.

*----- global message variables
  DATA:  l_msgty          LIKE sy-msgty,
         l_msgid          LIKE sy-msgid,
         l_msgno          LIKE sy-msgno,
         l_msgv1          LIKE sy-msgv1,
         l_msgv2          LIKE sy-msgv2,
         l_msgv3          LIKE sy-msgv3,
         l_msgv4          LIKE sy-msgv4.

*  INCLUDE <cntain>.

************************************************************************
*                             TRAITEMENT                               *
************************************************************************

  CLEAR: idocrel, srrelroles.

* Recherche de la valeur de l'id de l'idoc.
  SELECT  b~role_a INTO w_role_a "ex SELECT SINGLE
                FROM srrelroles AS a INNER JOIN idocrel AS b
                                     ON a~roleid = b~role_b
                WHERE a~objtype  = k_bkpf
                  AND a~objkey   = objkey
                  AND a~roletype = k_inbeleg.
  ENDSELECT.
  IF sy-subrc = 0.
* Recherche du n° d'idoc à pertir de l'id trouvé précédemment
    CLEAR srrelroles.
    SELECT  objkey INTO w_objkey2 "ex SELECT SINGLE
                         FROM srrelroles
                         WHERE objtype = k_objtype
                           AND roletype = k_inidoc
                           AND roleid = w_role_a.
  ENDSELECT.
  IF sy-subrc = 0.
  ELSE.
* Il n'y a pas d'idoc associé
    RAISE no_idoc_linked.
  ENDIF.
ELSE.
* Il n'y a pas d'idoc associé
  RAISE no_idoc_linked.
ENDIF.

ENDFUNCTION.
