FUNCTION zbalance_invoic_preenr_set.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  CHANGING
*"     REFERENCE(E_CHANGE) TYPE  C
*"     REFERENCE(E_RBKPV) TYPE  MRM_RBKPV
*"----------------------------------------------------------------------


  DATA: wl_start     TYPE flag.

  IMPORT start = wl_start FROM MEMORY ID wc_memory_id_02.

  CHECK wl_start EQ 'X'.

  e_rbkpv-rbstat = 'A'.
  e_change = 'X'.

ENDFUNCTION.
