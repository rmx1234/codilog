FUNCTION zbalance_user_exit_mm_015.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  TABLES
*"      T_FRSEG TYPE  MMCR_TFRSEG
*"      T_CO TYPE  MMCR_TCOBL_MRM
*"      T_MA TYPE  MMCR_TMA
*"  CHANGING
*"     VALUE(E_CHANGE) TYPE  C
*"     VALUE(E_RBKPV) TYPE  MRM_RBKPV
*"----------------------------------------------------------------------
  DATA : BEGIN OF w_coutadd OCCURS 0,
           bukrs LIKE rbkp-bukrs,
           hkont LIKE zbal_cadd-hkont,
           shkzg(1) TYPE c,
           betrg LIKE e1edk05-betrg,
           kobtr LIKE e1edk05-kobtr,
           mwskz LIKE e1edk05-mwskz,
           msatz LIKE e1edk05-msatz,
           kostl LIKE zbal_cadd-kostl,
           pspnr LIKE zbal_cadd-pspnr,
           aufnr LIKE zbal_cadd-aufnr,
         END OF w_coutadd.

  DATA : w_montant TYPE p DECIMALS 4.
  DATA : w_taux TYPE p DECIMALS 4.
  DATA : w_tva TYPE p DECIMALS 4.
  DATA : w_tabix LIKE sy-tabix.
  DATA : st_rbtx LIKE rbtx.

* Coûts additionnels

  REFRESH w_coutadd. CLEAR w_coutadd.
  IMPORT w_coutadd FROM MEMORY ID 'COUTADD'.

  LOOP AT w_coutadd.        .

    t_co-bukrs = w_coutadd-bukrs.
    t_co-saknr = w_coutadd-hkont.
    t_co-wrbtr = w_coutadd-betrg.
    t_co-aufnr = w_coutadd-aufnr.
    t_co-kostl = w_coutadd-kostl.
    t_co-ps_psp_pnr = w_coutadd-pspnr.
    t_co-mwskz = w_coutadd-mwskz.
    t_co-shkzg = w_coutadd-shkzg.
    APPEND t_co.

    w_montant = w_coutadd-betrg.
    w_taux = w_coutadd-msatz.
    IF NOT w_taux IS INITIAL.
      w_tva = w_montant * w_taux / 100.
    ELSE.
      w_tva = w_coutadd-kobtr - w_coutadd-betrg.
    ENDIF.
*   Code à mettre en commentaire pour SFD
*    e_rbkpv-rmwwr = e_rbkpv-rmwwr + w_montant + w_tva.
*
*    READ TABLE e_rbkpv-rbtx WITH KEY mwskz = w_coutadd-mwskz
*    INTO st_rbtx.
*    IF sy-subrc = 0.
*      st_rbtx-wmwst = st_rbtx-wmwst + w_tva.
*      MODIFY e_rbkpv-rbtx FROM st_rbtx INDEX sy-tabix.
*    ELSE.
*      st_rbtx-mwskz = w_coutadd-mwskz.
*      st_rbtx-wmwst = w_tva.
*      APPEND st_rbtx TO e_rbkpv-rbtx.
*    ENDIF.
*   Fin code à mettre en commentaire
  ENDLOOP.

  EXPORT w_coutadd TO MEMORY ID 'COUTADD'.

  e_change = 'X'.

ENDFUNCTION.
