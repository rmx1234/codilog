FUNCTION zbalance_auth_list.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(E_UCOMM) TYPE  SY-UCOMM
*"     REFERENCE(TYPFACT) TYPE  EDIDC-MESCOD
*"     REFERENCE(STATUS) TYPE  EDIDC-STATUS
*"  EXPORTING
*"     REFERENCE(SCREENTYPE)
*"     REFERENCE(PROFIL)
*"     REFERENCE(W_SUBRC)
*"----------------------------------------------------------------------

*Affichage de facture
  IF e_ucomm = 'DISP'.
    IF typfact = 'MM'.
      AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
               ID 'ACTVT' FIELD '02'
               ID 'ZBALPROF' FIELD 'ADM'.
      IF sy-subrc = 0.
        screentype = 1. "affichage écran rapprochement MM.
        profil = 'ADM'. "pas de modification par l'administrateur
      ELSE.
        AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                 ID 'ACTVT' FIELD '02'
                 ID 'ZBALPROF' FIELD 'GEST'.
        IF sy-subrc = 0.
          screentype = 1. "affichage écran rapprochement MM
          profil = 'GEST'. "par un gestionnaire
        ELSE.

          AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                   ID 'ACTVT' FIELD 'O2'
                   ID 'ZBALPROF' FIELD 'COMP'.
          IF sy-subrc = 0.
            screentype = 1. "affichage écran de
            "rapprochement MM
            profil = 'COMP'. "par un comptable
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.

    IF typfact = 'FI'.
      AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                  ID 'ACTVT' FIELD '02'
                  ID 'ZBALPROF' FIELD 'ADM'.
      IF sy-subrc = 0.
        screentype = 2. "affichage écran rapprochement FI.
        profil = 'ADM'. "pas de modification par l'administrateur
      ELSE.
        AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                 ID 'ACTVT' FIELD '02'
                 ID 'ZBALPROF' FIELD 'GEST'.
        IF sy-subrc = 0.
          screentype = 2. "affichage écran rapprochement FI
          profil = 'GEST'. "par un gestionnaire
        ELSE.

          AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                   ID 'ACTVT' FIELD 'O2'
                   ID 'ZBALPROF' FIELD 'COMP'.
          IF sy-subrc = 0.
            screentype = 2. "affichage écran de
            "rapprochement FI
            profil = 'COMP'. "par un comptable
          ENDIF.
        ENDIF.

      ENDIF.
    ENDIF.
  ENDIF.

*Modification de facture
  IF e_ucomm = 'UPDT'.
    IF typfact = 'MM'.
      AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
               ID 'ACTVT' FIELD '02'
               ID 'ZBALPROF' FIELD 'ADM'.
      IF sy-subrc = 0.
        PERFORM message_box USING text-m04 text-m07 'E'.
        w_subrc = 4.
      ELSE.
        AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                 ID 'ACTVT' FIELD '02'
                 ID 'ZBALPROF' FIELD 'GEST'.
        IF sy-subrc = 0.
          screentype = 3. "modification écran rapprochement MM
          profil = 'GEST'. "par un gestionnaire
        ELSE.

          AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                   ID 'ACTVT' FIELD '02'
                   ID 'ZBALPROF' FIELD 'COMP'.
          IF sy-subrc = 0.

*          Modifié par SDS le 19/05/2004

*            screentype = 3. "modification écran de
*            "rapprochement MM
*            profil = 'COMP'. "par un comptable

            PERFORM message_box USING text-m04 text-m07 'E'.
            w_subrc = 4.
*          fin modif

          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
    IF typfact = 'FI'.
      IF status = 51.
        AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
             ID 'ACTVT' FIELD '02'
             ID 'ZBALPROF' FIELD 'ADM'.
        IF sy-subrc = 0.
          PERFORM message_box USING text-m04 text-m07 'E'.
          w_subrc = 4.
        ELSE.
          AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                   ID 'ACTVT' FIELD '02'
                   ID 'ZBALPROF' FIELD 'GEST'.
          IF sy-subrc = 0.
            PERFORM message_box USING text-m04 text-m07 'E'.
            w_subrc = 4.
          ELSE.

            AUTHORITY-CHECK OBJECT 'ZBAL_PROF'
                     ID 'ACTVT' FIELD '02'
                     ID 'ZBALPROF' FIELD 'COMP'.
            IF sy-subrc = 0.
              screentype = 4. "modification écran de
              "rapprochement FI
              profil = 'COMP'. "par un comptable
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
  ENDIF.
ENDFUNCTION.
