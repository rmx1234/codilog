FUNCTION zbalance_predoc_disp_from_idoc.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(DOCNUM) LIKE  EDIDC-DOCNUM
*"  EXCEPTIONS
*"      DOC_NOT_FOUND
*"      OTHERS
*"----------------------------------------------------------------------
* Author            : Reynald GIL                               RGI001 *
* Date              : 12.02.2008                                       *
* Document  DIA     : FI_393_DIA                                       *
* Description       : addition MM document Type
* ----------------- * ------------------------------------------------ *

  DATA : w_gjahr LIKE bkpf-gjahr.

  SELECT SINGLE docnum INTO edidc-docnum FROM edidc
    WHERE docnum = docnum.


  IF sy-subrc NE 0.
    RAISE doc_not_found.
  ELSE.

    CALL FUNCTION 'ZBALANCE_INVOIC_MM_DETAILS'
      EXPORTING
        i_docnum = docnum
      IMPORTING
        e_header = zbalance_04.

    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_READ'
      EXPORTING
        document_number = docnum.


    CALL FUNCTION 'EDI_DOCUMENT_READ_LAST_STATUS'
      EXPORTING
        document_number        = docnum
      IMPORTING
        status                 = int_edids
      EXCEPTIONS
        document_not_open      = 1
        no_status_record_found = 2
        OTHERS                 = 3.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open'(w15).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'No status record found'(w16).
      ENDIF.
    ENDIF.


    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_READ'
      EXPORTING
        document_number   = docnum
      EXCEPTIONS
        document_not_open = 1
        parameter_error   = 2
        OTHERS            = 3.


    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open for reading'(w01).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'Parameter combination is invalid'(w02).
      ENDIF.

    ENDIF.

*    SELECT * FROM  bkpf
*           WHERE  belnr  = int_edids-stapa1
*           ORDER BY gjahr.
*    ENDSELECT.
    DATA number(10) TYPE n.

    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = int_edids-stapa1+0(10)
      IMPORTING
        output = number.
    .

    w_gjahr = int_edids-credat(4).
    ADD 1 TO w_gjahr.

    SELECT SINGLE belnr gjahr bukrs
    INTO (bkpf-belnr, bkpf-gjahr, bkpf-bukrs)
    FROM  bkpf CLIENT SPECIFIED
    WHERE  mandt  = sy-mandt
    AND    bukrs  = zbalance_04-bukrs
    AND    belnr  = number "int_edids-stapa1
    AND    gjahr  = w_gjahr.
    IF sy-subrc NE 0.
      w_gjahr = w_gjahr - 1.
      SELECT SINGLE belnr gjahr bukrs
      INTO (bkpf-belnr, bkpf-gjahr, bkpf-bukrs)
      FROM  bkpf CLIENT SPECIFIED
      WHERE  mandt  = sy-mandt
      AND    bukrs  = zbalance_04-bukrs
      AND    belnr  = number "int_edids-stapa1
      AND    gjahr  = w_gjahr.
    ENDIF.



    CALL FUNCTION 'ZBALANCE_PREDOCFAC_DISPLAY'
      EXPORTING
        belnr         = bkpf-belnr
        gjahr         = bkpf-gjahr
        bukrs         = bkpf-bukrs
      EXCEPTIONS
        doc_not_found = 1
        OTHERS        = 2.

    IF sy-subrc <> 0.
      CASE sy-subrc.
        WHEN '1'.
          RAISE doc_not_found.
        WHEN '2'.
          RAISE others.
      ENDCASE.
    ENDIF.

  ENDIF.

ENDFUNCTION.
