FUNCTION z_balance_role_acheteur_second.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(X_EBELN) TYPE  EKKO-EBELN
*"  TABLES
*"      W_ACTOR STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOBODY_FOUND
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
* Bug Correction RGI001
*----------------------------------------------------------------------*
* Author            : Reynald GIL                                RGI001*
* Date              : 30.01.2006                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Role determination *
* ----------------- * ------------------------------------------------ *
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Default Role determination                       *
* ----------------- * ------------------------------------------------ *
* Author            : Reynald GIL                                RGI003*
* Date              : 13.06.2007                                       *
* Document  DIA     : FI_296_DIA                                       *
* Description       : PO Request user exception                        *
* ----------------- * ------------------------------------------------ *
* Author            : JM.JOURON                                JMJ001  *
* Date              : 22.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : New Role determination                           *
* ----------------- * ------------------------------------------------ *

  DATA w_ernam TYPE ernam.
  DATA p_requis TYPE eket-banfn.
  DATA p_requis2 TYPE eban-banfn.
  DATA w_subrc LIKE sy-subrc.
  DATA: x_bukrs LIKE ekko-bukrs,
        x_werks LIKE ekpo-werks.
*>> START RGI002  - Adding organisation local variable
*>> END   RGI002  - Adding organisation local variable

  REFRESH w_actor.
  w_subrc = 0.
*>> DEL JMJ001 - done in a previous step
*>> ADD RGI003 - 13/06/2007
*  DATA : w_werks TYPE werks_d,
*         w_bukrs TYPE bukrs.
*
*  SELECT SINGLE bukrs INTO x_bukrs FROM ekko WHERE ebeln = x_ebeln.
*  IF sy-subrc = 0.
*    SELECT SINGLE werks INTO x_werks FROM ekpo WHERE ebeln = x_ebeln.
*    IF sy-subrc = 0.
*      SELECT SINGLE usnam INTO w_ernam FROM zbal_wfrole
*                     WHERE bukrs = x_bukrs
*                       AND werks = x_werks.
*      IF sy-subrc = 0.
*        CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
*          EXPORTING
*            usnam     = w_ernam
*          EXCEPTIONS
*            not_exist = 1
*            locked    = 2
*            not_valid = 3
*            OTHERS    = 4.
*        IF sy-subrc EQ 0.
*          w_actor-otype = 'US'.
*          w_actor-objid = w_ernam.
*          APPEND w_actor.
*        ELSE.
*          w_subrc = 4.     "User not active
*        ENDIF.
*      ELSE.
*        w_subrc = 4.     "User not active
*      ENDIF.
*    ENDIF.
*  ELSE.
*    w_subrc = 4.     "User not active
*  ENDIF.
*
*  CHECK w_subrc = 4.
*>> FIN RGI003
*<< DEL JMJ001 - done in a previous step
  SELECT SINGLE banfn INTO p_requis FROM eket WHERE ebeln = x_ebeln
*>> START RGI001
  AND banfn NE space.
*>> END   RGI001
  IF sy-subrc = 0.
    MOVE p_requis TO p_requis2.

    SELECT SINGLE ernam INTO w_ernam FROM eban
    WHERE banfn = p_requis2.
    IF sy-subrc = 0.

      CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
        EXPORTING
          usnam     = w_ernam
        EXCEPTIONS
          not_exist = 1
          locked    = 2
          not_valid = 3
          OTHERS    = 4.
      IF sy-subrc EQ 0.
        w_actor-otype = 'US'.
        w_actor-objid = w_ernam.
        APPEND w_actor.
        w_subrc = 0. "User active
      ELSE.
        w_subrc = 4.     "User not active
      ENDIF.
    ELSE.
      w_subrc = 4.       "Select from EBAN failed
    ENDIF.

  ELSE.
    SELECT SINGLE ernam INTO w_ernam FROM ekko WHERE ebeln = x_ebeln.
    IF sy-subrc = 0.
      CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
        EXPORTING
          usnam     = w_ernam
        EXCEPTIONS
          not_exist = 1
          locked    = 2
          not_valid = 3
          OTHERS    = 4.
      IF sy-subrc EQ 0.
        w_actor-otype = 'US'.
        w_actor-objid = w_ernam.
        APPEND w_actor.
        w_subrc = 0. "User active
      ELSE.
        w_subrc = 4.     "User not active
      ENDIF.
    ELSE.
      w_subrc = 4.      "Select from EKKO failed
    ENDIF.
  ENDIF.


*JMJ001 - 19/11/2007 - default treatment done in calling program - start deletion
  if w_subrc ne 0.
     raise nobody_found.
  endif.
**>> START RGI002
**>> Now we kill Miss BONAVENTURE to replace her by a table ZBAL_WFLOGISTIC
*  IF w_subrc = 4.
**>> Getting company code
*    SELECT SINGLE bukrs INTO w_bukrs FROM ekko WHERE ebeln = x_ebeln.
**>> Getting plant of first item
*    SELECT SINGLE werks INTO w_werks FROM ekpo WHERE ebeln = x_ebeln.
**>> Calling New FM for default user role.
*    CALL FUNCTION 'Z_BALANCE_ROLE_DEFAULT'
*      EXPORTING
*        i_ebeln   = x_ebeln
*        i_bukrs   = w_bukrs
*        i_werks   = w_werks
*        i_type    = 'MM'
*      TABLES
*        t_actor   = w_actor
*      EXCEPTIONS
*        not_found = 1.
*
*  ENDIF.
*JMJ001 - 19/11/2007 - default treatment done in calling program - end deletion

  CLEAR w_actor.
*>> END   RGI002

ENDFUNCTION.
