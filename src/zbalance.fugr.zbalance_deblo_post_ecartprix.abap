FUNCTION zbalance_deblo_post_ecartprix.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(I_BELNR) LIKE  RBKP-BELNR
*"     REFERENCE(I_GJAHR) LIKE  RBKP-GJAHR
*"  TABLES
*"      TI_SELECTED_INVOICE_ITEMS STRUCTURE  RSEG_BUZEI
*"----------------------------------------------------------------------
************************************************************************
* Identification : ZBALANCE_DEBLO_POST_ECARTPRIX
*
*                                                                      *
*                                                                      *
*  Description de la fonction :                                        *
*  Cette fonction permet de débloquer les postes d'un facture bloquée  *
*  pour un écart prix.                                                 *
*  En entrée: I_BELNR : Numéro de document de facturation              *
*             I_GJAHR : Exercice comptable                             *
*             TI_SELECTED_INVOICE_ITEMS: listes des postes à débloquer *
*----------------------------------------------------------------------*
*                                                                      *
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur :  Frédérick HUYNH   (Netinside)                              *
*                                                                      *
* Date : 22/01/2004                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport:                                                  *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

  LOOP AT ti_selected_invoice_items.

    SELECT * FROM rseg WHERE  belnr = i_belnr AND
                         gjahr = i_gjahr AND
                         buzei = ti_selected_invoice_items-buzei.
      rseg-spgrp = space.
      UPDATE rseg.

    ENDSELECT.


  ENDLOOP.



ENDFUNCTION.
