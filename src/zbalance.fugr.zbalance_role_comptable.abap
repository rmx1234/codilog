* T. NGUYEN (Appia Consulting) - 08/08/2006

FUNCTION zbalance_role_comptable .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  TABLES
*"      AC_CONTAINER STRUCTURE  SWCONT
*"      ACTOR_TAB STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOBODY_FOUND
*"----------------------------------------------------------------------
  INCLUDE <cntain>.

  REFRESH actor_tab.

  DATA w_actor TYPE swhactor.
  DATA wt_wfcomptable TYPE TABLE OF zbal_wfcomptable.
  DATA w_wfcomptable TYPE zbal_wfcomptable.

*\--> itvla20070110+
  DATA: w_ebeln TYPE ekko-ebeln,
        l_bukrs TYPE ekko-bukrs.

  break gilr.
  swc_get_element ac_container 'NoCommande' w_ebeln.

  IF w_ebeln IS INITIAL.
    RAISE nobody_found.
  ELSE.

    SELECT SINGLE bukrs FROM  ekko INTO l_bukrs
           WHERE  ebeln  = w_ebeln.
    IF sy-subrc NE 0.
      RAISE nobody_found.
    ENDIF.
*\--> itvla20070110+

    w_actor-otype = 'US'.

    SELECT * FROM zbal_wfcomptable INTO TABLE wt_wfcomptable
        WHERE bukrs = l_bukrs.                    "itvla20070110+

    LOOP AT wt_wfcomptable INTO w_wfcomptable.

      w_actor-objid = w_wfcomptable-usnam.
      APPEND w_actor TO actor_tab.

    ENDLOOP.

  ENDIF.                          "itvla20070110+


ENDFUNCTION.
