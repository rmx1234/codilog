************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Saisie des éléments manquants sur la facture        *
*                  pré-enregistrée FI                                  *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Gaëlle TRIMOREAU (Netinside)                                *
*                                                                      *
* Date   : 13/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport: DE2K900720                                       *
*									         *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_search_data_invoic.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(I_BUKRS) LIKE  BKPF-BUKRS
*"     REFERENCE(I_BELNR) LIKE  BKPF-BELNR
*"     REFERENCE(I_GJAHR) LIKE  BKPF-GJAHR
*"     REFERENCE(I_XBLNR) LIKE  BKPF-XBLNR
*"     REFERENCE(I_BLART) LIKE  BKPF-BLART
*"  EXPORTING
*"     REFERENCE(E_STATUT) LIKE  SY-INDEX
*"     REFERENCE(E_BUKRS) LIKE  BKPF-BUKRS
*"     REFERENCE(E_BLART) LIKE  BKPF-BLART
*"     REFERENCE(E_XBLNR) LIKE  BKPF-XBLNR
*"     REFERENCE(E_MONTTOT) LIKE  BSEG-WRBTR
*"  TABLES
*"      T_SEG STRUCTURE  ZSBALANCE_SEG
*"----------------------------------------------------------------------
************************************************************************
*                   DECLARATION DE DONNEES                             *
************************************************************************
***************************** Tables ***********************************

***************************** Données **********************************
* Statut de validation
  DATA:  w_status  LIKE sy-index,
* Société
         w_bukrs   LIKE bkpf-bukrs,
         w_xblnr   LIKE bkpf-xblnr,
         w_blart   LIKE bkpf-blart,
         w_monttot LIKE bseg-wrbtr.
***************************** Constantes *******************************

***************************** Tables internes **************************
  DATA: i_seg TYPE zsbalance_seg OCCURS 0 WITH HEADER LINE.

***************************** Structures *******************************

************************************************************************
*                             TRAITEMENT                               *
************************************************************************

* Sauvegarde des données
  SET PARAMETER ID 'ZBALANCE_PIECE'       FIELD i_belnr.
  SET PARAMETER ID 'ZBALANCE_SOCIETE'     FIELD i_bukrs.
  SET PARAMETER ID 'ZBALANCE_EXCPTABLE'   FIELD i_gjahr.
  SET PARAMETER ID 'ZBALANCE_REF'         FIELD i_xblnr.
  SET PARAMETER ID 'ZBALANCE_TYPEPCE'     FIELD i_blart.

  EXPORT i_seg FROM t_seg TO MEMORY ID 'TSEG'.

* Appel du programme permettant la saisie des données manquantes
  SUBMIT zbalancerfacturefi01 AND RETURN.

* Récupération des données
  GET PARAMETER ID 'ZBALANCE_SOCIETE'   FIELD w_bukrs.
  GET PARAMETER ID 'ZBALANCE_REF'       FIELD w_xblnr.
  GET PARAMETER ID 'ZBALANCE_TYPEPCE'   FIELD w_blart.

* T. NGUYEN 26072006
* Compatibilité UNICODE: champ CURR non authorisé
*
* GET PARAMETER ID 'ZBALANCE_TOTAL'     FIELD w_monttot.
  IMPORT wrtb = w_monttot FROM MEMORY ID 'ZBALANCE_TOTAL'.
* T. NGUYEN 26072006

  IMPORT i_seg = i_seg FROM MEMORY ID 'TSEG'.
  IMPORT w_status FROM MEMORY ID 'STATUS'.

* Gestion des paramètres d'export
  t_seg[] = i_seg[].
  MOVE w_status  TO e_statut.
  MOVE w_bukrs   TO e_bukrs.
  MOVE w_xblnr   TO e_xblnr.
  MOVE w_blart   TO e_blart.
  MOVE w_monttot TO e_monttot.

ENDFUNCTION.
