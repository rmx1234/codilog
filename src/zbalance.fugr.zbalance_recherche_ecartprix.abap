FUNCTION zbalance_recherche_ecartprix.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(I_BELNR) LIKE  RSEG-BELNR
*"     REFERENCE(I_GJAHR) LIKE  RSEG-GJAHR
*"     REFERENCE(I_BUZEI) LIKE  RSEG-BUZEI
*"  EXPORTING
*"     REFERENCE(E_EBELN) TYPE  RSEG-EBELN
*"     REFERENCE(E_EBELP) TYPE  RSEG-EBELP
*"     REFERENCE(E_NETPR) TYPE  EKPO-NETPR
*"     REFERENCE(E_NETPR2) TYPE  CHAR15
*"     REFERENCE(E_DWERT) TYPE  ARSEG-DWERT
*"     REFERENCE(E_WAERS) TYPE  EKKO-WAERS
*"  EXCEPTIONS
*"      NO_FOUND
*"----------------------------------------------------------------------
************************************************************************
* Identification : ZBALANCE_RECHERCHE_ECARTPRIX 		         *
*                                                                      *
*                                                                      *
*  Description de la fonction :                                        *
*  Cette fonction permet de rechercher l'écart de prix entre le poste  *
*  facture et le poste commande                                        *
*  En entrée: I_BELNR : Numéro de document de facturation              *
*             I_GJAHR : Exercice comptable                             *
*             I_BUZEI:  Poste du document de facturation               *
*  En sortie: E_EBELN : Numéro de document d'achat                     *
*             E_EBELP : Poste du document                              *
*             E_NETPR:  Prix net du document d'achat                   *
*             E_NETPR2: Prix net du document de facturation            *
*             E_DWERT:  Ecart de prix                                  *
*             E_WAERS:  Devise                                         *
*                                                                      *
**----------------------------------------------------------------------
*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur :  Frédérick HUYNH   (Netinside)                              *
*                                                                      *
* Date : 22/01/2004                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport:                                                  *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*  211107  ! JOURON JMarc !   JMJ211107 Calculate the unit price for   *
*          !              !   the invoice and do the conversion to the *
*          !              !   unit of the purchase order               *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

* ADD - JMB - BAL-20-021
  DATA : wt_rseg TYPE TABLE OF rseg.
  DATA : st_rseg TYPE rseg.
*>> Add JMJ211107
  data: e_tmp_netpr type rseg-wrbtr.
*<< Add JMJ211107
*>> Add RGI
  DATA e_menge LIKE ekpo-menge.
  DATA e_bpumn LIKE ekpo-bpumn.
  DATA e_bpumz LIKE ekpo-bpumz.
  DATA e_peinh LIKE ekpo-peinh.

  SELECT SINGLE ebeln ebelp FROM rseg
           INTO (e_ebeln, e_ebelp)
          WHERE belnr = i_belnr
            AND gjahr = i_gjahr
            AND buzei = i_buzei.

  SELECT * FROM  rseg
  INTO TABLE wt_rseg
         WHERE  ebeln  = e_ebeln
         AND    ebelp  = e_ebelp
         AND    belnr = i_belnr
         AND    gjahr = i_gjahr.
  clear e_tmp_netpr. "JMJ211107 - add
  LOOP AT wt_rseg INTO st_rseg.
    e_netpr2 = e_netpr2 + st_rseg-wrbtr.
    e_menge = e_menge + st_rseg-menge.
    e_tmp_netpr = e_tmp_netpr + st_rseg-wrbtr. "JMJ211107 - add
  ENDLOOP.

*>> RGI Correction.
*  SELECT SINGLE netpr FROM ekpo
*           INTO e_netpr
*          WHERE ebeln  = e_ebeln
*            AND ebelp  = e_ebelp.
*>> JMJ211107 add meins in selected fields from ekpo
  SELECT SINGLE netpr peinh bpumz bpumn  FROM ekpo
           INTO (e_netpr, e_peinh, e_bpumz, e_bpumn)
          WHERE ebeln  = e_ebeln
            AND ebelp  = e_ebelp.

  SELECT SINGLE waers FROM ekko
           INTO e_waers
          WHERE ebeln  = e_ebeln.

  e_netpr = e_netpr / e_peinh.
  e_dwert = e_netpr2 - ( (  e_netpr  * e_menge * e_bpumz ) / e_bpumn ).
* END ADD - JMB - BAL-20-021

*>> JMJ211107 - Add calculation of net price for the invoice
  e_menge = e_menge * e_bpumz / e_bpumn.
  catch system-exceptions bcd_zerodivide = 1.
    e_tmp_netpr = e_tmp_netpr / e_menge. "unit price
  endcatch.
  if sy-subrc ne 0.
"do nothing
  endif.
  write e_tmp_netpr to e_netpr2.
*<< JMJ211107 - Add calculation of net price for the invoice





* DEL - JMB - BAL-20-022
*** Déclaration données
*  DATA: tab_xek08rn_curr TYPE mmcr_xek08rn WITH HEADER LINE.
*  DATA: s_rbkpv  TYPE  mrm_rbkpv.
*  DATA: s_ekko   LIKE ekko.
*  DATA: s_ekpo   LIKE ekpo.
*  DATA:          f_dwert  LIKE  arseg-dwert.
*  DATA: l_netpr(15), l_dwert(15).
*  DATA: longueur TYPE i.
*  DATA: l_diffnetpr LIKE ekpo-netpr, l_netpr2(15),bprme_conv(5).
**
*
** Recherche postes bloqués pour écart prix
*  CLEAR rseg.
*  SELECT SINGLE * FROM rseg WHERE belnr = i_belnr
*                       AND gjahr = i_gjahr
*                       AND buzei = i_buzei
*                       AND spgrp = 'X'.
*
*  IF sy-subrc NE 0.
*    RAISE no_found.
*  ENDIF.
**
** Lecture de l'en-tête facture
*  CALL FUNCTION 'MRM_DBTAB_RBKPV_READ'
*       EXPORTING
*            i_belnr       = i_belnr
*            i_gjahr       = i_gjahr
*            i_buffer_on   = space
*       IMPORTING
*            e_rbkpv       = s_rbkpv
*       EXCEPTIONS
*            error_message = 1.
*
*  IF sy-subrc NE 0.
*    IF sy-subrc EQ 1.
*      MESSAGE s208(00) WITH 'Entry not found'(w30).
*    ENDIF.
*  ENDIF.
*
*
*  CLEAR s_ekko.
** Lecture de l'en-tête du poste
*  CALL FUNCTION 'ME_READ_HEADER_INVOICE'
*       EXPORTING
*            display      = 'X'
*            ebeln        = rseg-ebeln
*            re_kursf     = s_rbkpv-kursf
*            re_waers     = s_rbkpv-waers
*       IMPORTING
*            iekko        = s_ekko
*       EXCEPTIONS
*            not_activ    = 1
*            not_found    = 2
*            wrong_type   = 3
*            not_released = 4
*            OTHERS       = 5.
*
*  IF sy-subrc NE 0.
*    IF sy-subrc EQ 1.
*      MESSAGE s208(00) WITH 'Inactive'(w25).
*    ELSEIF sy-subrc EQ 2.
*      MESSAGE s208(00) WITH 'Not available'(w26).
*    ELSEIF sy-subrc EQ 3.
*      MESSAGE s208(00)
*      WITH 'No purchase order/scheduling agreement'(w27).
*    ELSEIF sy-subrc EQ 4.
*      MESSAGE s208(00) WITH 'Not released'(w28).
*    ELSEIF sy-subrc EQ 5.
*      MESSAGE s208(00) WITH 'Enqueue failed'(w29).
*    ENDIF.
*  ENDIF.
*
*
** Lecture du poste
*  CALL FUNCTION 'ME_READ_ITEM_INVOICE'
*       EXPORTING
*            display        = 'X'
*            ebelp          = rseg-ebelp
*            iekko          = s_ekko
*            re_kursf       = s_rbkpv-kursf
*            re_waers       = s_rbkpv-waers
*            re_wwert       = s_rbkpv-budat
*       TABLES
*            xek08rn_curr   = tab_xek08rn_curr
**         xek08rn        = tab_ek08rn
*
*       EXCEPTIONS
*            not_found_any  = 1
*            not_found_one  = 2
*            not_valid_any  = 3
*            not_valid_one  = 4
*            enqueue_failed = 5
*            OTHERS         = 6.
**
*
*  IF sy-subrc NE 0.
*    IF sy-subrc EQ 1.
*      MESSAGE s707(me) WITH s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 2.
*      MESSAGE s706(me) WITH rseg-ebelp s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 3.
*      MESSAGE s709(me) WITH s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 4.
*      MESSAGE s708(me) WITH rseg-ebelp s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 5.
*      MESSAGE s208(00) WITH 'Le blocage de l''objet a échoué'(w35).
*    ENDIF.
*  ENDIF.
*
*
*  READ TABLE tab_xek08rn_curr INDEX 1.
*  IF sy-subrc = 0.
*    CLEAR f_dwert.
*    CALL FUNCTION 'MRM_BLOCK_REASON_VAL_CHECK_P'
*         EXPORTING
*              i_remng      = tab_xek08rn_curr-remng
*              i_refwr      = tab_xek08rn_curr-refwr
*              i_netwr      = tab_xek08rn_curr-netwr
*              i_bsmng      = tab_xek08rn_curr-bsmng
*              i_bpumz      = tab_xek08rn_curr-bpumz
*              i_bpumn      = tab_xek08rn_curr-bpumn
*              i_peinh      = tab_xek08rn_curr-peinh
*              i_bukrs      = s_rbkpv-bukrs
*              i_waers      = s_rbkpv-waers
*              i_wwert      = s_rbkpv-budat
*              i_kursf      = s_rbkpv-kursf
*              i_hswae      = s_rbkpv-waers
*              i_noquantity = tab_xek08rn_curr-noquantity
*         IMPORTING
*              e_dwert      = f_dwert.
*
*  ENDIF.
*
*
**   read PO item
*  CALL FUNCTION 'ME_EKPO_SINGLE_READ'
*       EXPORTING
*            pi_ebeln         = rseg-ebeln
*            pi_ebelp         = rseg-ebelp
*       IMPORTING
*            po_ekpo          = s_ekpo
*       EXCEPTIONS
*            no_records_found = 1
*            OTHERS           = 2.
*
*  IF sy-subrc <> 0.
*    IF sy-subrc EQ 1.
*      MESSAGE s706(me) WITH rseg-ebelp rseg-ebelp.
*    ENDIF.
*  ENDIF.
*
*
*  l_diffnetpr = rseg-wrbtr / rseg-menge.
*
*  e_ebelp = rseg-ebelp.
*  e_ebeln = rseg-ebeln.
*  e_netpr = s_ekpo-netpr.
*  e_dwert = f_dwert.
*  e_waers = s_ekko-waers.
*  e_netpr2 = l_diffnetpr.
* END DEL - JMB - BAL-20-022


ENDFUNCTION.
