FUNCTION zbalance_idoc_change.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(DOCNUM) LIKE  EDIDD-DOCNUM
*"     REFERENCE(SEGNUM) LIKE  EDIDD-SEGNUM DEFAULT 000001
*"  EXCEPTIONS
*"      NO_IDOC_FOUND
*"      NO_DATA_RECORD_FOUND
*"      OTHERS
*"----------------------------------------------------------------------

  SELECT SINGLE * FROM edidc WHERE docnum = docnum.

  IF sy-subrc NE 0.
    RAISE no_idoc_found.
  ELSE.


    CALL FUNCTION 'EDI_DATA_RECORD_DISPLAY'
         EXPORTING
              docnum               = docnum
              segnum               = segnum
         EXCEPTIONS
              no_data_record_found = 1
              OTHERS               = 2.
    IF sy-subrc <> 0.
      CASE sy-subrc.
        WHEN '1'.
          RAISE no_data_record_found.
        WHEN '2'.
          RAISE others.
      ENDCASE.
    ENDIF.

  ENDIF.


ENDFUNCTION.
