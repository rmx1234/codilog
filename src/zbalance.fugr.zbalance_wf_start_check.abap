FUNCTION zbalance_wf_start_check.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(BUKRS) TYPE  BUKRS
*"     REFERENCE(LIFNR) TYPE  LIFNR
*"  EXPORTING
*"     VALUE(RETURN)
*"----------------------------------------------------------------------

  DATA : wt_wfstart TYPE TABLE OF zbal_wfstart.
  DATA : st_wfstart TYPE zbal_wfstart.


  SELECT * FROM  zbal_wfstart INTO TABLE wt_wfstart.

  return = '8'.

  READ TABLE wt_wfstart INTO st_wfstart
                        WITH KEY bukrs = '*'
                                 lifnr = '*'.
  IF sy-subrc = 0.
    return = '0'.
  ENDIF.

  CHECK return NE '0'.
  READ TABLE wt_wfstart INTO st_wfstart
                        WITH KEY bukrs = bukrs
                                 lifnr = '*'.
  IF sy-subrc = 0.
    return = '0'.
  ENDIF.

  CHECK return NE '0'.
  READ TABLE wt_wfstart INTO st_wfstart
                        WITH KEY bukrs = '*'
                                 lifnr = lifnr.
  IF sy-subrc = 0.
    return = '0'.
  ENDIF.

  CHECK return NE '0'.
  READ TABLE wt_wfstart INTO st_wfstart
                        WITH KEY bukrs = bukrs
                                 lifnr = lifnr.
  IF sy-subrc = 0.
    return = '0'.
  ENDIF.

ENDFUNCTION.
