*----------------------------------------------------------------------*
***INCLUDE LZBALANCEF05 .
*----------------------------------------------------------------------*

*{   INSERT         MS4K912754                                        1
*&---------------------------------------------------------------------*
*&      Form  park_to_invoice_post
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_RETURN_VARIABLES_DOC_NUMBER  text
*----------------------------------------------------------------------*
FORM park_to_invoice_post  USING    p_return_variables_doc_number.
  DATA object_30 LIKE swotrtime-object.
  DATA method_30 LIKE swotrm-method.
  DATA BEGIN OF return_30.
          INCLUDE STRUCTURE swotreturn.
  DATA END OF return_30.
  DATA container LIKE swcont OCCURS 0 WITH HEADER LINE.
  break gilr.
  CALL FUNCTION 'SWO_CREATE'
    EXPORTING
      objtype = 'BUS2081'
      objkey  = p_return_variables_doc_number
    IMPORTING
      object  = object_30
      return  = return_30.
  IF sy-subrc = 0.
    CALL FUNCTION 'SWO_INVOKE'
      EXPORTING
        access     = 'C'
        object     = object_30
        verb       = 'PreliminaryPost'
        persistent = 'X'
      IMPORTING
        return     = return_30
      TABLES
        container  = container.
  ENDIF.
ENDFORM.                    " park_to_invoice_post

*}   INSERT
