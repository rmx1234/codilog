************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : User exit                                           *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : Balance For MySAP Business Suite                            *
*                                                                      *
* Auteur :                                                             *
*                                                                      *
* Date   :                                                             *
*                                                                      *
* Ordre de transport:                                                  *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*  02.2007 ! ITGGH        ! CHG_01                          DE5K900049 *
*          !              ! Change EDI partner type LI -> LS           *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
   FUNCTION zbalance_user_exit_mm_011.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(I_LIFNR) LIKE  RBKP-LIFNR
*"     VALUE(I_BUKRS) LIKE  RBKP-BUKRS
*"     VALUE(I_IDOC_CONTRL) LIKE  EDIDC STRUCTURE  EDIDC
*"  EXPORTING
*"     VALUE(E_LIFNR) LIKE  RBKP-LIFNR
*"     VALUE(E_BUKRS) LIKE  RBKP-BUKRS
*"     VALUE(E_CHANGE) TYPE  C
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD
*"----------------------------------------------------------------------

     DATA : BEGIN OF w_coutadd OCCURS 0,
              bukrs LIKE rbkp-bukrs,
              hkont LIKE zbal_cadd-hkont,
              shkzg(1) TYPE c,
              betrg LIKE e1edk05-betrg,
              kobtr LIKE e1edk05-kobtr,
              mwskz LIKE e1edk05-mwskz,
              msatz LIKE e1edk05-msatz,
              kostl LIKE zbal_cadd-kostl,
              pspnr LIKE zbal_cadd-pspnr,
              aufnr LIKE zbal_cadd-aufnr,
            END OF w_coutadd.

     DATA : st_e1edk05 LIKE e1edk05.
     DATA : st_e1edka1 LIKE e1edka1.
     DATA : st_e1eds01 LIKE e1eds01.
     DATA : st_e1edk04 LIKE e1edk04.
     DATA : st_e1edk14 LIKE e1edk14.

     DATA : w_taux TYPE p DECIMALS 4.
     DATA : w_tva TYPE p DECIMALS 4.
     DATA : w_totaltva TYPE p DECIMALS 4.
     DATA : w_montant TYPE p DECIMALS 4.
     DATA : w_totalht TYPE p DECIMALS 4.
     DATA : w_totalttc TYPE p DECIMALS 4.

     DATA : w_tabix LIKE sy-tabix.

     DATA : st_e1edk02 LIKE e1edk02,
            w_lifnr LIKE e1edka1-partn,
            w_belnr LIKE e1edk02-belnr.

*    Récupération du fournisseur et n° facture
     LOOP AT t_idoc_data.
       IF t_idoc_data-segnam = 'E1EDKA1'.
         MOVE t_idoc_data-sdata TO st_e1edka1.
         IF st_e1edka1-parvw = 'LF'.
           w_lifnr = e1edka1-partn.
         ENDIF.
       ENDIF.
       IF t_idoc_data-segnam = 'E1EDK02'.
         MOVE t_idoc_data-sdata TO st_e1edk02.
         IF st_e1edk02-qualf = '009'.
           w_belnr = e1edk02-belnr.
         ENDIF.
       ENDIF.
     ENDLOOP.

*Test, ob Buchungskreis im IDOC eingegeben wird
*ITSCU_20070221 Schulz IT-Informatik Beginn
     LOOP AT t_idoc_data WHERE segnam = 'E1EDK14'.
       MOVE t_idoc_data-sdata TO st_e1edk14.
       CASE st_e1edk14-qualf.
         WHEN '011'.   "Buchungskreis im IDOC!
           i_bukrs = st_e1edk14-orgid.
           e_bukrs = st_e1edk14-orgid.
           e_change = 'X'.
       ENDCASE.
     ENDLOOP.
*ITSCU_20070221 Schulz IT-Informatik Ende


* <serco>
* -> the following function delete the position by parked invoices
** Pré-enregistrement
*     CALL FUNCTION 'ZBALANCE_PREENREG'
*          TABLES
*               t_idoc_data = t_idoc_data.
*</serco>

     LOOP AT t_idoc_data WHERE segnam = 'E1EDS01'.
       MOVE t_idoc_data-sdata TO st_e1eds01.

       CASE st_e1eds01-sumid.
         WHEN '010'.
           w_totalht = st_e1eds01-summe.
         WHEN '011'.
           w_totalttc = st_e1eds01-summe.
       ENDCASE.
     ENDLOOP.

     e_lifnr = i_lifnr.
     e_bukrs = i_bukrs.


     REFRESH w_coutadd. CLEAR w_coutadd.
*     IMPORT w_coutadd FROM MEMORY ID 'COUTADD'.


     IF NOT i_lifnr IS INITIAL.

       CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
         EXPORTING
           input  = i_lifnr
         IMPORTING
           output = i_lifnr.


       SELECT SINGLE * FROM  zbal_cadd
                       WHERE  partn  = i_lifnr.
       IF sy-subrc NE 0.
         CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
           EXPORTING
             input  = i_lifnr
           IMPORTING
             output = i_lifnr.
         SELECT SINGLE * FROM  zbal_cadd
                         WHERE  partn  = i_lifnr.

         IF sy-subrc NE 0.
           CLEAR zbal_cadd.
         ENDIF.
       ENDIF.
     ELSE.
       CLEAR zbal_cadd.
     ENDIF.

     LOOP AT t_idoc_data WHERE segnam = 'E1EDK05'.
       MOVE t_idoc_data-sdata TO st_e1edk05.

       IF st_e1edk05-msatz IS INITIAL.
*-----------------------------------------------------------------------
* ITGGH   23.02.2007  CHG_01                                 DE5K900049
* Extension for logical system
         SELECT * FROM  t076m
           WHERE  parart  = 'LS'
           AND    mwart   = st_e1edk05-mwskz
           AND    mwskz   = st_e1edk05-mwskz.
         ENDSELECT.
         IF sy-subrc <> 0.
*-----------------------------------------------------------------------
           SELECT * FROM  t076m
                     WHERE  parart  = 'LI'
                     AND    konto   = i_lifnr
                     AND    mwart   = st_e1edk05-mwskz
                     AND    mwskz   = st_e1edk05-mwskz.
           ENDSELECT.
         ENDIF.                            "* ITGGH 23.02.2007 CHG_01
         IF sy-subrc EQ 0.
           st_e1edk05-msatz = t076m-mwsatz.
         ENDIF.
       ENDIF.

       w_coutadd-hkont = zbal_cadd-hkont.
       IF st_e1edk05-alckz = '+'.
         MOVE 'S' TO w_coutadd-shkzg.
       ELSEIF st_e1edk05-alckz = '-'.
         MOVE 'H' TO w_coutadd-shkzg.
       ENDIF.
       w_coutadd-bukrs = i_bukrs.
       w_coutadd-betrg = st_e1edk05-betrg.


       IF st_e1edk05-kobtr IS INITIAL.
         w_taux = st_e1edk05-msatz.
         w_taux = w_taux / 100.
         w_taux = w_taux + 1.
         w_totalht = st_e1edk05-betrg.
         w_totalttc = w_totalht * w_taux.
         w_coutadd-kobtr = w_totalttc.
       ELSE.
         w_coutadd-kobtr = st_e1edk05-kobtr.
       ENDIF.

       w_coutadd-mwskz = st_e1edk05-mwskz.
       w_coutadd-msatz = st_e1edk05-msatz.

       SELECT SINGLE * FROM  zbal_cadd_unit
              WHERE  partn   = w_lifnr
              AND    belnr   = w_belnr
              AND    segnum  = t_idoc_data-segnum.
       IF sy-subrc EQ 0.
         w_coutadd-hkont = zbal_cadd_unit-hkont.
         w_coutadd-kostl = zbal_cadd_unit-kostl.
*        w_coutadd-pspnr = zbal_cadd_unit-pspnr.
         w_coutadd-aufnr = zbal_cadd_unit-aufnr.

         CALL FUNCTION 'CONVERSION_EXIT_KONPR_INPUT'
           EXPORTING
             input  = zbal_cadd_unit-pspnr
           IMPORTING
             output = w_coutadd-pspnr.


       ELSE.
         w_coutadd-hkont = zbal_cadd-hkont.
         w_coutadd-kostl = zbal_cadd-kostl.
*        w_coutadd-pspnr = zbal_cadd-pspnr.
         w_coutadd-aufnr = zbal_cadd-aufnr.

         CALL FUNCTION 'CONVERSION_EXIT_KONPR_INPUT'
           EXPORTING
             input  = zbal_cadd-pspnr
           IMPORTING
             output = w_coutadd-pspnr.

       ENDIF.

       APPEND w_coutadd.
     ENDLOOP.

     EXPORT w_coutadd TO MEMORY ID 'COUTADD'.


* Code spécial SFD pour gérer le problème des montant des coûts
* additionnels répercutés sur les montants totaux de l'idoc

*     CLEAR : w_totalht, w_totalttc.
*     LOOP AT w_coutadd.
*       CASE w_coutadd-shkzg.
*         WHEN 'S'.
*           w_totalht = w_totalht + w_coutadd-betrg.
*           w_totalttc = w_totalttc + w_coutadd-kobtr.
*         WHEN 'H'.
*           w_totalht = w_totalht - w_coutadd-betrg.
*           w_totalttc = w_totalttc - w_coutadd-kobtr.
*       ENDCASE.
*     ENDLOOP.
*
*     LOOP AT t_idoc_data WHERE segnam = 'E1EDS01'.
*       MOVE t_idoc_data-sdata TO st_e1eds01.
*       IF st_e1eds01-sumid = '010'.
*         w_montant = st_e1eds01-summe.
*         w_montant = w_montant - w_totalht.
*         st_e1eds01-summe = w_montant.
*         t_idoc_data-sdata = st_e1eds01.
*         MODIFY t_idoc_data.
*       ENDIF.
*       IF st_e1eds01-sumid = '011'.
*         w_montant = st_e1eds01-summe.
*         w_montant = w_montant - w_totalttc.
*         st_e1eds01-summe = w_montant.
*         t_idoc_data-sdata = st_e1eds01.
*         MODIFY t_idoc_data.
*       ENDIF.
*     ENDLOOP.
***

   ENDFUNCTION.
