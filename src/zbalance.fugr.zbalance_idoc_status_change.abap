FUNCTION zbalance_idoc_status_change.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(DOCNUM) TYPE  EDI_DOCNUM
*"     REFERENCE(STATUS) TYPE  EDI_STATUS
*"     REFERENCE(MSGTY) TYPE  SYMSGTY
*"     REFERENCE(MSGID) TYPE  MSGID
*"     REFERENCE(MSGNO) TYPE  MSGNO
*"     REFERENCE(MSGV1) TYPE  EDI_STAPA1 OPTIONAL
*"     REFERENCE(MSGV2) TYPE  EDI_STAPA2 OPTIONAL
*"     REFERENCE(MSGV3) TYPE  EDI_STAPA3 OPTIONAL
*"     REFERENCE(MSGV4) TYPE  EDI_STAPA4 OPTIONAL
*"  EXCEPTIONS
*"      IDOC_FOREIGN_LOCK
*"      IDOC_NOT_FOUND
*"      IDOC_STATUS_RECORDS_EMPTY
*"      IDOC_STATUS_INVALID
*"      DB_ERROR
*"----------------------------------------------------------------------

  DATA : wt_idoc_status LIKE bdidocstat OCCURS 0 WITH HEADER LINE.
  DATA : st_idoc_control TYPE edidc.
  DATA : w_idoc_opened_flag TYPE c.

  SELECT SINGLE * FROM edidc WHERE docnum = docnum.

  IF sy-subrc NE 0.
    RAISE no_idoc_found.
  ELSE.
    REFRESH wt_idoc_status.
    CLEAR wt_idoc_status.

    wt_idoc_status-docnum = docnum.
    wt_idoc_status-status = status.
    wt_idoc_status-msgty = msgty.
    wt_idoc_status-msgid = msgid.
    wt_idoc_status-msgno = msgno.
    wt_idoc_status-msgv1 = msgv1.
    wt_idoc_status-msgv2 = msgv2.
    wt_idoc_status-msgv3 = msgv3.
    wt_idoc_status-msgv4 = msgv4.
    APPEND wt_idoc_status.


    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_PROCESS'
      EXPORTING
        db_read_option          = 'N'
        document_number         = docnum
        enqueue_option          = 'S'
      IMPORTING
        idoc_control            = st_idoc_control
      EXCEPTIONS
        document_foreign_lock   = 01
        document_not_exist      = 02
        document_number_invalid = 03
        error_message           = 04
        OTHERS                  = 05.

    w_idoc_opened_flag = 'X'.


    CALL FUNCTION 'IDOC_STATUS_WRITE_TO_DATABASE'
      EXPORTING
        idoc_number               = docnum
        idoc_opened_flag          = w_idoc_opened_flag
        no_dequeue_flag           = ' '
      TABLES
        idoc_status               = wt_idoc_status
      EXCEPTIONS
        idoc_foreign_lock         = 1
        idoc_not_found            = 2
        idoc_status_records_empty = 3
        idoc_status_invalid       = 4
        db_error                  = 5
        OTHERS                    = 6.

    CASE sy-subrc.
      WHEN 1.
        RAISE idoc_foreign_lock.
      WHEN 2.
        RAISE idoc_not_found.
      WHEN 3.
        RAISE idoc_status_records_empty.
      WHEN 4.
        RAISE idoc_status_invalid.
      WHEN 5.
        RAISE db_error.
      WHEN 6.
        RAISE others.
    ENDCASE.
  ENDIF.

  CALL FUNCTION 'EDI_DOCUMENT_CLOSE_PROCESS'
    EXPORTING
      document_number           = docnum
*     BACKGROUND                = NO_BACKGROUND
*     NO_DEQUEUE                = ' '
*   IMPORTING
*     IDOC_CONTROL              =
*   EXCEPTIONS
*     DOCUMENT_NOT_OPEN         = 1
*     FAILURE_IN_DB_WRITE       = 2
*     PARAMETER_ERROR           = 3
*     STATUS_SET_MISSING        = 4
*     OTHERS                    = 5
            .
  IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.

* BEGIN INSERT OHA001
  CLEAR zbal_idoc.
  SELECT SINGLE * FROM zbal_idoc WHERE docnum = docnum.
  IF sy-subrc = 0.
    zbal_idoc-upddat = sy-datum.
    zbal_idoc-updtim = sy-uzeit.
    zbal_idoc-status = status.
    zbal_idoc-stamid = msgid.
    zbal_idoc-stamno = msgno.
    zbal_idoc-statyp = msgty.
    zbal_idoc-stapa1 = msgv1.
    zbal_idoc-stapa2 = msgv2.
    zbal_idoc-stapa3 = msgv3.
    zbal_idoc-stapa4 = msgv4.
    UPDATE zbal_idoc.
  ENDIF.
* END INSERT OHA001

ENDFUNCTION.
