*&---------------------------------------------------------------------*
*& Report ZTEST_RMX
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ZTEST_RMX.

tables : mara.

data : gt_mara type STANDARD TABLE OF mara,
       lv_matnr type matnr.

SELECT-OPTIONS : s_matnr for mara-matnr.

START-OF-SELECTION.


Select *
  from mara
  into table gt_mara
  where matnr in s_matnr.
check sy-subrc is initial.

READ TABLE gt_mara
  TRANSPORTING NO FIELDS
  with key matnr = lv_matnr
  BINARY SEARCH.
if sy-subrc is initial.
  write lv_matnr.
endif.
