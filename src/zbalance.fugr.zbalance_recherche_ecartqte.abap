FUNCTION zbalance_recherche_ecartqte .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(I_BELNR) TYPE  RSEG-BELNR
*"     REFERENCE(I_GJAHR) TYPE  RSEG-GJAHR
*"     REFERENCE(I_BUZEI) TYPE  RSEG-BUZEI
*"  EXPORTING
*"     REFERENCE(E_EBELN) LIKE  RSEG-EBELN
*"     REFERENCE(E_EBELP) LIKE  RSEG-EBELP
*"     REFERENCE(E_WEMNG) LIKE  EK08R-WEMNG
*"     REFERENCE(E_REMNG) LIKE  EK08R-REMNG
*"     REFERENCE(E_DMENG) LIKE  ARSEG-DMENG
*"     REFERENCE(E_BSTME) LIKE  RSEG-BSTME
*"  EXCEPTIONS
*"      NO_FOUND
*"----------------------------------------------------------------------
* ADD - JMB - BAL-20-021
  DATA : wt_rseg TYPE TABLE OF rseg.
  DATA : st_rseg TYPE rseg.
  DATA : wt_ekbe TYPE TABLE OF ekbe.
  DATA : st_ekbe TYPE ekbe.


  SELECT SINGLE ebeln ebelp FROM rseg
           INTO (e_ebeln, e_ebelp)
          WHERE belnr = i_belnr
            AND gjahr = i_gjahr
            AND buzei = i_buzei.

  SELECT * FROM  rseg
  INTO TABLE wt_rseg
         WHERE  ebeln  = e_ebeln
         AND    ebelp  = e_ebelp
         AND    belnr = i_belnr
         AND    gjahr = i_gjahr.
  LOOP AT wt_rseg INTO st_rseg.
    e_remng = e_remng + st_rseg-menge.
  ENDLOOP.

  SELECT * FROM ekbe
     INTO TABLE wt_ekbe
          WHERE ebeln  = st_rseg-ebeln
            AND ebelp  = st_rseg-ebelp.

  LOOP AT wt_ekbe INTO st_ekbe.
    IF st_ekbe-belnr = i_belnr AND
       st_ekbe-gjahr = i_gjahr.
      CONTINUE.
    ENDIF.
*   Goods receipt
    IF st_ekbe-vgabe EQ '1'.
      IF st_ekbe-shkzg = 'H'.
        e_wemng  = e_wemng  - st_ekbe-menge.
      ELSEIF st_ekbe-shkzg = 'S'.
        e_wemng  = e_wemng  + st_ekbe-menge.
      ENDIF.
*   Invoices
    ELSEIF st_ekbe-vgabe EQ '2'.
      IF st_ekbe-shkzg = 'S'.
        e_wemng  = e_wemng  - st_ekbe-menge.
      ELSEIF st_ekbe-shkzg = 'H'.
        e_wemng  = e_wemng  + st_ekbe-menge.
      ENDIF.
    ENDIF.
  ENDLOOP.

  e_dmeng = e_remng - e_wemng.
* END ADD - JMB - BAL-20-021







* DEL - JMB - BAL-20-021
** Déclarations
*  TYPE-POOLS: mrm,mmcr.
**  tables: rseg.
*  TABLES: ek08r.
*  DATA: s_rbkpv  TYPE  mrm_rbkpv.
*  DATA: s_ekko   LIKE ekko.
*  DATA: BEGIN OF tab_ek08rn OCCURS 50.
*          INCLUDE STRUCTURE ek08rn.
*  DATA: END OF   tab_ek08rn.
*  DATA: i_arseg  TYPE mrm_arseg.
*  DATA: f_wwert LIKE arseg-wwert.
*  DATA: longueur TYPE i.
*
*  DATA: l_wemng(15), l_remng(15), l_dmeng(15), l_bstme(5).
*
*
** Vérification postes bloqués pour écart quantité
*  CLEAR rseg.
*  SELECT SINGLE * FROM rseg WHERE belnr = i_belnr
*                       AND gjahr = i_gjahr
*                       AND buzei = i_buzei
*                       AND spgrm = 'X'.
*
*  IF sy-subrc NE 0.
*    RAISE no_found.
*  ENDIF.
*
** Lecture de l'en-tête facture
*  CALL FUNCTION 'MRM_DBTAB_RBKPV_READ'
*       EXPORTING
*            i_belnr       = i_belnr
*            i_gjahr       = i_gjahr
*            i_buffer_on   = space
*       IMPORTING
*            e_rbkpv       = s_rbkpv
*       EXCEPTIONS
*            error_message = 1.
*
*  IF sy-subrc NE 0.
*    MESSAGE s208(00) WITH 'Entry not found'(w11).
*  ENDIF.
*
*  CLEAR s_ekko.
** Lecture de l'en-tête du poste
*  CALL FUNCTION 'ME_READ_HEADER_INVOICE'
*       EXPORTING
*            display      = 'X'
*            ebeln        = rseg-ebeln
*            re_kursf     = s_rbkpv-kursf
*            re_waers     = s_rbkpv-waers
*       IMPORTING
*            iekko        = s_ekko
*       EXCEPTIONS
*            not_activ    = 1
*            not_found    = 2
*            wrong_type   = 3
*            not_released = 4
*            OTHERS       = 5.
*
*
*  IF sy-subrc NE 0.
*    IF sy-subrc EQ 1.
*      MESSAGE s208(00) WITH 'Inactive'(w25).
*    ELSEIF sy-subrc EQ 2.
*      MESSAGE s208(00) WITH 'Not available'(w26).
*    ELSEIF sy-subrc EQ 3.
*      MESSAGE s208(00)
*      WITH 'No purchase order/scheduling agreement'(w27).
*    ELSEIF sy-subrc EQ 4.
*      MESSAGE s208(00) WITH 'Not released'(w28).
*    ELSEIF sy-subrc EQ 5.
*      MESSAGE s208(00) WITH 'Enqueue failed'(w29).
*    ENDIF.
*  ENDIF.
*
*
** Lecture du poste
*  CALL FUNCTION 'ME_READ_ITEM_INVOICE'
*       EXPORTING
*            display        = 'X'
*            ebelp          = rseg-ebelp
*            iekko          = s_ekko
*            re_kursf       = s_rbkpv-kursf
*            re_waers       = s_rbkpv-waers
*            re_wwert       = s_rbkpv-budat
*       TABLES
**              xek08rn_curr   = tab_xek08rn_curr
*        xek08rn        = tab_ek08rn
*
*       EXCEPTIONS
*            not_found_any  = 1
*            not_found_one  = 2
*            not_valid_any  = 3
*            not_valid_one  = 4
*            enqueue_failed = 5
*            OTHERS         = 6.
*
*  IF sy-subrc NE 0.
*    IF sy-subrc EQ 1.
*      MESSAGE s707(me) WITH s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 2.
*      MESSAGE s706(me) WITH rseg-ebelp s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 3.
*      MESSAGE s709(me) WITH s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 4.
*      MESSAGE s708(me) WITH rseg-ebelp s_ekko-ebeln.
*    ELSEIF sy-subrc EQ 5.
*      MESSAGE s208(00) WITH 'Le blocage de l''objet a échoué'(w35).
*    ENDIF.
*  ENDIF.
*
*
*  MOVE-CORRESPONDING rseg TO i_arseg.
*  MOVE-CORRESPONDING tab_ek08rn TO ek08r.
*
*  IF i_arseg-wwert IS INITIAL.
*    f_wwert = i_arseg-wwert.
*  ELSE.
*    f_wwert = sy-datum.
*  ENDIF.
*
*  CALL FUNCTION 'MRM_BLOCK_REASON_VAL_CHECK_M'
*       EXPORTING
*            i_remng     = ek08r-remng
*            i_wemng     = ek08r-wemng
*            i_netwr     = ek08r-netwr
*            i_bsmng     = ek08r-bsmng
*            i_bpumz     = ek08r-bpumz
*            i_bpumn     = ek08r-bpumn
*            i_peinh     = ek08r-peinh
*            i_wepos     = ek08r-wepos
*            i_tbtkz     = i_arseg-tbtkz
*            i_bukrs     = i_arseg-bukrs
*            i_waers     = i_arseg-waers
*            i_wwert     = f_wwert
*            i_kursf     = i_arseg-kursf
*            i_hswae     = i_arseg-hswae
*       IMPORTING
*            e_spgrm_inv = i_arseg-spgrm_inv
*            e_dmeng     = i_arseg-dmeng.
*
*
*  e_ebeln = rseg-ebeln.
*  e_ebelp = rseg-ebelp.
*  e_wemng = ek08r-wemng.
*  e_remng = ek08r-remng.
*  e_dmeng = i_arseg-dmeng.
*  e_bstme = rseg-bstme.
* END DEL - JMB - BAL-20-021

ENDFUNCTION.
