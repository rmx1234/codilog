* T. NGUYEN (Appia Consulting) - 21.08.2006
* Copie de ZBALANCE_LISTPOSTBLO_QTE

FUNCTION zbalance_listpostblo_qte_2.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(I_BELNR) LIKE  RBKP-BELNR
*"     VALUE(I_GJAHR) LIKE  RBKP-GJAHR
*"     REFERENCE(I_LISTMODE) LIKE  SY-INDEX DEFAULT 1
*"  EXPORTING
*"     REFERENCE(E_DECISION) LIKE  SY-UCOMM
*"  TABLES
*"      TE_SELECTED_INVOICE_ITEMS STRUCTURE  RSEG_BUZEI
*"----------------------------------------------------------------------
*{   INSERT         GEDK900096                                        1
************************************************************************
* Identification : ZBALANCE_LISTPOSTBLO_QTE		         *
*                                                                      *
*                                                                      *
*  Description de la fonction :                                        *
*  Cette fonction permet de rechercher l'écart de prix entre le poste  *
*  facture et le poste commande                                        *
*  En entrée: I_BELNR : Numéro de document de facturation              *
*             I_GJAHR : Exercice comptable                             *
*             I_LISTMODE : Type d'affichage                            *
*  En sortie: E_DECISION : Décision                                    *
*                                                                      *
**----------------------------------------------------------------------
*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur :  Frédérick HUYNH   (Netinside)                              *
*                                                                      *
* Date : 27/01/2004                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport:                                                  *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*  02.2007 ! SERCO01      ! calculate the field "ls_item_qte-dmeng"    *
*          !              ! changes signed by "<serco>"                *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

* si I_LISTMODE = 1 => Affichage liste poste
* si I_LISTMODE = 2 => Validation Acheteur
* si I_LISTMODE = 3 => Validation Compta

*  <serco>
  DATA: lt_itemdata TYPE TABLE OF bapi_incinv_detail_item,
          ls_itemdata TYPE bapi_incinv_detail_item,
          lt_return   TYPE TABLE OF bapiret2.
  TYPES: BEGIN OF ty_st_item_qte,
          sel(1),
          belnr TYPE belnr_d,
          gjahr TYPE gjahr,
          buzei TYPE rblgp,
          ebeln TYPE ebeln,
          ebelp TYPE ebelp,
          wemng TYPE wemng,
          remng TYPE remng,
          dmeng TYPE dmeng,
          bstme TYPE bstme,
       END OF ty_st_item_qte.
  DATA: ls_item_qte TYPE ty_st_item_qte.
  DATA: ls_qchk TYPE i.
* </Serco>
* ---------------------------------------------------------------------


  CHECK i_listmode EQ '1' OR i_listmode EQ '2' OR i_listmode EQ '3'.

  w_belnr = i_belnr.
  w_gjahr = i_gjahr.
  w_listmode = i_listmode.

* Refresh of table to get only the current invoice of WF item
  CLEAR rseg.
  clear t_item_qte[].
  refresh  t_item_qte[].
  SELECT * APPENDING CORRESPONDING FIELDS OF TABLE t_item_qte
  FROM rseg
  WHERE belnr = i_belnr
    AND gjahr = i_gjahr
    AND spgrm = 'X'.


  LOOP AT t_item_qte.
    CALL FUNCTION 'ZBALANCE_RECHERCHE_ECARTQTE'
      EXPORTING
        i_belnr  = i_belnr
        i_gjahr  = i_gjahr
        i_buzei  = t_item_qte-buzei
      IMPORTING
        e_ebeln  = t_item_qte-ebeln
        e_ebelp  = t_item_qte-ebelp
        e_wemng  = t_item_qte-wemng
        e_remng  = t_item_qte-remng
        e_dmeng  = t_item_qte-dmeng
        e_bstme  = t_item_qte-bstme
      EXCEPTIONS
        no_found = 1
        OTHERS   = 2.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.
      MODIFY t_item_qte.
    ENDIF.

  ENDLOOP.


*----------------------------------------------------------------------
*<serco>

* calculate the field "ls_item_qte-dmeng"
  CALL FUNCTION 'BAPI_INCOMINGINVOICE_GETDETAIL'
    EXPORTING
      invoicedocnumber = i_belnr
      fiscalyear       = i_gjahr
    TABLES
      itemdata         = lt_itemdata
      return           = lt_return.

  CLEAR ls_qchk.

  LOOP AT lt_itemdata INTO ls_itemdata.

    READ TABLE  t_item_qte INTO ls_item_qte WITH KEY buzei =
    ls_itemdata-invoice_doc_item.

    ls_item_qte-remng = ls_itemdata-quantity.

    ls_item_qte-dmeng = ls_item_qte-remng - ls_item_qte-wemng.

    IF ls_item_qte-dmeng < 0.

      ls_item_qte-dmeng = ls_item_qte-dmeng * -1.

    ENDIF.

    IF ls_item_qte-wemng > ls_item_qte-remng.

      ls_qchk = 1.

    ENDIF.

    MODIFY TABLE t_item_qte FROM ls_item_qte.

  ENDLOOP.
*</serco>
*----------------------------------------------------------------------

* Appeler l'ecran utilisateur
  CALL SCREEN 9202.

* Retourner la decisin
  e_decision = w_decision.

* <serco> 19.02.07
*  Decision modified to trigger step post inv. of WF to leave loop
   IF e_decision = 'BACK' AND ls_qchk = 0.

    e_decision = 'ACC'.

   ENDIF.
* </serco>


* loop at t_item_qte where sel = 'X'.
* TE_SELECTED_INVOICE_ITEMS-buzei = t_item_qte-buzei.
* append TE_SELECTED_INVOICE_ITEMS.
* endloop.

*}   INSERT
ENDFUNCTION.
