FUNCTION zbalance_idoc_display.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(DOCNUM) LIKE  EDIDD-DOCNUM
*"  EXCEPTIONS
*"      NO_IDOC_FOUND
*"----------------------------------------------------------------------

  SELECT SINGLE * FROM edidc WHERE docnum = docnum.

  IF sy-subrc NE 0.
    RAISE no_idoc_found.
  ELSE.
    SUBMIT rseidoc2 WITH docnum = docnum
                    WITH credat IN r_credat
                    AND  RETURN.
  ENDIF.


ENDFUNCTION.
