************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Forcer préenregistrement de la facture MM           *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Attila Kovacs (Netinside)                                   *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*                                                                      *
* Ordre de transport: DE2K900720                                       *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_preenreg.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD
*"  EXCEPTIONS
*"      PRERECORDING_NOT_NECESSARY
*"----------------------------------------------------------------------

  TYPES: BEGIN OF t_suppr,
           debut TYPE sy-tabix,
           fin   TYPE sy-tabix,
         END OF t_suppr.

  DATA: wt_factures  TYPE STANDARD TABLE OF zbalance_01.
  DATA: ws_facture   TYPE zbalance_01.
  DATA: wl_start     TYPE flag.

  DATA: wt_suppr     TYPE STANDARD TABLE OF t_suppr WITH HEADER LINE.
  DATA: ws_prev      TYPE t_suppr.

  IMPORT factures = wt_factures FROM MEMORY ID  wc_memory_id_03.
  CHECK sy-subrc EQ 0.
  IMPORT start = wl_start FROM MEMORY ID  wc_memory_id_02.
  CHECK sy-subrc EQ 0.
  IF wl_start = 'X'.
*   Nettoyer l'iDoc
    LOOP AT wt_factures INTO ws_facture.
      wt_suppr-debut = ws_facture-debut.
      wt_suppr-fin  = ws_facture-fin.
      APPEND wt_suppr.
    ENDLOOP.
    SORT wt_suppr BY debut fin.
    LOOP AT wt_suppr.
      IF ws_prev-fin GT wt_suppr-fin.
        DELETE wt_suppr.
        CONTINUE.
      ELSEIF wt_suppr-debut LT ws_prev-fin.
        wt_suppr-debut = ws_prev-fin + 1.
        MODIFY wt_suppr.
      ENDIF.
      ws_prev = wt_suppr.
    ENDLOOP.
    SORT wt_suppr BY debut DESCENDING.
    LOOP AT wt_suppr.
      sy-index = wt_suppr-fin - wt_suppr-debut.
      DO sy-index TIMES.
        DELETE t_idoc_data INDEX wt_suppr-debut.
      ENDDO.
    ENDLOOP.
  ELSE.
  MESSAGE s532(0u) WITH  'Préenregistrement n''est pas nécessaire'(w50).
*    RAISE  prerecording_not_necessary.
  ENDIF.

ENDFUNCTION.
