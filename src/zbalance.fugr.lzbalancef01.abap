*---------------------------------------------------------------------*
*   INCLUDE LZBALANCEF01                                              *
*---------------------------------------------------------------------*
*       FORM f_120_recherche_segment                                  *
*---------------------------------------------------------------------*
*       Déterminer la position du segment suivante                    *
*---------------------------------------------------------------------*
*  -->  PT_IDOC_DATA : données d'idoc                                 *
*  -->  P_SEGNAM     : nom du segment à rechercher                    *
*  -->  P_DEBUT      : rechercher à partir de cette position          *
*  -->  P_MAX        : rechercher jusqu'à cette position ou 0         *
*  -->  P_TABIX      : la position du segment suivant (p_max sinon)   *
*  -->  P_DEST       : contenu du segment suivant ( vide si aucune    *
*                      resultat )                                     *
*---------------------------------------------------------------------*
FORM f_120_recherche_segment TABLES pt_idoc_data    TYPE edidd_tt
                              USING value(p_segnam) TYPE edidd-segnam
                                    value(p_debut)  TYPE sy-tabix
                                    value(p_max)    TYPE sy-tabix
                                    p_tabix         TYPE sy-tabix
                                    p_dest.
  FIELD-SYMBOLS: <fs_idoc_data> TYPE edidd.

  CLEAR: p_tabix, p_dest.
  DO.
    ADD 1 TO p_debut.
    IF p_max GT 0 AND p_debut GT p_max.
      p_tabix = p_debut.
      EXIT.
    ENDIF.
    READ TABLE pt_idoc_data INDEX p_debut ASSIGNING <fs_idoc_data>.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.
    IF <fs_idoc_data>-segnam EQ p_segnam.
      p_tabix = p_debut.
      p_dest  = <fs_idoc_data>-sdata.
      EXIT.
    ENDIF.
  ENDDO.
ENDFORM.                    "f_120_recherche_segment

*&---------------------------------------------------------------------*
*&      Form  f_130_message
*&---------------------------------------------------------------------*
FORM f_130_message TABLES pt_return TYPE bal_t_msg
                   USING p_msgno
                         p_msgty
                         p_msgv1
                         p_msgv2
                         p_msgv3
                         p_msgv4.
  DATA: wl_msg TYPE bal_s_msg.
  CLEAR wl_msg.
  wl_msg-msgid = wc_msgid.
  wl_msg-msgty = p_msgty.
  wl_msg-msgno = p_msgno.
  wl_msg-msgv1 = p_msgv1.
  wl_msg-msgv2 = p_msgv2.
  wl_msg-msgv3 = p_msgv3.
  wl_msg-msgv4 = p_msgv4.
  SHIFT wl_msg-msgv1 LEFT DELETING LEADING space.
  SHIFT wl_msg-msgv2 LEFT DELETING LEADING space.
  SHIFT wl_msg-msgv3 LEFT DELETING LEADING space.
  SHIFT wl_msg-msgv4 LEFT DELETING LEADING space.
  APPEND wl_msg TO pt_return.
ENDFORM.                    " f_130_message

*&---------------------------------------------------------------------*
*&      Form  f_140_recherche_caractere
*&---------------------------------------------------------------------*
*       combien de lettres de la chaine de caractere (mot) du
*       libellé du poste de la facture
*       se retrouvent dans le mot du libellé du poste de la commande
*----------------------------------------------------------------------*
FORM f_140_recherche_caractere TABLES pt_reschar TYPE tt_reschar
                                USING p_chaine_in
                                      p_chaine_for.

  DATA : wl_len_in TYPE i,
         wl_dec_in TYPE i,
         wl_len_for TYPE i,
         wl_dec_for TYPE i,
         wl_fin_for TYPE i.
  DATA : wl_char_ok TYPE i.

  wl_len_in = STRLEN( p_chaine_in ).
  wl_len_for = STRLEN( p_chaine_for ).

  wl_dec_for = 0.
  wl_fin_for = wl_len_for.
  wl_dec_in = 0.

  DO wl_len_in TIMES.
    CHECK wl_fin_for NE 0.
    IF p_chaine_for+wl_dec_for(wl_fin_for) CS p_chaine_in+wl_dec_in(1).

      ADD 1 TO wl_char_ok.
      wl_dec_for = sy-fdpos + 1.
      wl_fin_for = wl_len_for - wl_dec_for.
      ADD 1 TO wl_dec_in.
*'ABCDE' CA 'CY' is true; SY-FDPOS = 2.
    ELSE.
*      ADD 1 TO w_dec_for.
*      w_fin_for = w_len_for - w_dec_for.
      ADD 1 TO wl_dec_in.
    ENDIF.
  ENDDO.
  pt_reschar-proba = ( wl_char_ok / wl_len_in ) *
                     (  wl_char_ok / wl_len_for ).
  APPEND pt_reschar.
ENDFORM.                    " f_140_recherche_caractere

*&---------------------------------------------------------------------*
*&      Form  f_150_suppression_caracteres
*&---------------------------------------------------------------------*
FORM f_150_suppression_caracteres USING p_chaine.

  DATA : wl_len TYPE i,
         wl_pos TYPE i,
         wl_char.
  TRANSLATE p_chaine TO LOWER CASE.

  wl_pos = 0.
  wl_len = STRLEN(  p_chaine ).
  DO wl_len TIMES.
    wl_char =  p_chaine+wl_pos(1).
    IF wl_char CA 'àâäçéèêëîïôùûü.,'';:!?-_+*/\"&~#{}=()[]'.

      CASE wl_char.
        WHEN 'à' OR 'â' OR 'ä'.
          p_chaine+wl_pos(1) = 'a'.
        WHEN 'ç'.
          p_chaine+wl_pos(1) = 'c'.
        WHEN 'é' OR 'è' OR 'ê' OR 'ë'.
          p_chaine+wl_pos(1) = 'e'.
        WHEN 'î' OR 'ï' .
          p_chaine+wl_pos(1) = 'i'.
        WHEN 'ô'.
          p_chaine+wl_pos(1) = 'o'.
        WHEN 'ù' OR 'û' OR 'ü' .
          p_chaine+wl_pos(1) = 'u'.
        WHEN '.' OR ',' OR '''' OR ';' OR ':' OR '!' OR '?' OR '-' OR '_'
                 OR '+' OR '*'  OR '/' OR '\' OR '"' OR '&' OR '~' OR '#'
                       OR '{' OR '}'  OR '=' OR '(' OR ')' OR '[' OR ']' .

          p_chaine+wl_pos(1) = ' '.
      ENDCASE.
    ENDIF.
    ADD 1 TO wl_pos.

  ENDDO.


ENDFORM.                    " f_150_suppression_caracteres

*&---------------------------------------------------------------------*
*&      Form  f_160_ctrl_matnr
*&---------------------------------------------------------------------*
*FORM f_160_ctrl_matnr USING value(p_lifnr) TYPE lifnr
*                            p_matnr        TYPE edi_idtnr.
*
*  DATA: wl_i TYPE int4.
*
*  PERFORM f_170_ctrl_lifnr USING p_lifnr.
*
*  SELECT SINGLE matnr COUNT(*) FROM eina INTO (p_matnr,wl_i)
*                              WHERE lifnr = p_lifnr
*                                AND idnlf = p_matnr
*                           GROUP by matnr.
*  IF wl_i NE 1.
*    CLEAR p_matnr.
*  ENDIF.
*ENDFORM.                    " f_160_ctrl_matnr

*&---------------------------------------------------------------------*
*&      Form  f_170_ctrl_lifnr
*&---------------------------------------------------------------------*
FORM f_170_ctrl_lifnr USING p_lifnr TYPE zbalance_01-lifnr.
  DATA: wl_lifnr TYPE lifnr.

  wl_lifnr = p_lifnr.
  SHIFT wl_lifnr LEFT DELETING LEADING space.
  SELECT SINGLE * FROM lfa1 WHERE lifnr = wl_lifnr.
  IF sy-subrc NE 0.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = p_lifnr
      IMPORTING
        output = wl_lifnr.
    SELECT SINGLE * FROM lfa1 WHERE lifnr = wl_lifnr.
  ENDIF.
  IF sy-subrc EQ 0.
    p_lifnr = lfa1-lifnr.
  ELSE.
    CLEAR p_lifnr.
  ENDIF.
ENDFORM.                    " f_170_ctrl_lifnr

*&---------------------------------------------------------------------*
*&      Form  f_180_reste_fact
*&---------------------------------------------------------------------*
FORM f_180_reste_fact USING  p_ebeln    TYPE ebeln
                             p_ebelp    TYPE ebelp
                             p_montant  TYPE wrbtr
                             p_quantite TYPE menge_d
                             p_rcpticon TYPE icon_d.
  DATA:
    wl_shkzg TYPE ekbe-shkzg,
    wl_vgabe TYPE ekbe-vgabe,
    wl_bwart TYPE ekbe-bwart,
    wl_menge TYPE ekbe-menge,
    wl_wrbtr TYPE ekbe-wrbtr.

  CLEAR p_rcpticon.

  SELECT SINGLE * FROM  ekpo CLIENT SPECIFIED
         WHERE  mandt  = sy-mandt
         AND    ebeln  = p_ebeln
         AND    ebelp  = p_ebelp.
  IF ekpo-webre = 'X'.
    SELECT shkzg vgabe bwart menge wrbtr
                    INTO (wl_shkzg,wl_vgabe,wl_bwart,wl_menge,wl_wrbtr)
                     FROM ekbe
                    WHERE ebeln EQ p_ebeln
                      AND ebelp EQ p_ebelp
                      AND ( vgabe EQ '1' OR
                            vgabe EQ '2' ).
      IF sy-subrc EQ 0.
        p_rcpticon = icon_transport.
        IF wl_vgabe EQ '1'.  " Entrée de marchandise
          IF wl_bwart = '102'.  " Annulation de réception
            p_montant   = p_montant   - wl_wrbtr.
            p_quantite  = p_quantite  - wl_menge.
          ELSEIF wl_bwart = '101'.
            p_montant   = p_montant   + wl_wrbtr.
            p_quantite  = p_quantite  + wl_menge.
          ENDIF.
        ELSEIF wl_vgabe EQ '2'. " Entrée de facture
          IF wl_shkzg = 'S'. " Débit ( Annulation de facture )
            p_montant   = p_montant   - wl_wrbtr.
            p_quantite  = p_quantite  - wl_menge.
          ELSE.              " Crédit
            p_montant   = p_montant   + wl_wrbtr.
            p_quantite  = p_quantite  + wl_menge.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDSELECT.
  ELSE.
    IF ekpo-loekz IS INITIAL.
      p_montant = ekpo-netwr.
      p_quantite = ekpo-menge.

      SELECT shkzg vgabe bwart menge wrbtr
                    INTO (wl_shkzg,wl_vgabe,wl_bwart,wl_menge,wl_wrbtr)
                     FROM ekbe
                    WHERE ebeln EQ p_ebeln
                      AND ebelp EQ p_ebelp
                      AND vgabe EQ '2'.
        IF sy-subrc EQ 0.
          IF wl_shkzg = 'S'. " Débit ( Annulation de facture )
            p_montant   = p_montant   - wl_wrbtr.
            p_quantite  = p_quantite  - wl_menge.
          ELSE.              " Crédit
            p_montant   = p_montant   + wl_wrbtr.
            p_quantite  = p_quantite  + wl_menge.
          ENDIF.
        ENDIF.
      ENDSELECT.
    ELSE.
      CLEAR : p_montant, p_quantite.
    ENDIF.
  ENDIF.

ENDFORM.                    " f_180_reste_fact

*&---------------------------------------------------------------------*
*&      Form  f_190_ctrle_em
*&---------------------------------------------------------------------*
FORM f_190_ctrle_em USING    p_ebeln TYPE ekpo-ebeln
                             p_ebelp TYPE ekpo-ebelp
                             p_docnum TYPE edidc-docnum
                             p_belnr TYPE edi_belnr
                             p_fact_menge TYPE zbalance_01-menge
                             p_cmde_menge TYPE zbalance_02-menge.
  DATA:
    wl_shkzg TYPE ekbe-shkzg,
    wl_vgabe TYPE ekbe-vgabe,
    wl_bwart TYPE ekbe-bwart,
    wl_menge TYPE ekbe-menge,
    wl_wrbtr TYPE ekbe-wrbtr,
    wl_montant  TYPE wrbtr,
    wl_quantite TYPE menge_d,
    wl_qte_rcpt TYPE menge_d, " Seulement les réceptions
    wl_qte_fact TYPE menge_d. " Seulement les facturations

  SELECT shkzg vgabe bwart menge wrbtr
                   INTO (wl_shkzg,wl_vgabe,wl_bwart,wl_menge,wl_wrbtr)
                   FROM ekbe
                  WHERE ebeln EQ p_ebeln
                    AND ebelp EQ p_ebelp
                    AND ( vgabe EQ '1' OR
                          vgabe EQ '2' ).
    IF sy-subrc = 0.
      IF wl_vgabe EQ '1'.  " Entrée de marchandise
        IF wl_bwart = '102'.  " Annulation de réception
          wl_montant   = wl_montant   - wl_wrbtr.
          wl_quantite  = wl_quantite  - wl_menge.
          wl_qte_rcpt  = wl_qte_rcpt  - wl_menge.
        ELSEIF wl_bwart = '101'.
          wl_montant   = wl_montant   + wl_wrbtr.
          wl_quantite  = wl_quantite  + wl_menge.
          wl_qte_rcpt  = wl_qte_rcpt  + wl_menge.
        ENDIF.
      ELSEIF wl_vgabe EQ '2'. " Entrée de facture
        IF wl_shkzg = 'S'. " Débit ( Annulation de facture )
          wl_montant   = wl_montant   - wl_wrbtr.
          wl_quantite  = wl_quantite  - wl_menge.
          wl_qte_fact  = wl_qte_fact  - wl_menge.
        ELSE.              " Crédit
          wl_montant   = wl_montant   + wl_wrbtr.
          wl_quantite  = wl_quantite  + wl_menge.
          wl_qte_fact  = wl_qte_fact  + wl_menge.
        ENDIF.
      ENDIF.
    ENDIF.

  ENDSELECT.

  IF sy-subrc NE 0.
    MESSAGE s534(0u) WITH text-w47 text-w36 text-w46 ekpo-ebeln.
  ENDIF.

* wl_qte_fact => Quantité total facturée (comprend la qté
* de la facture encours de traitement)
  wl_qte_fact = wl_qte_fact + p_fact_menge.

  IF wl_qte_fact LE p_cmde_menge OR
     wl_qte_rcpt LT p_cmde_menge.
    IF p_fact_menge GT wl_quantite.
      w_em_manq = 'X'.
      w_em_ebeln = p_ebeln.
      w_em_belnr = p_belnr.
      EXIT.
    ENDIF.
  ENDIF.

ENDFORM.                    " f_190_ctrle_em

*&---------------------------------------------------------------------*
*&      Form  f_150_delimite_e1edp01
*&---------------------------------------------------------------------*
FORM f_150_delimite_e1edp01 TABLES   pt_idoc_data STRUCTURE edidd
                            USING    ps_facture TYPE zbalance_01.
  DATA: w_idx TYPE sy-tabix.
  IF wt_segnam[] IS INITIAL.
    SELECT segtyp FROM idocsyn INTO TABLE wt_segnam
           WHERE  idoctyp  = 'INVOIC01'
           AND    parseg   = 'E1EDP01'.
  ENDIF.

  w_idx = ps_facture-debut + 1.
  DO.
    READ TABLE pt_idoc_data INDEX w_idx.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.
    READ TABLE wt_segnam WITH KEY table_line = pt_idoc_data-segnam
    TRANSPORTING NO FIELDS.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.
    ADD 1 TO w_idx.
  ENDDO.
  ps_facture-fin = w_idx.

ENDFORM.                    " f_150_delimite_e1edp01
*
*&---------------------------------------------------------------------*
*&      Form  f_200_raise_events
*&
*& - Création de l'évènement Réception manquante de ZIDOCINVOIC
*& - Passage de l'iDoc au statut 51
*& - Création de l'évènement Input Error Occured MM pour faire le lien
*&   avec l'image
*&                                               TBR 28092004
*&---------------------------------------------------------------------*
FORM f_200_raise_events USING p_ebeln p_belnr p_docnum
                        CHANGING p_em_manq.

  DATA: w_objkey  LIKE sweinstcou-objkey,
        w_eventid LIKE swedumevid-evtid,
        w_ebeln LIKE ekko-ebeln,
        w_belnr LIKE rbkp-belnr,
        w_lifnr LIKE ekko-lifnr.

  SELECT lifnr FROM ekko INTO w_lifnr WHERE ebeln = p_ebeln.
    EXIT.
  ENDSELECT.

  CHECK sy-subrc = 0.

  MOVE p_docnum TO w_objkey.

  swc_set_element event_container 'Commande' p_ebeln.
  swc_set_element event_container 'Facture' p_belnr.
  swc_set_element event_container 'Fournisseur' w_lifnr.

  CALL FUNCTION 'SWE_EVENT_CREATE'
    EXPORTING
      objtype           = 'ZIDOINVOIC'
      objkey            = w_objkey
      event             = 'MissingReception'
    IMPORTING
      event_id          = w_eventid
    TABLES
      event_container   = event_container
    EXCEPTIONS
      objtype_not_found = 1
      OTHERS            = 2.

  CHECK w_eventid NE 0.

  p_em_manq = 'X'.

* Création du lien image <-> iDoc dans le cas d'une réception manquante
  CALL FUNCTION 'SWE_EVENT_CREATE'
    EXPORTING
      objtype           = 'IDOCINVOIC'
      objkey            = w_objkey
*CHG_01- / ITVLA      event             = 'InputErrorOccuredMM'
      event             = 'InputErrorOccurredMM'   "CHG_01+ / ITVLA
    IMPORTING
      event_id          = w_eventid
    EXCEPTIONS
      objtype_not_found = 1
      OTHERS            = 2.

ENDFORM.                    "f_200_raise_events

*&--------------------------------------------------------------------*
*&      Form  f_190_ctrle_em_2
*&--------------------------------------------------------------------*
*       text
*---------------------------------------------------------------------*
*      -->P_EBELN    text
*      -->P_EBELP    text
*      -->P_DOCNUM   text
*      -->P_BELNR    text
*      -->P_FACT_MENGtext
*      -->P_CMDE_MENGtext
*---------------------------------------------------------------------*
FORM f_190_ctrle_em_2 USING    p_ebeln TYPE ekpo-ebeln
                             p_ebelp TYPE ekpo-ebelp
                             p_docnum TYPE edidc-docnum
                             p_belnr TYPE edi_belnr
                             p_fact_menge TYPE zbalance_01-menge
                             p_cmde_menge TYPE zbalance_02-menge.

  DATA wt_ekbe TYPE TABLE OF ekbe.

  SELECT * FROM ekbe INTO TABLE wt_ekbe
  WHERE ebeln = p_ebeln
    AND ebelp = p_ebelp
    AND vgabe = '1'.

  IF LINES( wt_ekbe ) = 0.
    w_em_manq = 'X'.
    w_em_ebeln = p_ebeln.
    w_em_belnr = p_belnr.
*>> JMJ006 - Partial Receipt
  ELSE.
    CLEAR w_em_manq.
    w_em_ebeln = p_ebeln.
    w_em_belnr = p_belnr.
*<< JMJ006 - Partial Receipt
  ENDIF.

ENDFORM.                    " f_190_ctrle_em_2
*&---------------------------------------------------------------------*
*&      Form  archive_link
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_SAP_OBJECT  text
*      -->P_AR_OBJECT  text
*      -->P_AR_STATUS  text
*      -->P_IDOC_CONTROL  text
*      -->P_INVOICENUMBER  text
*----------------------------------------------------------------------*
FORM archive_link  USING    value(p_sap_object)
                            value(p_ar_object)
                            p_idoc_control LIKE edidc
                            value(p_invoicenumber)
                            value(p_fiscalyear)
                            value(p_compcode).
  DATA l_objkey LIKE bapibds01-objkey.
  DATA lt_property  TYPE STANDARD TABLE OF sdokpropty.
  DATA ls_property  TYPE sdokpropty.
  DATA l_archiv_id LIKE toaom-archiv_id.
  DATA ls_toacm TYPE toacm.
  DATA ls_edidc TYPE edidc.
  DATA ls_toaom TYPE toaom.
  DATA l_barcode LIKE bapibds01-barcode.
  DATA l_contrep LIKE bds_bar_ex-contrep.
  DATA l_log_sys LIKE bapibds01-log_system.
*..Recherche de la valeur du content repository - table TOAOM
  CLEAR l_archiv_id.
  SELECT SINGLE archiv_id FROM toaom INTO l_archiv_id
                      WHERE sap_object = p_sap_object       "'BUS2081'
                        AND ar_object = p_ar_object "'ZMMINVOICE'
                        AND ar_status = 'X'.

  MOVE l_archiv_id TO l_contrep.
*        READ TABLE idoc_contrl INTO ls_edidc INDEX 1.
  MOVE p_idoc_control-rcvprn TO l_log_sys.
  MOVE p_idoc_control-arckey TO l_barcode.

* alimentation de la structure S_TOACM pour garder l'enregistrement
*  dans la table BDS_BAR_EX et pour créer un enregistrement dans
*  la table BDS_BAR_IN
  IF p_compcode IS INITIAL.
    CONCATENATE p_invoicenumber p_fiscalyear INTO l_objkey.
  ELSE.
    CONCATENATE p_compcode p_invoicenumber p_fiscalyear INTO l_objkey.
  ENDIF.
  CLEAR ls_toacm.
  MOVE 'X' TO ls_toacm-keep_bar_i.
  MOVE 'X' TO ls_toacm-keep_bar_e.

  CLEAR ls_property.
  ls_property-name = 'BDS_DOCUMENTTYPE'.
  ls_property-value = p_ar_object.
  APPEND ls_property TO lt_property.

  CALL FUNCTION 'BDS_BARCODE_CREATE'
    EXPORTING
      barcode        = l_barcode
      log_system     = l_log_sys
      classname      = p_sap_object
      classtype      = 'BO'
      objkey         = l_objkey
      client         = sy-mandt
      s_toacm        = ls_toacm
    TABLES
      properties     = lt_property
    EXCEPTIONS
      error_barcode  = 1
      error_kpro     = 2
      internal_error = 3
      OTHERS         = 4.

  IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ELSE.
    COMMIT WORK AND WAIT.
  ENDIF.


ENDFORM.                    " archive_link
