************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Analyser l'iDoc facture MM                          *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Attila Kovacs (Netinside)                                   *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*			                                             *
* Ordre de transport: DE2K900720                                       *
*							           *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*& Auteur : Reynald GIL RGI001                                         *
*& Date   : 04.08.2007 - FI_305_DIA                                    *
*&        : Ajout Gestion Unité alternative                            *
************************************************************************

FUNCTION zbalance_invoic_get.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(X_RAPP) TYPE  C
*"  EXPORTING
*"     REFERENCE(E_TTC) TYPE  EDI_SUMME
*"     REFERENCE(E_TVA) TYPE  EDI_SUMME
*"     REFERENCE(E_DATUM) TYPE  EDIDAT8
*"     REFERENCE(E_SOCIETE) TYPE  EDI_ORGID
*"     REFERENCE(E_DEVISE) TYPE  CHAR3
*"     REFERENCE(E_REFERENCE) TYPE  EDI_BELNR
*"     REFERENCE(E_LIFNR) TYPE  C
*"  TABLES
*"      T_IDOC_DATA STRUCTURE  EDIDD
*"      T_FACTURES STRUCTURE  ZBALANCE_01
*"----------------------------------------------------------------------

* Flags dans t_factures
*   wt_factures-ident = '1': présent dans l'idoc
*   wt_factures-ident = '2': impossible à déterminer
*   wt_factures-ident = '3': commande inexistant
*   wt_factures-ident = '4': bon de livraison inexistant
*   wt_factures-ident = 'X': rapprochement effectué
*   wt_factures-ident = ' ': rapprochement échoué

* Zone de travail pour les postes de la commande
  DATA: ws_facture TYPE zbalance_01.
* Variables temporaires
  DATA: wl_idx     TYPE sy-tabix.
  DATA: wl_i       TYPE int4.

  CLEAR: t_factures[].

  DATA: wl_lifnr TYPE lfa1-lifnr.

* Lire le fournisseur (Provider Lieferant)
  DO.
    PERFORM f_120_recherche_segment TABLES t_idoc_data
      USING  'E1EDKA1' wl_idx 0 wl_idx e1edka1.
    IF wl_idx = 0.
      EXIT.
    ENDIF.
    CHECK e1edka1-parvw = 'LF'.
    wl_lifnr = e1edka1-partn.
    EXIT.
  ENDDO.

* Déterminer les postes
  DO.
    PERFORM f_120_recherche_segment TABLES t_idoc_data
      USING  'E1EDP01' wl_idx 0 wl_idx e1edp01.
    IF wl_idx = 0.
      EXIT.
    ENDIF.
    IF NOT ws_facture IS INITIAL.
      ws_facture-fin   = wl_idx.
      APPEND ws_facture TO t_factures.
      CLEAR ws_facture.
    ENDIF.
    ws_facture-debut = wl_idx.
    ws_facture-posex = e1edp01-posex.
    ws_facture-menge = e1edp01-menge.
    ws_facture-peinh = e1edp01-peinh.
    ws_facture-matnr = e1edp01-matnr.
*>> RGI008 - Balance V2 Unité de quantité
    ws_facture-bpumz = e1edp01-bpumz.
    ws_facture-bpumn = e1edp01-bpumn.
*>> RGI008
  ENDDO.
  IF NOT ws_facture IS INITIAL.
    APPEND ws_facture TO t_factures.
  ENDIF.

*"----------------------------------------------------------------------
* T. NGUYEN - 20/07/2006
* Rendre le rapprochement de factures sans postes possible
*
*  CHECK NOT ( t_factures[] IS INITIAL ).
  IF e_lifnr IS REQUESTED.
    e_lifnr = wl_lifnr.
  ENDIF.
*"----------------------------------------------------------------------

* Mieux limiter les segments E1EDP01
  LOOP AT t_factures INTO ws_facture.
    PERFORM f_150_delimite_e1edp01 TABLES t_idoc_data
                                   USING  ws_facture.
    MODIFY t_factures FROM ws_facture.
  ENDLOOP.

* Charger les données de comparaison
  LOOP AT t_factures INTO ws_facture.
    wl_idx = ws_facture-debut.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDP02' wl_idx ws_facture-fin wl_idx e1edp02.
      IF wl_idx = 0 OR
         ( ws_facture-fin NE 0 AND
           wl_idx GE ws_facture-fin ).
        EXIT.
      ENDIF.
      CASE e1edp02-qualf.
        WHEN '001'.
          MOVE: wl_idx TO ws_facture-pos_1.
          ws_facture-belnr = e1edp02-belnr.
          IF NOT ( e1edp02-belnr IS INITIAL ) AND
             NOT ( e1edp02-zeile IS INITIAL ).
            ws_facture-zeile = e1edp02-zeile.
            ws_facture-ident = '1'.
          ENDIF.
        WHEN '012'.
          ws_facture-verur = e1edp02-belnr.
          MOVE: wl_idx TO ws_facture-pos_2.
      ENDCASE.
    ENDDO.
    IF ws_facture-belnr IS INITIAL AND
       ws_facture-verur IS INITIAL.
      ws_facture-ident = '2'.
    ENDIF.

    wl_idx = ws_facture-debut.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDP26' wl_idx ws_facture-fin wl_idx e1edp26.
      IF wl_idx = 0 OR
         ( ws_facture-fin NE 0 AND
           wl_idx GE ws_facture-fin ).
        EXIT.
      ENDIF.
      CASE e1edp26-qualf.
        WHEN '002'.
          ws_facture-ttc   = e1edp26-betrg.
          ws_facture-betrg = e1edp26-betrg.
          EXIT.
      ENDCASE.
    ENDDO.

    wl_idx = ws_facture-debut.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDP19' wl_idx ws_facture-fin wl_idx e1edp19.
      IF wl_idx = 0 OR
         ( ws_facture-fin NE 0 AND
           wl_idx GE ws_facture-fin ).
        EXIT.
      ENDIF.
      CASE e1edp19-qualf.
        WHEN '002'.
          IF NOT e1edp19-ktext IS INITIAL AND
             ws_facture-ktext IS INITIAL.
            ws_facture-ktext = e1edp19-ktext.
          ENDIF.
          IF NOT e1edp19-ktext IS INITIAL AND
             ws_facture-maktx IS INITIAL.
            ws_facture-maktx = e1edp19-ktext.
          ENDIF.
          IF NOT e1edp19-idtnr IS INITIAL AND
             ws_facture-matnr IS INITIAL.
            ws_facture-matnr = e1edp19-idtnr.
          ENDIF.
      ENDCASE.
    ENDDO.


    wl_idx = ws_facture-debut.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDP04' wl_idx ws_facture-fin wl_idx e1edp04.
      IF wl_idx = 0 OR
         ( ws_facture-fin NE 0 AND
           wl_idx GE ws_facture-fin ).
        EXIT.
      ENDIF.

* T. NGUYEN 24072006
* Convertir en code TVA interne si le code TVA est un code externe
*
*      ws_facture-mwskz = e1edp04-mwskz.
      IF x_rapp = 'X'.
        PERFORM conv_code_tva_interne
          USING e1edp04-mwskz
                wl_lifnr
          CHANGING ws_facture-mwskz.
      ENDIF.
* T. NGUYEN 24072006

      ws_facture-mwsbt = e1edp04-mwsbt.
      CATCH SYSTEM-EXCEPTIONS arithmetic_errors = 1.
        ws_facture-ttc = ws_facture-ttc + ws_facture-mwsbt.
      ENDCATCH.
      EXIT.
    ENDDO.

    ws_facture-lifnr = wl_lifnr.

    IF NOT ( ws_facture-menge IS INITIAL ).
      CATCH SYSTEM-EXCEPTIONS arithmetic_errors = 1.
* L. Roussard, le 22.09.06
*Désactivation de la formule pour correction
*       ws_facture-netpr = ws_facture-betrg / ws_facture-menge .
* Correction de la formule du prix unitaire
*        ws_facture-netpr = ( ws_facture-betrg / ws_facture-menge *
*                            ws_facture-peinh ) .
*        Fin calcul du prix unitaire
*           22.09.06
*>> RGI001 Correction
        ws_facture-netpr =  ws_facture-betrg / ( ( ws_facture-menge / ws_facture-peinh )
                                                * ( ws_facture-bpumz / ws_facture-bpumn ) ).
      ENDCATCH.
    ENDIF.
    MODIFY t_factures FROM ws_facture.
  ENDLOOP.


* Lire la devise
  IF e_devise IS REQUESTED.
    wl_idx = 0.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDK01' wl_idx 0 wl_idx e1edk01.
      IF wl_idx = 0.
        EXIT.
      ENDIF.
      e_devise = e1edk01-curcy.
      EXIT.
    ENDDO.
  ENDIF.

* Lire les montants ( TTC et TVA )
  IF e_ttc IS REQUESTED OR
     e_tva IS REQUESTED.

    CLEAR: e_tva, e_ttc.

    wl_idx = 0.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDS01' wl_idx 0 wl_idx e1eds01.
      IF wl_idx = 0.
        EXIT.
      ENDIF.
      CASE e1eds01-sumid.
        WHEN '011'.
          e_ttc = e1eds01-summe.
          EXIT.
      ENDCASE.
    ENDDO.

    wl_idx = 0.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDS01' wl_idx 0 wl_idx e1eds01.
      IF wl_idx = 0.
        EXIT.
      ENDIF.
      CASE e1eds01-sumid.
        WHEN '010'.
          e_tva = e1eds01-summe.
          e_tva = e_ttc - e_tva.
          EXIT.
      ENDCASE.
    ENDDO.
    SHIFT e_ttc LEFT DELETING LEADING space.
    SHIFT e_tva LEFT DELETING LEADING space.
  ENDIF.

* Lire la date comptable
  IF e_datum IS REQUESTED.
    wl_idx = 0.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDK03' wl_idx 0 wl_idx e1edk03.
      IF wl_idx = 0.
        EXIT.
      ENDIF.
      CASE e1edk03-iddat.
        WHEN '016'.
          e_datum = e1edk03-datum.
          EXIT.
      ENDCASE.
    ENDDO.
  ENDIF.

* Lire la société
  IF e_societe IS REQUESTED.
    wl_idx = 0.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDK14' wl_idx 0 wl_idx e1edk14.
      IF wl_idx = 0.
        EXIT.
      ENDIF.
      CASE e1edk14-qualf.
        WHEN '011'.
          e_societe = e1edk14-orgid.
          EXIT.
      ENDCASE.
    ENDDO.
  ENDIF.
* Ajout JMB
* 20070214 Hans-Peter Schulz IT-Informatik  Begin
*    Umstellung auf logisches System
*    get company code from IDOC
*-----------------------------------------------------------------------
**  IF e_societe IS INITIAL.
**    SELECT * FROM  t076b UP TO 1 ROWS
**           WHERE  parart  = 'LI'
**           AND    konto   = wl_lifnr.
**
**    ENDSELECT.
**
**    IF sy-subrc NE 0.
**
**      CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
**        EXPORTING
**          input  = wl_lifnr
**        IMPORTING
**          output = wl_lifnr.
**      SELECT * FROM  t076b UP TO 1 ROWS
**            WHERE  parart  = 'LI'
**            AND    konto   = wl_lifnr.
**      ENDSELECT.
**      IF sy-subrc NE 0.
**          MESSAGE s398(00) WITH text-w47
**                                text-w42
**                                text-w48
**                                wl_lifnr.
**        ENDIF.
**    ENDIF.
**
**    e_societe = t076b-bukrs.
**  ENDIF.
*-----------------------------------------------------------------------
  DATA : st_e1edk14 LIKE e1edk14.

  IF e_societe IS INITIAL.
    LOOP AT t_idoc_data WHERE segnam = 'E1EDK14'.
      MOVE t_idoc_data-sdata TO st_e1edk14.
      CASE st_e1edk14-qualf.
        WHEN '011'.
          e_societe = st_e1edk14-orgid.
          EXIT.
      ENDCASE.
    ENDLOOP.
    IF sy-subrc <> 0.
      SELECT * FROM  t076b UP TO 1 ROWS
             WHERE  parart  = 'LI'
             AND    konto   = wl_lifnr.

      ENDSELECT.

      IF sy-subrc NE 0.

        CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
          EXPORTING
            input  = wl_lifnr
          IMPORTING
            output = wl_lifnr.
        SELECT * FROM  t076b UP TO 1 ROWS
              WHERE  parart  = 'LI'
              AND    konto   = wl_lifnr.
        ENDSELECT.
        IF sy-subrc NE 0.
          MESSAGE s398(00) WITH text-w47
                                text-w42
                                text-w48
                                wl_lifnr.
        ENDIF.
      ENDIF.

      e_societe = t076b-bukrs.
    ENDIF.
  ENDIF.
*-----------------------------------------------------------------------
* 20070214 Hans-Peter Schulz IT-Informatik  Ende

* Lire la référence fournisseur
  IF e_reference IS REQUESTED.
    wl_idx = 0.
    DO.
      PERFORM f_120_recherche_segment TABLES t_idoc_data
        USING  'E1EDK02' wl_idx 0 wl_idx  e1edk02.
      IF wl_idx = 0.
        EXIT.
      ENDIF.
      CASE e1edk02-qualf.
        WHEN '009'.
          e_reference = e1edk02-belnr.
          EXIT.
      ENDCASE.
    ENDDO.
  ENDIF.
ENDFUNCTION.

*&--------------------------------------------------------------------*
*&      Form  conv_code_tva_interne
*&--------------------------------------------------------------------*
*       text
*---------------------------------------------------------------------*
*      -->X_MWSKZ    text
*      -->X_NEW_MWSKZtext
*---------------------------------------------------------------------*
FORM conv_code_tva_interne
  USING x_mwskz TYPE c
        x_lifnr
  CHANGING x_new_mwskz TYPE c.

  DATA w_mwskz TYPE t076m-mwskz.

*LRO  : 29-09-06
*Compléter la requête en ajoutant une restriction sur le fournisseur

  SELECT SINGLE mwskz INTO w_mwskz FROM t076m
    WHERE mwart = x_mwskz
    AND konto = x_lifnr
    .
*Fin de la correction
*LRO

  IF sy-subrc = 0.
    x_new_mwskz = w_mwskz.
  ELSE.
    CLEAR x_new_mwskz.
  ENDIF.

ENDFORM.                    "conv_code_tva_interne
