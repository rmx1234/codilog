FUNCTION ZBALANCE_LISTPOSTBLO_QTE.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(I_BELNR) LIKE  RBKP-BELNR
*"     VALUE(I_GJAHR) LIKE  RBKP-GJAHR
*"     REFERENCE(I_LISTMODE) LIKE  SY-INDEX DEFAULT 1
*"  EXPORTING
*"     REFERENCE(E_DECISION) LIKE  SY-UCOMM
*"  TABLES
*"      TE_SELECTED_INVOICE_ITEMS STRUCTURE  RSEG_BUZEI
*"----------------------------------------------------------------------
************************************************************************
* Identification : ZBALANCE_LISTPOSTBLO_QTE		         *
*                                                                      *
*                                                                      *
*  Description de la fonction :                                        *
*  Cette fonction permet de rechercher l'écart de prix entre le poste  *
*  facture et le poste commande                                        *
*  En entrée: I_BELNR : Numéro de document de facturation              *
*             I_GJAHR : Exercice comptable                             *
*             I_LISTMODE : Type d'affichage                            *
*  En sortie: E_DECISION : Décision                                    *
*                                                                      *
**----------------------------------------------------------------------
*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur :  Frédérick HUYNH   (Netinside)                              *
*                                                                      *
* Date : 27/01/2004                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport:                                                  *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

* si I_LISTMODE = 1 => Affichage liste poste
* si I_LISTMODE = 2 => Validation Acheteur
* si I_LISTMODE = 3 => Validation Compta

check I_LISTMODE eq '1' or I_LISTMODE eq '2' or I_LISTMODE eq '3'.

w_belnr = i_belnr.
w_gjahr = i_gjahr.
w_listmode = i_listmode.
  clear rseg.
  select * from rseg where belnr = i_belnr
                       and gjahr = i_gjahr
                       and spgrp = 'X' ORDER BY PRIMARY KEY.

    move-corresponding rseg to t_item_qte.
    append t_item_qte.
  endselect.

  loop at t_item_qte.
call function 'ZBALANCE_RECHERCHE_ECARTQTE'
  exporting
    i_belnr        = i_belnr
    i_gjahr        = i_gjahr
    i_buzei        = t_item_qte-buzei
 IMPORTING
   E_EBELN        = t_item_qte-ebeln
   E_EBELP        = t_item_qte-ebelp
   E_WEMNG        = t_item_qte-wemng
   E_REMNG        = t_item_qte-remng
   E_DMENG        = t_item_qte-dmeng
   E_BSTME        = t_item_qte-bstme
 EXCEPTIONS
   NO_FOUND       = 1
   OTHERS         = 2
          .

   if sy-subrc <> 0.
      message id sy-msgid type sy-msgty number sy-msgno
              with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    else.
      modify t_item_qte.
    endif.

  endloop.

 CALL SCREEN 9201.
 E_DECISION = w_decision.
 loop at t_item_qte where sel = 'X'.
 TE_SELECTED_INVOICE_ITEMS-buzei = t_item_qte-buzei.
 append TE_SELECTED_INVOICE_ITEMS.
 endloop.

endfunction.
