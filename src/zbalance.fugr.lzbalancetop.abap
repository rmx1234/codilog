FUNCTION-POOL zbalance MESSAGE-ID zsapbalance.

INCLUDE <icon>.
INCLUDE <cntn01>.
INCLUDE zbalanceall01.

TYPE-POOLS: mrm, mmcr.

TABLES: e1edp01, e1edp02, e1edp04, *e1edp02, e1edka1,
        e1edp26, e1edp19, e1edk05, edidc, edids, edidd, *edidd,
        ekko, mara, makt, lfa1, *lfa1, likp, lips, ekpo, ekbe, rbkp,
        rbselbest, rbsellifs, t000, stacust, stalight,
        e1edk01, e1eds01, e1edk03, e1edk02, e1edk14, bkpf, vbsegs,
        t076m, t076b, zbal_cadd, zbal_cadd_unit, zbal_params, t001,
        zbal_rmsession, zbal_wfstart, zbal_idoc.



CONSTANTS: wc_msgid        TYPE syst-msgid VALUE 'ZSAPBALANCE',
           wc_memory_id_01(40) TYPE c      VALUE 'ZBALANCE_SET_BL',
           wc_memory_id_02(40) TYPE c      VALUE 'ZBALANCE_PREENR',
           wc_memory_id_03(40) TYPE c      VALUE 'ZBALANCE_FACTURE'.

TYPES:  BEGIN OF t_reschar,
           proba(5) TYPE p DECIMALS 2,
        END OF t_reschar.

TYPES:  tt_reschar  TYPE STANDARD TABLE OF t_reschar.

DATA:   wt_segnam TYPE HASHED TABLE OF idocsyn-segtyp
                  WITH UNIQUE KEY table_line.

* Variables pour comparaison de text
TYPES:  BEGIN OF t_result,
          posex TYPE e1edp01-posex,
          belnr TYPE e1edp02-belnr,
          zeile TYPE e1edp02-zeile,
          vbeln TYPE zbalance_01-vbeln,
          pourcent(5) TYPE p DECIMALS 2,
        END OF t_result.

TYPES:  tt_result TYPE STANDARD TABLE OF t_result.

TYPES:  BEGIN OF t_libs,
          belnr   TYPE zbalance_01-belnr,
          ktext   TYPE zbalance_01-ktext,
          ok      TYPE flag,
          t_bc    TYPE zbalance_02 OCCURS 0,
        END OF t_libs.
TYPES:  tt_libs TYPE STANDARD TABLE OF t_libs.

*** Déclaration données pour module fonction ZBALANCE_IDOC_DISPLAY
RANGES : r_credat FOR edidc-credat.

*** Déclaration données pour module fonction ZBALANCE_IDOC_PROCESS
DATA ls TYPE REF TO bd_ls_mon.
DATA istat TYPE bdmon_stat.
DATA ws_info TYPE bdii_typ.
DATA ws_docnums TYPE edi_docnum.
DATA l_lines TYPE int4.
DATA w_cpt_append TYPE sy-tfill.
DATA int_edids LIKE edids.

*** Déclaration pour MF 'ZBALANCE_MODIF_PCE_PRE_ENR'
DATA  BEGIN OF i_bdcdata OCCURS 0.
        INCLUDE STRUCTURE bdcdata.
DATA  END   OF i_bdcdata.

DATA  BEGIN OF it_bdcdata OCCURS 0.
        INCLUDE STRUCTURE bdcdata.
DATA  END   OF it_bdcdata.

* TBR 29102004 - Changement dans déclarations de l'article pour éviter
* le dump offset too large -> t_fact & t_cmde
*** Dynpro 9210
TYPES : BEGIN OF t_fact,
         status(4) TYPE c,
         postefact(10),
         bdl      TYPE likp-verur,
         numcmde(10),
         postecmde(10),
         article(18), " ex(15) TBR 29102004
         maktx(70),
         ktext(70),
         quantite TYPE ekpo-menge,
         condi TYPE ekpo-peinh,
         prixunit TYPE ekpo-netpr,
         totalht  TYPE ekpo-netwr,
         tva      TYPE ekpo-brtwr,
         codetva(2) TYPE c, "T. NGUYEN 27072006
         totalttc TYPE ekpo-brtwr,
         numcompt(15),
         clecompt(15),
         mark,               "TBR 03082004
       END OF t_fact.

TYPES : BEGIN OF t_header_vat,
           codetva(2) TYPE c,
           totalht  TYPE ekpo-netwr,
           tva      TYPE ekpo-brtwr,
         END OF t_header_vat.


TYPES : BEGIN OF t_cmde,
         rcpticon TYPE icon_d,
         cmde(10),
         poste(10),
         article(18), " ex(15) TBR 29102004
         designation(30),
         quantite TYPE ekpo-menge,
         condi TYPE ekpo-peinh,
         prixunit TYPE ekpo-netpr,
         totalht  TYPE ekpo-netwr,
         reste_q TYPE ekpo-menge,
         reste_m TYPE ekpo-netwr,
         codetva  TYPE t007a-mwskz,
       END OF t_cmde.

TYPES : BEGIN OF t_cadd,
         hkont LIKE zbal_cadd-hkont,
         alckz LIKE e1edk05-alckz,
         betrg LIKE e1edk05-betrg,
         kobtr LIKE e1edk05-kobtr,
         mwskz LIKE e1edk05-mwskz,
         msatz LIKE e1edk05-msatz,
         kostl LIKE zbal_cadd-kostl,
         pspnr LIKE zbal_cadd-pspnr,
         aufnr LIKE zbal_cadd-aufnr,
       END OF t_cadd.

TABLES: zbalance_04, *zbalance_04.
DATA  w_9210_init.
DATA  w_9210_okcode TYPE sy-ucomm.

DATA : w_tabix LIKE syst-tabix.


DATA : wt_alv_fact TYPE TABLE OF t_fact.
DATA : wt_alv_header_vat TYPE TABLE OF t_header_vat.
DATA : wa_alv_header_vat TYPE t_header_vat.
DATA : wt_alv_cmde TYPE TABLE OF t_cmde.
DATA : wt_alv_cmde_aff TYPE TABLE OF t_cmde.
DATA : wt_alv_cadd TYPE TABLE OF t_cadd.
DATA : wt_alv_cadd_save TYPE TABLE OF t_cadd.

DATA: wa_alv_cmde TYPE t_cmde.
DATA: ws_fact TYPE t_fact.
DATA: ws_cmde TYPE t_cmde.
DATA: ws_cadd TYPE t_cadd.

DATA : wt_sort_fact TYPE TABLE OF lvc_s_sort.
DATA : wt_sort_cmde TYPE TABLE OF lvc_s_sort.
DATA : wt_sort_cadd TYPE TABLE OF lvc_s_sort.
DATA : wt_catalog_fact TYPE TABLE OF lvc_s_fcat.
DATA : wt_catalog_cmde TYPE TABLE OF lvc_s_fcat.
DATA : wt_catalog_cadd TYPE TABLE OF lvc_s_fcat.

*** Ajout JMB pour Drag 'n' Drop
TYPES: BEGIN OF g_ty_s_test,
         select_amount     TYPE i,
         no_info_popup     TYPE char1,
         info_popup_once   TYPE char1,
         events_info_popup TYPE lvc_fname OCCURS 0,
         edit_type         TYPE i,
         edit_mode         TYPE i,
         edit_event        TYPE i,
         edit_fields       TYPE lvc_fname OCCURS 0,
         dragdrop_type     TYPE i,
         dragdrop_effect   TYPE i,
         dragdrop_fields   TYPE lvc_fname OCCURS 0,
         bypassing_buffer  TYPE char1,
         buffer_active     TYPE char1,
       END   OF g_ty_s_test.

DATA: gs_test TYPE g_ty_s_test.

CONSTANTS: con_exit TYPE sy-ucomm VALUE 'EXIT',
           con_canc TYPE sy-ucomm VALUE 'CANC',
           con_back TYPE sy-ucomm VALUE 'BACK',

           con_true TYPE char1 VALUE 'X',

           con_edit_grid         TYPE i VALUE 1,
           con_edit_column       TYPE i VALUE 2,
           con_edit_cell         TYPE i VALUE 3,

           con_edit_mode_display TYPE i VALUE 1,
           con_edit_mode_change  TYPE i VALUE 2,

           con_edit_event_enter  TYPE i VALUE 1,
           con_edit_event_modify TYPE i VALUE 2,

           con_source          TYPE i VALUE 1,
           con_target          TYPE i VALUE 2,

           con_dragdrop_row    TYPE i VALUE 1,
           con_dragdrop_column TYPE i VALUE 2,
           con_dragdrop_cell   TYPE i VALUE 3,

           c_699999            type char6 value '699999',
           c_scan              type char5 value '*SCAN',
           c_cont              type char5 value '*CONT',
           c_pstyp_9           type ekpo-pstyp value '9',    "FCH001+

           con_dragdrop_effect_move TYPE i VALUE 1,
           con_dragdrop_effect_copy TYPE i VALUE 2,
           con_dragdrop_effect_all  TYPE i VALUE 3.

DATA : o_dragdrop            TYPE REF TO cl_dragdrop.
DATA : o_dragdrop_background TYPE REF TO cl_dragdrop.

CLASS cl_gui_resources DEFINITION LOAD.

*---------------------------------------------------------------------*
*       CLASS lcl_dragdrop_obj DEFINITION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS lcl_dragdrop_obj DEFINITION.
  PUBLIC SECTION.
    DATA: line  TYPE t_cmde,
          index TYPE i.
ENDCLASS.                    "lcl_dragdrop_obj_d0100 DEFINITION
*** Fin ajout JMB

DATA : w_repid TYPE sy-repid,
       w_dynnr TYPE sy-dynnr.

* Clé d'objet pour l'affichage de l'image de la facture
DATA : w_object_id     LIKE toav0-object_id,
       w_archiv_doc_id LIKE sapb-sapadokid,
       w_archiv_id     LIKE toaar-archiv_id.
* Affichage/Modification sur 2XXX
DATA : w_modif_poss(10) TYPE c.

DATA:  w_lib1(15).
DATA:  w_lib2(16).

*---------------------------------------------------------------------*
*       CLASS cl_list DEFINITION
*---------------------------------------------------------------------*
CLASS cl_list DEFINITION INHERITING FROM cl_gui_alv_grid.
  PUBLIC SECTION.
    METHODS maj
      IMPORTING update_now TYPE c DEFAULT space
      EXPORTING is_changed TYPE c.
ENDCLASS.                    "cl_list DEFINITION

*---------------------------------------------------------------------*
*       CLASS cl_cmde DEFINITION
*---------------------------------------------------------------------*
CLASS cl_cmde DEFINITION INHERITING FROM cl_gui_alv_grid.
  PUBLIC SECTION.
    DATA: w_cmde TYPE ekpo-ebeln.
    DATA: w_bdl  TYPE ekpo-ebeln.
    METHODS show_cmde
      IMPORTING i_cmde TYPE ekpo-ebeln.
    METHODS show_bdl
      IMPORTING i_bdl  TYPE likp-verur.
ENDCLASS.                    "cl_cmde DEFINITION

DATA : o_fact_grid TYPE REF TO cl_list.
DATA : o_fact_container TYPE REF TO cl_gui_custom_container.
DATA : o_cmde_grid TYPE REF TO cl_cmde.
DATA : o_cmde_container TYPE REF TO cl_gui_custom_container.
DATA : o_cadd_grid TYPE REF TO cl_cmde.
DATA : o_cadd_container TYPE REF TO cl_gui_custom_container.

CLASS cl_event_receiver_fact DEFINITION DEFERRED.
CLASS cl_event_receiver_cmde DEFINITION DEFERRED.
CLASS cl_event_receiver_cadd DEFINITION DEFERRED.

DATA : o_event_receiver_fact TYPE REF TO cl_event_receiver_fact.
DATA : o_event_receiver_cmde TYPE REF TO cl_event_receiver_cmde.
DATA : o_event_receiver_cadd TYPE REF TO cl_event_receiver_cadd.

DATA : wa_alv_fact TYPE t_fact.
DATA : wa_alv_cadd TYPE t_cadd.
DATA : wa_oldcmde TYPE t_fact-numcmde,
       wa_newcmde LIKE wa_oldcmde.

*>> START RGI003 Adding flag change for HT Total
Data : w_manual_price.
*>> END   RGI003.


INCLUDE mrm_const_mrm.
INCLUDE mrm_const_common.

* Début - Dynpro 9200
DATA: w_belnr LIKE rbkp-belnr.
DATA: w_gjahr LIKE rbkp-gjahr.
DATA: w_listmode LIKE  sy-index.
DATA: w_decision LIKE  sy-ucomm.
DATA: w_answer(1).
DATA  w_9200_okcode TYPE sy-ucomm.
DATA: BEGIN OF t_item_prix OCCURS 100,
        sel(1),
        belnr TYPE belnr_d,
        gjahr TYPE gjahr,
        buzei TYPE rblgp,
        ebeln TYPE ebeln,
        ebelp TYPE ebelp,
        netpr TYPE bprei,
*          netpr2 type bprei,
        netpr2(15),
        dwert TYPE dwert,
        waers TYPE waers.
DATA: END OF t_item_prix.

*----------------------------------------------------------------------*
* ITGGH   16.04.2007      Display image
DATA: BEGIN OF w_idoc,
        docnum LIKE edids-docnum,
        credat LIKE edids-credat,
      END OF w_idoc,
      t_idoc LIKE TABLE OF w_idoc.
*----------------------------------------------------------------------*

*CONTROLS: item_prix TYPE TABLEVIEW USING SCREEN 4100.
* solution alv
* Types
TYPES: BEGIN OF tp_item_prix,
          sel(1),
        belnr TYPE belnr_d,
        gjahr TYPE gjahr,
        buzei TYPE rblgp,
        ebeln TYPE ebeln,
        ebelp TYPE ebelp,
        netpr TYPE bprei,
*          netpr2 type bprei,
        netpr2(15),
        dwert TYPE dwert,
        waers TYPE waers,
       END OF tp_item_prix.

* Class

DATA : o_prix_grid TYPE REF TO cl_list.
DATA : o_prix_container TYPE REF TO cl_gui_custom_container.


CLASS cl_event_receiver_prix DEFINITION DEFERRED.
DATA : event_receiver_prix TYPE REF TO cl_event_receiver_prix.

DATA : wt_alv_prix TYPE TABLE OF tp_item_prix.
DATA: ls_alv_prix LIKE LINE OF wt_alv_prix.
DATA : wt_sort_prix TYPE TABLE OF lvc_s_sort.
DATA : wt_catalog_prix TYPE TABLE OF lvc_s_fcat.
DATA: wt_row_prix TYPE TABLE OF lvc_s_row.
DATA: wt_roid_prix TYPE TABLE OF lvc_s_roid.


* Fin - Dynpro 9200

* Début - Dynpro 9201
*data: w_belnr like RBKP-BELNR.
*data: w_gjahr like RBKP-GJAHR.
*data: w_listmode LIKE  SY-INDEX.
*data: w_decision LIKE  SY-UCOMM.
*data: w_answer(1).
DATA  w_9201_okcode TYPE sy-ucomm.
DATA  w_9202_okcode TYPE sy-ucomm.
DATA: BEGIN OF t_item_qte OCCURS 100,
          sel(1),
        belnr TYPE belnr_d,
        gjahr TYPE gjahr,
        buzei TYPE rblgp,
        ebeln TYPE ebeln,
        ebelp TYPE ebelp,
        wemng TYPE wemng,
        remng TYPE remng,
        dmeng TYPE dmeng,
        bstme TYPE bstme.
DATA: END OF t_item_qte.

* Types
TYPES: BEGIN OF tp_item_qte,
          sel(1),
        belnr TYPE belnr_d,
        gjahr TYPE gjahr,
        buzei TYPE rblgp,
        ebeln TYPE ebeln,
        ebelp TYPE ebelp,
        wemng TYPE wemng,
        remng TYPE remng,
        dmeng TYPE dmeng,
        bstme TYPE bstme,
       END OF tp_item_qte.

* Class

DATA : o_qte_grid TYPE REF TO cl_list.
DATA : o_qte_container TYPE REF TO cl_gui_custom_container.


CLASS cl_event_receiver_qte DEFINITION DEFERRED.
DATA : event_receiver_qte TYPE REF TO cl_event_receiver_qte.

DATA : wt_alv_qte TYPE TABLE OF tp_item_qte.
DATA: ls_alv_qte LIKE LINE OF wt_alv_qte.
DATA : wt_sort_qte TYPE TABLE OF lvc_s_sort.
DATA : wt_catalog_qte TYPE TABLE OF lvc_s_fcat.
DATA: wt_row_qte TYPE TABLE OF lvc_s_row.
DATA: wt_roid_qte TYPE TABLE OF lvc_s_roid.


* Fin - Dynpro 4200


TABLES rseg.

* Dvt v1 - Datas pour ajout /suppression de lignes - TBR 24082004
DATA :  l_cpt_conso TYPE i, l_cpt_decompo TYPE i.

DATA  w_decompose.     "temoin poste splité
DATA  w_consolidation. "temoin poste consolidé
DATA  w_suppr.         "temoin poste supprimé
DATA : l_sup.
DATA : l_totalht  TYPE ekpo-netwr,
       l_tva      TYPE bseg-mwsts,
       l_quantite TYPE ekpo-menge,
       l_prixunitaire TYPE ekpo-netpr,
       l_prix_alvfact TYPE ekpo-netpr,
       l_prix_ekpo TYPE ekpo-netpr.

DATA  w_new_poste TYPE i . "nbre de postes supplémentaires.
DATA : l_value LIKE spop-varvalue1, l_answer(1).
DATA : l_poste TYPE i,
       l_belnr LIKE ekpo-ebeln,
       l_ktext LIKE ekpo-txz01,
       l_maktx LIKE ekpo-txz01,
       l_codetva LIKE bseg-mwskz. "code TVA

DATA l_trt(1).

DATA : w_diagnosetext(100).
DATA: w_docnum TYPE edidc-docnum,i_docnum TYPE  edi_docnum.

DATA : w_objecttype LIKE toav0-sap_object.

DATA : w_rm_dtct(1) TYPE c.
DATA : w_em_manq(1) TYPE c.
DATA : w_ep_dtct(1) TYPE c.
DATA : w_eq_dtct(1) TYPE c.
DATA : w_epq_force(1) TYPE c.

DATA : w_flag_integration(1) TYPE c.

DATA : w_em_ebeln LIKE ekko-ebeln.
DATA : w_em_belnr LIKE rbkp-belnr.

* Fin Dvt v1- Datas pour ajout /suppression de lignes - TBR 24082004
* Données pour WF - TBR 29092004
TABLES toa01.

DATA: BEGIN OF event_container OCCURS 0.
        INCLUDE STRUCTURE swcont.
DATA: END OF event_container.


DATA w_multi(1).

RANGES : r_seg FOR edidd-segnam.
*lecture de l'Idoc

DATA: BEGIN OF t_idoc_data OCCURS 10.
        INCLUDE STRUCTURE edidd.
DATA: END OF t_idoc_data.

DATA: wt_alv_sel TYPE lvc_t_row,
      wa_alv_sel TYPE lvc_s_row.

DATA: wl_ret      TYPE c.
DATA: wl_i        TYPE int4.
*DATA: wa_alv_fact TYPE t_fact.
DATA: wt_return   TYPE bal_t_msg.
DATA: w_dir(100).
DATA: wa_fieldcatalog TYPE lvc_s_fcat.

DATA: wl_kbetr   TYPE konp-kbetr.
DATA: wl_comp    TYPE konp-kbetr.
DATA: wl_land1   TYPE a003-aland.
DATA: wl_bukrs   TYPE t001-bukrs.
DATA: wl_test    TYPE konp-kbetr.

DATA : w_typef1 TYPE f.
DATA : w_typef2 TYPE f.

DATA wa_txtva(11).

DATA : w_subrc LIKE syst-subrc.
DATA : w_taux_tva TYPE p DECIMALS 2.



*** Données pour onglet sur écran de rapprochement
CONTROLS:  tabs TYPE TABSTRIP.
DATA:      ok_code LIKE sy-ucomm.
CONSTANTS: BEGIN OF c_tabs,
             tab1 LIKE sy-ucomm VALUE 'TS_HEAD',
             tab2 LIKE sy-ucomm VALUE 'TS_CADD',
           END OF c_tabs.
DATA:      BEGIN OF g_tabs,
             subscreen   LIKE sy-dynnr,
             prog        LIKE sy-repid VALUE 'SAPLZBALANCE',
             pressed_tab LIKE sy-ucomm VALUE c_tabs-tab1,
           END OF g_tabs.

DATA ws_prenerg type c.
data ws_prgr_liv(1).
data  calctva.
data : vfile like filename-fileextern.

*>>JMJ001  new logic Z_CONTROL_WORKFLOW_RBDMANI2
  DATA:    BEGIN OF xobjectconn OCCURS 1.
          INCLUDE STRUCTURE objectconn.
  DATA:    END OF xobjectconn.
  DATA : wt_worklist LIKE swr_wihdr OCCURS 0 WITH HEADER LINE.
  constants :  c_completed type swwwihead-wi_stat value 'COMPLETED',
               c_error     type swwwihead-wi_stat value 'ERROR',
               c_cancelled type swwwihead-wi_stat value 'CANCELLED'.
*<<JMJ001  new logic Z_CONTROL_WORKFLOW_RBDMANI2
