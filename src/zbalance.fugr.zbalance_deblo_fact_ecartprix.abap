FUNCTION zbalance_deblo_fact_ecartprix .
**&---------------------------------------------------------------------*
*& Modifications                                                       *
*&---------------------------------------------------------------------*
*& Project           : SGS-SAP Upgrade ECC5_TO_ECC6                    *
*& Author SGID       : A5916198                                        *
*& Author name       : Anuradha Gummaraju                              *
*& Modifications TAG : AGU001                                           *
*& Date              : 08.12.2010                                      *
*& QB DevT           : 6692                                            *
*& Panaya Task       : 26531                                         *
*& Description       : UPG_ECC5_TO_ECC6 Code Correction                *
*&---------------------------------------------------------------------*
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(I_BELNR) LIKE  RBKP-BELNR
*"     REFERENCE(I_GJAHR) LIKE  RBKP-GJAHR
*"  TABLES
*"      TI_SELECTED_INVOICE_ITEMS STRUCTURE  RSEG_BUZEI
*"----------------------------------------------------------------------

************************************************************************
* Identification : ZBALANCE_DEBLO_POST_ECARTPRIX             *
*                                                                      *
*                                                                      *
*  Description de la fonction :                                        *
*  Cette fonction permet de débloquer les postes d'un facture bloquée  *
*  pour un écart prix. La facture est débloquée si il n'éxiste pas     *
*  d'autres types d'écart                                              *
*  En entrée: I_BELNR : Numéro de document de facturation              *
*             I_GJAHR : Exercice comptable                             *
*             TI_SELECTED_INVOICE_ITEMS: listes des postes à débloquer *
**----------------------------------------------------------------------
*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur :  Frédérick HUYNH   (Netinside)                              *
*                                                                      *
* Date : 16/01/2004                                                    *
*                                                                      *
* Frequence :                                                          *
*                               *
* Ordre de transport:                                                  *
*                                    *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

*** Déclaration des données
  DATA:
   tab_ekgrp         LIKE TABLE OF ekko-ekgrp WITH HEADER LINE,
   tab_rseg          LIKE TABLE OF rseg WITH HEADER LINE,
   tab_rbco          LIKE TABLE OF rbco WITH HEADER LINE,
   tab_blockedprices LIKE TABLE OF rseg_buzei WITH HEADER LINE,
   tab_rbkp_blocked  TYPE  mrm_tab_rbkp_blocked WITH HEADER LINE,
   tab_rseg_old      LIKE mrmrseg OCCURS 10 WITH HEADER LINE,
   tab_rseg_new      LIKE mrmrseg OCCURS 10 WITH HEADER LINE,
   tab_rbco_equal    LIKE mrmrbco OCCURS 10,
   tab_rbma_equal    LIKE mrmrbma OCCURS 10,
   tab_rbtx_equal    LIKE mrmrbtx OCCURS 10,
   tab_rbvs_equal    LIKE mrmrbvs OCCURS 10,
   tab_rbws_equal    LIKE mrmrbws OCCURS 10,
   TAB_IVEXT         LIKE MRMIVEXT OCCURS 10,                  "NCF IV "INS AGU001
   s_rbkp_equal   LIKE rbkp,
   s_rbkp_blocked TYPE rbkp_blocked,
   s_rbkpv    TYPE mrm_rbkpv,
   s_t000     TYPE t000,
   s_accpmblk TYPE accpmblk,
   s_ekko    TYPE   ekko,

   f_exit                    TYPE boole-boole,
   f_icdtxt_incominginvoice  LIKE cdtxt OCCURS 10,
   f_lines_with_blockedprice LIKE sy-index,
   f_lines_for_delete        LIKE sy-index,
   f_objectid                LIKE  cdhdr-objectid,
   f_update_rbkp_blocked     TYPE c,
   f_zbdtoff                 LIKE accpmblk-zbdtoff.  " offset skonto

* are they any items for deleting the blocking reason price?
  DESCRIBE TABLE ti_selected_invoice_items LINES f_lines_for_delete.
  CHECK f_lines_for_delete > 0.

* read invoice header
  CALL FUNCTION 'MRM_DBTAB_RBKPV_READ'
       EXPORTING
            i_belnr       = i_belnr
            i_gjahr       = i_gjahr
            i_buffer_on   = space
       IMPORTING
            e_rbkpv       = s_rbkpv
       EXCEPTIONS
            error_message = 1.

  IF sy-subrc NE 0.
    MESSAGE s208(00) WITH 'Entry not found'(w11).
  ENDIF.

* only posted invoices
  CHECK s_rbkpv-rbstat = c_rbstat_correc
    OR  s_rbkpv-rbstat = c_rbstat_posted.

* authority-check for all purchasing groups
  CALL FUNCTION 'MRM_RSEG_READ_TO_INVOICE'
       EXPORTING
            i_belnr       = i_belnr
            i_gjahr       = i_gjahr
       TABLES
            t_rseg        = tab_rseg
            t_rbco        = tab_rbco
       EXCEPTIONS
            error_message = 1.

  IF sy-subrc <> 0.
    MESSAGE s208(00) WITH 'No item found'(w14).
  ENDIF.

  LOOP AT ti_selected_invoice_items.
    READ TABLE tab_rseg WITH KEY
                               belnr = i_belnr
                               gjahr = i_gjahr
                               buzei = ti_selected_invoice_items-buzei
.
    IF sy-subrc = 0.
      IF tab_rseg-spgrp IS INITIAL.
        f_exit = c_x.
        EXIT.
      ENDIF.
    ELSE.
      f_exit = c_x.
      EXIT.
    ENDIF.
  ENDLOOP.

* at least one wrong entry in table ti_selected_invoice_items => EXIT
  CHECK f_exit IS INITIAL.

  LOOP AT tab_rseg.
    CALL FUNCTION 'ME_EKKO_SINGLE_READ'
         EXPORTING
              pi_ebeln            = tab_rseg-ebeln
              pi_bypassing_buffer = c_x
         IMPORTING
              po_ekko             = s_ekko
         EXCEPTIONS
              error_message       = 1.

    IF sy-subrc = 0.
      MOVE s_ekko-ekgrp TO tab_ekgrp.
    ENDIF.
    COLLECT tab_ekgrp.

  ENDLOOP.

  LOOP AT tab_ekgrp.
    AUTHORITY-CHECK OBJECT 'M_RECH_EKG'
                   ID 'ACTVT' FIELD c_actvt_change
                   ID 'EKGRP' FIELD tab_ekgrp.

    IF sy-subrc <> 0.
      MESSAGE ID 'M8' TYPE 'E' NUMBER '383'
*                      WITH i_belnr i_gjahr tab_ekgrp.
                      WITH i_belnr tab_ekgrp.
    ENDIF.
  ENDLOOP.

* how many items with blocking reason price are in existance?
  LOOP AT tab_rseg.
    IF ( tab_rseg-belnr = i_belnr AND
         tab_rseg-gjahr = i_gjahr AND
         tab_rseg-spgrp = 'X' ).
      MOVE-CORRESPONDING tab_rseg TO tab_blockedprices.
      APPEND tab_blockedprices.
    ENDIF.
  ENDLOOP.

  DESCRIBE TABLE tab_blockedprices LINES f_lines_with_blockedprice.

* delete the wished blocking reasons price
*  CALL FUNCTION 'ZMM20SWF002' IN UPDATE TASK
  CALL FUNCTION 'ZBALANCE_DEBLO_POST_ECARTPRIX'
       EXPORTING
            i_belnr                   = i_belnr
            i_gjahr                   = i_gjahr
       TABLES
            ti_selected_invoice_items = ti_selected_invoice_items.

* is the blocking reason price deleted for all items?
  CLEAR: f_update_rbkp_blocked.
  IF f_lines_with_blockedprice = f_lines_for_delete.

* are there any blocked items left?
    SELECT * FROM rseg WHERE belnr = i_belnr "ex SELECT SINGLE
                                AND gjahr = i_gjahr
                                AND (   spgrm = 'X'
                                    OR  spgrt = 'X'
                                    OR  spgrg = 'X'
                                    OR  spgrv = 'X'
                                    OR  spgrq = 'X'
                                    OR  spgrs = 'X'
                                    OR  spgrc = 'X' ).
ENDSELECT.
    IF sy-subrc <> 0." ex SELECT SINGLE
      SELECT  * FROM rbkp_blocked INTO tab_rbkp_blocked
                  WHERE belnr = i_belnr
                  AND   gjahr = i_gjahr.

      ENDSELECT.
*{   REPLACE        MS4K912257                                        1
*\      CALL FUNCTION 'MRM_OFFSET_BASELINE_DATE'
*\           EXPORTING
*\                i_zfbdt   = s_rbkpv-zfbdt
*\                i_datum   = sy-datum
*\           IMPORTING
*\                e_zbdtoff = f_zbdtoff.

*>> START RGI CORRECTION
*>> NO DELAY FOR BLOCKING
F_ZBDTOFF = 0.
*}   INSERT
*}   REPLACE

      tab_rbkp_blocked-zbdtoff = f_zbdtoff.
      APPEND tab_rbkp_blocked.
      f_update_rbkp_blocked = c_delete.

*   release invoice
      CALL FUNCTION 'MRM_INVOICE_RELEASE_UPDATE'
           EXPORTING
                ti_rbkp_blocked = tab_rbkp_blocked[]
           IMPORTING
                te_rbkp_blocked = tab_rbkp_blocked[].
    ENDIF.
  ENDIF.

* update document changes
  MOVE i_belnr TO f_objectid.
  MOVE i_gjahr TO f_objectid+10.
  IF f_update_rbkp_blocked = c_delete.
    READ TABLE tab_rbkp_blocked INDEX 1.
    s_rbkp_blocked = tab_rbkp_blocked.
  ENDIF.

  LOOP AT ti_selected_invoice_items.
    tab_rseg_new-belnr = i_belnr.
    tab_rseg_new-gjahr = i_gjahr.
    tab_rseg_new-buzei = ti_selected_invoice_items-buzei.
    tab_rseg_new-spgrp = ' '.
    APPEND tab_rseg_new.
    tab_rseg_old-belnr = i_belnr.
    tab_rseg_old-gjahr = i_gjahr.
    tab_rseg_old-buzei = ti_selected_invoice_items-buzei.
    tab_rseg_old-spgrp = c_x.
    APPEND tab_rseg_old.
  ENDLOOP.

*  CALL FUNCTION 'INCOMINGINVOICE_WRITE_DOCUMENT'
CALL FUNCTION 'MRM_INVOICE_CHANGEDOC_WRITE'                  "NCF IV "INS AGU001
       EXPORTING
            objectid               = f_objectid
            tcode                  = sy-tcode
            utime                  = sy-uzeit
            udate                  = sy-datum
            username               = sy-uname
            n_rbkp                 = s_rbkp_equal
            o_rbkp                 = s_rbkp_equal
            n_rbkp_blocked         = s_rbkp_blocked
            o_rbkp_blocked         = s_rbkp_blocked
            upd_rbkp_blocked       = f_update_rbkp_blocked
            upd_rseg               = c_update
       TABLES
            icdtxt_incominginvoice = f_icdtxt_incominginvoice
            xrbco                  = tab_rbco_equal
            yrbco                  = tab_rbco_equal
            xrbma                  = tab_rbma_equal
            yrbma                  = tab_rbma_equal
            xrbtx                  = tab_rbtx_equal
            yrbtx                  = tab_rbtx_equal
            xrbvs                  = tab_rbvs_equal
            yrbvs                  = tab_rbvs_equal
            xrbws                  = tab_rbws_equal
            yrbws                  = tab_rbws_equal
            xrseg                  = tab_rseg_new
            yrseg                  = tab_rseg_old.

  COMMIT WORK AND WAIT.




ENDFUNCTION.
