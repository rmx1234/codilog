*----------------------------------------------------------------------*
*   INCLUDE LZBALANCEF03                                               *
*----------------------------------------------------------------------*
* Auteur                : R.GIL
* Date                  : 11.04.2008
* MODIFICATIONS         : FI_414_DIA - RGI011
* Description           : Image Ling when idoc status 73
*---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& Modifications                                                       *
*&---------------------------------------------------------------------*
*& Project           : SGS-SAP Upgrade ECC5_TO_ECC6                    *
*& Author SGID       : T3560272                                        *
*& Author name       : Tharani Kanagarajan                             *
*& Modifications TAG : TKA001                                          *
*& Date              : 09.11.2010                                      *
*& QB DevT           : 6692                                            *
*& Panaya Task       : 193 / 264 / 451                                 *
*& Description       : UPG_ECC5_TO_ECC6 Code Correction                *
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*& Author SGID        : S2297242                                       *
*& Author name        : Sivamurugan Varadharajan                       *
*& Modifications TAG  : SVA001                                         *
*& Date               : 15-Nov-2016                                    *
*& SNNOW #            : DevT0023691                                    *
*& Description        : EHP8-Upgrade dump issue.                       *
*&                      Value Zero has been assigned to a variable to  *
*&                      be used in SHIFT delete statement .            *
*&---------------------------------------------------------------------*
CLASS cl_list IMPLEMENTATION.
  METHOD maj.
    DATA: wt_loc TYPE lvc_t_moce.
    DATA: wa_loc TYPE LINE OF lvc_t_moce.
    DATA: wa_fact TYPE t_fact.
    FIELD-SYMBOLS <fs> TYPE ANY.
*
    is_changed = space.
    CALL METHOD get_modified_cells
      IMPORTING
        cell_table = wt_loc.
    READ TABLE wt_loc INDEX 1 TRANSPORTING NO FIELDS.
    CHECK sy-subrc EQ 0.
    is_changed = 'X'.
    CHECK update_now = 'X'.
    LOOP AT wt_loc INTO wa_loc.
      READ TABLE wt_alv_fact INTO wa_fact INDEX wa_loc-row_id.
      ASSIGN COMPONENT wa_loc-col_id OF STRUCTURE wa_fact TO <fs>.
      <fs> = wa_loc-value.
      MODIFY wt_alv_fact FROM wa_fact INDEX wa_loc-row_id.
    ENDLOOP.
  ENDMETHOD.                    "maj
ENDCLASS.                    "cl_list IMPLEMENTATION

*---------------------------------------------------------------------*
*       CLASS cl_cmde IMPLEMENTATION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_cmde IMPLEMENTATION.
  METHOD show_cmde.
    DATA: ws_cmde TYPE t_cmde.
    DATA: wt_dynpread TYPE STANDARD TABLE OF dynpread.
    DATA: ws_dynpread TYPE dynpread.
    DATA: w_montant  TYPE wrbtr.
    DATA: w_quantite TYPE menge_d.

    IF w_cmde NE i_cmde.
      w_cmde = i_cmde.

      CLEAR: wt_alv_cmde[], ekko, *lfa1, wt_dynpread[].

* Mise en forme avec 10 positions TBR 11102004
      CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
        EXPORTING
          input  = w_cmde
        IMPORTING
          output = w_cmde.


      SELECT SINGLE * FROM ekko INTO ekko WHERE ebeln = w_cmde.
      CHECK sy-subrc EQ 0.
      ekbe-xblnr = ekko-ebeln.
      CLEAR w_bdl.
      SELECT * FROM  ekpo INTO ekpo
        WHERE ebeln = ekko-ebeln
        ORDER BY ebelp.
        IF sy-subrc = 0.
          SHIFT ekpo-matnr LEFT DELETING LEADING space.
          SHIFT ekpo-matnr LEFT DELETING LEADING '0'.
          PERFORM f_180_reste_fact USING
             ekpo-ebeln
             ekpo-ebelp
             w_montant
             w_quantite
             ws_cmde-rcpticon.


          MOVE: ekpo-ebeln TO ws_cmde-cmde,
                ekpo-ebelp TO ws_cmde-poste,
                ekpo-matnr TO ws_cmde-article,
                ekpo-txz01 TO ws_cmde-designation,
                ekpo-menge TO ws_cmde-quantite,
                ekpo-peinh TO ws_cmde-condi,
                ekpo-netpr TO ws_cmde-prixunit,
                ekpo-brtwr TO ws_cmde-totalht,
                w_montant  TO ws_cmde-reste_m,
                w_quantite TO ws_cmde-reste_q.
          APPEND ws_cmde TO wt_alv_cmde.
          CLEAR: ws_cmde, w_montant, w_quantite.
        ENDIF.
      ENDSELECT.

      wt_alv_cmde_aff[] = wt_alv_cmde[].
      PERFORM cache_item_rapproches.
      PERFORM cache_reste_a_calcul_ooo.

      IF sy-subrc NE 0.
        MESSAGE s533(0u)
        WITH
 'Aucune entrée correspondante dans table Poste document d''achat'(w37)
   'pour Num du doc d''achat'(w39) ekko-ebeln.
      ENDIF.


      CALL METHOD refresh_table_display.
      SELECT SINGLE * FROM lfa1 INTO *lfa1 WHERE lifnr = ekko-lifnr.
      IF sy-subrc = 0.
        w_lib1(15) = 'N° de commande'(d01).
        w_lib2(15) = 'Date commande'(d02).

        CLEAR ws_dynpread.
        ws_dynpread-fieldname = 'W_LIB1'.
        ws_dynpread-stepl = ''.
        ws_dynpread-fieldvalue = w_lib1.
        ws_dynpread-fieldinp = 'X'.
        APPEND ws_dynpread TO  wt_dynpread.
        CLEAR ws_dynpread.
        ws_dynpread-fieldname = 'W_LIB2'.
        ws_dynpread-stepl = ''.
        ws_dynpread-fieldvalue = w_lib2.
        ws_dynpread-fieldinp = 'X'.
        APPEND ws_dynpread TO  wt_dynpread.
        CLEAR ws_dynpread.
        ws_dynpread-fieldname = 'EKBE-XBLNR'.
        ws_dynpread-stepl = ''.
        ws_dynpread-fieldvalue = ekbe-xblnr.
        ws_dynpread-fieldinp = 'X'.
        APPEND ws_dynpread TO  wt_dynpread.
        ws_dynpread-fieldname = '*LFA1-LIFNR'.
        ws_dynpread-fieldvalue = *lfa1-lifnr.
        APPEND ws_dynpread TO  wt_dynpread.
        ws_dynpread-fieldname = 'EKKO-BEDAT'.
        WRITE:  ekko-bedat TO ws_dynpread-fieldvalue.
        APPEND ws_dynpread TO  wt_dynpread.
        ws_dynpread-fieldname = 'EKKO-BUKRS'.
        ws_dynpread-fieldvalue = ekko-bukrs.
        APPEND ws_dynpread TO  wt_dynpread.
        ws_dynpread-fieldname = '*LFA1-NAME1'.
        ws_dynpread-fieldvalue = *lfa1-name1.
        APPEND ws_dynpread TO wt_dynpread.

        CALL FUNCTION 'DYNP_UPDATE_FIELDS'
          EXPORTING
            dyname               = w_repid
            dynumb               = w_dynnr
            request              = 'A'
          TABLES
            dynpfields           = wt_dynpread
          EXCEPTIONS
            invalid_abapworkarea = 1
            invalid_dynprofield  = 2
            invalid_dynproname   = 3
            invalid_dynpronummer = 4
            invalid_request      = 5
            no_fielddescription  = 6
            undefind_error       = 7
            OTHERS               = 8.
        CHECK sy-subrc EQ 0.
      ELSE.
        MESSAGE s208(00) WITH 'Aucune entrée dans la table LFA1'(w40).
      ENDIF.
    ENDIF.
  ENDMETHOD.                    "show_cmde

  METHOD show_bdl.
    DATA: ws_cmde TYPE t_cmde.
    DATA: wt_dynpread TYPE STANDARD TABLE OF dynpread.
    DATA: ws_dynpread TYPE dynpread.
    DATA: w_montant  TYPE wrbtr.
    DATA: w_quantite TYPE menge_d.

    IF w_bdl NE i_bdl.
      w_bdl = i_bdl.

      CLEAR: wt_alv_cmde[], ekko, *lfa1, wt_dynpread[].

      MOVE i_bdl TO ekbe-xblnr.
      SELECT DISTINCT ebeln ebelp FROM ekbe
                                  INTO (ekpo-ebeln,ekpo-ebelp)
                                 WHERE xblnr EQ ekbe-xblnr
                                   AND vgabe EQ '1'
                                   AND bwart EQ '101'
                                 ORDER BY ebeln ebelp.
        SELECT SINGLE * FROM ekpo INTO ekpo
                       WHERE ebeln EQ ekpo-ebeln
                         AND ebelp EQ ekpo-ebelp.
        SHIFT ekpo-matnr LEFT DELETING LEADING space.
        SHIFT ekpo-matnr LEFT DELETING LEADING '0'.
        PERFORM f_180_reste_fact USING
           ekpo-ebeln
           ekpo-ebelp
           w_montant
           w_quantite
           ws_cmde-rcpticon.
        MOVE: ekpo-ebeln TO ws_cmde-cmde,
              ekpo-ebelp TO ws_cmde-poste,
              ekpo-matnr TO ws_cmde-article,
              ekpo-txz01 TO ws_cmde-designation,
              ekpo-menge TO ws_cmde-quantite,
              ekpo-netpr TO ws_cmde-prixunit,
              ekpo-brtwr TO ws_cmde-totalht,
              w_montant  TO ws_cmde-reste_m,
              w_quantite TO ws_cmde-reste_q,
              ekpo-mwskz TO ws_cmde-codetva.
        APPEND ws_cmde TO wt_alv_cmde.
        CLEAR: ws_cmde, w_montant, w_quantite.
      ENDSELECT.
      IF sy-subrc EQ 0.
        SELECT SINGLE * FROM ekko INTO ekko WHERE ebeln = ekpo-ebeln.
        IF sy-subrc = 0.
          SELECT SINGLE * FROM lfa1 INTO *lfa1 WHERE lifnr = ekko-lifnr.
        ELSE.
          MESSAGE s532(0u)
          WITH 	'Aucune entrée correspondante dans la table'(w47)
                 'En-tête document d''achat'(w41).
        ENDIF.
      ELSE.
        CLEAR: ekko, ekpo, lfa1.
      ENDIF.
      CLEAR w_cmde.

      CALL METHOD refresh_table_display.


      w_lib1 = 'N° de bon livr.'(d03).
      w_lib2 = 'Date commande'(d02).
      CLEAR ws_dynpread.
      ws_dynpread-fieldname = 'W_LIB1'.
      ws_dynpread-stepl = ''.
      ws_dynpread-fieldvalue = w_lib1.
      ws_dynpread-fieldinp = 'X'.
      APPEND ws_dynpread TO  wt_dynpread.
      CLEAR ws_dynpread.
      ws_dynpread-fieldname = 'W_LIB2'.
      ws_dynpread-stepl = ''.
      ws_dynpread-fieldvalue = w_lib2.
      ws_dynpread-fieldinp = 'X'.
      APPEND ws_dynpread TO  wt_dynpread.
      CLEAR ws_dynpread.
      ws_dynpread-fieldname = 'EKBE-XBLNR'.
      ws_dynpread-stepl = ''.
      ws_dynpread-fieldvalue = ekbe-xblnr.
      ws_dynpread-fieldinp = 'X'.
      APPEND ws_dynpread TO  wt_dynpread.
      ws_dynpread-fieldname = '*LFA1-LIFNR'.
      ws_dynpread-fieldvalue = *lfa1-lifnr.
      APPEND ws_dynpread TO  wt_dynpread.
      ws_dynpread-fieldname = 'EKKO-BEDAT'.
      WRITE:  ekko-bedat TO ws_dynpread-fieldvalue.
      APPEND ws_dynpread TO  wt_dynpread.
      ws_dynpread-fieldname = 'EKKO-BUKRS'.
      ws_dynpread-fieldvalue = ekko-bukrs.
      APPEND ws_dynpread TO  wt_dynpread.
      ws_dynpread-fieldname = '*LFA1-NAME1'.
      ws_dynpread-fieldvalue = *lfa1-name1.
      APPEND ws_dynpread TO wt_dynpread.

      CALL FUNCTION 'DYNP_UPDATE_FIELDS'
        EXPORTING
          dyname               = w_repid
          dynumb               = w_dynnr
          request              = 'A'
        TABLES
          dynpfields           = wt_dynpread
        EXCEPTIONS
          invalid_abapworkarea = 1
          invalid_dynprofield  = 2
          invalid_dynproname   = 3
          invalid_dynpronummer = 4
          invalid_request      = 5
          no_fielddescription  = 6
          undefind_error       = 7
          OTHERS               = 8.
      CHECK sy-subrc EQ 0.
    ENDIF.
  ENDMETHOD.                    "show_bdl

ENDCLASS.                    "cl_cmde IMPLEMENTATION

*&---------------------------------------------------------------------*
*&      Form  f1_dynpro
*&---------------------------------------------------------------------*
* Table interne pour alimenter zone pour call transaction
*----------------------------------------------------------------------*
*      --> dynbegin : Lancement BDC d'un dynpro
*      --> name     : Pool de modules BDC
*      --> value    : Numéro de dynpro BDC
*----------------------------------------------------------------------*
FORM f1_dynpro USING f_dynbegin
                      f_name
                      f_value.

  IF f_dynbegin = 'X'.
    MOVE : f_name  TO i_bdcdata-program,
           f_value TO i_bdcdata-dynpro,
           'X'     TO i_bdcdata-dynbegin.
    APPEND i_bdcdata.
    CLEAR  i_bdcdata.
  ELSE.
    MOVE : f_name  TO i_bdcdata-fnam,
           f_value TO i_bdcdata-fval.
    APPEND i_bdcdata.
    CLEAR  i_bdcdata.
  ENDIF.

ENDFORM.                                                    " f1_dynpro

************************************************************************
* ROUTINES                                                             *
************************************************************************

*---------------------------------------------------------------------*
*       CLASS lcl_event_receiver DEFINITION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_fact DEFINITION.

  PUBLIC SECTION.
    METHODS:

    handle_user_command
        FOR EVENT user_command OF cl_gui_alv_grid
            IMPORTING e_ucomm,

    handle_toolbar
        FOR EVENT toolbar OF cl_gui_alv_grid
            IMPORTING e_object e_interactive,

    handle_double_click
        FOR EVENT double_click OF cl_gui_alv_grid
            IMPORTING e_row e_column,

    handle_hotspot_click
        FOR EVENT hotspot_click OF cl_gui_alv_grid
            IMPORTING e_row_id es_row_no e_column_id,

    onf4
        FOR EVENT onf4 OF cl_gui_alv_grid
            IMPORTING e_fieldname,

*** Test JMB Drag 'n' Drop
* TARGET
    ondrop               FOR EVENT ondrop
                         OF cl_gui_alv_grid
                         IMPORTING e_row
                                   e_column
                                   es_row_no
                                   e_dragdropobj.
*** Fin test JMB Deag 'n' Drop

  PRIVATE SECTION.

ENDCLASS.                    "cl_event_receiver_fact DEFINITION
*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_cmde DEFINITION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_cmde DEFINITION.

  PUBLIC SECTION.
    METHODS:

    handle_double_click
        FOR EVENT double_click OF cl_gui_alv_grid
            IMPORTING e_row e_column.
    METHODS:
        handle_hotspot_click
            FOR EVENT hotspot_click OF cl_gui_alv_grid
                IMPORTING e_row_id e_column_id,

*** Test JMB Drag 'n' Drop
* SOURCE
    ondrag               FOR EVENT ondrag
                         OF cl_gui_alv_grid
                         IMPORTING e_row
                                   e_column
                                   es_row_no
                                   e_dragdropobj,

    ondropcomplete       FOR EVENT ondropcomplete
                         OF cl_gui_alv_grid
                         IMPORTING e_row
                                   e_column
                                   es_row_no
                                   e_dragdropobj.
*** Fin test JMB Deag 'n' Drop

  PRIVATE SECTION.

ENDCLASS.                    "cl_event_receiver_cmde DEFINITION
*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_cadd DEFINITION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_cadd DEFINITION.

  PUBLIC SECTION.
    METHODS:

  handle_user_command
      FOR EVENT user_command OF cl_gui_alv_grid
          IMPORTING e_ucomm,

  handle_toolbar
      FOR EVENT toolbar OF cl_gui_alv_grid
          IMPORTING e_object e_interactive.

  PRIVATE SECTION.

ENDCLASS.                    "cl_event_receiver_cadd DEFINITION

*---------------------------------------------------------------------*
*       CLASS lcl_event_receiver_fact IMPLEMENTATION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_fact IMPLEMENTATION.

  METHOD handle_double_click.

    DATA: wa_alv_fact TYPE t_fact.

    READ TABLE wt_alv_fact INTO wa_alv_fact INDEX e_row.

    CHECK sy-subrc EQ 0.

    CASE e_column.
      WHEN 'ARTICLE'.
        PERFORM f_aff_article USING wa_alv_fact-article
                                    wa_alv_fact-postefact.
      WHEN 'NUMCMDE'.
        CALL METHOD o_cmde_grid->show_cmde
          EXPORTING
            i_cmde = wa_alv_fact-numcmde.

      WHEN 'BDL'.
        CALL METHOD o_cmde_grid->show_bdl
          EXPORTING
            i_bdl = wa_alv_fact-bdl.
    ENDCASE.

    wa_alv_fact-mark = 'X'.             " Mark pour suppr TBR 010904
    MODIFY wt_alv_fact FROM wa_alv_fact INDEX e_row.
    "                 TBR 010904
    LOOP AT wt_alv_fact INTO wa_alv_fact.
      IF sy-tabix NE e_row.
        wa_alv_fact-mark = space.
        MODIFY wt_alv_fact FROM wa_alv_fact.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.                           "handle_double_click

  METHOD handle_hotspot_click.
*    DATA: wt_rows TYPE lvc_t_roid.
*    APPEND es_row_no TO wt_rows.
*
*    CALL METHOD o_fact_grid->set_selected_rows
*       EXPORTING
*          it_row_no  = wt_rows.

    DATA: wa_alv_fact TYPE t_fact.
    READ TABLE wt_alv_fact INTO wa_alv_fact INDEX e_row_id.
    CHECK sy-subrc EQ 0.

    CALL METHOD o_cmde_grid->show_cmde
      EXPORTING
        i_cmde = wa_alv_fact-numcmde.
  ENDMETHOD.                           "handle_double_click

  METHOD handle_user_command.
    PERFORM f_2xxx_user_command USING e_ucomm.
  ENDMETHOD.                           "handle_user_command

  METHOD handle_toolbar.
    DATA: ls_toolbar  TYPE stb_button.

    CLEAR ls_toolbar.
    MOVE 3 TO ls_toolbar-butn_type.
    APPEND ls_toolbar TO e_object->mt_toolbar.

    CLEAR ls_toolbar.
    MOVE 'CTRL' TO ls_toolbar-function.
    MOVE icon_check  TO ls_toolbar-icon.
    MOVE 'Contrôler la facture'(b01) TO ls_toolbar-quickinfo.
    MOVE 'Contrôle de la facture'(b02) TO ls_toolbar-text.
    MOVE ' ' TO ls_toolbar-disabled.
    APPEND ls_toolbar TO e_object->mt_toolbar.

    CLEAR ls_toolbar.
    MOVE 3 TO ls_toolbar-butn_type.
    APPEND ls_toolbar TO e_object->mt_toolbar.

*   Affichage des boutons de modification de ligne
*   si le user a les droits
    AUTHORITY-CHECK OBJECT 'ZBAL_ACT'
                   ID 'BAL_ACTION' FIELD 'UPDT'.
    IF sy-subrc = 0.
      CLEAR ls_toolbar.
      MOVE 'UPD' TO ls_toolbar-function.
      MOVE icon_change_text  TO ls_toolbar-icon.
      MOVE text-b11 TO ls_toolbar-quickinfo.
      MOVE ' ' TO ls_toolbar-disabled.
      APPEND ls_toolbar TO e_object->mt_toolbar.

      CLEAR ls_toolbar.
      MOVE 'ADD' TO ls_toolbar-function.
      MOVE icon_insert_row  TO ls_toolbar-icon.
      MOVE text-b03 TO ls_toolbar-quickinfo.
      MOVE ' ' TO ls_toolbar-disabled.
      APPEND ls_toolbar TO e_object->mt_toolbar.

      CLEAR ls_toolbar.
      MOVE 'DEL' TO ls_toolbar-function.
      MOVE icon_delete_row  TO ls_toolbar-icon.
      MOVE text-b05 TO ls_toolbar-quickinfo.
      MOVE ' ' TO ls_toolbar-disabled.
      APPEND ls_toolbar TO e_object->mt_toolbar.
    ENDIF.

  ENDMETHOD.                    "handle_toolbar

  METHOD onf4.
*REGISTER_F4_FOR_FIELDS
    CASE e_fieldname.
      WHEN 'CODETVA'.


    ENDCASE.
  ENDMETHOD.                           "handle_double_click

*** TEST JMB Drag 'n' Drop
* TARGET
  METHOD ondrop.

    PERFORM event_ondrop USING e_row
                               e_column
                               e_dragdropobj.

  ENDMETHOD.                    "ondrop
*** Fin

ENDCLASS.                    "cl_event_receiver_fact IMPLEMENTATION
*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_cmde IMPLEMENTATION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_cmde IMPLEMENTATION.

  METHOD handle_double_click.

    REFRESH wt_alv_sel.

    READ TABLE wt_alv_cmde_aff INTO ws_cmde INDEX e_row.
*   READ TABLE wt_alv_cmde INTO ws_cmde INDEX e_row.
    CHECK sy-subrc EQ 0.

    CASE e_column.
      WHEN 'ARTICLE'.
        mara-matnr = ws_cmde-article.
        SET PARAMETER ID 'MAT' FIELD mara-matnr.
        SET PARAMETER ID 'LIF' FIELD zbalance_04-lifnr.
        CALL TRANSACTION 'ME13' AND SKIP FIRST SCREEN.
      WHEN 'CMDE'.
        CALL METHOD o_cmde_grid->show_cmde
          EXPORTING
            i_cmde = ws_cmde-cmde.
    ENDCASE.
  ENDMETHOD.                           "handle_double_click

  METHOD handle_hotspot_click.


  ENDMETHOD.                           "handle_double_click

*** TEST JMB Drag 'n' Drop
* SOURCE
  METHOD ondrag.
    PERFORM event_ondrag USING e_row
                               e_column
                               e_dragdropobj.
  ENDMETHOD.                    "ondrag


  METHOD ondropcomplete.
    PERFORM ondropcomplete.
  ENDMETHOD.                    "ondropcomplete


ENDCLASS.                    "cl_event_receiver_cmde IMPLEMENTATION
*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_cadd IMPLEMENTATION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_cadd IMPLEMENTATION.

  METHOD handle_user_command.
    PERFORM f_2xxx_user_command USING e_ucomm.
  ENDMETHOD.                           "handle_user_command

  METHOD handle_toolbar.
    DATA: ls_toolbar  TYPE stb_button.

    MOVE 'CADD_CHG' TO ls_toolbar-function.
    MOVE icon_change_text  TO ls_toolbar-icon.
    MOVE text-b11 TO ls_toolbar-quickinfo.
    MOVE ' ' TO ls_toolbar-disabled.
    APPEND ls_toolbar TO e_object->mt_toolbar.

    CLEAR ls_toolbar.
    MOVE 'CADD_ADD' TO ls_toolbar-function.
    MOVE icon_insert_row  TO ls_toolbar-icon.
    MOVE text-b09 TO ls_toolbar-quickinfo.
    MOVE ' ' TO ls_toolbar-disabled.
    APPEND ls_toolbar TO e_object->mt_toolbar.

    CLEAR ls_toolbar.
    MOVE 'CADD_DEL' TO ls_toolbar-function.
    MOVE icon_delete_row  TO ls_toolbar-icon.
    MOVE text-b10 TO ls_toolbar-quickinfo.
    MOVE ' ' TO ls_toolbar-disabled.
    APPEND ls_toolbar TO e_object->mt_toolbar.
  ENDMETHOD.                    "handle_toolbar

ENDCLASS.                    "cl_event_receiver_cadd IMPLEMENTATION

*&---------------------------------------------------------------------*
*&      Form  toolbar_params_for_alv
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM toolbar_params USING p_type_liste
                    CHANGING pt_exclude TYPE ui_functions.


  DATA ls_exclude TYPE ui_func.

  ls_exclude = cl_gui_alv_grid=>mc_fc_auf.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_average.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_back_classic.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_abc.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_chain.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_crbatch.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_crweb.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_lineitems.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_master_data.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_more.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_report.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_xint.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_call_xxl.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_check.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_col_invisible.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_col_optimize.
  APPEND ls_exclude TO pt_exclude.

  IF p_type_liste NE 'FACT'.
    ls_exclude = cl_gui_alv_grid=>mc_fc_current_variant.
    APPEND ls_exclude TO pt_exclude.

    ls_exclude = cl_gui_alv_grid=>mc_fc_data_save.
    APPEND ls_exclude TO pt_exclude.

    ls_exclude = cl_gui_alv_grid=>mc_fc_maintain_variant.
    APPEND ls_exclude TO pt_exclude.

    ls_exclude = cl_gui_alv_grid=>mc_fc_load_variant.
    APPEND ls_exclude TO pt_exclude.

    ls_exclude = cl_gui_alv_grid=>mc_fc_save_variant.
    APPEND ls_exclude TO pt_exclude.
  ENDIF.

  ls_exclude = cl_gui_alv_grid=>mc_fc_delete_filter.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_deselect_all.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_detail.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_excl_all.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrdata.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrdesig.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrtempl.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_expmdb.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_extend.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_f4.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_filter.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_find.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_fix_columns.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_graph.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_help.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_html.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_info.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_append_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_cut.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_delete_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_move_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste_new_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_undo.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_maximum.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_minimum.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_pc_file.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_print.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_print_back.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_print_prev.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_refresh.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_reprep.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_select_all.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_send.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_separator.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_sort.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_sort_asc.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_sort_dsc.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_subtot.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_sum.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_to_office.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_to_rep_tree.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_unfix_columns.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_views.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_view_crystal.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_view_excel.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_view_grid.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_word_processor.
  APPEND ls_exclude TO pt_exclude.



ENDFORM.                               " TOOLBAR_PARAMS_FOR_ALV
*&---------------------------------------------------------------------*
*&      Form  load_data_fact
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM load_data_fact.
  DATA : ws_variante_fact TYPE disvariant.
  DATA : ws_layout_fact   TYPE lvc_s_layo.
  DATA : ws_toolbar_fact TYPE ui_functions.
  DATA : w_save.
  DATA : l_consistency_check TYPE char1.


  PERFORM catalog_params_fact.
  PERFORM toolbar_params USING 'FACT'
                       CHANGING ws_toolbar_fact.
  PERFORM layout CHANGING ws_layout_fact.
  PERFORM set_drag_drop USING    con_target
                        CHANGING ws_layout_fact
                                 wt_catalog_fact.
*  perform green_tick_update.

  w_save = 'X'.
  CONCATENATE sy-repid '_FACT' INTO ws_variante_fact-report.
  ws_variante_fact-username = sy-uname.

* Chargement des données dans la grille et affichage
  CALL METHOD o_fact_grid->set_table_for_first_display
    EXPORTING
      i_structure_name     = 'T_FACT'
      i_buffer_active      = space
      i_consistency_check  = l_consistency_check
      is_variant           = ws_variante_fact
      is_layout            = ws_layout_fact
      i_save               = w_save
      it_toolbar_excluding = ws_toolbar_fact
    CHANGING
      it_fieldcatalog      = wt_catalog_fact
      it_sort              = wt_sort_fact
      it_outtab            = wt_alv_fact.

  CREATE OBJECT o_event_receiver_fact.
  SET HANDLER o_event_receiver_fact->handle_double_click
              FOR o_fact_grid.
  SET HANDLER o_event_receiver_fact->handle_hotspot_click
              FOR o_fact_grid.
  SET HANDLER o_event_receiver_fact->handle_toolbar
              FOR o_fact_grid.
  SET HANDLER o_event_receiver_fact->onf4
              FOR o_fact_grid.
  SET HANDLER o_event_receiver_fact->handle_user_command
              FOR o_fact_grid.
  CALL METHOD o_fact_grid->set_toolbar_interactive.


* TARGET
  SET HANDLER o_event_receiver_fact->ondrop
              FOR o_fact_grid.

ENDFORM.                    " load_data_fact
*&---------------------------------------------------------------------*
*&      Form  load_data_cde
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM load_data_cde.
  DATA : ws_variante_cmde TYPE disvariant.
  DATA : ws_layout_cmde   TYPE lvc_s_layo.
  DATA : ws_toolbar_cmde TYPE ui_functions.
  DATA : w_save.
  DATA : l_consistency_check TYPE char1.


  PERFORM catalog_params_cmde.
  PERFORM toolbar_params USING 'CMDE'
                         CHANGING ws_toolbar_cmde.
  PERFORM layout CHANGING ws_layout_cmde.

  PERFORM set_drag_drop USING  con_source
                      CHANGING ws_layout_cmde
                               wt_catalog_cmde.



  wt_alv_cmde_aff[] = wt_alv_cmde[].
  PERFORM cache_item_rapproches.

* load data into the grid and display them
  CALL METHOD o_cmde_grid->set_table_for_first_display
  EXPORTING i_structure_name = 'T_CMDE'
*** Ajout JMB Drag 'n' Drop
             i_buffer_active     = space
             i_consistency_check = l_consistency_check
*** Fin ajout JMB Drag 'n' Drop
             is_variant       = ws_variante_cmde
             is_layout        = ws_layout_cmde
             i_save           = w_save
             it_toolbar_excluding = ws_toolbar_cmde
   CHANGING  it_fieldcatalog  = wt_catalog_cmde
             it_sort          = wt_sort_cmde
             it_outtab        = wt_alv_cmde_aff.


  CREATE OBJECT o_event_receiver_cmde.
  SET HANDLER o_event_receiver_cmde->handle_double_click
    FOR o_cmde_grid.
  SET HANDLER o_event_receiver_cmde->handle_hotspot_click
    FOR o_cmde_grid.

*** SOURCE
  SET HANDLER o_event_receiver_cmde->ondrag
  FOR o_cmde_grid.
  SET HANDLER o_event_receiver_cmde->ondropcomplete
  FOR o_cmde_grid.

ENDFORM.                    " load_data_cde

*---------------------------------------------------------------------*
*       FORM catalog_params_fact                                      *
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
FORM catalog_params_fact.

  DATA: wa_fieldcatalog TYPE lvc_s_fcat.

  wa_fieldcatalog-fieldname = 'STATUS'.
  wa_fieldcatalog-coltext = 'Statut'(t42).
  wa_fieldcatalog-outputlen = 7.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'POSTEFACT'.
  wa_fieldcatalog-coltext = 'Poste facture'(t36).
  wa_fieldcatalog-outputlen = 7.
*  authority-check object 'ZBAL_ZONE'
*         id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'BDL'.
  wa_fieldcatalog-coltext = 'Bon de livraison'(t35).
  AUTHORITY-CHECK OBJECT 'ZBAL_ZONE'
           ID 'ZBAL_ZONE' FIELD wa_fieldcatalog-fieldname.
  IF sy-subrc EQ 0.
    wa_fieldcatalog-rollname = 'VBELN_VL'.
    wa_fieldcatalog-f4availabl = 'X'.
    wa_fieldcatalog-checktable = 'VBUK'.
    wa_fieldcatalog-ref_field = 'VBELN'.
    wa_fieldcatalog-ref_table = 'LIKP'.
    wa_fieldcatalog-outputlen = 10.
    wa_fieldcatalog-datatype = 'CHAR'.
    wa_fieldcatalog-edit    = 'X'.
  ENDIF.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.


  wa_fieldcatalog-fieldname = 'NUMCMDE'.
  wa_fieldcatalog-coltext = text-t34.
  AUTHORITY-CHECK OBJECT 'ZBAL_ZONE'
           ID 'ZBAL_ZONE' FIELD wa_fieldcatalog-fieldname.
  IF sy-subrc EQ 0.
    wa_fieldcatalog-rollname = 'EBELN'.
    wa_fieldcatalog-f4availabl = 'X'.
    wa_fieldcatalog-ref_field = 'EBELN'.
    wa_fieldcatalog-ref_table = 'EKKO'.
    wa_fieldcatalog-edit    = 'X'.
  ENDIF.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'POSTECMDE'.
  wa_fieldcatalog-coltext = 'Poste commande'(t33).
  AUTHORITY-CHECK OBJECT 'ZBAL_ZONE'
           ID 'ZBAL_ZONE' FIELD wa_fieldcatalog-fieldname.
  IF sy-subrc EQ 0.
    wa_fieldcatalog-outputlen = 7.
    wa_fieldcatalog-edit    = 'X'.
  ENDIF.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'ARTICLE'.
  wa_fieldcatalog-coltext = 'Article'(t32).
  wa_fieldcatalog-outputlen = 20.
  wa_fieldcatalog-datatype = 'CHAR'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'MAKTX'.
  wa_fieldcatalog-coltext = 'Désignation'(t31).
  wa_fieldcatalog-outputlen = 7.
  wa_fieldcatalog-datatype = 'CHAR'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'QUANTITE'.
  wa_fieldcatalog-coltext   = text-t30.
  wa_fieldcatalog-ref_table = 'EKPO'.
  wa_fieldcatalog-ref_field = 'MENGE'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'CONDI'.
  wa_fieldcatalog-coltext   = text-t56.
  wa_fieldcatalog-ref_table = 'EKPO'.
  wa_fieldcatalog-ref_field = 'PEINH'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'PRIXUNIT'.
  wa_fieldcatalog-coltext   = text-t29.
  wa_fieldcatalog-ref_table = 'EKPO'.
  wa_fieldcatalog-ref_field = 'NETPR'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'TOTALHT'.
  wa_fieldcatalog-coltext   = text-t28.
  wa_fieldcatalog-ref_table = 'EKPO'.
  wa_fieldcatalog-ref_field = 'NETWR'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'TVA'.
  wa_fieldcatalog-coltext   = text-t27.
  wa_fieldcatalog-ref_table = 'EKPO'.
  wa_fieldcatalog-ref_field = 'BRTWR'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname  = 'CODETVA'.
  wa_fieldcatalog-coltext    = text-t26.
  wa_fieldcatalog-rollname   = 'MWSKZ'.
  wa_fieldcatalog-f4availabl = 'X'.
  wa_fieldcatalog-ref_field  = 'MWSKZ'.
  wa_fieldcatalog-ref_table  = 'T007AC'.
*  authority-check object 'ZBAL_ZONE'
*           id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'TOTALTTC'.
  wa_fieldcatalog-coltext   = text-t25.
  wa_fieldcatalog-ref_table = 'EKPO'.
  wa_fieldcatalog-ref_field = 'BRTWR'.
*  authority-check object 'ZBAL_ZONE'
*       id 'ZBAL_ZONE' field wa_fieldcatalog-fieldname.
*  if sy-subrc eq 0.
*    wa_fieldcatalog-edit    = 'X'.
*  endif.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'DUMMY'.
  wa_fieldcatalog-no_out    = 'X'.
  wa_fieldcatalog-edit      = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_fact.  CLEAR wa_fieldcatalog.

ENDFORM.                    " catalog_params_fact
*&---------------------------------------------------------------------*
*&      Form  catalog_params_cmde
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM catalog_params_cmde.
  DATA: wa_fieldcatalog TYPE lvc_s_fcat.

  wa_fieldcatalog-fieldname = 'RCPTICON'.
  wa_fieldcatalog-coltext = text-t42.
  wa_fieldcatalog-outputlen = 7.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'CMDE'.
  wa_fieldcatalog-coltext = text-t13.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'POSTE'.
  wa_fieldcatalog-coltext = text-t12.
  wa_fieldcatalog-dragdropid = '0001'.
  wa_fieldcatalog-outputlen = 6.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'ARTICLE'.
  wa_fieldcatalog-coltext = text-t32.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'DESIGNATION'.
  wa_fieldcatalog-coltext = text-t31.
  wa_fieldcatalog-outputlen = 25.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'QUANTITE'.
  wa_fieldcatalog-coltext = text-t30.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'CONDI'.
  wa_fieldcatalog-coltext = text-t56.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'PRIXUNIT'.
  wa_fieldcatalog-coltext = text-t29.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'TOTALHT'.
  wa_fieldcatalog-coltext = text-t28.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'RESTE_Q'.
  wa_fieldcatalog-coltext = text-t11.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'RESTE_M'.
  wa_fieldcatalog-coltext = text-t10.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.
  wa_fieldcatalog-fieldname = 'CODETVA'.
  wa_fieldcatalog-coltext = text-t26.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.

* Ajout d'une zone modifiable non affichée pour que les autres zones
* non modificables soient grisées
  wa_fieldcatalog-fieldname = 'DUMMY'.
  wa_fieldcatalog-no_out    = 'X'.
  wa_fieldcatalog-edit      = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cmde.  CLEAR wa_fieldcatalog.

ENDFORM.                    " catalog_params_cmde

*&---------------------------------------------------------------------*
*&      Form  f_2xxx_user_command
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_W_9210_OKCODE  text
*----------------------------------------------------------------------*
FORM f_2xxx_user_command USING value(p_okcode) TYPE sy-ucomm.

  DATA : wt_matnr LIKE ekpo-matnr.
  DATA: wa_alv_sel TYPE lvc_t_row WITH HEADER LINE.
*  DATA : wt_alv_sel1 TYPE lvc_t_row.

  CALL METHOD o_fact_grid->maj
    EXPORTING
      update_now = 'X'
    IMPORTING
      is_changed = wl_ret.

  PERFORM update_idoc_block USING w_docnum.


  CASE p_okcode.
    WHEN 'CTRL'.
*     Contrôler la facture
      PERFORM f_ctrl_fact TABLES wt_return USING w_modif_poss.
*     Contrôler les lignes de coûts addtionnels
      PERFORM f_ctrl_cadd TABLES wt_return.

      IF NOT wl_ret IS INITIAL.
        PERFORM f_ctrl_amount.
      ENDIF.

      PERFORM green_tick_update.

      IF wt_return[] IS INITIAL.
        MESSAGE s024.
      ELSE.
        PERFORM f_aff_rapport TABLES wt_return.
      ENDIF.

      PERFORM f_maj_grid.

    WHEN 'FACTURE'.
*     Afficher l'image de la facture
      AUTHORITY-CHECK OBJECT 'ZBAL_ACT'
               ID 'BAL_ACTION' FIELD 'IMGF'.
      IF sy-subrc EQ 0.
        PERFORM f_display_invoice.
      ELSE.
        MESSAGE e025.
      ENDIF.

    WHEN 'SAVE' OR 'EXIT' OR 'BACK' OR 'RAPPR'.
      LEAVE TO SCREEN 0.
**debut ajout 53
    WHEN 'ART'.

      PERFORM det_sel.

*      REFRESH wt_alv_sel1.
*      CALL METHOD o_cmde_grid->get_selected_rows
*         IMPORTING
*           et_index_rows = wt_alv_sel1.

      IF  wt_alv_sel IS NOT INITIAL.

        DESCRIBE TABLE wt_alv_fact LINES wl_i.
        IF wl_i GT 0.
*      CHECK wl_i GT 0.
*      IF wl_i EQ 1.

          READ  TABLE wt_alv_sel INDEX 1 INTO  wa_alv_sel.
          READ TABLE wt_alv_fact INDEX wa_alv_sel-index  INTO  wa_alv_fact.
*        CHECK sy-subrc EQ 0.
*        PERFORM f_aff_article USING wa_alv_fact-article
*                            wa_alv_fact-postefact.

          SET PARAMETER ID 'MAT' FIELD wa_alv_fact-article.
          CALL TRANSACTION 'MM03' AND SKIP FIRST SCREEN.
**fin ajout 53
        ELSE.
          MESSAGE s020.
        ENDIF.
      ELSE.

* IF  wt_alv_sel1 IS NOT INITIAL.
        REFRESH wt_alv_sel.
        CALL METHOD o_cmde_grid->get_selected_rows
          IMPORTING
            et_index_rows = wt_alv_sel.

        DESCRIBE TABLE wt_alv_cmde LINES wl_i.
        IF wl_i GT 0.

          READ  TABLE wt_alv_sel INDEX 1 INTO  wa_alv_sel.
          READ TABLE wt_alv_cmde_aff INDEX wa_alv_sel-index INTO wa_alv_cmde.

          SET PARAMETER ID 'MAT' FIELD wa_alv_cmde-article.
          CALL TRANSACTION 'MM03' AND SKIP FIRST SCREEN.

        ELSE.
          MESSAGE s020.
        ENDIF.

      ENDIF.
**fin ajout 53

    WHEN 'FOURN'.
*     Afficher le fiche fournisseur
      IF NOT ( zbalance_04-lifnr IS INITIAL ).
        SET PARAMETER ID 'LIF' FIELD zbalance_04-lifnr.
        SET PARAMETER ID 'BUK' FIELD zbalance_04-bukrs.
        CALL TRANSACTION 'XK03'.
      ENDIF.
    WHEN 'ADD'. "Décomposer en plusieurs postes
      PERFORM add_item_to_fact.
      PERFORM update_amounts.
    WHEN 'DEL'. "Consolider/Supprimer les postes.
      PERFORM delete_item_from_fact.
      PERFORM update_amounts.
    WHEN 'UPD'.
      PERFORM update_item_from_fact.
      PERFORM update_amounts.
    WHEN 'CADD_ADD'. " Ajout de ligne coûts additionnels
      PERFORM add_cadd.
      PERFORM update_amounts.
    WHEN 'CADD_DEL'. " Suppression de ligne coûts additionnels
      PERFORM del_cadd.
      PERFORM update_amounts.
    WHEN 'CADD_CHG'. " Modification ligne coûts additionnels
      PERFORM chg_cadd.
      PERFORM update_amounts.
  ENDCASE.

ENDFORM.                    " f_2xxx_user_command

*&---------------------------------------------------------------------*
*&      Form  f_aff_article
*&---------------------------------------------------------------------*
FORM f_aff_article USING p_article p_posex.
  DATA: wl_i TYPE int4.

  IF p_article IS INITIAL.
    MESSAGE s022 WITH p_posex.
    EXIT.
  ENDIF.

  CLEAR wl_i.
  SELECT COUNT(*) FROM eina INTO wl_i
    WHERE idnlf = p_article
      AND lifnr = zbalance_04-lifnr.
  IF sy-subrc = 0.
    IF wl_i EQ 0.
      SHIFT p_article LEFT DELETING LEADING space.
      SELECT COUNT(*) FROM eina INTO wl_i
        WHERE idnlf = p_article
          AND lifnr = zbalance_04-lifnr.
      IF sy-subrc NE 0.
        MESSAGE s532(0u)
        WITH 	'Aucune entrée correspondante dans la table'(w47)
        'Fiche infos-achats - données générales'(w38).
      ENDIF.
    ENDIF.
    IF wl_i NE 1.
      MESSAGE s021 WITH zbalance_04-lifnr p_article.
      EXIT.
    ENDIF.
    SELECT  SINGLE matnr FROM eina INTO mara-matnr "ex SELECT SINGLE
        WHERE idnlf = p_article
          AND lifnr = zbalance_04-lifnr.
*    ENDSELECT.
    IF sy-subrc EQ 0.
      SET PARAMETER ID 'MAT' FIELD mara-matnr.
      SET PARAMETER ID 'LIF' FIELD zbalance_04-lifnr.
      CALL TRANSACTION 'ME13' AND SKIP FIRST SCREEN.
    ELSE.
      MESSAGE s532(0u)
         WITH 'Aucune entrée correspondante dans la table'(w47)
              'Fiche infos-achats - données générales'(w38).
    ENDIF.
  ELSE.
    MESSAGE s532(0u)
       WITH 'Aucune entrée correspondante dans la table'(w47)
            'Fiche infos-achats - données générales'(w38).
  ENDIF.
ENDFORM.                    " f_aff_article

*---------------------------------------------------------------------*
*       FORM f_aff_rapport                                            *
*---------------------------------------------------------------------*
FORM f_aff_rapport TABLES pt_msg TYPE bal_t_msg.
  DATA: wl_msg TYPE bal_s_msg.
  DATA: wl_log_handle TYPE balloghndl,
        wl_log      TYPE bal_s_log,
        wl_display_profile TYPE bal_s_prof,
        wl_lines      TYPE balcoord.

  LOOP AT pt_msg INTO wl_msg.
    IF wl_lines = 0.
      CALL FUNCTION 'BAL_GLB_MEMORY_REFRESH'
        EXPORTING
          i_refresh_all = 'X'.
      wl_log-extnumber = 'Application Log'(t04).
      wl_log-aluser    = sy-uname.
      wl_log-alprog    = sy-repid.
      CALL FUNCTION 'BAL_LOG_CREATE'
        EXPORTING
          i_s_log = wl_log
        EXCEPTIONS
          OTHERS  = 1.
      IF sy-subrc <> 0.
        CONTINUE.
      ENDIF.
    ENDIF.

    CALL FUNCTION 'BAL_LOG_MSG_ADD'
      EXPORTING
        i_log_handle = wl_log_handle
        i_s_msg      = wl_msg
      EXCEPTIONS
        OTHERS       = 1.
    IF sy-subrc NE 0.
      CONTINUE.
    ENDIF.
    ADD 1 TO wl_lines.
  ENDLOOP.

  IF wl_lines GT 0.
    CLEAR wl_display_profile.
    CALL FUNCTION 'BAL_DSP_PROFILE_NO_TREE_GET'
      IMPORTING
        e_s_display_profile = wl_display_profile
      EXCEPTIONS
        OTHERS              = 1.
    IF sy-subrc <> 0.
      EXIT.
    ENDIF.
    wl_display_profile-disvariant-report = sy-repid.
    wl_display_profile-title     =
      'Rapport d''exécution - SAP balance'(t03).
    wl_display_profile-pop_adjst = 'X'.
    wl_display_profile-start_col = 5.
    wl_display_profile-start_row = 5.
    wl_lines =  wl_lines + 10.
    IF wl_lines LT 20.
      wl_display_profile-end_row   = wl_lines.
    ELSE.
      wl_display_profile-end_row   = 20.
    ENDIF.
    wl_display_profile-end_col   = 100.
    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
         EXPORTING
            i_s_display_profile  = wl_display_profile
*               i_amodal             = i_amodal
         EXCEPTIONS
              OTHERS               = 0.
  ENDIF.
ENDFORM.                    "f_aff_rapport

*&---------------------------------------------------------------------*
*&      Form  f_ctrl_fact
*&---------------------------------------------------------------------*
FORM f_ctrl_fact TABLES pt_return TYPE bal_t_msg
                  USING p_maj.
  DATA: wa_alv_fact TYPE t_fact.
  DATA: BEGIN OF wt_ctrl OCCURS 0,
         postecmde(10),
         numcmde(10),
        END OF wt_ctrl.
  DATA: w_montant  TYPE wrbtr.
  DATA: w_quantite TYPE menge_d.

  DATA : w_dummy TYPE icon_d.

***** Début modif - GTR(appia) - 22.09.06
  CONSTANTS: c_task_tva LIKE zuue_activation-task VALUE 'FI258'.
  CONSTANTS: c_key1_tva LIKE zuue_activation-key1 VALUE 'MWSKZ'.
  DATA: wa_zuue TYPE zuue_activation.
  DATA l_flag_tva TYPE c.
  FIELD-SYMBOLS:<fs>.
  DATA l_count TYPE c.
  DATA l_name(16) TYPE c.
***** fin modif - GTR(appia) - 22.09.06

  CLEAR pt_return[].

  IF sy-dynnr EQ w_dynnr.
    CALL FUNCTION 'GET_DYNP_VALUE'
      EXPORTING
        i_field             = 'ZBALANCE_04-BUKRS'
        i_repid             = w_repid
        i_dynnr             = '9211'
*       I_CONV_INPUT        = ' '
*       I_CONV_OUTPUT       = ' '
      CHANGING
        o_value             = wl_bukrs.
  ELSE.
    wl_bukrs = zbalance_04-bukrs.
  ENDIF.

* Vérification de la société
  IF wl_bukrs IS INITIAL.
    PERFORM f_130_message
       TABLES pt_return USING '027' 'E' '' '' '' ''.
  ELSE.
    SELECT SINGLE land1 FROM t001
                        INTO wl_land1
                       WHERE bukrs = wl_bukrs.
    IF sy-subrc NE 0.
      PERFORM f_130_message
         TABLES pt_return USING '026' 'E' wl_bukrs '' '' ''.
    ENDIF.
  ENDIF.

*
  LOOP AT wt_alv_fact INTO wa_alv_fact.
*   Vérification de la présence d'un
*   poste de commande sur chaque poste de facture
    IF wa_alv_fact-postecmde IS INITIAL.
      PERFORM f_130_message
         TABLES pt_return
         USING '029' 'E'
               wa_alv_fact-postefact wa_alv_fact-numcmde '' ''.
    ENDIF.

*   Vérification de la présence d'un
*   n° de commande sur chaque poste de facture
    IF wa_alv_fact-numcmde IS INITIAL.
      PERFORM f_130_message
              TABLES pt_return
              USING '028' 'E' wa_alv_fact-postefact '' '' ''.
    ENDIF.

    IF NOT ( wa_alv_fact-postecmde IS INITIAL ) AND
       NOT ( wa_alv_fact-numcmde IS INITIAL   ).
      READ TABLE wt_ctrl WITH KEY
        postecmde = wa_alv_fact-postecmde
        numcmde   = wa_alv_fact-numcmde.
      IF sy-subrc EQ 0.
        PERFORM f_130_message
           TABLES pt_return
           USING '030' 'E' wa_alv_fact-postefact
                           wa_alv_fact-postecmde wa_alv_fact-numcmde ''.
      ELSE.
        wt_ctrl-postecmde = wa_alv_fact-postecmde.
        wt_ctrl-numcmde   = wa_alv_fact-numcmde.
        APPEND wt_ctrl.

*       Vérification commande existe
        ekko-ebeln = wa_alv_fact-numcmde.
        SELECT SINGLE * FROM ekko
                       WHERE ebeln = ekko-ebeln.
        IF sy-subrc NE 0.
          CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
            EXPORTING
              input  = wa_alv_fact-numcmde
            IMPORTING
              output = ekko-ebeln.
          SELECT SINGLE * FROM ekko
                         WHERE ebeln = ekko-ebeln.
          IF sy-subrc EQ 0.
            IF p_maj = 'X'.
              wa_alv_fact-numcmde = ekko-ebeln.
              MODIFY wt_alv_fact FROM wa_alv_fact.
            ENDIF.
          ELSE.
            MESSAGE s532(0u) WITH   text-w47 text-w41.
          ENDIF.
        ENDIF.

        IF sy-subrc NE 0.
          PERFORM f_130_message
             TABLES pt_return
             USING '031' 'E'
                   wa_alv_fact-postefact wa_alv_fact-numcmde '' ''.
        ELSE.
          CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
            EXPORTING
              input  = wa_alv_fact-postecmde
            IMPORTING
              output = ekpo-ebelp.
          SELECT SINGLE * FROM ekpo
                    WHERE ebeln = ekko-ebeln
                      AND ebelp = ekpo-ebelp.
          IF sy-subrc NE 0.
            PERFORM f_130_message
               TABLES pt_return
               USING '032' 'E' wa_alv_fact-postefact
                           wa_alv_fact-postecmde wa_alv_fact-numcmde ''.
          ELSE.
            IF p_maj = 'X'.
              wa_alv_fact-postecmde = ekpo-ebelp.
              MODIFY wt_alv_fact FROM wa_alv_fact.
            ENDIF.
            CLEAR: w_montant, w_quantite.
            PERFORM f_180_reste_fact USING
              ekpo-ebeln
              ekpo-ebelp
              w_montant
              w_quantite
              w_dummy.
            IF ekpo-webre = 'X'.
              IF w_quantite < wa_alv_fact-quantite.
*              perform f_130_message                                "JMB
*                 tables pt_return                                  "JMB
*                 using '035' 'E' wa_alv_fact-postefact             "JMB
*                             w_quantite wa_alv_fact-quantite ''.   "JMB
              ELSEIF w_montant < wa_alv_fact-totalht.
*>> RGI008 - Baance V2
*>> Changin Error TYPE E->W
*                PERFORM f_130_message
*                   TABLES pt_return
*                   USING '036' 'E' wa_alv_fact-postefact
*                               wa_alv_fact-totalttc w_montant ''.
                PERFORM f_130_message
                   TABLES pt_return
                   USING '036' 'W' wa_alv_fact-postefact
                               wa_alv_fact-totalttc w_montant ''.
*>> FIN RGI008 - Balance V2
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.

*   Controle du code TVA
****** début ajout - GTR (appia) - 22.09.06
* contrôler qu'il ne s'agit pas d'un code TVA pour l'export.
* si oui ne pas chercher les conditions de prix.
*-- Recherche dans la table ZUUE_ACTIVATION si le code TVA est présent
*   Si oui ne pas créer de segment TVA
    CLEAR wa_zuue.
    SELECT SINGLE * FROM zuue_activation INTO wa_zuue
                            WHERE task = c_task_tva
                              AND key1 = c_key1_tva.
    IF sy-subrc = 0.
      CLEAR l_count.
      DO 5 TIMES.
        ADD 1 TO l_count.
        CONCATENATE 'WA_ZUUE-VALCHAR' l_count INTO l_name.
        ASSIGN (l_name) TO <fs>.
        IF <fs> =  wa_alv_fact-codetva
               AND NOT wa_alv_fact-codetva IS INITIAL.
          MOVE 'X' TO l_flag_tva.
          EXIT.
        ELSE.
          CLEAR l_flag_tva.
        ENDIF.
      ENDDO.
    ELSE.
      CLEAR l_flag_tva.
    ENDIF.

*>> RGI008 - Balance V2 Arrondi TVA.
    IF calctva = space.
      l_flag_tva = 'X'.
    ENDIF.
*>> FIN RGI008

    IF l_flag_tva IS INITIAL.
***** fin modif - GTR(appia) - 22.09.06

      IF wa_alv_fact-codetva IS INITIAL.
        PERFORM f_130_message
           TABLES pt_return
           USING '033' 'E' wa_alv_fact-postefact '' '' ''.
      ELSEIF NOT ( wl_bukrs IS INITIAL ).

        SELECT  b~kbetr INTO wl_kbetr "ex SELECT SINGLE
                      FROM a003 AS a INNER JOIN konp AS b
                                        ON a~knumh = b~knumh
                      WHERE a~aland = wl_land1
                        AND a~kappl = 'TX'
                        AND a~mwskz = wa_alv_fact-codetva.
        ENDSELECT.
        IF sy-subrc NE 0.
          PERFORM f_130_message
             TABLES pt_return
             USING '034' 'E'
                   wa_alv_fact-postefact wa_alv_fact-codetva
                   wl_bukrs ''.
        ELSE.
* ITGGH   23.02.2007  Extension for logical system         DE5K900049
          SELECT * FROM  t076m
                WHERE  parart  = 'LS'
                AND    land1   = wl_land1
                AND    mwart   = wa_alv_fact-codetva
                AND    mwskz   = wa_alv_fact-codetva.
          ENDSELECT.
          IF sy-subrc <> 0.
            SELECT * FROM  t076m
                  WHERE  parart  = 'LS'
                  AND    land1   = ''
                  AND    mwart   = wa_alv_fact-codetva
                  AND    mwskz   = wa_alv_fact-codetva.
            ENDSELECT.
            IF sy-subrc <> 0.
* ITGGH   23.02.2007  Extension for logical system    end
              SELECT * FROM  t076m
               WHERE  parart  = 'LI'
               AND    land1   = wl_land1
               AND    konto   = zbalance_04-lifnr
               AND    mwart   = wa_alv_fact-codetva
               AND    mwskz   = wa_alv_fact-codetva.
              ENDSELECT.

              IF sy-subrc NE 0.
                SELECT * FROM  t076m "ex SELECT SINGLE
                 WHERE  parart  = 'LI'

                 AND    konto   = zbalance_04-lifnr
                 AND    mwart   = wa_alv_fact-codetva
                 AND    mwskz   = wa_alv_fact-codetva.
                ENDSELECT.
              ENDIF.
            ENDIF.     "* ITGGH 23.02.2007 Extension for logical system
          ENDIF.       "* ITGGH 23.02.2007 Extension for logical system

          IF sy-subrc NE 0.
            PERFORM f_130_message
               TABLES pt_return
               USING '038' 'E'
                     wa_alv_fact-postefact wa_alv_fact-codetva
                     zbalance_04-lifnr ''.
          ELSE.
            CLEAR wl_comp.
            CHECK wa_alv_fact-totalht GT 1.
            IF NOT wa_alv_fact-totalht IS INITIAL.
              wl_test = wl_kbetr MOD 10.
              IF wl_test EQ 0.
                wl_comp = wa_alv_fact-tva / wa_alv_fact-totalht.
                wl_comp = wl_comp * 1000.
              ELSE.
                wl_comp = wa_alv_fact-tva * 10 / wa_alv_fact-totalht.
                wl_comp = wl_comp * 100.
              ENDIF.
            ENDIF.

            w_typef1 = wl_comp.
            w_typef2 = wl_kbetr.

            IF wl_comp NE wl_kbetr.
              wl_comp  = wl_comp / 10.
              wl_kbetr = wl_kbetr / 10.
              PERFORM f_130_message
                 TABLES pt_return
                 USING '037' 'E'
                       wa_alv_fact-postefact wl_kbetr wl_comp space .
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
***** début modif - GTR(appia) - 22.09.06
    ENDIF.
***** fin modif - GTR(appia) - 22.09.06
  ENDLOOP.
ENDFORM.                    " f_ctrl_fact

*&---------------------------------------------------------------------*
*&      Form  f_decomposition
*&---------------------------------------------------------------------*
*        Combien de nouveau postes ?
*  -->  p_answer : Ajout de lignes (1) / Coût additionnel (2)
*----------------------------------------------------------------------*
FORM f_decomposition USING p_answer.

  DATA : wa_status(4) TYPE c,
         wa_maktx LIKE ekpo-txz01.

  DESCRIBE TABLE wt_alv_sel LINES l_cpt_decompo.

  PERFORM f_check_multi CHANGING w_multi.

  IF p_answer EQ 1.
    PERFORM decompose_poste.
  ELSEIF p_answer EQ 2.
    PERFORM ajout_poste.
  ENDIF.


ENDFORM.                    " f_decomposition
*&---------------------------------------------------------------------*
*&      Form  f_ctrl_amount
*&---------------------------------------------------------------------*
*       Contrôle des montants de la facture par rapport aux montants
*       de la commande
*----------------------------------------------------------------------*
FORM f_ctrl_amount.

  DATA: wa_alv_fact TYPE t_fact, wa_alv_cmde TYPE t_cmde.
  DATA: l_totalfact TYPE ekpo-netwr, l_totalcmde TYPE ekpo-netwr.
  DATA: ws_totalfact(10), ws_totalcmde(10).
  DATA: wa_title(80).

  DATA: BEGIN OF wt_amount OCCURS 0,
         status(4),
         msg(85),
  END OF wt_amount.

  CLEAR : l_totalfact, l_totalcmde, ws_totalfact, ws_totalcmde.

  LOOP AT wt_alv_fact INTO wa_alv_fact.
    ADD wa_alv_fact-totalht TO l_totalfact.
  ENDLOOP.

*         tva      TYPE ekpo-brtwr,
*         codetva  TYPE t007a-mwskz,
*         totalttc TYPE ekpo-brtwr,

  LOOP AT wt_alv_cmde INTO wa_alv_cmde.
    ADD wa_alv_cmde-totalht TO l_totalcmde.
  ENDLOOP.

  MOVE 'Vérification montants facture / commande'(w61) TO wa_title.

  MOVE: l_totalfact TO ws_totalfact,
          l_totalcmde TO ws_totalcmde.

  SHIFT : ws_totalfact LEFT DELETING LEADING space,
          ws_totalcmde LEFT DELETING LEADING space.

  IF l_totalfact GT l_totalcmde.
    MOVE icon_idoc  TO wt_amount-status.
    CONCATENATE 'Le montant total facturé'(w59)
                ws_totalfact 'est supérieur au montant commandé'(w60)
                ws_totalcmde INTO wt_amount-msg SEPARATED BY space.
    APPEND wt_amount.
  ELSEIF l_totalfact LT l_totalcmde.
    MOVE icon_order TO wt_amount-status.
    CONCATENATE 'Le montant total commandé'(w57)
                ws_totalcmde 'est supérieur au montant facturé'(w58)
                ws_totalfact INTO wt_amount-msg SEPARATED BY space.
    APPEND wt_amount.
  ENDIF.

  IF NOT wt_amount IS INITIAL.
    CALL FUNCTION 'POPUP_WITH_TABLE_DISPLAY'
      EXPORTING
        endpos_col         = 90
        endpos_row         = 2
        startpos_col       = 1
        startpos_row       = 1
        titletext          = wa_title
*     IMPORTING
*       CHOISE             =
      TABLES
        valuetab           = wt_amount
     EXCEPTIONS
       break_off          = 1
       OTHERS             = 2
              .
    IF sy-subrc <> 0.
    ENDIF.
  ELSE.
    MESSAGE s531(0u)
    WITH 'Les montants sont corrects'(w55).
  ENDIF.


ENDFORM.                    " f_ctrl_amount
*&---------------------------------------------------------------------*
*&      Form  f_consolidation
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_consolidation.

  DATA: wa_alv_fact TYPE t_fact,
        ws_fact TYPE t_fact,
        wa_temp_alv TYPE t_fact.
  DATA: w_cpt TYPE i VALUE 0.
  DATA: wa_oldpostecmde(10), wa_oldarticle(18).
  DATA wa_index TYPE sy-index.
  DATA : wa_flgposte(1),wa_flgarticle.
* EHP 8 Upgrade dump issue.
DATA : lv_zero TYPE c VALUE 0. " INS SVA001 DevT0023691

  DESCRIBE TABLE wt_alv_sel LINES l_cpt_conso.

  IF l_cpt_conso LE 1.

    MESSAGE s531(0u)
    WITH 'Sélectionnez au moins deux postes'(w51).
    EXIT.
  ELSE.

    CLEAR :  l_totalht, l_tva, l_quantite,
             wa_flgposte,l_prixunitaire,l_prix_alvfact.

    PERFORM f_check_multi CHANGING w_multi.

    IF NOT w_multi IS INITIAL.
      MESSAGE s531(0u) WITH 'Les commandes sont différentes'(w64).
      EXIT.
    ELSE.
* Il n'y a qu'une seul commande dans la facture

* Vérification que: - les postes sont différents
*                   - les articles sont identiques
      IF  l_cpt_conso GT 1.
        LOOP AT wt_alv_sel INTO wa_alv_sel.
          READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index.

          IF wa_oldpostecmde IS INITIAL.
            wa_oldpostecmde = wa_alv_fact-postecmde.
          ELSE.

            IF wa_oldpostecmde NE wa_alv_fact-postecmde
            AND NOT wa_oldpostecmde IS INITIAL
            AND NOT wa_alv_fact-postecmde IS INITIAL.
              wa_flgposte = 'X'. "Il n'y a pas qu'un seul poste
*              EXIT.
            ENDIF.

          ENDIF.

          IF wa_oldarticle IS INITIAL.
            wa_oldarticle = wa_alv_fact-article.
          ELSE.

            IF wa_oldarticle NE wa_alv_fact-article
            AND NOT wa_oldarticle IS INITIAL
            AND NOT wa_alv_fact-article IS INITIAL.
              wa_flgarticle = 'X'. "Il n'y a pas qu'un seul poste
*              EXIT.
            ENDIF.

          ENDIF.

          CHECK NOT wa_flgposte IS INITIAL
          OR NOT wa_flgarticle IS INITIAL.

        ENDLOOP.
      ENDIF.

      CLEAR wa_alv_fact.



      IF wa_flgposte IS INITIAL AND wa_flgarticle IS INITIAL .

* Si l'article a été saisi, on alimente par défaut le prix unitaire avec
* sa valeur dans EKPO - TBR 29102004
        LOOP AT wt_alv_fact INTO wa_alv_fact
                             WHERE NOT article IS INITIAL
                             AND NOT postecmde IS INITIAL.
        ENDLOOP.
        IF sy-subrc EQ 0.
          MOVE wa_alv_fact-prixunit TO l_prix_alvfact.
          SELECT SINGLE netpr INTO l_prix_ekpo FROM ekpo
                              WHERE matnr = wa_alv_fact-article
                              AND ebeln = wa_alv_fact-numcmde.
        ELSE.
          LOOP AT wt_alv_fact INTO wa_alv_fact
                               WHERE NOT postecmde IS INITIAL.
          ENDLOOP.
          SELECT SINGLE netpr INTO l_prix_ekpo FROM ekpo
                              WHERE ebelp = wa_alv_fact-postecmde
                              AND ebeln = wa_alv_fact-numcmde.
        ENDIF.

        LOOP AT wt_alv_sel INTO wa_alv_sel.
          READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index.

          IF sy-subrc = 0.
            l_prix_alvfact = wa_alv_fact-prixunit.
            l_prixunitaire = wa_alv_fact-prixunit.

            IF w_cpt IS INITIAL.
              w_cpt = l_cpt_conso.
            ENDIF.

            IF w_cpt GE 1.

              ADD  : wa_alv_fact-quantite TO l_quantite.
              IF l_prixunitaire IS INITIAL.
                IF NOT l_prix_ekpo IS INITIAL.
                  MOVE l_prix_ekpo TO : wa_alv_fact-prixunit,
                                        l_prixunitaire.
                ELSE.
                  MOVE  l_prix_alvfact TO l_prixunitaire.
                ENDIF.
              ELSE.
                IF wa_alv_fact-prixunit IS INITIAL.
                  wa_alv_fact-prixunit = l_prixunitaire.
                ELSE.
                  IF l_prixunitaire NE wa_alv_fact-prixunit.
                    EXIT. "message à gérer ?
                  ENDIF.
                ENDIF.
              ENDIF.
              w_cpt = w_cpt - 1.
            ENDIF.

          ENDIF.

        ENDLOOP.

        CLEAR l_sup.

        SORT wt_alv_sel BY index DESCENDING.

        LOOP AT wt_alv_sel INTO wa_alv_sel.
          READ TABLE wt_alv_fact INTO wa_alv_fact
                                 INDEX wa_alv_sel-index.
          DELETE wt_alv_fact INDEX wa_alv_sel-index.
        ENDLOOP.


        wl_bukrs = zbalance_04-bukrs.

        SELECT SINGLE land1 FROM t001
                            INTO wl_land1
                           WHERE bukrs = wl_bukrs.

        SELECT  b~kbetr INTO wl_kbetr "ex SELECT SINGLE
                      FROM a003 AS a INNER JOIN konp AS b
                                        ON a~knumh = b~knumh
                      WHERE a~aland = wl_land1
                        AND a~kappl = 'TX'
                        AND a~mwskz = wa_alv_fact-codetva.
        ENDSELECT.

        MOVE wl_kbetr TO wa_txtva.
* SHIFT : wa_txtva LEFT DELETING LEADING 0. " COM SVA001 DevT0023691
 SHIFT : wa_txtva LEFT DELETING LEADING lv_zero. " INS SVA001 DevT0023691
        wa_txtva = wa_txtva / 10.




        CLEAR wa_alv_fact-status.

        MOVE : icon_message_question_small TO wa_alv_fact-status,
               'POSTE CONSOLIDE' TO wa_alv_fact-maktx,
               wa_oldpostecmde TO wa_alv_fact-postecmde,
               l_prixunitaire TO wa_alv_fact-prixunit.

        wa_alv_fact-totalht = l_quantite * l_prixunitaire.
        l_tva = ( wa_alv_fact-totalht * wa_txtva ) / 100.
        wa_alv_fact-totalttc = wa_alv_fact-totalht + l_tva.

        MOVE: l_tva      TO wa_alv_fact-tva,
              l_quantite TO wa_alv_fact-quantite.

        APPEND wa_alv_fact TO wt_alv_fact.

        IF sy-subrc = 0.
          w_consolidation = 'X'.
          CLEAR: w_suppr,w_decompose.
        ENDIF.



*        LOOP AT wt_alv_sel INTO wa_alv_sel.
*         READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index
*.
*
*          IF sy-subrc = 0.
*
*            CLEAR wa_alv_fact-status.
*
*            MOVE : icon_message_question_small TO wa_alv_fact-status,
*                   'POSTE CONSOLIDE' TO wa_alv_fact-maktx,
*                   wa_oldpostecmde TO wa_alv_fact-postecmde,
*                   l_prixunitaire TO wa_alv_fact-prixunit.
*
*
*            wa_alv_fact-totalht = l_quantite * l_prixunitaire.
*            l_tva = ( wa_alv_fact-totalht * wa_txtva ) / 100.
*            wa_alv_fact-totalttc = wa_alv_fact-totalht + l_tva.
*
*            MOVE: l_tva      TO wa_alv_fact-tva,
*                  l_quantite TO wa_alv_fact-quantite.
*
*            MODIFY wt_alv_fact FROM wa_alv_fact INDEX wa_alv_sel-index.
*
*            IF sy-subrc = 0.
*              w_consolidation = 'X'.
*              CLEAR: w_suppr,w_decompose.
*            ENDIF.
*          ENDIF.
*        ENDLOOP.

      ELSE.

        IF NOT wa_flgposte IS INITIAL.
          MESSAGE s531(0u)
          WITH 'Les postes à consolider sont différents'(w63).
          EXIT.
        ELSEIF NOT wa_flgarticle IS INITIAL .
          MESSAGE s531(0u)
          WITH 'Les articles à consolider sont différents'(w65).
          EXIT.
        ENDIF.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.                    " f_consolidation
*&---------------------------------------------------------------------*
*&      Form  f_suppression
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_suppression.

  DATA: wa_alv_fact TYPE t_fact,
        wa_alv_cmde TYPE t_cmde,
        ws_fact TYPE t_fact.
  DATA wa_index TYPE sy-tabix.

  SORT wt_alv_sel BY index DESCENDING.

  LOOP AT wt_alv_sel INTO wa_alv_sel.
    READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index.
    wa_index = sy-tabix.
    DELETE wt_alv_fact  INDEX wa_index.
  ENDLOOP.

  LOOP AT wt_alv_fact INTO wa_alv_fact.

    READ TABLE wt_alv_fact INTO ws_fact
                       WITH KEY status    = icon_incomplete
                                numcmde   = wa_alv_fact-numcmde
                                postecmde = wa_alv_fact-postecmde.
    IF sy-subrc = 0.
      CLEAR wa_alv_fact-status.
      MOVE icon_message_question_small TO wa_alv_fact-status.
      MODIFY wt_alv_fact FROM wa_alv_fact.
    ENDIF.
  ENDLOOP.

  w_suppr = 'X'.
  CLEAR: w_consolidation,w_decompose.


ENDFORM.                    " f_suppression
*&---------------------------------------------------------------------*
*&      Form  f_display_invoice
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_display_invoice.

  CALL FUNCTION 'ZBALANCE_IMAGE_DISPLAY'
    EXPORTING
      docnum = w_docnum.

ENDFORM.                    " f_display_invoice
*&---------------------------------------------------------------------*
*&      Form  f_delete_postes_actuels_idoc
*&---------------------------------------------------------------------*
*       Dans  cette étape on supprime les postes de l'actuel Idoc
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_delete_postes_actuels_idoc TABLES pt_idoc_data.

  DATA: BEGIN OF int_edidd OCCURS 30.
          INCLUDE STRUCTURE edidd.
  DATA: END OF int_edidd.

  REFRESH r_seg.
  CLEAR r_seg.

  MOVE 'I' TO r_seg-sign.
  MOVE 'EQ' TO r_seg-option.
  MOVE 'E1EDP01' TO r_seg-low.
  APPEND r_seg.
  MOVE 'E1EDP02' TO r_seg-low.
  APPEND r_seg.
  MOVE 'E1EDP19' TO r_seg-low.
  APPEND r_seg.
  MOVE 'E1EDP26' TO r_seg-low.
  APPEND r_seg.
  MOVE 'E1EDP04' TO r_seg-low.
  APPEND r_seg.

  LOOP AT pt_idoc_data INTO int_edidd.
    IF int_edidd-segnam IN r_seg.
      DELETE pt_idoc_data.
    ENDIF.
  ENDLOOP.

ENDFORM.                    " f_delete_postes_actuels_idoc
*&---------------------------------------------------------------------*
*&      Form  f_inserer_nouveaux_postes
*&---------------------------------------------------------------------*
*       Après avoir effacé les postes de l'Idoc original
*       on ajoute les nouveaux postes "décomposés" "supprimés"
*       ou "consolidés"
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_inserer_nouveaux_postes TABLES pt_idoc_data
                                      pt_factures
                                USING p_bukrs.

  DATA: ws_facture  TYPE zbalance_01.

  DATA : l_segnum LIKE edidd-segnum,
         l_docnum LIKE edidd-docnum,
         l_psgnum LIKE edidd-psgnum,
         l_hlevel_02 LIKE edidd-hlevel VALUE '02',
         l_hlevel_03 LIKE edidd-hlevel VALUE '03'.

  DATA: BEGIN OF int_edidd OCCURS 30.
          INCLUDE STRUCTURE edidd.
  DATA: END OF int_edidd.

  DATA : w_bukrs LIKE t001-bukrs.
  DATA : st_e1edk14 LIKE e1edk14.
  DATA : st_e1edk02 LIKE e1edk02.
  DATA : w_lifnr LIKE lfa1-lifnr.

  LOOP AT pt_idoc_data INTO int_edidd.
    APPEND int_edidd.
  ENDLOOP.

*-----------------------------------------------------------------------
* ITGGH   12.02.2007   CHG_01                               DE5K900049
* get company code from IDOC

**  loop at int_edidd where segnam = 'E1EDK14'.
**    move int_edidd-sdata to st_e1edk14.
**    case st_e1edk14-qualf.
**      when '011'.
**        w_bukrs = st_e1edk14-orgid.
**        exit.
**    endcase.
**  endloop.
*
*  READ TABLE pt_factures INTO ws_facture INDEX 1.
*  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
*    EXPORTING
*      input  = ws_facture-lifnr
*    IMPORTING
*      output = w_lifnr.
*
*  IF p_bukrs IS INITIAL.
*    SELECT * FROM  t076b UP TO 1 ROWS
*           WHERE  parart  = 'LI'
*           AND    konto   = w_lifnr.
*    ENDSELECT.
*    IF sy-subrc EQ 0.
*      w_bukrs = t076b-bukrs.
*    ENDIF.
*  ELSE.
*    w_bukrs = p_bukrs.
*  ENDIF.

  IF p_bukrs IS INITIAL.
    LOOP AT int_edidd WHERE segnam = 'E1EDK14'.
      MOVE int_edidd-sdata TO st_e1edk14.
      CASE st_e1edk14-qualf.
        WHEN '011'.
          WRITE st_e1edk14-orgid TO p_bukrs RIGHT-JUSTIFIED.
      ENDCASE.
    ENDLOOP.
  ENDIF.

  IF p_bukrs IS INITIAL.
    READ TABLE pt_factures INTO ws_facture INDEX 1.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = ws_facture-lifnr
      IMPORTING
        output = w_lifnr.

    SELECT * FROM  t076b UP TO 1 ROWS
           WHERE  parart  = 'LI'
           AND    konto   = w_lifnr.
    ENDSELECT.
    IF sy-subrc EQ 0.
      w_bukrs = t076b-bukrs.
    ENDIF.
  ELSE.
    w_bukrs = p_bukrs.
  ENDIF.
* ITGGH end

  READ TABLE int_edidd WITH KEY segnam = 'E1EDK14'.
  MOVE : int_edidd-segnum TO l_segnum,
         int_edidd-docnum TO l_docnum.
*
*
  LOOP AT pt_factures INTO ws_facture.
*
*   e1edp01
    CLEAR e1edp01.
    MOVE : sy-tabix            TO e1edp01-posex,
           ws_facture-menge    TO e1edp01-menge,
           ws_facture-peinh    TO e1edp01-peinh.

    CLEAR int_edidd.

    ADD 1 TO l_segnum.
    l_psgnum = l_segnum.
    MOVE : sy-mandt        TO int_edidd-mandt,
           l_docnum        TO int_edidd-docnum,
           l_segnum        TO int_edidd-segnum,
           'E1EDP01'       TO int_edidd-segnam,
           '000000'        TO int_edidd-psgnum,
           l_hlevel_02     TO int_edidd-hlevel,
           '0'             TO int_edidd-dtint2,
           e1edp01         TO int_edidd-sdata.
    APPEND int_edidd TO pt_idoc_data.

*   e1edp02
    CLEAR e1edp02.
    MOVE : '001'                TO e1edp02-qualf,
            ws_facture-belnr    TO e1edp02-belnr,
            ws_facture-zeile    TO e1edp02-zeile.

    ADD 1 TO l_segnum.
    MOVE : l_segnum        TO int_edidd-segnum,
           'E1EDP02'       TO int_edidd-segnam,
           l_psgnum        TO int_edidd-psgnum,
           l_hlevel_03     TO int_edidd-hlevel,
           e1edp02         TO int_edidd-sdata.
    APPEND int_edidd TO pt_idoc_data.

** BEGIN INSERT MM_408_DIA
**   e1edp02 qualifiant 16
*    CLEAR st_e1edk02.
*    LOOP AT int_edidd WHERE segnam = 'E1EDK02'.
*      MOVE int_edidd-sdata TO st_e1edk02.
*      CASE st_e1edk02-qualf.
*        WHEN '063'.
*          EXIT.
*      ENDCASE.
*    ENDLOOP.
*
*    IF st_e1edk02-qualf = '063'.
*      CLEAR e1edp02.
*      MOVE : '016'                TO e1edp02-qualf,
*              st_e1edk02-belnr    TO e1edp02-belnr.
**            ws_facture-zeile    TO e1edp02-zeile.
*
*      ADD 1 TO l_segnum.
*      MOVE : l_segnum        TO int_edidd-segnum,
*             'E1EDP02'       TO int_edidd-segnam,
*             l_psgnum        TO int_edidd-psgnum,
*             l_hlevel_03     TO int_edidd-hlevel,
*             e1edp02         TO int_edidd-sdata.
*      APPEND int_edidd TO pt_idoc_data.
*    ENDIF.
** END INSERT MM_408_DIA

*   e1edp19
    CLEAR e1edp19.
    MOVE : '002'                TO e1edp19-qualf,
            ws_facture-matnr    TO e1edp19-idtnr,
            ws_facture-ktext    TO e1edp19-ktext.

    ADD 1 TO l_segnum.
    MOVE : l_segnum        TO int_edidd-segnum,
           'E1EDP19'       TO int_edidd-segnam,
           e1edp19         TO int_edidd-sdata.
    APPEND int_edidd TO pt_idoc_data.

*   e1edp26
    CLEAR e1edp26.
    MOVE : '002'                TO e1edp26-qualf,
            ws_facture-betrg    TO e1edp26-betrg.

    ADD 1 TO l_segnum.
    MOVE : l_segnum        TO int_edidd-segnum,
           'E1EDP26'       TO int_edidd-segnam,
           e1edp26         TO int_edidd-sdata.
    APPEND int_edidd TO pt_idoc_data.


*>> RGI008 bALANCE v2 - ARRONDI
*    CHECK calctva = 'X'.
*   e1edp04
    CLEAR e1edp04.
    MOVE : ws_facture-mwskz  TO e1edp04-mwskz,
           ws_facture-mwsbt  TO e1edp04-mwsbt.


    SELECT SINGLE * FROM t001
                    WHERE bukrs = w_bukrs.
    IF sy-subrc EQ 0.
* ITGGH   23.02.2007  Extension for logical system         DE5K900049
      SELECT * FROM  t076m
            WHERE  parart  = 'LS'
            AND    land1   = t001-land1
            AND    mwart   = ws_facture-mwskz
            AND    mwskz   = ws_facture-mwskz.
      ENDSELECT.
      IF sy-subrc <> 0.
        SELECT * FROM  t076m
              WHERE  parart  = 'LS'
              AND    land1   = ''
              AND    mwart   = ws_facture-mwskz
              AND    mwskz   = ws_facture-mwskz.
        ENDSELECT.
      ENDIF.
      IF sy-subrc EQ 0.
        e1edp04-msatz = t076m-mwsatz.
      ELSE.
* ITGGH   23.02.2007  Extension for logical system    end

        SELECT * FROM  t076m
                 WHERE  parart  = 'LI'
                 AND    land1   = t001-land1
                 AND    konto   = ws_facture-lifnr
                 AND    mwart   = ws_facture-mwskz
                 AND    mwskz   = ws_facture-mwskz.
        ENDSELECT.

        IF sy-subrc NE 0.
          SELECT * FROM  t076m
                   WHERE  parart  = 'LI'
                   AND    konto   = ws_facture-lifnr
                   AND    mwart   = ws_facture-mwskz
                   AND    mwskz   = ws_facture-mwskz.
          ENDSELECT.
        ENDIF.
        IF sy-subrc EQ 0.
          e1edp04-msatz = t076m-mwsatz.
        ELSE.
          CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
            EXPORTING
              input  = ws_facture-lifnr
            IMPORTING
              output = w_lifnr.
          SELECT * FROM  t076m
           WHERE  parart  = 'LI'
           AND    land1   = t001-land1
           AND    konto   = w_lifnr
           AND    mwart   = ws_facture-mwskz
           AND    mwskz   = ws_facture-mwskz.
          ENDSELECT.

          IF sy-subrc NE 0.
            SELECT * FROM  t076m
                     WHERE  parart  = 'LI'
                     AND    konto   = w_lifnr
                     AND    mwart   = ws_facture-mwskz
                     AND    mwskz   = ws_facture-mwskz.
            ENDSELECT.
          ENDIF.
          IF sy-subrc EQ 0.
            e1edp04-msatz = t076m-mwsatz.
          ELSE.
            CLEAR e1edp04-msatz.
          ENDIF.
        ENDIF.
      ENDIF.     "* ITGGH 23.02.2007 Extension for logical system
    ENDIF.

    ADD 1 TO l_segnum.
    MOVE : l_segnum        TO int_edidd-segnum,
           'E1EDP04'       TO int_edidd-segnam,
           e1edp04         TO int_edidd-sdata.
    APPEND int_edidd TO pt_idoc_data.

  ENDLOOP.

ENDFORM.                    " f_inserer_nouveaux_postes
*&---------------------------------------------------------------------*
*&      Form  det_sel
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM det_sel.

  REFRESH wt_alv_sel.
  CALL METHOD o_fact_grid->get_selected_rows
    IMPORTING
      et_index_rows = wt_alv_sel.

  CLEAR: w_consolidation, w_decompose, w_suppr.

ENDFORM.                    " det_sel
*&---------------------------------------------------------------------*
*&      Form  f_check_multi
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_check_multi CHANGING w_multi.

  CLEAR : wa_oldcmde, wa_newcmde, w_multi.


  LOOP AT wt_alv_sel INTO wa_alv_sel.
    READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index.
    MOVE wa_alv_fact-numcmde TO wa_newcmde.
    IF wa_oldcmde IS INITIAL.
      MOVE wa_alv_fact-numcmde TO wa_oldcmde.
    ENDIF.

    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = wa_newcmde
      IMPORTING
        output = wa_newcmde.

    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = wa_oldcmde
      IMPORTING
        output = wa_oldcmde.

    IF wa_newcmde NE wa_oldcmde.
      w_multi = 'X'.
    ENDIF.
  ENDLOOP.

ENDFORM.                    " f_check_multi
*&---------------------------------------------------------------------*
*&      Form  f_maj_grid
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_maj_grid.

  wt_alv_cmde_aff[] = wt_alv_cmde[].
  PERFORM cache_item_rapproches.

  CALL METHOD o_fact_grid->refresh_table_display.
  CALL METHOD o_cmde_grid->refresh_table_display.

ENDFORM.                    " f_maj_grid
*&---------------------------------------------------------------------*
*&      Form  f_setnoneditable
*&---------------------------------------------------------------------*
* This forms sets the style of columns ARTICLE, MAKTX and QUANTITE.
* editable
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_setnoneditable.

  DATA: BEGIN OF gt_outtab OCCURS 0.     "with header line
          INCLUDE STRUCTURE sflight.
  DATA: celltab TYPE lvc_t_styl.
  DATA: END OF gt_outtab.

  DATA: t_celltab TYPE lvc_t_styl,
        ls_celltab TYPE lvc_s_styl,
        l_index LIKE sy-tabix,
        l_mode TYPE raw4,
        ls_outtab LIKE LINE OF gt_outtab.

  LOOP AT wt_alv_fact INTO wa_alv_fact.

    l_index = sy-tabix.
    REFRESH t_celltab.

    l_mode = cl_gui_alv_grid=>mc_style_disabled.

    ls_celltab-fieldname = 'ARTICLE'.
    ls_celltab-style = l_mode.
    INSERT ls_celltab INTO TABLE t_celltab.
    ls_celltab-fieldname = 'MAKTX'.
    ls_celltab-style = l_mode.
    INSERT ls_celltab INTO TABLE t_celltab.
    ls_celltab-fieldname = 'QUANTITE'.
    ls_celltab-style = l_mode.
    INSERT ls_celltab INTO TABLE t_celltab.

    INSERT LINES OF t_celltab INTO TABLE ls_outtab-celltab.
    MODIFY wt_alv_fact FROM wa_alv_fact INDEX l_index.

  ENDLOOP.
ENDFORM.                    " f_setnoneditable
*&---------------------------------------------------------------------*
*&      Form  set_drag_drop
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_W_SOURCE  text
*      <--P_WS_LAYOUT_FACT  text
*      <--P_WT_CATALOG_FACT  text
*----------------------------------------------------------------------*
FORM set_drag_drop USING value(i_grid) TYPE i
                CHANGING cs_layo TYPE lvc_s_layo
                         ct_fcat TYPE lvc_t_fcat.

  DATA: l_effect                     TYPE i,
        l_dragdrop_handle            TYPE i,
        ls_dragdrop                  TYPE lvc_s_dd01,
        ls_drag_cell                 TYPE lvc_s_drdr,
        lt_drag_cells                TYPE lvc_t_drdr,
        ls_fcat                      TYPE lvc_s_fcat,
        l_field                      TYPE lvc_fname,
        l_source                     TYPE char1,
        l_target                     TYPE char1,
        ls_outtab                    TYPE t_fact,
        ls_tabcolor                  TYPE lvc_s_scol,
        lt_tabcolor                  TYPE lvc_t_scol.

  CASE i_grid.
    WHEN con_source.
      l_source = con_true.
    WHEN con_target.
      l_target = con_true.
  ENDCASE.

  CREATE OBJECT o_dragdrop.
  CREATE OBJECT o_dragdrop_background.

  CASE gs_test-dragdrop_effect.
    WHEN con_dragdrop_effect_move.
      l_effect = cl_dragdrop=>move.
    WHEN con_dragdrop_effect_copy.
      l_effect = cl_dragdrop=>copy.
    WHEN con_dragdrop_effect_all.
      l_effect = cl_dragdrop=>move + cl_dragdrop=>copy.
  ENDCASE.

  CASE gs_test-dragdrop_type.
    WHEN con_dragdrop_row.

      CALL METHOD o_dragdrop->add
        EXPORTING
          flavor     = 'Line'                               "#EC NOTEXT
          dragsrc    = l_source
          droptarget = l_target
          effect     = l_effect.

      CALL METHOD o_dragdrop->get_handle
        IMPORTING
          handle = l_dragdrop_handle.

      ls_dragdrop-cntr_ddid = l_dragdrop_handle. "auch bei leerem Grid
      ls_dragdrop-grid_ddid = space.
      ls_dragdrop-col_ddid  = space.
      ls_dragdrop-row_ddid  = l_dragdrop_handle.
      ls_dragdrop-fieldname = space.
      cs_layo-s_dragdrop    = ls_dragdrop.

    WHEN con_dragdrop_column.
*... setzen für Spalten
*    Im Feldkatalog wird DRAGDROPID mit Handle gefüllt
      CALL METHOD o_dragdrop->add
        EXPORTING
          flavor     = 'Line'
          dragsrc    = l_source
          droptarget = l_target
          effect     = l_effect.

      CALL METHOD o_dragdrop->get_handle
        IMPORTING
          handle = l_dragdrop_handle.

      ls_dragdrop-cntr_ddid = l_dragdrop_handle. "auch bei leerem Grid
      ls_dragdrop-grid_ddid = space.
      ls_dragdrop-col_ddid  = l_dragdrop_handle.
      ls_dragdrop-row_ddid  = space.
      ls_dragdrop-fieldname = space.
      cs_layo-s_dragdrop    = ls_dragdrop.

      ls_fcat-dragdropid    = l_dragdrop_handle.
      LOOP AT gs_test-dragdrop_fields INTO l_field.
        ls_tabcolor-fname     = l_field.
        ls_tabcolor-color-col = cl_gui_resources=>list_col_negative.
        ls_tabcolor-color-int = 0.
        ls_tabcolor-color-inv = 0.
        ls_tabcolor-nokeycol  = con_true.
        APPEND ls_tabcolor TO lt_tabcolor.

        MODIFY ct_fcat FROM ls_fcat
                       TRANSPORTING dragdropid
                       WHERE fieldname = l_field.
      ENDLOOP.

*      ls_outtab-tabcolor = lt_tabcolor.
*      MODIFY gt_outtab_source FROM ls_outtab
*                       TRANSPORTING tabcolor
*                       WHERE tabcolor IS initial.

    WHEN con_dragdrop_cell.
*... setzen für Zellen
      CALL METHOD o_dragdrop->add
        EXPORTING
          flavor     = 'Line'
          dragsrc    = l_source
          droptarget = l_target
          effect     = l_effect.

      CALL METHOD o_dragdrop->get_handle
        IMPORTING
          handle = l_dragdrop_handle.

      ls_dragdrop-cntr_ddid = l_dragdrop_handle. "auch bei leerem Grid
      ls_dragdrop-grid_ddid = space.
      ls_dragdrop-col_ddid  = space.
      ls_dragdrop-row_ddid  = space.
      ls_dragdrop-fieldname = 'HANDLE_DRAGDROP'.
      cs_layo-s_dragdrop    = ls_dragdrop.

      LOOP AT gs_test-dragdrop_fields INTO l_field.
        ls_drag_cell-fieldname  = l_field.
        ls_drag_cell-dragdropid = l_dragdrop_handle.
        INSERT ls_drag_cell INTO TABLE lt_drag_cells.

        ls_tabcolor-fname     = l_field.
        ls_tabcolor-color-col = cl_gui_resources=>list_col_negative.
        ls_tabcolor-color-int = 0.
        ls_tabcolor-color-inv = 0.
        ls_tabcolor-nokeycol  = con_true.
        APPEND ls_tabcolor TO lt_tabcolor.
      ENDLOOP.

      DATA: l_index   TYPE i,
            l_erg     TYPE i.

*      LOOP AT gt_outtab_source INTO ls_outtab.
*        l_index = sy-tabix.
*        l_erg = sy-tabix MOD 2.
*        IF l_erg EQ 0.
*          ls_outtab-handle_dragdrop = lt_drag_cells.
*          ls_outtab-tabcolor        = lt_tabcolor.
*          MODIFY gt_outtab_source INDEX l_index FROM ls_outtab
*                           TRANSPORTING handle_dragdrop
*                                        tabcolor.
*        ENDIF.
*      ENDLOOP.

  ENDCASE.


ENDFORM.                    " set_drag_drop
*&---------------------------------------------------------------------*
*&      Form  layout_fact
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      <--P_WS_layout_fact  text
*----------------------------------------------------------------------*
FORM layout CHANGING cs_layo TYPE lvc_s_layo.

  cs_layo-stylefname  = space.

  cs_layo-cwidth_opt  = space.
  cs_layo-zebra       = space.
  cs_layo-smalltitle  = space.
  cs_layo-graphics    = space.
  cs_layo-frontend    = space.
  cs_layo-template    = space.

*... ALV-Control: Gridcustomizing
  cs_layo-no_colexpd  = space.
  cs_layo-no_hgridln  = space.
  cs_layo-no_vgridln  = space.
  cs_layo-no_rowmark  = space.
  cs_layo-no_headers  = space.
  cs_layo-no_merging  = space.
  cs_layo-grid_title  = space.

  cs_layo-no_toolbar  = space.

  cs_layo-sel_mode    = 'D'.

  cs_layo-box_fname   = space.

  cs_layo-sgl_clk_hd  = space.

*... ALV-Control: Summenoptionen
  cs_layo-totals_bef  = space.
  cs_layo-no_totline  = space.
  cs_layo-numc_total  = space.
  cs_layo-no_utsplit  = space.

*... ALV-Control: Exceptions
  cs_layo-excp_fname  = space.
  cs_layo-excp_rolln  = space.
  cs_layo-excp_conds  = space.
  cs_layo-excp_led    = space.

*... ALV-Control: Steuerung Interaktion
  cs_layo-detailinit  = space.
  cs_layo-detailtitl  = space.
  cs_layo-keyhot      = space.
  cs_layo-no_keyfix   = space.
  cs_layo-no_author   = space.
  CLEAR cs_layo-s_dragdrop.

*... ALV-Control: Farben
  cs_layo-info_fname  = space.
  cs_layo-ctab_fname  = 'TABCOLOR'.

*... ALV-Control: Eingabefähigkeit
  cs_layo-edit        = space.
  cs_layo-edit_mode   = space.

  cs_layo-no_rowins   = space.
  cs_layo-no_rowmove  = space.

*... ALV-Control: Web-Optionen
  cs_layo-weblook     = space.
  cs_layo-webstyle    = space.
  cs_layo-webrows     = space.
  cs_layo-webxwidth   = space.
  cs_layo-webxheight  = space.

ENDFORM.                    " layout_fact
*&---------------------------------------------------------------------*
*&      Form  event_ondrag
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_E_ROW  text
*      -->P_E_COLUMN  text
*      -->P_E_DRAGDROPOBJ  text
*----------------------------------------------------------------------*
FORM event_ondrag USING e_row         TYPE lvc_s_row
                        e_column      TYPE lvc_s_col
                        e_dragdropobj TYPE REF TO
                                           cl_dragdropobject.

  DATA: ls_alv_cmde TYPE t_cmde,
        l_obj     TYPE REF TO lcl_dragdrop_obj.

  CREATE OBJECT l_obj.

* On récupère ici l'enregistrement complet de la ligne de commande qui
* a été déplacée
  READ TABLE wt_alv_cmde_aff INTO ls_alv_cmde INDEX e_row-index.
  IF sy-subrc EQ 0.
    l_obj->line           = ls_alv_cmde.
    l_obj->index          = e_row-index.
    e_dragdropobj->object = l_obj.
  ENDIF.



ENDFORM.                    " event_ondrag
*&---------------------------------------------------------------------*
*&      Form  event_ondrop
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_E_ROW  text
*      -->P_E_COLUMN  text
*      -->P_E_DRAGDROPOBJ  text
*----------------------------------------------------------------------*
FORM event_ondrop USING e_row         TYPE lvc_s_row
                        e_column      TYPE lvc_s_col
                        e_dragdropobj TYPE REF TO
                                           cl_dragdropobject.

  DATA: l_obj     TYPE REF TO lcl_dragdrop_obj,
        ls_alv_fact TYPE t_fact,
        ls_alv_cmde TYPE t_cmde.

  CREATE OBJECT l_obj.

  CHECK NOT e_row-index IS INITIAL.

  READ TABLE wt_alv_fact INTO ls_alv_fact INDEX e_row-index.


  CATCH SYSTEM-EXCEPTIONS move_cast_error = 1.
    l_obj ?= e_dragdropobj->object.
    ls_alv_cmde = l_obj->line.
    ls_alv_fact-postecmde = ls_alv_cmde-poste.
    MODIFY wt_alv_fact FROM ls_alv_fact INDEX e_row-index.
  ENDCATCH.
  IF sy-subrc <> 0.
    CALL METHOD e_dragdropobj->abort.
  ENDIF.
ENDFORM.                    " event_ondrop
*&---------------------------------------------------------------------*
*&      Form  ondropcomplete
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM ondropcomplete.

  PERFORM f_maj_grid.

ENDFORM.                    " ondropcomplete
*&---------------------------------------------------------------------*
*&      Form  decompose_poste
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM decompose_poste.

  DATA : wa_status(4) TYPE c,
         wa_maktx LIKE ekpo-txz01.


  wa_status = icon_message_question_small.

  CLEAR w_new_poste.

  CALL FUNCTION 'POPUP_TO_GET_ONE_VALUE'
    EXPORTING
    textline1            = 'Nombre de postes supplémentaires'(t43)
*   TEXTLINE2            = ' '
*   TEXTLINE3            = ' '
    titel                =  'Saisissez le nombre de postes à créer'(t44)
    valuelength          = '2'
   IMPORTING
     answer               = l_answer
     value1               = l_value
   EXCEPTIONS
     titel_too_long       = 1
     OTHERS               = 2
            .
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.
  CHECK l_answer = 'J'.

  LOOP AT wt_alv_sel INTO wa_alv_sel.
    READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index.
    MOVE  'POSTE DECOMPOSE' TO wa_alv_fact-maktx.
    MODIFY wt_alv_fact INDEX wa_alv_sel-index FROM wa_alv_fact.
  ENDLOOP.


  IF sy-subrc EQ 0.
    PACK l_value TO w_new_poste.
    MOVE :  wa_alv_fact-postefact TO l_poste,
            wa_alv_fact-numcmde   TO l_belnr,
            wa_alv_fact-article   TO l_ktext,
            wa_maktx              TO l_maktx,
            wa_alv_fact-codetva   TO l_codetva.

    DO w_new_poste TIMES.
      CLEAR : wa_alv_fact.
      l_poste  = l_poste + 1.
      MOVE: l_poste TO wa_alv_fact-postefact,
      l_belnr TO wa_alv_fact-numcmde,
      l_ktext TO wa_alv_fact-maktx,
      l_maktx TO wa_alv_fact-maktx,
      l_codetva TO wa_alv_fact-codetva,
      wa_status TO wa_alv_fact-status.
      APPEND wa_alv_fact TO wt_alv_fact.
    ENDDO.
    w_decompose = 'X'.
    CLEAR: w_suppr,w_consolidation.
  ELSE.

    READ TABLE wt_alv_fact INTO wa_alv_fact
                           WITH KEY numcmde = wa_oldcmde.
    PACK l_value TO w_new_poste.
    MOVE : wa_oldcmde          TO l_belnr,
           wa_maktx            TO l_ktext,
           wa_alv_fact-codetva TO l_codetva.

    DO w_new_poste TIMES.
      CLEAR : wa_alv_fact.
      MOVE: l_belnr TO wa_alv_fact-numcmde,
      l_ktext TO wa_alv_fact-maktx,
      l_codetva TO wa_alv_fact-codetva,
      wa_status TO wa_alv_fact-status,
      sy-tabix TO wa_alv_fact-postefact.
      APPEND wa_alv_fact TO wt_alv_fact.
    ENDDO.
    w_decompose = 'X'.
    CLEAR: w_suppr,w_consolidation.

  ENDIF.

ENDFORM.                    " decompose_poste
*&---------------------------------------------------------------------*
*&      Form  ajout_poste
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM ajout_poste.

  CLEAR wa_alv_fact.
  CLEAR wa_alv_sel.
  CALL SCREEN 9101 STARTING AT 10 10.

ENDFORM.                    " ajout_poste
*&---------------------------------------------------------------------*
*&      Form  recalculate_line
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM recalculate_line.

  IF NOT wa_alv_fact-codetva IS INITIAL.

* CHG - JMB - 27.03.2007
* Old code :
*    PERFORM get_vatrate.

* New code :
    PERFORM get_vatrate  USING    wa_alv_fact-codetva
                                  zbalance_04-bukrs
                                  zbalance_04-lifnr
                         CHANGING w_taux_tva.
* END CHG - JMB - 27.03.2007

  ENDIF.

* Si le conditionnement n'est pas renseigne on met par défaut 1
  IF wa_alv_fact-condi IS INITIAL.
    wa_alv_fact-condi = 1.
  ENDIF.

* Change - Jean-Michel BRUNOD - 03.04.2007
* Old code :
** Calcul du total HT
*  wa_alv_fact-totalht = wa_alv_fact-quantite * wa_alv_fact-prixunit.
** Change - Jean-Michel BRUNOD - 19.03.2007
** Old code :
**  wa_alv_fact-totalht = wa_alv_fact-totalht * wa_alv_fact-condi.
** New code :
*  wa_alv_fact-totalht = wa_alv_fact-totalht / wa_alv_fact-condi.
** End change - Jean-Michel BRUNOD - 19.03.2007

* New code :
* Calcul du prix unitaire
  IF NOT wa_alv_fact-quantite IS INITIAL.
    wa_alv_fact-prixunit =
    ( wa_alv_fact-totalht / wa_alv_fact-quantite ) * wa_alv_fact-condi .
  ENDIF.
* End change - Jean-Michel BRUNOD - 03.04.2007

* Calcul de la TVA
*>> RGI008 Balance V2
  IF calctva = 'X'.
    wa_alv_fact-tva = wa_alv_fact-totalht * ( w_taux_tva / 100 ).
  ENDIF.

  wa_alv_fact-totalttc = wa_alv_fact-totalht + wa_alv_fact-tva.

* Récupération de la description article
  IF wa_alv_fact-maktx IS INITIAL AND
  NOT wa_alv_fact-article IS INITIAL.
    SELECT SINGLE * FROM  makt CLIENT SPECIFIED
           WHERE  mandt  = sy-mandt
           AND    matnr  = wa_alv_fact-article
           AND    spras  = sy-langu.
    IF sy-subrc EQ 0.
      MOVE makt-maktx TO wa_alv_fact-maktx.
    ENDIF.
  ENDIF.


ENDFORM.                    " recalculate_line
*&---------------------------------------------------------------------*
*&      Form  check_line
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM check_line.

  CLEAR w_subrc.

  IF wa_alv_sel IS INITIAL.
    LOOP AT wt_alv_fact INTO ws_fact.
      IF ws_fact-postefact = wa_alv_fact-postefact.
        PERFORM message_box USING text-m04 text-m08 'E'.
        w_subrc = 4.
      ENDIF.
    ENDLOOP.
  ENDIF.

  CHECK w_subrc EQ 0.

  IF wa_alv_fact-quantite IS INITIAL.
    PERFORM message_box USING text-m04 text-m09 'E'.
    w_subrc = 4.
  ENDIF.
  IF wa_alv_fact-prixunit IS INITIAL.
    PERFORM message_box USING text-m04 text-m10 'E'.
    w_subrc = 4.
  ENDIF.
  IF wa_alv_fact-codetva IS INITIAL." AND calctva = 'X'.
    PERFORM message_box USING text-m04 text-m11 'E'.
    w_subrc = 4.
  ENDIF.




ENDFORM.                    " check_line
*&---------------------------------------------------------------------*
*&      Form  save_line
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM save_line.

  IF wa_alv_sel-index IS INITIAL.
    wa_alv_fact-status = icon_warning.
    APPEND wa_alv_fact TO wt_alv_fact.
  ELSE.
    MODIFY wt_alv_fact FROM wa_alv_fact INDEX wa_alv_sel-index.
  ENDIF.

*>> START RGI003 - Manual Price.
  CLEAR w_manual_price.
*>> END   RGI003

ENDFORM.                    " save_line
*&---------------------------------------------------------------------*
*&      Form  add_item_to_fact
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM add_item_to_fact.

  CLEAR l_cpt_decompo.

  PERFORM det_sel.

*   DESCRIBE TABLE wt_alv_sel LINES l_cpt_decompo.
*   IF l_cpt_decompo EQ 0.
  w_answer = 2.
*   ELSE.
*     w_answer = 1.
*   ENDIF.

  PERFORM f_decomposition USING  w_answer.

*   Vérification de la numérotation des postes
*   et modif le cas échéant
  LOOP AT wt_alv_fact INTO wa_alv_fact.
    IF wa_alv_fact-postefact NE sy-tabix.
      MOVE sy-tabix TO wa_alv_fact-postefact.
      MODIFY wt_alv_fact FROM wa_alv_fact.
    ENDIF.
  ENDLOOP.
*   On rafraichi l'affichage de la liste ALV
  PERFORM f_maj_grid.

ENDFORM.                    " add_item_to_fact
*&---------------------------------------------------------------------*
*&      Form  delete_item_from_fact
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM delete_item_from_fact.

  PERFORM det_sel.

  IF  wt_alv_sel IS INITIAL.
    MESSAGE s531(0u) WITH text-w62.
    EXIT.
  ENDIF.

  CLEAR : l_cpt_conso, w_answer.

  MOVE text-t45 TO w_diagnosetext.
* Begin of UPG_ECC5_TO_ECC6 DEL TKA001
*  CALL FUNCTION 'POPUP_TO_DECIDE'
*    EXPORTING
*      textline1         = w_diagnosetext
*      text_option1      = 'Consolidation'(t47)
*      text_option2      = 'Suppression'(t48)
*      icon_text_option1 = 'ICON_COLLAPSE'
*      icon_text_option2 = 'ICON_DELETE'
*      titel             = text-t49
*      start_column      = 25
*      start_row         = 6
*      cancel_display    = 'X'
*    IMPORTING
*      answer            = w_answer.
* End of   UPG_ECC5_TO_ECC6 DEL TKA001

* Begin of UPG_ECC5_TO_ECC6 INS TKA001
CALL FUNCTION 'POPUP_TO_CONFIRM'
  EXPORTING
   TITLEBAR                    = text-t49
   TEXT_QUESTION               = w_diagnosetext
   TEXT_BUTTON_1               = 'Consolidation'(t47)
   ICON_BUTTON_1               = 'ICON_COLLAPSE'
   TEXT_BUTTON_2               = 'Suppression'(t48)
   ICON_BUTTON_2               = 'ICON_DELETE'
   DISPLAY_CANCEL_BUTTON       = 'X'
   START_COLUMN                = 25
   START_ROW                   = 6
 IMPORTING
   ANSWER                      = w_answer
 EXCEPTIONS
   TEXT_NOT_FOUND              = 1
   OTHERS                      = 2
          .
* End of   UPG_ECC5_TO_ECC6 INS TKA001

  IF w_answer NE 'A'.
    IF w_answer EQ '1'.
      PERFORM f_consolidation.
    ELSEIF w_answer EQ '2'.
      PERFORM f_suppression.
    ENDIF.
*   Vérification de la numérotation des postes et modif le cas échéant
    PERFORM renumerate_items.
    PERFORM f_maj_grid.
  ENDIF.


ENDFORM.                    " delete_item_from_fact
*&---------------------------------------------------------------------*
*&      Form  renumerate_items
*&---------------------------------------------------------------------*
*       Cette fonction permet de renuméroter toutes les lignes de la
*       facture
*----------------------------------------------------------------------*
FORM renumerate_items.

  LOOP AT wt_alv_fact INTO wa_alv_fact.
    IF wa_alv_fact-postefact NE sy-tabix.
      MOVE sy-tabix TO wa_alv_fact-postefact.
      MODIFY wt_alv_fact FROM wa_alv_fact.
    ENDIF.
  ENDLOOP.

ENDFORM.                    " renumerate_items
*&---------------------------------------------------------------------*
*&      Form  green_tick_update
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM green_tick_update.

  LOOP AT wt_alv_fact INTO wa_alv_fact.
    CLEAR wa_alv_fact-status.
    IF NOT wa_alv_fact-numcmde IS INITIAL AND
       NOT wa_alv_fact-postecmde IS INITIAL.
      MOVE icon_checked TO wa_alv_fact-status.
    ELSE.
      MOVE icon_incomplete TO wa_alv_fact-status.
    ENDIF.
    MODIFY wt_alv_fact FROM wa_alv_fact.
  ENDLOOP.

ENDFORM.                    " green_tick_update
*&---------------------------------------------------------------------*
*&      Form  get_vatrate
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_vatrate  USING    p_codetva
                           p_bukrs
                           p_lifnr
                  CHANGING p_taux.

* CHG JMB - 27.03.2007
* Replace wa_alv_fact-codetva by p_codetva
*         zbalance_04-lifnr by p_lifnr
*         zbalance_04-bukrs by p_bukrs
*         w_taux_tva by p_taux

  SELECT SINGLE land1 FROM t001
                      INTO wl_land1
                      WHERE bukrs = p_bukrs.
  IF sy-subrc EQ 0.
* it_scu20070222 Extension for logical system begin
    SELECT * FROM  t076m
          WHERE  parart  = 'LS'
          AND    land1   = wl_land1
          AND    mwart   = p_codetva
          AND    mwskz   = p_codetva.
    ENDSELECT.
    IF sy-subrc <> 0.
      SELECT * FROM  t076m
            WHERE  parart  = 'LS'
            AND    land1   = ''
            AND    mwart   = p_codetva
            AND    mwskz   = p_codetva.
      ENDSELECT.
      IF sy-subrc <> 0.
* it_scu20070222 Extension for logical system end
        SELECT * FROM  t076m
              WHERE  parart  = 'LI'
              AND    land1   = wl_land1
              AND    konto   = p_lifnr
              AND    mwart   = p_codetva
              AND    mwskz   = p_codetva.
        ENDSELECT.
        IF sy-subrc NE 0.
          SELECT * FROM  t076m
                   WHERE  parart  = 'LI'
                   AND    konto   = p_lifnr
                   AND    mwart   = p_codetva
                   AND    mwskz   = p_codetva.
          ENDSELECT.
        ENDIF.
      ENDIF. "* it_scu20070222 Extension for logical system
    ENDIF. "* it_scu20070222 Extension for logical system
    IF sy-subrc EQ 0..
      p_taux = t076m-mwsatz.
    ELSE.
      PERFORM message_box USING text-m04 text-m12 'E'.

    ENDIF.
  ENDIF.

ENDFORM.                    " get_vatrate
*&---------------------------------------------------------------------*
*&      Form  update_amounts
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM update_amounts.

  DATA : w_dyn_value LIKE dynpread-fieldvalue.
  DATA : wt_dynpfields LIKE dynpread OCCURS 0 WITH HEADER LINE.
  DATA : w_ecart LIKE zbalance_04-ttc.

  CLEAR zbalance_04-ttc.
  CLEAR zbalance_04-tva.

* Change - JMB - 15.03.2007
* The caculation method of the VAT amount is false
* Instead of adding Item VAT amount to get the total VAT amount, we have
* to calculate it globally

* Old code
*  LOOP AT wt_alv_fact INTO wa_alv_fact.
*    ADD wa_alv_fact-totalttc TO zbalance_04-ttc.
*    ADD wa_alv_fact-tva TO zbalance_04-tva.
*  ENDLOOP.

* New code



* Initialization
  REFRESH wt_alv_header_vat.

* Here we collect the amounts by vat code in an internal table.
* The aim is to have for each vat code the total net amount.
  LOOP AT wt_alv_fact INTO wa_alv_fact.
    wa_alv_header_vat-totalht = wa_alv_fact-totalht.
    wa_alv_header_vat-codetva = wa_alv_fact-codetva.
*>> ADD RGI008 Balance V2
    wa_alv_header_vat-tva = wa_alv_fact-tva.
    COLLECT wa_alv_header_vat INTO wt_alv_header_vat.
  ENDLOOP.

*>> RGI008 V2 - Balance
* For each VAT code we get the rate and calculate the vat globally
  LOOP AT wt_alv_header_vat INTO wa_alv_header_vat.
*    PERFORM get_vatrate  USING    wa_alv_header_vat-codetva
*                                  zbalance_04-bukrs
*                                  zbalance_04-lifnr
*                         CHANGING w_taux_tva.
*
*    wa_alv_header_vat-tva =
*    wa_alv_header_vat-totalht * ( w_taux_tva / 100 ).
    MODIFY wt_alv_header_vat FROM wa_alv_header_vat.
  ENDLOOP.

* Here the internal table is complete with vat code, net amount and vat
* amount. With this we can calculate global amounts
  LOOP AT wt_alv_header_vat INTO wa_alv_header_vat.
    ADD wa_alv_header_vat-tva TO zbalance_04-tva.
    ADD wa_alv_header_vat-tva TO zbalance_04-ttc.
    ADD wa_alv_header_vat-totalht TO zbalance_04-ttc.
  ENDLOOP.
* End change - JMB - 15.03.2007


  w_dyn_value = zbalance_04-ttc.

*>> Start RGI003 - Warning Message for Total Idoc <> New Total TTC
  IF zbalance_04-ttc NE zbalance_04-ttc_idoc.
    w_ecart = ABS( zbalance_04-ttc - zbalance_04-ttc_idoc ).
    MESSAGE i051(zsapbalance) WITH w_ecart zbalance_04-waers .
  ENDIF.
*>> End   RGI003 -

  CALL FUNCTION 'SET_DYNP_VALUE'
    EXPORTING
      i_field = 'ZBALANCE_04-TTC'
      i_repid = w_repid
      i_dynnr = w_dynnr
      i_value = w_dyn_value.


  w_dyn_value = zbalance_04-tva.

  CALL FUNCTION 'SET_DYNP_VALUE'
    EXPORTING
      i_field = 'ZBALANCE_04-TVA'
      i_repid = w_repid
      i_dynnr = w_dynnr
      i_value = w_dyn_value.

  LEAVE SCREEN.

ENDFORM.                    " update_amounts
*&---------------------------------------------------------------------*
*&      Form  load_data_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM load_data_cadd.
  DATA : ws_variante_cadd TYPE disvariant.
  DATA : ws_layout_cadd   TYPE lvc_s_layo.
  DATA : ws_toolbar_cadd TYPE ui_functions.
  DATA : w_save.
  DATA : l_consistency_check TYPE char1.

  PERFORM catalog_params_cadd.
  PERFORM toolbar_params USING 'CADD'
                       CHANGING ws_toolbar_cadd.
  PERFORM layout CHANGING ws_layout_cadd.


* Chargement des données dans la liste ALV et affichage
  CALL METHOD o_cadd_grid->set_table_for_first_display
    EXPORTING
      i_structure_name     = 'T_CADD'
      is_variant           = ws_variante_cadd
      is_layout            = ws_layout_cadd
      i_save               = w_save
      it_toolbar_excluding = ws_toolbar_cadd
    CHANGING
      it_fieldcatalog      = wt_catalog_cadd
      it_sort              = wt_sort_cadd
      it_outtab            = wt_alv_cadd.

  CREATE OBJECT o_event_receiver_cadd.
  SET HANDLER o_event_receiver_cadd->handle_toolbar
              FOR o_cadd_grid.
  SET HANDLER o_event_receiver_cadd->handle_user_command
              FOR o_cadd_grid.
  CALL METHOD o_cadd_grid->set_toolbar_interactive.

ENDFORM.                    " load_data_cadd
*&---------------------------------------------------------------------*
*&      Form  catalog_params_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM catalog_params_cadd.
  DATA: wa_fieldcatalog TYPE lvc_s_fcat.

  wa_fieldcatalog-fieldname  = 'HKONT'.
  wa_fieldcatalog-ref_table  = 'ZBAL_CADD'.
  wa_fieldcatalog-ref_field  = 'HKONT'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname  = 'ALCKZ'.
  wa_fieldcatalog-ref_table  = 'E1EDK05'.
  wa_fieldcatalog-ref_field  = 'ALCKZ'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'BETRG'.
  wa_fieldcatalog-ref_table = 'E1EDK05'.
  wa_fieldcatalog-ref_field = 'BETRG'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname  = 'MWSKZ'.
  wa_fieldcatalog-ref_table  = 'E1EDK05'.
  wa_fieldcatalog-ref_field  = 'MWSKZ'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname  = 'KOSTL'.
  wa_fieldcatalog-ref_table  = 'ZBAL_CADD'.
  wa_fieldcatalog-ref_field  = 'KOSTL'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname  = 'PSPNR'.
  wa_fieldcatalog-ref_table  = 'ZBAL_CADD'.
  wa_fieldcatalog-ref_field  = 'PSPNR'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname  = 'AUFNR'.
  wa_fieldcatalog-ref_table  = 'ZBAL_CADD'.
  wa_fieldcatalog-ref_field  = 'AUFNR'.
  wa_fieldcatalog-colddictxt = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

  wa_fieldcatalog-fieldname = 'DUMMY'.
  wa_fieldcatalog-no_out    = 'X'.
  APPEND wa_fieldcatalog TO wt_catalog_cadd.  CLEAR wa_fieldcatalog.

ENDFORM.                    " catalog_params_cadd
*&---------------------------------------------------------------------*
*&      Form  workflow_event_create_updtask
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_W_BELNR  text
*      -->P_W_GJAHR  text
*      -->P_C_EVENT_BLOCKEDQUANT  text
*----------------------------------------------------------------------*
FORM workflow_event_create_updtask USING  i_belnr TYPE re_belnr
                                          i_gjahr TYPE gjahr
                                        i_event LIKE swetypecou-event.

  DATA: t_event_container LIKE swcont OCCURS 1
                     WITH HEADER LINE. " workflow container

  DATA: BEGIN OF t_assignments OCCURS 1.
          INCLUDE STRUCTURE wfas2.   " workflow assignment
  DATA: END OF t_assignments.

  DATA: BEGIN OF s_rbkp_objkey,      " objekt key invoice
          belnr TYPE re_belnr,
          gjahr TYPE gjahr,
        END OF s_rbkp_objkey.

  DATA: f_objkey_workflow LIKE sweinstcou-objkey. "objekt key workflow

  s_rbkp_objkey-belnr = i_belnr.
  s_rbkp_objkey-gjahr = i_gjahr.
  f_objkey_workflow = s_rbkp_objkey. " objkey workflow = 70 CHAR

  IF i_event = c_event_assigned.
    t_assignments-asgtp  = c_objtyp_bus2081.   " object type
    t_assignments-asgky  = f_objkey_workflow.  " object ID
    t_assignments-action = 'I'.      " insert
    APPEND t_assignments.

    CALL FUNCTION 'WF_COMMIT'
      EXPORTING
        in_update_task = 'X'
      TABLES
        assignments    = t_assignments.
  ELSE.
    CALL FUNCTION 'SWE_EVENT_CREATE_FOR_UPD_TASK'
      EXPORTING
        objtype           = c_objtyp_bus2081
        objkey            = f_objkey_workflow
        event             = i_event
      TABLES
        event_container   = t_event_container
      EXCEPTIONS
        objtype_not_found = 1
        OTHERS            = 2.

    IF sy-subrc NE 0.
*  do nothing
    ENDIF.
  ENDIF.

ENDFORM.                             " WORKFLOW_EVENT_CREATE
*&---------------------------------------------------------------------*
*&      Form  display_image_archivelink
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM display_image_archivelink.
* JMJ001 - add xblnr structure for deleted parked document
  DATA: BEGIN OF next_doc,
        f1(2),
        belnr TYPE rbkp-belnr,
        gjahr TYPE rbkp-gjahr,
        END OF next_doc,
        l_rbkp TYPE rbkp,
        l_gjahr TYPE rbkp-gjahr.
*
*  IF edidc-status = '53'.
*    IF edidc-idoctp = 'INVOIC01'.
*      w_objecttype = 'BUS2081'.
*      CONCATENATE int_edids-stapa1
*                  edidc-credat(4)
*                  INTO w_object_id.
*    ELSE.
*      w_objecttype = 'BKPF'.
*      CONCATENATE int_edids-stapa2
*                  int_edids-stapa1
*                  edidc-credat(4)
*                  INTO w_object_id.
*    ENDIF.
*  ELSE.
*    w_objecttype = 'IDOC'.
*    MOVE edidc-docnum TO w_object_id.
*
*  ENDIF.
*
*  SELECT * FROM  toa01 UP TO 1 ROWS
*           WHERE  sap_object  = w_objecttype
*           AND    object_id   = w_object_id.
*
*  ENDSELECT.
*
*  IF sy-subrc = 0.
*    w_archiv_id = toa01-archiv_id.
*    w_archiv_doc_id = toa01-arc_doc_id.
*  ENDIF.
*
*  CALL FUNCTION 'ARCHIVOBJECT_DISPLAY'
*    EXPORTING
*      archiv_doc_id            = w_archiv_doc_id
*      archiv_id                = w_archiv_id
*      objecttype               = w_objecttype
*      object_id                = w_object_id
*    EXCEPTIONS
*      error_archiv             = 1
*      error_communicationtable = 2
*      error_kernel             = 3
*      OTHERS                   = 4.
*  IF sy-subrc <> 0.
*
*    PERFORM message_box USING text-m04 text-m06 'A'.
*  ENDIF.

  CLEAR : w_objecttype, w_object_id, w_archiv_id, w_archiv_doc_id.
*§..RGI Modification DIA_414_FI - Image linkk
*  IF edidc-status = '53'. "RGI Deletion
*§.. Addition for status 73
  IF edidc-status = '53' OR edidc-status = '73'.
****************DEV: MJR******DIA: 943****START INSERTION001***************
***************************************************************************
SELECT SINGLE object_key FROM bds_bar_in
INTO w_object_id WHERE barcode = edidc-arckey.
***************************************************************************
****************DEV: MJR******DIA: 943*****END INSERTION001****************
    IF edidc-idoctp = 'INVOIC01'.
      w_objecttype = 'BUS2081'.
      IF edidc-status = '73'.
        SELECT SINGLE * INTO int_edids FROM edids WHERE docnum = edidc-docnum
                                                    AND status = '53'.
      ENDIF.
* JMJ001 > MM_375_DIA
*Parked document not completed before end-closure, can be deleted and created again
*in the next fiscal year, in this case we need to get the following document.

*      CONCATENATE int_edids-stapa1         "JMJ001 del
*                  edidc-credat(4)          "JMJ001 del
*                  INTO w_object_id.        "JMJ001 del
****************DEV: MJR******DIA: 943****START INSERTION002***************
***************************************************************************
    IF w_object_id IS INITIAL.
***************************************************************************
****************DEV: MJR******DIA: 943*****END INSERTION002****************
      SELECT SINGLE * FROM  rbkp INTO l_rbkp
             WHERE  belnr  = int_edids-stapa1
              AND   gjahr = edidc-credat(4).
      IF sy-subrc NE 0.
        l_gjahr = edidc-credat(4) + 1.
        SELECT SINGLE * FROM  rbkp INTO l_rbkp
             WHERE  belnr  = int_edids-stapa1
              AND   gjahr = l_gjahr.

      ENDIF.
      IF l_rbkp-rbstat = '2'.                               "JMJ001 add
        MOVE l_rbkp-xblnr TO next_doc.
        SELECT SINGLE * FROM rbkp INTO l_rbkp
                WHERE belnr = next_doc-belnr
                  AND gjahr = next_doc-gjahr.
      ENDIF.
      CONCATENATE l_rbkp-belnr l_rbkp-gjahr INTO w_object_id.
****************DEV: MJR******DIA: 943****START INSERTION003***************
***************************************************************************
    ENDIF.
***************************************************************************
****************DEV: MJR******DIA: 943*****END INSERTION003****************
*JMJ001 < MM_375_DIA
    ELSE.

*-----------------------------------------------------------------------*
* Gestion lien IXOS
* Mise en commentaire et insertion APPIA 25.09.2006
* Affichage image pour les factures FI & idoc en statut 51
* Factures FI : objet FIPP pour les pièces pré-enregistrées
*-----------------------------------------------------------------------*

* Begin of deletion APPIA 25.09.2006
*      w_objecttype = 'BKPF'.
* End of deletion APPIA 25.09.2006

* Begin of insertion APPIA 25.09.2006
      edidc-idoctp = 'INVOIC02'.
      w_objecttype = 'FIPP'.
* End of insertion APPIA 25.09.2006
****************DEV: MJR******DIA: 943****START INSERTION004***************
***************************************************************************
    IF w_object_id IS INITIAL.
***************************************************************************
****************DEV: MJR******DIA: 943*****END INSERTION004****************
      CONCATENATE int_edids-stapa2
            int_edids-stapa1
            edidc-credat(4)
            INTO w_object_id.
****************DEV: MJR******DIA: 943****START INSERTION005***************
***************************************************************************
    ENDIF.
***************************************************************************
****************DEV: MJR******DIA: 943*****END INSERTION005****************
    ENDIF.

* Begin of deletion APPIA 25.09.2006
*  ELSE.
*    w_objecttype = 'IDOC'.
*    MOVE edidc-docnum TO w_object_id.
*  ENDIF.
* End of deletaion APPIA 25.09.2006

* Begin of insertion APPIA 25.09.2006
  ENDIF.
* End of insertion APPIA 25.09.2006


  SELECT * FROM  toa01 UP TO 1 ROWS
           WHERE  sap_object  = w_objecttype
           AND    object_id   = w_object_id.

  ENDSELECT.

  IF sy-subrc = 0.
    w_archiv_id = toa01-archiv_id.
    w_archiv_doc_id = toa01-arc_doc_id.
  ELSE.
*************************************************************************************
* Begin of insertion APPIA 25.09.2006
* si l'entrée n'exsiste pas dans TOA01
* rechercher dans la TABLE bds_bar_ex pour renseigner les données du module fonction
* Clé de recherche : barcode dans l'idoc segment controle zone ARCKEY
*************************************************************************************

    SELECT * FROM bds_bar_ex WHERE barcode = edidc-arckey.
    ENDSELECT.
    IF sy-subrc = 0.
      w_archiv_doc_id = bds_bar_ex-ex_doc_id.
      w_archiv_id = bds_bar_ex-contrep.
      w_objecttype =''.
      w_object_id = ''.
    ELSE.

    ENDIF.
*ARCHIV_ID = bds_bar_ex
  ENDIF.

  IF w_archiv_id IS INITIAL OR
     w_archiv_doc_id IS INITIAL.
    PERFORM message_box USING text-m04 text-m06 'A'.
    EXIT.
  ENDIF.

*************************************************************************************
* End of insertion APPIA 25.09.2006
*************************************************************************************

  CALL FUNCTION 'ARCHIVOBJECT_DISPLAY'
    EXPORTING
      archiv_doc_id            = w_archiv_doc_id
      archiv_id                = w_archiv_id
      objecttype               = w_objecttype
      object_id                = w_object_id
    EXCEPTIONS
      error_archiv             = 1
      error_communicationtable = 2
      error_kernel             = 3
      OTHERS                   = 4.
  IF sy-subrc <> 0.

    PERFORM message_box USING text-m04 text-m06 'A'.
  ENDIF.



ENDFORM.                    " display_image_archivelink
*&---------------------------------------------------------------------*
*&      Form  display_image_urlsrv
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM display_image_urlsrv.

**Picture Data
*DATA: URIS LIKE BAPIURI OCCURS 0 WITH HEADER LINE.
*DATA: URL TYPE CNDP_URL.
*DATA: PICTURE TYPE REF TO CL_GUI_PICTURE.
*DATA PICTURE_CONTAINER TYPE REF TO CL_GUI_CUSTOM_CONTAINER.
*
*
*call screen 200.
*
**Get the picture from the server
*CALL FUNCTION 'BDS_BUSINESSDOCUMENT_GET_URL'
*EXPORTING
*CLASSNAME = 'PICTURES'
*CLASSTYPE = 'OT'
*OBJECTKEY = 'IE'
*TABLES
*URIS = URIS.
*
*LOOP AT URIS.
*SEARCH URIS-URI FOR 'IE.GIF'.
*IF SY-SUBRC = 0.
*URL = URIS-URI.
*EXIT.
*ENDIF.
*ENDLOOP.
*
**create container and place picture in it
*IF PICTURE IS INITIAL.
*CREATE OBJECT PICTURE_CONTAINER
*EXPORTING CONTAINER_NAME = 'PICTURE'.
*
*CREATE OBJECT PICTURE
*EXPORTING PARENT = PICTURE_CONTAINER.
*
*CALL METHOD:
*
*PICTURE->LOAD_PICTURE_FROM_URL
*EXPORTING URL = URL,
*
*PICTURE->SET_DISPLAY_MODE
*EXPORTING DISPLAY_MODE = PICTURE->DISPLAY_MODE_normal_center.
*ENDIF.


ENDFORM.                    " display_image_urlsrv
*&---------------------------------------------------------------------*
*&      Form  display_image_url
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_W_URL  text
*----------------------------------------------------------------------*
FORM display_image_url USING    p_url.

* Begin of UPG_ECC5_TO_ECC6 DEL TKA001
*  CALL FUNCTION 'WS_EXECUTE'
*    EXPORTING
*      program = p_url.
* End of   UPG_ECC5_TO_ECC6 DEL TKA001

* Begin of UPG_ECC5_TO_ECC6 INS TKA001
CALL METHOD CL_GUI_FRONTEND_SERVICES=>EXECUTE
  EXPORTING
    APPLICATION            = p_url
  EXCEPTIONS
    CNTL_ERROR             = 1
    ERROR_NO_GUI           = 2
    BAD_PARAMETER          = 3
    FILE_NOT_FOUND         = 4
    PATH_NOT_FOUND         = 5
    FILE_EXTENSION_UNKNOWN = 6
    ERROR_EXECUTE_FAILED   = 7
    SYNCHRONOUS_FAILED     = 8
    NOT_SUPPORTED_BY_GUI   = 9
    others                 = 10
        .
IF SY-SUBRC <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*            WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
ENDIF.
* End of   UPG_ECC5_TO_ECC6 INS TKA001
ENDFORM.                    " display_image_url
*&---------------------------------------------------------------------*
*&      Form  cache_item_rapproches
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM cache_item_rapproches.

  LOOP AT wt_alv_cmde_aff INTO wa_alv_cmde.
    MOVE sy-tabix TO w_tabix.

    LOOP AT wt_alv_fact INTO ws_fact.
      IF ws_fact-numcmde = wa_alv_cmde-cmde AND
         ws_fact-postecmde = wa_alv_cmde-poste.
        DELETE wt_alv_cmde_aff INDEX w_tabix.
      ENDIF.
    ENDLOOP.
  ENDLOOP.

ENDFORM.                    " cache_item_rapproches
*&---------------------------------------------------------------------*
*&      Form  add_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM add_cadd.

  CLEAR wa_alv_cadd.
  CLEAR wa_alv_sel.
  CALL SCREEN 9102 STARTING AT 10 10.

  CALL METHOD o_cadd_grid->refresh_table_display.


*  zbalance_04-ttc = zbalance_04-ttc + wa_alv_cadd-betrg.
*  zbalance_04-tva = zbalance_04-tva +
*                    wa_alv_cadd-kobtr -
*                    wa_alv_cadd-betrg.


ENDFORM.                    " add_cadd
*&---------------------------------------------------------------------*
*&      Form  del_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM del_cadd.

  REFRESH wt_alv_sel.
  CALL METHOD o_cadd_grid->get_selected_rows
    IMPORTING
      et_index_rows = wt_alv_sel.

  SORT wt_alv_sel DESCENDING BY index.

  LOOP AT wt_alv_sel INTO wa_alv_sel.
    READ TABLE wt_alv_cadd INTO wa_alv_cadd INDEX wa_alv_sel-index.

*    zbalance_04-ttc = zbalance_04-ttc - wa_alv_cadd-betrg.
*    zbalance_04-tva = zbalance_04-tva + wa_alv_cadd-betrg -
*                      wa_alv_cadd-kobtr.
    DELETE wt_alv_cadd INDEX wa_alv_sel-index.
  ENDLOOP.

  CALL METHOD o_cadd_grid->refresh_table_display.

ENDFORM.                    " del_cadd
*&---------------------------------------------------------------------*
*&      Form  check_line_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM check_line_cadd.

  w_subrc = 0.

  IF wa_alv_cadd-hkont IS INITIAL.
    w_subrc = 4.
    PERFORM message_box USING text-m04 text-m13 'E'.
  ENDIF.
  IF wa_alv_cadd-alckz IS INITIAL.
    w_subrc = 4.
    PERFORM message_box USING text-m04 text-m14 'E'.
  ENDIF.
  IF wa_alv_cadd-betrg IS INITIAL.
    w_subrc = 4.
    PERFORM message_box USING text-m04 text-m15 'E'.
  ENDIF.
  IF wa_alv_cadd-mwskz IS INITIAL.
    w_subrc = 4.
    PERFORM message_box USING text-m04 text-m16 'E'.
  ENDIF.
  IF wa_alv_cadd-pspnr IS INITIAL AND
     wa_alv_cadd-kostl IS INITIAL AND
     wa_alv_cadd-aufnr IS INITIAL.
    w_subrc = 4.
    PERFORM message_box USING text-m04 text-m17 'E'.
  ENDIF.

ENDFORM.                    " check_line_cadd
*&---------------------------------------------------------------------*
*&      Form  save_line_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM save_line_cadd.

  IF wa_alv_sel-index IS INITIAL.
    APPEND wa_alv_cadd TO wt_alv_cadd.
  ELSE.
    MODIFY wt_alv_cadd FROM wa_alv_cadd INDEX wa_alv_sel-index.
  ENDIF.

ENDFORM.                    " save_line_cadd
*&---------------------------------------------------------------------*
*&      Form  recalculate_line_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM recalculate_line_cadd.

  IF NOT wa_alv_cadd-mwskz IS INITIAL.
    PERFORM get_vatrate_cadd.
  ENDIF.

* Calcul de la TVA
  wa_alv_cadd-kobtr = wa_alv_cadd-betrg * ( wa_alv_cadd-msatz / 100 ).
  wa_alv_cadd-kobtr = wa_alv_cadd-kobtr + wa_alv_cadd-betrg.

ENDFORM.                    " recalculate_line_cadd
*&---------------------------------------------------------------------*
*&      Form  get_vatrate_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_vatrate_cadd.

  SELECT SINGLE land1 FROM t001
                      INTO wl_land1
                      WHERE bukrs = zbalance_04-bukrs.
  IF sy-subrc EQ 0.
* ITGGH   23.02.2007  Extension for logical system         DE5K900049
    SELECT * FROM  t076m
          WHERE  parart  = 'LS'
          AND    land1   = wl_land1
          AND    mwart   = wa_alv_cadd-mwskz
          AND    mwskz   = wa_alv_cadd-mwskz.
    ENDSELECT.
    IF sy-subrc <> 0.
      SELECT * FROM  t076m
            WHERE  parart  = 'LS'
            AND    land1   = ''
            AND    mwart   = wa_alv_cadd-mwskz
            AND    mwskz   = wa_alv_cadd-mwskz.
      ENDSELECT.
      IF sy-subrc <> 0.
* ITGGH   23.02.2007  Extension for logical system    end
        SELECT * FROM  t076m
             WHERE  parart  = 'LI'
             AND    land1   = wl_land1
             AND    konto   = zbalance_04-lifnr
             AND    mwart   = wa_alv_cadd-mwskz
             AND    mwskz   = wa_alv_cadd-mwskz.
        ENDSELECT.
        IF sy-subrc NE 0.
          SELECT * FROM  t076m
                   WHERE  parart  = 'LI'
                   AND    konto   = zbalance_04-lifnr
                   AND    mwart   = wa_alv_cadd-mwskz
                   AND    mwskz   = wa_alv_cadd-mwskz.
          ENDSELECT.
        ENDIF.
      ENDIF.     "* ITGGH 23.02.2007 Extension for logical system
    ENDIF.       "* ITGGH 23.02.2007 Extension for logical system
    IF sy-subrc EQ 0.
      wa_alv_cadd-msatz = t076m-mwsatz.
    ELSE.
      PERFORM message_box USING text-m04 text-m12 'E'.
    ENDIF.
  ENDIF.

ENDFORM.                    " get_vatrate_cadd
*&---------------------------------------------------------------------*
*&      Form  t_inserer_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_T_IDOC_DATA  text
*      -->P_T_CADD  text
*----------------------------------------------------------------------*
FORM t_inserer_cadd TABLES   p_idoc_data STRUCTURE edidd
                             p_cadd STRUCTURE zbalance_05.


  DATA : w_tabix LIKE sy-tabix,
         l_segnum LIKE edidd-segnum,
         l_docnum LIKE edidd-docnum,
         l_psgnum LIKE edidd-psgnum,
         l_hlevel_02 LIKE edidd-hlevel VALUE '02',
         l_hlevel_03 LIKE edidd-hlevel VALUE '03',
         st_e1edk05 LIKE e1edk05.

  DATA : st_e1edka1 LIKE e1edka1,
         st_e1edk02 LIKE e1edk02,
         w_lifnr LIKE e1edka1-partn,
         w_belnr LIKE e1edk02-belnr.

* Récupération du fournisseur et n° facture
  LOOP AT p_idoc_data.
    IF p_idoc_data-segnam = 'E1EDKA1'.
      MOVE p_idoc_data-sdata TO st_e1edka1.
      IF st_e1edka1-parvw = 'LF'.
        w_lifnr = e1edka1-partn.
      ENDIF.
    ENDIF.
    IF p_idoc_data-segnam = 'E1EDK02'.
      MOVE p_idoc_data-sdata TO st_e1edk02.
      IF st_e1edk02-qualf = '009'.
        w_belnr = e1edk02-belnr.
      ENDIF.
    ENDIF.
  ENDLOOP.
* Gestion des valeurs spécifiques pour la facture
* Suppression des données déjà enregistrées
  SELECT * FROM  zbal_cadd_unit
         WHERE  partn  = w_lifnr
         AND    belnr  = w_belnr.
    DELETE zbal_cadd_unit.
  ENDSELECT.


  LOOP AT p_idoc_data WHERE segnam = 'E1EDK05'.
    DELETE p_idoc_data.
  ENDLOOP.

  LOOP AT p_idoc_data WHERE segnam = 'E1EDK03'.
    MOVE : sy-tabix TO w_tabix,
           p_idoc_data-segnum TO l_segnum,
           p_idoc_data-docnum TO l_docnum.
  ENDLOOP.

  ADD 1 TO w_tabix.

  LOOP AT p_cadd.

    CLEAR st_e1edk05.
    st_e1edk05-alckz = p_cadd-alckz.
    st_e1edk05-betrg = p_cadd-betrg.
    st_e1edk05-kobtr = p_cadd-kobtr.
    st_e1edk05-mwskz = p_cadd-mwskz.
    st_e1edk05-msatz = p_cadd-msatz.

    ADD 1 TO l_segnum.
    l_psgnum = l_segnum.

    MOVE : sy-mandt        TO p_idoc_data-mandt,
           l_docnum        TO p_idoc_data-docnum,
           l_segnum        TO p_idoc_data-segnum,
           'E1EDK05'       TO p_idoc_data-segnam,
           '000000'        TO p_idoc_data-psgnum,
           l_hlevel_02     TO p_idoc_data-hlevel,
           '0'             TO p_idoc_data-dtint2,
           st_e1edk05      TO p_idoc_data-sdata.
    INSERT p_idoc_data INDEX w_tabix.

* Gestion des valeurs spécifiques pour la facture
* Ajout des valeurs actuelles
    zbal_cadd_unit-partn = w_lifnr.
    zbal_cadd_unit-belnr = w_belnr.
    zbal_cadd_unit-segnum = l_segnum.
    zbal_cadd_unit-hkont = p_cadd-hkont.
    zbal_cadd_unit-kostl = p_cadd-kostl.
    zbal_cadd_unit-pspnr = p_cadd-pspnr.
    zbal_cadd_unit-aufnr = p_cadd-aufnr.
    INSERT zbal_cadd_unit.

  ENDLOOP.



ENDFORM.                    " t_inserer_cadd
*&---------------------------------------------------------------------*
*&      Form  t_inserer_tva
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_T_IDOC_DATA  text
*----------------------------------------------------------------------*
FORM t_inserer_tva TABLES   p_idoc_data STRUCTURE edidd
                   USING    p_tva LIKE zbalance_04-tva.

  DATA : BEGIN OF wt_tva OCCURS 0,
           mwskz LIKE e1edk04-mwskz,
           msatz LIKE e1edk04-msatz,
           mwsbt LIKE e1edk04-mwsbt,
           betrg LIKE e1edp26-betrg,
         END OF wt_tva.

  DATA : wa_tva LIKE wt_tva.
  DATA : w_tabix LIKE sy-tabix,
         l_segnum LIKE edidd-segnum,
         l_docnum LIKE edidd-docnum,
         l_psgnum LIKE edidd-psgnum,
         l_hlevel_02 LIKE edidd-hlevel VALUE '02',
         l_hlevel_03 LIKE edidd-hlevel VALUE '03',
         st_e1edka1 LIKE e1edka1,
         st_e1edk05 LIKE e1edk05,
         st_e1edk04 LIKE e1edk04,
         st_e1edp04 LIKE e1edp04,
         st_e1edp26 LIKE e1edp26.
  DATA : w_tva_dec2(9) TYPE p DECIMALS 2.

  DATA : w_lifnr LIKE lfa1-lifnr.
  break gilr.
*>> RGI008 - Balance V2 - ARRONDI
*  IF calctva = space.
** Suppression des segment récapitulatifs de TVA
*    LOOP AT p_idoc_data WHERE segnam = 'E1EDK04'.
**      DELETE p_idoc_data.
*      MOVE p_idoc_data-sdata TO st_e1edk04.
*      MOVE p_tva TO st_e1edk04-mwsbt.
*      MOVE st_e1edk04 TO p_idoc_data-sdata.
*      MODIFY p_idoc_data INDEX sy-tabix.
*    ENDLOOP.
*    IF sy-subrc NE 0.
*
*    ENDIF.
*  ENDIF.
*  CHECK calctva = 'X'.
*>> FIN RGI008

* Récupération du fournisseur
  LOOP AT p_idoc_data.
    IF p_idoc_data-segnam = 'E1EDKA1'.
      MOVE p_idoc_data-sdata TO st_e1edka1.
      IF st_e1edka1-parvw = 'LF'.
        w_lifnr = e1edka1-partn.

        CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
          EXPORTING
            input  = w_lifnr
          IMPORTING
            output = w_lifnr.
      ENDIF.
    ENDIF.
  ENDLOOP.

  break gilr.
* Récupération des Mise à jour de la TVA au niveau "postes"
  LOOP AT p_idoc_data.
    IF p_idoc_data-segnam = 'E1EDP26'.
      MOVE p_idoc_data-sdata TO st_e1edp26.
      wa_tva-betrg = st_e1edp26-betrg.
    ENDIF.

    IF p_idoc_data-segnam = 'E1EDP04'.

      MOVE p_idoc_data-sdata TO st_e1edp04.

      wa_tva-mwskz = st_e1edp04-mwskz.
      wa_tva-msatz = st_e1edp04-msatz.
      wa_tva-mwsbt = st_e1edp04-mwsbt.

      READ TABLE wt_tva WITH KEY mwskz = wa_tva-mwskz
                                 msatz = wa_tva-msatz.
      IF sy-subrc EQ 0.
        ADD wa_tva-betrg TO wt_tva-betrg.
*>> ADD RGI008 - ARRONDI
        IF calctva = 'X'.
          w_tva_dec2 = wt_tva-betrg * ( wa_tva-msatz / 100 ).
        ELSE.
          w_tva_dec2 = p_tva.
        ENDIF.
*>> FIN RGI008
* T. NGUYEN 24072006
* Pb de format du montant total TVA
*
*        WRITE w_tva_dec2 TO wt_tva-mwsbt.
*        TRANSLATE wt_tva-mwsbt USING ',.'.
        wt_tva-mwsbt = w_tva_dec2.
* T. NGUYEN 24072006

        MODIFY wt_tva INDEX sy-tabix.
      ELSE.
        wt_tva-mwskz = wa_tva-mwskz.
        wt_tva-msatz = wa_tva-msatz.
*>> ADD RGI008 - ARRONDI
        IF calctva = 'X'.
          w_tva_dec2 = wa_tva-betrg * ( wa_tva-msatz / 100 ).
        ELSE.
          w_tva_dec2 = p_tva.
        ENDIF.
*>> FIN RGI008

* T. NGUYEN 24072006
* Pb de format du montant total TVA
*
*        WRITE w_tva_dec2 TO wt_tva-mwsbt.
*        TRANSLATE wt_tva-mwsbt USING ',.'.
        wt_tva-mwsbt = w_tva_dec2.
* T. NGUYEN 24072006

        wt_tva-betrg = wa_tva-betrg.

        APPEND wt_tva.
      ENDIF.
    ENDIF.
  ENDLOOP.

* Récupération des Mise à jour de la TVA au niveau "coûts addtionnels"
  LOOP AT p_idoc_data.
    IF p_idoc_data-segnam = 'E1EDK05'.

      MOVE p_idoc_data-sdata TO st_e1edk05.

      wa_tva-mwskz = st_e1edk05-mwskz.
      wa_tva-msatz = st_e1edk05-msatz.

      IF wa_tva-msatz IS INITIAL.
* ITGGH   23.02.2007  Extension for logical system         DE5K900049
        SELECT * FROM  t076m
              WHERE  parart  = 'LS'
              AND    mwart   = st_e1edk05-mwskz
              AND    mwskz   = st_e1edk05-mwskz.
        ENDSELECT.
        IF sy-subrc <> 0.
* ITGGH   23.02.2007  Extension for logical system    end
          SELECT * FROM  t076m
             WHERE  parart  = 'LI'
             AND    konto   = w_lifnr
             AND    mwart   = st_e1edk05-mwskz
             AND    mwskz   = st_e1edk05-mwskz.
          ENDSELECT.
        ENDIF.       "* ITGGH 23.02.2007 Extension for logical system
        IF sy-subrc EQ 0.
          MOVE t076m-mwsatz TO wa_tva-msatz.
        ENDIF.
      ENDIF.

*>> ADD RGI008 - ARRONDI
      IF calctva = 'X'.
        wa_tva-mwsbt = st_e1edk05-betrg * ( wa_tva-msatz / 100 ).
      ELSE.
        wa_tva-mwsbt = p_tva.
      ENDIF.
*>> FIN RGI008

      READ TABLE wt_tva WITH KEY mwskz = wa_tva-mwskz
                                 msatz = wa_tva-msatz.
      IF sy-subrc EQ 0.
        CASE st_e1edk05-alckz.
          WHEN '+'.
            wt_tva-mwsbt = wt_tva-mwsbt + wa_tva-mwsbt.
            MODIFY wt_tva INDEX sy-tabix.
          WHEN '-'.
            wt_tva-mwsbt = wt_tva-mwsbt - wa_tva-mwsbt.
            MODIFY wt_tva INDEX sy-tabix.
        ENDCASE.
      ELSE.
        CASE st_e1edk05-alckz.
          WHEN '+'.
            wt_tva-mwskz = wa_tva-mwskz.
            wt_tva-msatz = wa_tva-msatz.
            wt_tva-mwsbt = wa_tva-mwsbt.
            APPEND wt_tva.
          WHEN '-'.
            wt_tva-mwskz = wa_tva-mwskz.
            wt_tva-msatz = wa_tva-msatz.
            wt_tva-mwsbt = 0 - wa_tva-mwsbt.
            APPEND wt_tva.
        ENDCASE.
      ENDIF.
    ENDIF.
  ENDLOOP.

* Suppression des segment récapitulatifs de TVA
  LOOP AT p_idoc_data WHERE segnam = 'E1EDK04'.
    DELETE p_idoc_data.
  ENDLOOP.

  LOOP AT p_idoc_data WHERE segnam = 'E1EDK05' OR
                            segnam = 'E1EDK03'.
    MOVE : sy-tabix TO w_tabix,
           p_idoc_data-segnum TO l_segnum,
           p_idoc_data-docnum TO l_docnum.
  ENDLOOP.

  ADD 1 TO w_tabix.

* Regénération des segments de TVA
  LOOP AT wt_tva INTO wa_tva.
    CLEAR st_e1edk04.
    st_e1edk04-mwskz = wa_tva-mwskz.
    st_e1edk04-msatz = wa_tva-msatz.
    st_e1edk04-mwsbt = wa_tva-mwsbt.

    ADD 1 TO l_segnum.
    l_psgnum = l_segnum.

    MOVE : sy-mandt        TO p_idoc_data-mandt,
           l_docnum        TO p_idoc_data-docnum,
           l_segnum        TO p_idoc_data-segnum,
           'E1EDK04'       TO p_idoc_data-segnam,
           '000000'        TO p_idoc_data-psgnum,
           l_hlevel_02     TO p_idoc_data-hlevel,
           '0'             TO p_idoc_data-dtint2,
           st_e1edk04      TO p_idoc_data-sdata.
    INSERT p_idoc_data INDEX w_tabix.

  ENDLOOP.

*>> RGI008 - Balance V2 - ARRONDI
  IF calctva = space.
** Suppression des segment postes de TVA - ARRONDI seul en-tête
    LOOP AT p_idoc_data WHERE segnam = 'E1EDP04'.
      DELETE p_idoc_data.
    ENDLOOP.
  ENDIF.
*>> FIN RGI008
ENDFORM.                    " t_inserer_tva
*&---------------------------------------------------------------------*
*&      Form  chg_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM chg_cadd.

  REFRESH wt_alv_sel.
  CALL METHOD o_cadd_grid->get_selected_rows
    IMPORTING
      et_index_rows = wt_alv_sel.

  DESCRIBE TABLE wt_alv_sel LINES sy-tfill.

  CASE sy-tfill.
    WHEN 0.
      PERFORM message_box USING text-m04 text-m17 'E'.
    WHEN 1.
      READ TABLE wt_alv_sel INTO wa_alv_sel INDEX 1.
      READ TABLE wt_alv_cadd INTO wa_alv_cadd INDEX wa_alv_sel-index.

**     On enlève les montants de TVA des totaux de la facture
*      zbalance_04-ttc = zbalance_04-ttc - wa_alv_cadd-betrg.
*      zbalance_04-tva = zbalance_04-tva -
*                        wa_alv_cadd-kobtr +
*                        wa_alv_cadd-betrg.

      CALL SCREEN 9102 STARTING AT 10 10.

      CALL METHOD o_cadd_grid->refresh_table_display.

**     On ajoute les montants de TVA des totaux de la facture
*      zbalance_04-ttc = zbalance_04-ttc + wa_alv_cadd-betrg.
*      zbalance_04-tva = zbalance_04-tva +
*                        wa_alv_cadd-kobtr -
*                        wa_alv_cadd-betrg.

    WHEN OTHERS.
      PERFORM message_box USING text-m04 text-m18 'E'.
  ENDCASE.

ENDFORM.                    " chg_cadd
*&---------------------------------------------------------------------*
*&      Form  f_ctrl_cadd
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_ctrl_cadd TABLES pt_return TYPE bal_t_msg.

  DATA : w_error_cadd TYPE flag.

  LOOP AT wt_alv_cadd INTO wa_alv_cadd.
    CLEAR w_error_cadd.
    IF wa_alv_cadd-hkont IS INITIAL.
      w_error_cadd = 'X'.
    ENDIF.
    IF wa_alv_cadd-betrg IS INITIAL.
      w_error_cadd = 'X'.
    ENDIF.
    IF wa_alv_cadd-kostl IS INITIAL AND
       wa_alv_cadd-pspnr IS INITIAL AND
       wa_alv_cadd-aufnr IS INITIAL.
      w_error_cadd = 'X'.
    ENDIF.

    IF w_error_cadd EQ 'X'.
      PERFORM f_130_message
             TABLES pt_return USING '043' 'E' sy-tabix '' '' ''.
    ENDIF.

  ENDLOOP.

ENDFORM.                    " f_ctrl_cadd
*&---------------------------------------------------------------------*
*&      Form  update_item_from_fact
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM update_item_from_fact.

  REFRESH wt_alv_sel.
  CALL METHOD o_fact_grid->get_selected_rows
    IMPORTING
      et_index_rows = wt_alv_sel.

  DESCRIBE TABLE wt_alv_sel LINES sy-tfill.

  CASE sy-tfill.
    WHEN 0.
      PERFORM message_box USING text-m04 text-m17 'E'.
    WHEN 1.
      READ TABLE wt_alv_sel INTO wa_alv_sel INDEX 1.
      READ TABLE wt_alv_fact INTO wa_alv_fact INDEX wa_alv_sel-index.

*     On enlève les montants de TVA des totaux de la facture
      zbalance_04-ttc = zbalance_04-ttc - wa_alv_fact-totalttc.
      zbalance_04-tva = zbalance_04-tva - wa_alv_fact-tva.

      CALL SCREEN 9101 STARTING AT 10 10.

      CALL METHOD o_fact_grid->refresh_table_display.

*     On ajoute les montants de TVA des totaux de la facture
      zbalance_04-ttc = zbalance_04-ttc + wa_alv_fact-totalttc.
      zbalance_04-tva = zbalance_04-tva + wa_alv_fact-tva.

    WHEN OTHERS.
      PERFORM message_box USING text-m04 text-m18 'E'.
  ENDCASE.



ENDFORM.                    " update_item_from_fact
*&---------------------------------------------------------------------*
*&      Form  idoc_unblock
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_DOCNUM  N° Idoc
*----------------------------------------------------------------------*
FORM idoc_unblock USING    p_docnum.

  SELECT SINGLE * FROM zbal_rmsession WHERE docnum = p_docnum.

  IF sy-subrc EQ 0.
    IF zbal_rmsession-uname = sy-uname.
      DELETE FROM zbal_rmsession WHERE docnum = p_docnum.
    ENDIF.
  ENDIF.

ENDFORM.                    " idoc_unblock
*&---------------------------------------------------------------------*
*&      Form  update_idoc_block
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_DOCNUM  N° Idoc
*----------------------------------------------------------------------*
FORM update_idoc_block USING    p_docnum.

  SELECT SINGLE * FROM zbal_rmsession WHERE docnum = p_docnum.

  IF sy-subrc EQ 0.
    IF zbal_rmsession-uname = sy-uname.

      UPDATE zbal_rmsession SET uzeit = sy-uzeit
                                datum = sy-datum
                          WHERE docnum = p_docnum.

    ELSE.
      PERFORM message_box USING text-m04 text-m15 'A'.
    ENDIF.
  ENDIF.

ENDFORM.                    " update_idoc_block

*
*&---------------------------------------------------------------------*
*&      Form  cache_reste_a_calcul_ooo
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM cache_reste_a_calcul_ooo .


  LOOP AT wt_alv_cmde_aff INTO wa_alv_cmde.
    MOVE sy-tabix TO w_tabix.
*    LOOP AT wt_alv_fact INTO ws_fact.
*      IF ws_fact-numcmde = wa_alv_cmde-cmde AND
*         ws_fact-postecmde = wa_alv_cmde-poste.
    IF wa_alv_cmde-reste_q = 0.
      DELETE wt_alv_cmde_aff INDEX w_tabix.
    ENDIF.
*    ENDLOOP.
  ENDLOOP.










ENDFORM.                    " cache_reste_a_calcul_ooo
*&---------------------------------------------------------------------*
*&      Form  get_file_info
*&---------------------------------------------------------------------*
*       CHG_02   #ITVLA
*       Check file exist's before " PERFORM display_image_url USING
*       w_url"
*----------------------------------------------------------------------*
*      -->P_W_URL  text
*----------------------------------------------------------------------*
FORM get_file_info  USING    pu_url.                        "CHG_02+
  DATA: l_file_size TYPE i.
*        l_filename TYPE rlgrap-filename.  "CHG TKA01
data : l_filename type string.   "INS TKA001
  l_filename = pu_url.
* Begin of UPG_ECC5_TO_ECC6 DEL TKA001
*  CALL FUNCTION 'GUI_GET_FILE_INFO'
*    EXPORTING
*      fname                = l_filename
*    IMPORTING
**   FILE_VERSION         =
**   FILE_DBGREL          =
**   FILE_LANG            =
*     file_size            = l_file_size
*   EXCEPTIONS
*     fileinfo_error       = 1
*     OTHERS               = 2
            .
* End of   UPG_ECC5_TO_ECC6 DEL TKA001

* Begin of UPG_ECC5_TO_ECC6 INS TKA001

CALL METHOD CL_GUI_FRONTEND_SERVICES=>FILE_GET_SIZE
  EXPORTING
    FILE_NAME            =  l_filename
  IMPORTING
    FILE_SIZE            = l_file_size
  EXCEPTIONS
    FILE_GET_SIZE_FAILED = 1
    CNTL_ERROR           = 2
    ERROR_NO_GUI         = 3
    NOT_SUPPORTED_BY_GUI = 4
    others               = 5
        .
* End of   UPG_ECC5_TO_ECC6 INS TKA001

  IF sy-subrc <> 0.
    CLEAR pu_url.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

  IF l_file_size LT 0.
    CLEAR pu_url.
    MESSAGE i101(zsapbalance).
    EXIT.
  ENDIF.


ENDFORM.                    " get_file_info
*&---------------------------------------------------------------------*
*&      Form  calculate_TVA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM calculate_tva .
*  IF NOT wa_alv_fact-codetva IS INITIAL.
*
** CHG - JMB - 27.03.2007
** Old code :
**    PERFORM get_vatrate.
*
** New code :
*    PERFORM get_vatrate  USING    wa_alv_fact-codetva
*                                  zbalance_04-bukrs
*                                  zbalance_04-lifnr
*                         CHANGING w_taux_tva.
** END CHG - JMB - 27.03.2007
*
*  ENDIF.

ENDFORM.                    " calculate_TVA
