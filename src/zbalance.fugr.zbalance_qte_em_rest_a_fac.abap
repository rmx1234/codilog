************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Calcule la quantité entrée en stock restant à       *
*                    facturer                                          *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Tan NGUYEN (Appia Consulting)                               *
*                                                                      *
* Date   : 30/06/06                                                    *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Reynald GIL                                                 *
*                                                                      *
* Date   : 14/04/2007                                                  *
*	  : Ajout execption MF quad pas EM		                       *
************************************************************************
FUNCTION zbalance_qte_em_rest_a_fac.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(X_EBELN) TYPE  EKPO-EBELN
*"     VALUE(X_EBELP) TYPE  EKPO-EBELP
*"     VALUE(X_XBLNR) TYPE  MKPF-XBLNR OPTIONAL
*"  EXPORTING
*"     VALUE(X_QTE_EM_REST_A_FAC) TYPE  EKPO-MENGE
*"     VALUE(X_QTE_FACT) TYPE  EKPO-MENGE
*"     VALUE(X_QTE_EM) TYPE  EKPO-MENGE
*"  EXCEPTIONS
*"      NO_EM_FOUND
*"----------------------------------------------------------------------

  DATA w_ekbe TYPE ekbe.
  DATA wt_ekbe TYPE TABLE OF ekbe.
  DATA w_qte_rcpt TYPE menge_d.
  DATA w_qte_fact TYPE menge_d.
  DATA w_qte_cmde TYPE menge_d.

  SELECT SINGLE menge INTO w_qte_cmde FROM ekpo WHERE ebeln = x_ebeln
                                                  AND ebelp = x_ebelp.
  IF x_xblnr IS INITIAL.

    SELECT *  FROM ekbe INTO TABLE wt_ekbe
      WHERE ebeln = x_ebeln
        AND ebelp = x_ebelp
        AND ( vgabe = '1' OR vgabe = '2' ).
*>> Ajout RGI003 - Exception sans EM
    IF sy-subrc NE 0.
*      RAISE no_em_found.
    ENDIF.
  ELSE.

    SELECT *  FROM ekbe INTO TABLE wt_ekbe
      WHERE ebeln = x_ebeln
        AND ebelp = x_ebelp
        AND ( vgabe = '1' OR vgabe = '2' )
        AND xblnr = x_xblnr.
*>> Ajout RGI003 - Exception sans EM
    IF sy-subrc NE 0.
*      RAISE no_em_found.
    ENDIF.
  ENDIF.


  LOOP AT wt_ekbe INTO w_ekbe.

*   Entrée de marchandise
    IF w_ekbe-vgabe = '1'.

*     Annulation de reception
      IF w_ekbe-bwart = '102'.
        w_qte_rcpt  = w_qte_rcpt - w_ekbe-menge.

*     Entrée
      ELSEIF w_ekbe-bwart = '101'.
        w_qte_rcpt  = w_qte_rcpt  + w_ekbe-menge.
      ENDIF.

*   Entrée de facture
    ELSEIF w_ekbe-vgabe = '2'.

*     Débit
      IF w_ekbe-shkzg = 'S'.
        w_qte_fact  = w_qte_fact + w_ekbe-menge.

*     Crédit
      ELSE.              " Crédit
        w_qte_fact  = w_qte_fact - w_ekbe-menge.
      ENDIF.

    ENDIF.

  ENDLOOP.

  REFRESH wt_ekbe.
* Calcul de la qté entrée restant à facturer
  IF w_qte_rcpt NE space.
    x_qte_em_rest_a_fac = w_qte_rcpt - w_qte_fact.
  ELSE.
    x_qte_em_rest_a_fac = w_qte_cmde - w_qte_fact.
  ENDIF.

x_qte_fact = w_qte_fact.

* FCH001 Quantité EM
  x_qte_em = w_qte_rcpt.   "FCH001+


ENDFUNCTION.
