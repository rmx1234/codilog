FUNCTION zbalance_user_exit_mm_014.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_IDOC_DATA) TYPE  EDIDD
*"  EXPORTING
*"     VALUE(E_CHANGE) TYPE  C
*"  TABLES
*"      T_FRSEG TYPE  MMCR_TFRSEG
*"  CHANGING
*"     VALUE(E_RBKPV) TYPE  MRM_RBKPV
*"----------------------------------------------------------------------

  CALL FUNCTION 'ZBALANCE_INVOIC_PREENR_SET'
       CHANGING
            e_change = e_change
            e_rbkpv  = e_rbkpv.


  IF ( i_idoc_data-segnam = 'E1EDK03' ) AND
     ( i_idoc_data-sdata+0(3) = '016' ).
    e_rbkpv-budat = i_idoc_data-sdata+3(8).
    e_change = 'X'.
  ENDIF.

ENDFUNCTION.
