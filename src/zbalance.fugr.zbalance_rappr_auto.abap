************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Rapprochement automatique                           *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Attila Kovacs (Netinside)                                   *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*			                                             *
* Ordre de transport: DE2K900720                                       *
*							           *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_rappr_auto.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(W_DOCNUM) TYPE  EDIDC-DOCNUM OPTIONAL
*"  EXPORTING
*"     REFERENCE(RM_DTCT) TYPE  FLAG
*"  TABLES
*"      T_FACTURES STRUCTURE  ZBALANCE_01 OPTIONAL
*"----------------------------------------------------------------------

* Flags dans t_factures
*   wt_factures-ident = '1': présent dans l'idoc
*   wt_factures-ident = '2': impossible à déterminer
*   wt_factures-ident = '3': commande inexistant
*   wt_factures-ident = '4': bon de livraison inexistant
*   wt_factures-ident = '5': bon de livraison - pas de l'entrée de march
*   wt_factures-ident = 'X': rapprochement effectué
*   wt_factures-ident = 'Z': rapprochement effectué (inversion prix/qté)
*   wt_factures-ident = ' ': rapprochement échoué

* Flag: bon de livraison si 'X'
  DATA:          wl_bl          TYPE c.

* Variables temporaires
  FIELD-SYMBOLS: <fs_idoc_data> TYPE edidd.
  DATA:          wl_idx          TYPE sy-tabix.
  DATA:          wl_msg TYPE bal_s_msg.
  DATA:          wl_i           TYPE int4.
  DATA:          wt_result      TYPE tt_result WITH HEADER LINE.

* Zone de travail pour les postes de la commande
  DATA:  ws_facture     TYPE zbalance_01.
* Etape suivante de l'algorythme
  DATA:  wl_etape(4) TYPE c.
* Sélection de l'étape suivante (si branche dans l'algorythme)
  DATA:  wl_suivante TYPE c.
* Table des postes de(s) commandes
  DATA:  wt_postes   TYPE STANDARD TABLE OF zbalance_02.
  DATA:  ws_poste    TYPE zbalance_02.
* Table de contrôle des libellés de l'iDoc
  DATA: wt_libs      TYPE STANDARD TABLE OF t_libs.
  DATA: wt_libs2     TYPE STANDARD TABLE OF t_libs.

  DATA: wl_montant   TYPE zbalance_02-montant,
        wl_quantite  TYPE zbalance_02-quantite.

  DATA: wl_lib_ok    TYPE flag.
  DATA : w_zeile_old LIKE zbalance_01-zeile.
  DATA : w_belnr_old LIKE zbalance_01-belnr.

  CLEAR rm_dtct.

* ALGORYTME VERSION 2
  LOOP AT t_factures INTO ws_facture WHERE ident IS INITIAL.
    wl_etape = '1'.
    CLEAR : wt_postes[].
    DO.
      CLEAR : wl_suivante.
      CASE wl_etape.
        WHEN '1'.
          IF ws_facture-pos_2 IS INITIAL.
*           Sélection: Commande
            wl_etape = '1.a'.
            PERFORM f_210_lire_commande TABLES wt_postes
                                        USING  ws_facture.
          ELSE.
*           Sélection: Bon de livraison
            PERFORM f_210_lire_bdl TABLES wt_postes
                                   USING  ws_facture.
            wl_etape = '1.b'.
          ENDIF.

        WHEN '1.a' OR '1.b'.
*         1.a : la commande est monoposte
*         1.b : la commande référée par le bon de livraison
*               est monoposte
          PERFORM f_210_monoposte TABLES wt_postes
                                  USING  ws_facture
                                         wl_suivante.
*         Changer branche dans l'algorythme
          wl_etape(1) = '2'.

        WHEN '2.a' OR '2.b'.
*         2.a :  Article fournisseur: on lit le FIA
*         2.b :  Article fournisseur: on lit le FIA
          PERFORM f_220_fia       TABLES wt_postes
                                  USING  ws_facture
                                         wl_suivante.
*         Changer branche dans l'algorythme
          wl_etape(1) = '3'.

        WHEN '3.a' OR '3.b'.
*         3.a : Déterminer le rapprochement via couple prix/quantité
*         3.b : Déterminer le rapprochement via couple prix/quantité
          PERFORM f_230_couple_pq TABLES wt_postes
                                  USING  ws_facture
                                         wl_suivante.
*         Changer branche dans l'algorythme
          wl_etape(1) = '4'.

        WHEN '4.a' OR '4.b'.
*         4.a : Libellés
*         4.b : Libellés

          IF NOT ( ws_facture-belnr IS INITIAL ).
            PERFORM f_250_libelle_ok TABLES t_factures
                                            wt_libs
                                     USING  ws_facture
                                            wl_lib_ok.
          ELSE.
            PERFORM f_250_libelle_ok2 TABLES t_factures
                                             wt_libs2
                                      USING  ws_facture
                                             wl_lib_ok.
          ENDIF.
          IF wl_lib_ok = 'X'.
            PERFORM f_240_libelle   TABLES wt_postes
                                           wt_result
                                    USING  ws_facture
                                           wl_suivante.
          ENDIF.
          EXIT.

        WHEN OTHERS.
          EXIT.
      ENDCASE.
*     Vérification si le poste de la commande a été identifié
      IF ws_facture-ident NE space.
        EXIT.
      ENDIF.
    ENDDO.

    IF ws_facture-ident EQ space.
      READ TABLE wt_result
        WITH KEY posex = ws_facture-posex.
      IF sy-subrc EQ 0 AND wt_result-pourcent GT 50.
        ws_facture-belnr = wt_result-belnr.
        ws_facture-zeile = wt_result-zeile.
        ws_facture-proba = wt_result-pourcent.
        ws_facture-ident = 'X'.
      ENDIF.
    ENDIF.

    MODIFY t_factures FROM ws_facture.
  ENDLOOP.


* Dvt v1: Récupération des postes de la commande - TBR 30082004

  LOOP AT t_factures INTO ws_facture.
    PERFORM f_210_lire_commande TABLES wt_postes
                              USING  ws_facture.
  ENDLOOP.
* Fin Dvt v1 -Récupération des postes de la commande - TBR 30082004

** Détecter si le préenregistrement est nécessaire
*  LOOP AT t_factures INTO ws_facture.
*    t_docnum-ebeln = ws_facture-belnr.
*    t_docnum-ebelp = ws_facture-zeile.
*    t_docnum-lfsnr = ws_facture-verur.
*    CASE ws_facture-ident.
*      WHEN '1' OR 'X'.
*        INSERT TABLE t_docnum.
*
*        READ TABLE wt_postes WITH KEY belnr = ws_facture-belnr
*                                      zeile = ws_facture-zeile
*        INTO ws_poste.
*        CHECK sy-subrc EQ 0.
**       Contrôle facture sur l'entrée de marchandise?
*        IF ws_poste-webre = 'X'.
*          PERFORM f_190_ctrle_em USING
*            ws_poste-ebeln
*            ws_poste-ebelp
*            w_docnum
*            ws_poste-belnr
**            e_start
*            ws_facture-menge
*            ws_poste-menge.
*          IF  w_em_manq = 'X'.
*            em_manq = 'X'.
*            EXIT.
*          ENDIF.
*        ENDIF.
*        ws_facture-ident = 'X'.
*        MODIFY t_factures FROM ws_facture.
*    ENDCASE.
*
*  ENDLOOP.


  SORT t_factures BY zeile.

  LOOP AT t_factures INTO ws_facture.

    IF NOT w_zeile_old IS INITIAL.
      IF w_zeile_old = ws_facture-zeile AND
         w_belnr_old = ws_facture-belnr.
        CLEAR : ws_facture-zeile,
                ws_facture-ident.
        MODIFY t_factures FROM ws_facture.
      ENDIF.
    ENDIF.

    SELECT SINGLE * FROM  ekpo CLIENT SPECIFIED
           WHERE  mandt  = sy-mandt
           AND    ebeln  = ws_facture-belnr
           AND    ebelp  = ws_facture-zeile.
    IF sy-subrc NE 0.
      CLEAR : ws_facture-zeile,
              ws_facture-ident.
      MODIFY t_factures FROM ws_facture.
    ENDIF.
    IF ws_facture-ident IS INITIAL.
      rm_dtct = 'X'.
    ENDIF.
    MOVE ws_facture-zeile TO w_zeile_old.
    MOVE ws_facture-belnr TO w_belnr_old.
  ENDLOOP.

ENDFUNCTION.
