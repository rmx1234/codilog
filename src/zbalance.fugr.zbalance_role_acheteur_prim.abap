* T. NGUYEN (Appia Consulting) - 08/08/2006

FUNCTION zbalance_role_acheteur_prim.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(X_EBELN) TYPE  EKKO-EBELN
*"  TABLES
*"      ACTOR_TAB STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOBODY_FOUND
*"----------------------------------------------------------------------
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Default Role determination                       *
* ----------------- * ------------------------------------------------ *
* Author            : JM.JOURON                                JMJ001  *
* Date              : 22.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : New Role determination                           *
* ----------------- * ------------------------------------------------ *
  break gilr.
  DATA w_ernam TYPE ekko-ernam.
  DATA w_bukrs LIKE ekko-bukrs.
  DATA : l_custom_data LIKE swd_custom.

  REFRESH actor_tab.

  SELECT  bukrs ernam INTO (w_bukrs, w_ernam)
  FROM ekko WHERE ebeln = x_ebeln.
  ENDSELECT.
  IF sy-subrc <> 0.
     RAISE nobody_found. "JMJ001-Add
*JMJ001 - 19/11/2007 - default treatment done in calling program -start deletion
**{DEL APPIA 25.10.2006
**    RAISE nobody_found.
**}DEL APPIA 25.10.2006
*
**{INS APPIA 25.10.2006
**    w_actor-otype = 'US'.
**    w_actor-objid = 'BONAVENTURE'.
**    APPEND w_actor.
**}INS APPIA 25.10.2006
**>> START RGI002
**>> Now we kill Miss BONAVENTURE to replace her by a table
*    CALL FUNCTION 'Z_BALANCE_ROLE_DEFAULT'
*      EXPORTING
*        i_ebeln   = x_ebeln
*        i_bukrs   = w_bukrs
*        i_type    = 'MM'
*      TABLES
*        t_actor   = actor_tab
*      EXCEPTIONS
*        not_found = 1.
*    IF sy-subrc = 0.
*      EXIT.
*    ELSE.
**>>      Now we get Administator of System from WF point of view
**        get customizing information
*      CALL FUNCTION 'SWD_GET_CUSTOMIZING'
*        IMPORTING
*          custom_data = l_custom_data.
** default administrator was found in customizing
*      IF NOT l_custom_data-def_admin IS INITIAL.
*        actor_tab      = l_custom_data-def_admin.
*        APPEND actor_tab.
*      ENDIF.
*    ENDIF.
**>> END   RGI002
**>> Now we kill Miss BONAVENTURE to replace her by a table
*JMJ001 - 19/11/2007 - default treatment done in calling program - end deletion
  ELSE.

    CALL FUNCTION 'ZBALANCE_USER_ACTIVE'
      EXPORTING
        usnam     = w_ernam
      EXCEPTIONS
        not_exist = 1
        locked    = 2
        not_valid = 3
        OTHERS    = 4.
    IF sy-subrc EQ 0.
      actor_tab-otype = 'US'.
      actor_tab-objid = w_ernam.
      APPEND actor_tab.
    ELSE.
     RAISE nobody_found.     "JMJ001 - Add
*JMJ001 - 19/11/2007 - default treatment done in calling program -start deletion
**      w_actor-otype = 'US'.
**      w_actor-objid = 'BONAVENTURE'.
**      APPEND w_actor.
**>> START RGI002
**>> Now we kill Miss BONAVENTURE to replace her by a table
**>>      Now we get Administator of System from WF point of view
**        get customizing information
*      CALL FUNCTION 'Z_BALANCE_ROLE_DEFAULT'
*        EXPORTING
*          i_ebeln   = x_ebeln
*          i_bukrs   = w_bukrs
*          i_type    = 'MM'
*        TABLES
*          t_actor   = actor_tab
*        EXCEPTIONS
*          not_found = 1.
*      IF sy-subrc = 0.
*        EXIT.
*      ELSE.
** Now we get the Custo
*        CALL FUNCTION 'SWD_GET_CUSTOMIZING'
*          IMPORTING
*            custom_data = l_custom_data.
** default administrator was found in customizing
*        IF NOT l_custom_data-def_admin IS INITIAL.
*          actor_tab      = l_custom_data-def_admin.
*          APPEND actor_tab.
*        ENDIF.
*      ENDIF.
*JMJ001 - 19/11/2007 - default treatment done in calling program - end deletion
    ENDIF.
  ENDIF.
  CLEAR actor_tab.
ENDFUNCTION.
