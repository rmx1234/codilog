************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    :  Contrôle d'autorisations sélection des factures    *
*                                 						  *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Renaud BEZARD (Netinside)                                   *
*                                                                      *
* Date   : 26/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport: DE2K900720                                       *
*									         *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

function zbalance_auth_sel.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(TYPFACT) TYPE  EDIDC-MESCOD
*"     REFERENCE(STATUS)
*"  EXPORTING
*"     REFERENCE(MESSAGE) TYPE  SY-MSGV1
*"----------------------------------------------------------------------

  if typfact = 'MM' or typfact = 'FI'.
    if status = 'SYS' or status = 'APP' or status = 'AUT' or status =
'EMP'.
      authority-check object 'ZBAL_PROF'
               id 'ACTVT' field '02'
               id 'ZBALPROF' field 'ADM'.
      if sy-subrc ne 0.
        move 'X' to message.
      endif.

    endif.
  endif.
endfunction.
