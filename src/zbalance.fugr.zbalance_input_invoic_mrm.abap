************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Module d'intégration des factures MM                *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : Balance For MySAP Business Suite                            *
*                                                                      *
* Auteur : Jean-Michel BRUNOD (Netinside)                              *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Ordre de transport: DE2K900720                                       *
*							                       *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*25.09.2006! ITVLA        ! CHG_01                          GEDK900124 *
*                         ! In FORM f_200_raise_events Parameter event *
*                         ! of Function SWE_EVENT_CREATE +R            *
*                         ! event = 'InputErrorOccurredMM'             *
*----------!--------------!--------------------------------------------*
*20.02.2007! SERCO01      ! create parked invoice,                     *
*          !              ! changes signed by <serco>                  *
*----------!--------------!--------------------------------------------*
*  03.2007 ! ITGGH        ! CHG_02                          DE5K900098 *
*          !              ! Share workflow                             *
*----------!--------------!--------------------------------------------*
*  10.2007 ! HAVYO        ! Parked Credit memo CRME         OHA001     *
*          !              ! FI_332_DIA                                 *
*----------!--------------!--------------------------------------------*
*  10.2007 ! KROMJONG     ! Don't park an Assets Invoice    MAK001     *
*          !              ! FI_320_DIA (See also source ZXM08F25)      *
*----------!--------------!--------------------------------------------*
* 01.2008  ! JM.JOURON    !  FI_365_DIA new logic for missing receipt  *
*          !              !  JMJ001                                    *
*----------!--------------!--------------------------------------------*
*03.07.2008! RHA          ! Performance optimization in display Invoice*
*          !              ! #0000199 - image RHA001                    *
************************************************************************
*06.08.2008! RHA          ! Selecting IDOC number from ZBAL_IDOC when  *
*                         !  IDOC# is blank.                           *
*          !              ! #0000199 - image RHA002                    *
************************************************************************
*09.09.2008! J2013032     ! DIA 551 transferring the Arclink image for *
*                         !  all the created invoice without checking  *
*          !              ! the status                      JMJ002     *
************************************************************************
*18.09.2008! J2013032     ! DIA 611 - Check Fully Invoiced PO          *
*          !              !                                 JMJ003     *
************************************************************************


FUNCTION zbalance_input_invoic_mrm.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(INPUT_METHOD) LIKE  BDWFAP_PAR-INPUTMETHD
*"     VALUE(MASS_PROCESSING) LIKE  BDWFAP_PAR-MASS_PROC
*"  EXPORTING
*"     VALUE(WORKFLOW_RESULT) LIKE  BDWFAP_PAR-RESULT
*"     VALUE(APPLICATION_VARIABLE) LIKE  BDWFAP_PAR-APPL_VAR
*"     VALUE(IN_UPDATE_TASK) LIKE  BDWFAP_PAR-UPDATETASK
*"     VALUE(CALL_TRANSACTION_DONE) LIKE  BDWFAP_PAR-CALLTRANS
*"  TABLES
*"      IDOC_CONTRL STRUCTURE  EDIDC
*"      IDOC_DATA STRUCTURE  EDIDD
*"      IDOC_STATUS STRUCTURE  BDIDOCSTAT
*"      RETURN_VARIABLES STRUCTURE  BDWFRETVAR
*"      SERIALIZATION_INFO STRUCTURE  BDI_SER
*"  EXCEPTIONS
*"      WRONG_FUNCTION_CALLED
*"----------------------------------------------------------------------

  DATA: wt_factures  TYPE STANDARD TABLE OF zbalance_01.
  DATA: ws_facture   TYPE zbalance_01.
  DATA: ws_ctrl      TYPE zbalance_01.
* DATA: wt_return    TYPE bal_t_msg.
  DATA: wl_start     TYPE flag.
  DATA: wl_factfin   TYPE flag.
  DATA: wl_datum     TYPE edidat8.
  DATA: wl_reference TYPE edi_belnr.

  DATA : wl_menge LIKE zbalance_01-menge.
  DATA : wl_netpr LIKE zbalance_01-netpr.

  DATA : w_tabix LIKE sy-tabix.

  DATA: wt_docnum    TYPE STANDARD TABLE OF zbalance_03.
  DATA w_docnum LIKE edidc-docnum.

  DATA : st_e1edk04 LIKE e1edk04.
  DATA : st_e1edk01 LIKE e1edk01.
  DATA : st_e1edp04 LIKE e1edp04.
  DATA : st_e1edp01 LIKE e1edp01.
  DATA : st_e1edp02 LIKE e1edp02.
  data : ws_avoir  type c.
  DATA : w_int(17) TYPE c.
  DATA : w_dec(17) TYPE c.
  DATA : w_typec(17) TYPE c.
  DATA : w_ebeln LIKE ekpo-ebeln.

  DATA : w_flag_rapman TYPE flag.
  DATA : w_lifnr LIKE e1edka1-partn.
  DATA : st_e1edka1 LIKE e1edka1.

***** Début modif - GTR(appia) - 22.09.06
  TABLES bds_bar_ex.

  DATA l_objkey LIKE bapibds01-objkey.
  DATA lt_property  TYPE STANDARD TABLE OF sdokpropty.
  DATA ls_property  TYPE sdokpropty.
  DATA l_archiv_id LIKE toaom-archiv_id.
  DATA ls_toacm TYPE toacm.
  DATA ls_edidc TYPE edidc.
  DATA ls_toaom TYPE toaom.
  DATA l_barcode LIKE bapibds01-barcode.
  DATA l_contrep LIKE bds_bar_ex-contrep.
  DATA l_log_sys LIKE bapibds01-log_system.
***** Fin modif - GTR(appia) - 22.09.06
*>> JMJ001 - new logic for partial receipt
  data: lwt_ekbe_101 TYPE table of ekbe with header line,
        lwt_ekbe_102 type table of ekbe with header line,
        lst_e1edp02 LIKE e1edp02,
        lst_e1edk14 like e1edk14,
        lst_e1edka1 LIKE e1edka1,
        lw_ebeln LIKE ekko-ebeln,
        lw_ebelp like ekpo-ebelp,
        lst_e1edk02 like e1edk02,
        lst_e1eds01 like e1eds01,
        lw_docnum type edidd-docnum,
        lw_psgnum type edidd-psgnum.
*<< JMJ001 - new logic for partial receipt
  DATA : ls_zbal_params TYPE zbal_params.     "Serco
  DATA : w_wf_share(1) TYPE c.                "ITGGH  CHG_02
  DATA : v_recipient   TYPE viaufks-gewrk.    "ITGGH  CHG_02

* Initialisation des variables
  CLEAR : w_em_manq, ws_avoir.

* BEGIN INSERT OHA001 FI_332_DIA
* Get if bsart = CRME
  LOOP AT idoc_data WHERE segnam = 'E1EDK01'.
    MOVE idoc_data-sdata TO st_e1edk01.
    if st_e1edk01-bsart = 'CRME'.
    ws_avoir = 'X'.
    endif.
  ENDLOOP.
* END INSERT OHA001 FI_332_DIA

* Formattage des n° de commande
  LOOP AT idoc_data WHERE segnam = 'E1EDP02'.
    MOVE idoc_data-sdata TO st_e1edp02.
    IF st_e1edp02-qualf = '001'.
      CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
        EXPORTING
          input  = st_e1edp02-belnr
        IMPORTING
          output = w_ebeln.

      st_e1edp02-belnr = w_ebeln.
      idoc_data-sdata = st_e1edp02.
      MODIFY idoc_data.
    ENDIF.
  ENDLOOP.

*
*   Vérification sur les factures sur Immo
*   L. Roussard, le 14/09/2006

  TABLES zuue_activation.
  DATA w_fact TYPE e1edka1.
  DATA w_societe TYPE e1edk14.
  DATA w_poste TYPE e1edp01.
  DATA c_commande TYPE ekko-ebeln.
  DATA c_poste TYPE ekpo-ebelp.
  DATA num_ordre TYPE ekkn-aufnr .
  DATA c_num_ordre(12).
  DATA i_societe TYPE ekko-bukrs.
  DATA init_societe(3) TYPE c.
  DATA w_fact_immo.
  data: w_fully_invoiced.  "JMJ002 - Add
*   Numero de commande

  LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
    w_fact = idoc_data-sdata.
    IF w_fact-parvw = 'RE'.
      c_commande = w_fact-name1.
      EXIT.
    ENDIF.
  ENDLOOP.

*  La société
  LOOP AT idoc_data WHERE segnam = 'E1EDK14'.
    w_societe = idoc_data-sdata.
    IF w_societe-qualf = '011'.
      i_societe = w_societe-orgid.
      EXIT.
    ENDIF.
  ENDLOOP.



*  Poste de commande
  LOOP AT idoc_data WHERE segnam = 'E1EDP01'.
    w_poste = idoc_data-sdata.
*    IF w_fact-parvw = 'RE'.
    c_poste = w_poste-posex.


*Aller chercher le numéro d'ordre

    SELECT aufnr FROM ekkn INTO num_ordre
    WHERE ebeln = c_commande
    AND ebelp = c_poste
*>> RGI008 - Ajout Contôle si comde avec ordre !!
    AND aufnr NE space.
*>> Fin RGI008
      WRITE num_ordre TO c_num_ordre.
      IF NOT c_num_ordre IS INITIAL.

* 2. Regarder dans la table ZUU_ACTIVATION le numéro d'ordre
        SELECT * FROM zuue_activation
        WHERE task = 'FI247'
        AND key1 = 'BUKRS'
        AND key2 = 'AUFNR'.
          IF sy-subrc = 0.
            CONCATENATE i_societe(2) '*' INTO init_societe.
            IF zuue_activation-key3 = i_societe.
              IF c_num_ordre(3) = zuue_activation-valchar1(3)
                 OR c_num_ordre(3) = zuue_activation-valchar2(3)
                 OR c_num_ordre(3) = zuue_activation-valchar3(3).
                w_fact_immo = 'X'.
                EXIT.
              ENDIF.
            ELSEIF zuue_activation-key3 = init_societe.
              IF c_num_ordre(3) = zuue_activation-valchar1(3)
         OR c_num_ordre(3) = zuue_activation-valchar2(3)
         OR c_num_ordre(3) = zuue_activation-valchar3(3).
                w_fact_immo = 'X'.
                EXIT.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDSELECT.
      ENDIF.
    ENDSELECT.
*    ENDIF.
  ENDLOOP.

* Récupération des données utiles depuis l'idoc

* nouveau part de IT-Informatik pour la travaille avec
* logique systems  20070206 H.-P. Schulz
* TEST
  DATA: tmp_societe  TYPE  edi_orgid.

  IF idoc_contrl-sndprt = 'LS'.
    LOOP AT idoc_contrl WHERE sndprt = 'LS'.
*      idoc_contrl-sndprt = 'LI'.
      idoc_contrl-status = '32'.
      MODIFY idoc_contrl.
    ENDLOOP.

    CALL FUNCTION 'ZBALANCE_INVOIC_GET'
      EXPORTING
        x_rapp      = space
      IMPORTING
        e_datum     = wl_datum
        e_reference = wl_reference
*        E_SOCIETE   = TMP_SOCIETE
      TABLES
        t_idoc_data = idoc_data
        t_factures  = wt_factures.

  ELSE.
*  old part it has still to work
    CALL FUNCTION 'ZBALANCE_INVOIC_GET'
      EXPORTING
        x_rapp      = space
      IMPORTING
        e_datum     = wl_datum
        e_reference = wl_reference
      TABLES
        t_idoc_data = idoc_data
        t_factures  = wt_factures.

  ENDIF.
* Fin de LS

  MOVE idoc_data-docnum TO w_docnum.

* On récupère le status actuel de l'idoc
  PERFORM read_last_status_idoc USING w_docnum
                                CHANGING int_edids.

* Rapprochement automatique
  CALL FUNCTION 'ZBALANCE_RAPPR_AUTO'
    EXPORTING
      w_docnum   = w_docnum
    IMPORTING
      rm_dtct    = w_rm_dtct
    TABLES
      t_factures = wt_factures.

* Contrôles sur la facture
  CALL FUNCTION 'ZBALANCE_INVOIC_MM_CHECK'
    EXPORTING
      w_docnum   = w_docnum
    IMPORTING
      em_manq    = w_em_manq
      ep_dtct    = w_ep_dtct
      eq_dtct    = w_eq_dtct
      e_fully_invoiced = w_fully_invoiced          "JMJ002 - Add
    TABLES
      t_factures = wt_factures
      t_docnum   = wt_docnum.

  if w_fully_invoiced = 'X'.
    READ TABLE idoc_contrl  INDEX 1.
    MOVE  idoc_contrl-docnum TO idoc_status-docnum.
    idoc_status-msgty = 'E'.
    idoc_status-status = '51'.
    idoc_status-msgid = wc_msgid.
    idoc_status-msgno = '001'.
    APPEND  idoc_status.
    CLEAR w_flag_integration.
    exit.
  endif.
** Add - Jean-Michel BRUNOD - 05.04.2007
** Bug missing good rcpt SGS
*  IF w_em_manq NE 'X'.
*    DESCRIBE TABLE wt_factures LINES sy-tfill.
*    IF sy-tfill = 0.
**     No items in this invoice
**     Check if there at least one delivery on this po
*
**     Retreive the po number (specific SGS)
*      CLEAR w_ebeln.
*      LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
*        MOVE idoc_data-sdata TO st_e1edka1.
*        IF st_e1edka1-parvw = 'RE'.
*          w_ebeln = st_e1edka1-name1.
*        ENDIF.
*      ENDLOOP.
*
*      IF NOT w_ebeln IS INITIAL.
*
*        SELECT ebeln INTO ekbe-ebeln
*        FROM ekbe UP TO 1 ROWS
*        WHERE ebeln = w_ebeln
*          AND vgabe = '1'.
*        endselect.
*
*          IF sy-subrc NE 0.
**           There is no delivery on this po
**           => Missing good receipt
*            w_em_manq = 'X'.
*          ENDIF.
*        ENDIF.
*      ENDIF.
*    ENDIF.
** End add - Jean-Michel BRUNOD - 05.04.2007

************************************************************************
*                        Gestion des litiges                           *
************************************************************************

* Correction des postes de facture détectés avec une inversion prix/qté
  LOOP AT wt_factures INTO ws_facture.
    IF ws_facture-ident = 'Z'.

      CONDENSE : ws_facture-netpr, ws_facture-menge.

      wl_menge = ws_facture-netpr.
      wl_netpr = ws_facture-menge.

      ws_facture-menge = wl_menge.
      ws_facture-netpr = wl_netpr.

      ws_facture-ident = 'X'.

      MODIFY wt_factures FROM ws_facture INDEX sy-tabix.

      READ TABLE idoc_data INDEX ws_facture-debut.
      IF sy-subrc EQ 0.
        MOVE idoc_data-sdata TO st_e1edp01.
        WRITE ws_facture-netpr TO st_e1edp01-menge.
        MOVE st_e1edp01 TO idoc_data-sdata.
        MODIFY idoc_data INDEX ws_facture-debut.
      ENDIF.
    ENDIF.
  ENDLOOP.

* Formattage du taux de TVA
  LOOP AT idoc_data WHERE segnam = 'E1EDK04' OR
                          segnam = 'E1EDP04'.
    CASE idoc_data-segnam.
      WHEN 'E1EDK04'.
        MOVE idoc_data-sdata TO st_e1edk04.
        TRANSLATE st_e1edk04-msatz USING ',.'.
*        SPLIT st_e1edk04-msatz AT '.' INTO w_int w_dec.
*        IF w_dec CO '0 '.
*          st_e1edk04-msatz = w_int.
*          CONDENSE st_e1edk04-msatz.
*        ELSE.
**            SHIFT w_dec RIGHT DELETING TRAILING '0 '.
*        CONDENSE w_dec.
*        CONCATENATE w_int '.' w_dec INTO st_e1edk04-msatz.
        CONDENSE st_e1edk04-msatz.
*        ENDIF.
        idoc_data-sdata = st_e1edk04.
        MODIFY idoc_data INDEX sy-tabix.
      WHEN 'E1EDP04'.
        MOVE idoc_data-sdata TO st_e1edp04.
        TRANSLATE st_e1edp04-msatz USING ',.'.
*        SPLIT st_e1edp04-msatz AT '.' INTO w_int w_dec.
*        IF w_dec CO '0 '.
*          st_e1edp04-msatz = w_int.
*          CONDENSE st_e1edp04-msatz.
*        ELSE.
*            SHIFT w_dec RIGHT DELETING TRAILING '0 '.
*        CONDENSE w_dec.
*        CONCATENATE w_int '.' w_dec INTO st_e1edp04-msatz.
        CONDENSE st_e1edp04-msatz.
*        ENDIF.
        idoc_data-sdata = st_e1edp04.
        MODIFY idoc_data INDEX sy-tabix.
    ENDCASE.
  ENDLOOP.

************************************************************************
*     Rapprochement manuel nécessaire même si postes rapprochés ?      *
************************************************************************
* Si l'année de la date comptable est 9999 alors
* on passe en rapprochement manuel
  IF wl_datum(4) EQ '9999'.
    w_rm_dtct = 'X'.
  ENDIF.

* Vérification des lignes de coûts addtionnels
* Si des lignes incomplètes sont présentes et que le fournisseur
* n'est pas géré dans la table ZBAL_CADD alors on passe la facture
* en rapprochement manuel

* Récupération du fournisseur
  IF w_rm_dtct IS INITIAL.
    LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
      MOVE idoc_data-sdata TO st_e1edka1.
      IF st_e1edka1-parvw = 'LF'.
        w_lifnr = e1edka1-partn.
        EXIT.
      ENDIF.
    ENDLOOP.

    SELECT SINGLE * FROM  zbal_cadd
           WHERE  partn  = w_lifnr.
    IF sy-subrc NE 0.
*   Le fournisseur n'est pas géré dans ZBAL_CADD
*   On vérifie maintenant si les lignes de coûts
*   additionnels sont complète ou pas
      LOOP AT idoc_data WHERE segnam = 'E1EDK05'.
        SELECT SINGLE * FROM  zbal_cadd_unit
               WHERE  partn   = w_lifnr
               AND    belnr   = wl_reference
               AND    segnum  = idoc_data-segnum.
        IF sy-subrc NE 0.
          w_rm_dtct = 'X'.
        ENDIF.
      ENDLOOP.
    ENDIF.
  ENDIF.


************************************************************************
*                        Gestion des litiges                           *
************************************************************************
* Vérification d'un éventuel statut de forçage
* pour les écarts de prix/qté
  IF int_edids-status = '51' AND
     int_edids-stamid = 'ZSAPBALANCE' AND
     int_edids-stamno = '047'.
    w_epq_force = 'X'.
  ELSE.
    CLEAR w_epq_force.
  ENDIF.

  w_flag_integration = 'X'.

*-----------------------------------------------------------------------
* ITGGH   09.03.2007   CHG_02                               DE5K900049
* Share workflow
  CLEAR w_wf_share.
  SELECT SINGLE * FROM  zbal_params INTO ls_zbal_params
         WHERE  param  = 'ZBAL_WF_MM_SHARE'.
  IF sy-subrc EQ 0 AND ls_zbal_params-value = 'WITH'.
    w_wf_share = 'X'.
    wl_start = 'X'.
  ENDIF.
*-----------------------------------------------------------------------
*<serco>
* On ne traite les litiges que si la facture à été totalement rapprochée
  IF w_rm_dtct IS INITIAL.

    IF w_wf_share IS INITIAL.                "ITGGH  CHG_02

*     Gestion des entrées de marchandises manquantes
*     CHECK goods received
      IF w_em_manq = 'X'.

        CALL METHOD lcl_zbalance=>handle_no_goods_received
          EXPORTING
            iv_msgid            = wc_msgid
            it_factures         = wt_factures[]
            it_idoc_control     = idoc_contrl[]
            it_idoc_status      = idoc_status[]
          IMPORTING
            et_idoc_status      = idoc_status[]
            ev_flag_integration = w_flag_integration
            ev_start            = wl_start.

      ELSE.  "Goods received OK

*       check quantity/ Ecart de quantité
        IF w_eq_dtct = 'X'.
          ws_prenerg = 'X'.

*          CALL METHOD lcl_zbalance=>handle_quantity_discrepance
*            EXPORTING
*              iv_msgid            = wc_msgid
*              iv_epq_force        = w_epq_force
*              it_factures         = wt_factures[]
*              it_idoc_control     = idoc_contrl[]
*              it_idoc_status      = idoc_status[]
*            IMPORTING
*              et_idoc_status      = idoc_status[]
*              ev_flag_integration = w_flag_integration
*              ev_start            = wl_start.


        ELSE. "Quantity OK

*         check price/ Ecart de prix
          IF w_ep_dtct = 'X'.
            ws_prenerg = 'X'.

*            CALL METHOD lcl_zbalance=>handle_price_discrepance
*              EXPORTING
*                iv_msgid            = wc_msgid
*                iv_epq_force        = w_epq_force
*                it_factures         = wt_factures[]
*                it_idoc_control     = idoc_contrl[]
*                it_idoc_status      = idoc_status[]
*              IMPORTING
*                et_idoc_status      = idoc_status[]
*                ev_flag_integration = w_flag_integration
*                ev_start            = wl_start.

          ENDIF.  "Price
        ENDIF.  "Quanity
      ENDIF.  "Goods Reiceived

    ENDIF.                                    "ITGGH  CHG_02

    IF w_em_manq = 'X'.
      ws_prenerg = ' '.
    ELSE.
      ws_prenerg = 'X'.
    ENDIF.
    IF ws_prgr_liv = 'X'.
      ws_prenerg = 'X'.
    ENDIF.
** On ne traite les litiges que si la facture à été totalement
*rapprochée
*  IF w_rm_dtct IS INITIAL.
*
**   Gestion des entrées de marchandises manquantes
*    IF w_em_manq = 'X'.
**     Méthode Idoc ?
*      SELECT SINGLE * FROM  zbal_params
*             WHERE  param  = 'RB_MEM1'.
*      IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*        LOOP AT wt_factures INTO ws_facture.
*        ENDLOOP.
*
*        READ TABLE idoc_contrl  INDEX 1.
*        MOVE  idoc_contrl-docnum TO idoc_status-docnum.
*        idoc_status-msgty = 'E'.
*        idoc_status-status = '51'.
*        idoc_status-msgid = wc_msgid.
*        idoc_status-msgv1 = ws_facture-belnr.
*        idoc_status-msgno = '041'.
*        APPEND  idoc_status.
*        CLEAR w_flag_integration.
*      ELSE.
**       Méthode Pré-enreg ?
*        SELECT SINGLE * FROM  zbal_params
*               WHERE  param  = 'RB_MEM2'.
*        IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*          wl_start = 'X'.
*          w_flag_integration = 'X'.
*        ELSE.
**         Méthode standard SAP...
**         Rien à faire dans ce cas là => Traitement standard
*          w_flag_integration = 'X'.
*        ENDIF.
*      ENDIF.
*
**     Workflow à déclencher ?
*      SELECT SINGLE * FROM  zbal_params
*             WHERE  param  = 'CB_MEM'.
*
*      IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*        PERFORM f_200_raise_events USING w_em_ebeln
*                                         w_em_belnr
*                                         idoc_contrl-docnum
*                                CHANGING w_em_manq.
*      ENDIF.
*    ELSE.
*
**     Ecart de prix
*      IF w_ep_dtct = 'X'.
**       Méthode Idoc ?
*        SELECT SINGLE * FROM  zbal_params
*               WHERE  param  = 'RB_MEP1'.
*        IF sy-subrc EQ 0 AND
*           zbal_params-value = 'X' AND
*           w_epq_force IS INITIAL.
*          LOOP AT wt_factures INTO ws_facture.
*          ENDLOOP.
*
*          READ TABLE idoc_contrl INDEX 1.
*          MOVE  idoc_contrl-docnum TO idoc_status-docnum.
*          idoc_status-msgty = 'E'.
*          idoc_status-status = '51'.
*          idoc_status-msgid = wc_msgid.
*          idoc_status-msgv1 = ws_facture-belnr.
*          idoc_status-msgno = '045'.
*          APPEND  idoc_status.
*          CLEAR w_flag_integration.
**         Workflow à déclencher ?
*          SELECT SINGLE * FROM  zbal_params
*                 WHERE  param  = 'CB_MEP'.
*
*          IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*            PERFORM workflow_event_create_updtask
*                    USING rbkp-belnr
*                          rbkp-gjahr
*                          c_event_blockedprice.
*
*          ENDIF.
*        ELSE.
**         Méthode Pré-enreg ?
*          SELECT SINGLE * FROM  zbal_params
*                 WHERE  param  = 'RB_MEP2'.
*          IF ( sy-subrc EQ 0 AND zbal_params-value = 'X') OR
*              w_epq_force = 'X'.
*            wl_start = 'X'.
*            w_flag_integration = 'X'.
**           Workflow à déclencher ?
*            SELECT SINGLE * FROM  zbal_params
*                   WHERE  param  = 'CB_MEP'.
*
*            IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*              PERFORM workflow_event_create_updtask
*                      USING rbkp-belnr
*                            rbkp-gjahr
*                            c_event_blockedprice.
*
*            ENDIF.
*          ELSE.
**           Méthode standard SAP...
**           Rien à faire dans ce cas là => Traitement standard
*            w_flag_integration = 'X'.
*          ENDIF.
*        ENDIF.
*
*
*      ELSE.
*
**       Ecart de quantité
*        IF w_eq_dtct = 'X'.
**         Méthode Idoc ?
*          SELECT SINGLE * FROM  zbal_params
*                 WHERE  param  = 'RB_MEQ1'.
*          IF sy-subrc EQ 0 AND
*             zbal_params-value = 'X' AND
*             w_epq_force IS INITIAL.
*            LOOP AT wt_factures INTO ws_facture.
*            ENDLOOP.
*
*            READ TABLE idoc_contrl  INDEX 1.
*            MOVE  idoc_contrl-docnum TO idoc_status-docnum.
*            idoc_status-msgty = 'E'.
*            idoc_status-status = '51'.
*            idoc_status-msgid = wc_msgid.
*            idoc_status-msgv1 = ws_facture-belnr.
*            idoc_status-msgno = '046'.
*            APPEND  idoc_status.
*            CLEAR w_flag_integration.
**           Workflow à déclencher ?
*            SELECT SINGLE * FROM  zbal_params
*                   WHERE  param  = 'CB_MEQ'.
*
*            IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*              PERFORM workflow_event_create_updtask
*                      USING rbkp-belnr
*                            rbkp-gjahr
*                            c_event_blockedquant.
*            ENDIF.
*          ELSE.
**           Méthode Pré-enreg ?
*            SELECT SINGLE * FROM  zbal_params
*                   WHERE  param  = 'RB_MEQ2'.
*            IF ( sy-subrc EQ 0 AND zbal_params-value = 'X') OR
*                 w_epq_force = 'X'.
*              wl_start = 'X'.
*              w_flag_integration = 'X'.
**             Workflow à déclencher ?
*              SELECT SINGLE * FROM  zbal_params
*                     WHERE  param  = 'CB_MEQ'.
*
*              IF sy-subrc EQ 0 AND zbal_params-value = 'X'.
*                PERFORM workflow_event_create_updtask
*                        USING rbkp-belnr
*                              rbkp-gjahr
*                              c_event_blockedquant.
*              ENDIF.
*            ELSE.
**             Méthode standard SAP...
**             Rien à faire dans ce cas là => Traitement standard
*              w_flag_integration = 'X'.
*            ENDIF.
*          ENDIF.
*        ENDIF.
*      ENDIF.
*    ENDIF.

*</serco>
*-----------------------------------------------------------------------

  ELSE.
*   Rapprochement manuel
    READ TABLE idoc_contrl  INDEX 1.
    MOVE  idoc_contrl-docnum TO idoc_status-docnum.
    idoc_status-msgty = 'E'.
    idoc_status-status = '51'.
    idoc_status-msgid = wc_msgid.
    idoc_status-msgno = '001'.
    APPEND  idoc_status.
    CLEAR w_flag_integration.
  ENDIF.

************************************************************************
*                      Sauvegarde de la facture                        *
************************************************************************
*Louis Roussard, le 14/09/2006
*Pré-enregistrer la facture si facture d'immo
*ou si Avoir.
* BEGIN INSERT OHA001 FI_332_DIA
if ws_avoir = 'X'.
ws_prenerg = 'X'.
endif.
* END INSERT OHA001 FI_332_DIA
*>> JMJ001 - new logic for partial receipt
  loop at idoc_data WHERE segnam = 'E1EDP02'.
    move idoc_data-sdata to lst_e1edp02.
    lw_docnum = idoc_data-docnum.
    check lst_e1edp02-qualf = '001'.
    lw_psgnum = idoc_data-psgnum.
    lw_ebeln = lst_e1edp02-belnr.
    lw_ebelp = lst_e1edp02-zeile.
    SELECT  *  FROM ekbe INTO table lwt_ekbe_101
     WHERE ebeln = lw_ebeln
       AND ebelp = lw_ebelp
       AND vgabe = '1'.
*
     lwt_ekbe_102[] = lwt_ekbe_101[].
*
     delete lwt_ekbe_102 where bwart = '101'.
     delete lwt_ekbe_101 where bwart = '102'.
     loop at lwt_ekbe_102.
       read table lwt_ekbe_101 with key belnr = lwt_ekbe_102-lfbnr.
        if sy-subrc eq 0.
           delete lwt_ekbe_101 index sy-tabix.
        endif.
     endloop.
* no receipt > delete the item data on the Idoc
     if lwt_ekbe_101[] is initial.
       delete idoc_data where psgnum = lw_psgnum.
       delete idoc_data where segnum = lw_psgnum.
       delete wt_factures where debut = lw_psgnum.
     endif.
   endloop.
*<< JMJ001 - new logic for partial receipt

  IF ( w_flag_integration = 'X'
     AND w_fact_immo = 'X' )
     OR  ( w_flag_integration = 'X'
     AND ws_prenerg = 'X' ).
    wl_start = 'X'.
  ENDIF.
*LRO, 14/06/2006

  EXPORT docnum   = wt_docnum   TO MEMORY ID  wc_memory_id_01.
  EXPORT start    = wl_start    TO MEMORY ID  wc_memory_id_02.
  EXPORT factures = wt_factures TO MEMORY ID  wc_memory_id_03.

* Sauvegarder l'iDoc
  CALL FUNCTION 'ZBALANCE_UPDATE_INVOIC_MM'
    EXPORTING
      i_idoc_contrl = idoc_contrl
    TABLES
      t_idoc_data   = idoc_data
      t_factures    = wt_factures.

** IT-Informatik 20070220 Hans-Peter Schulz
** Anpassung für Logische Systeme  Beginn
*
**SNDPOR	EDI_SNDPOR
**SNDPRT	EDI_SNDPRT
**SNDPRN	EDI_SNDPRN
**SNDSAD	EDI_SNDSAD
**SNDSMN	EDI_SNDSMN
**SNDSNA	EDI_SNDSNA
**SNDSCA	EDI_SNDSCA
*  idoc_contrl-sndpor = 'FMFI'.
*  idoc_contrl-sndprt = 'LS'.
*  idoc_contrl-sndprn = 'ITESOFT_IC'.
*
** 20070220 Anpassung für Logische Systeme  Ende

* Appeler le module d'intégration Standard
  IF w_flag_integration = 'X'.
    CALL FUNCTION 'IDOC_INPUT_INVOIC_MRM'
      EXPORTING
        input_method          = input_method
        mass_processing       = mass_processing
      IMPORTING
        workflow_result       = workflow_result
        application_variable  = application_variable
        in_update_task        = in_update_task
        call_transaction_done = call_transaction_done
      TABLES
        idoc_contrl           = idoc_contrl
        idoc_data             = idoc_data
        idoc_status           = idoc_status
        return_variables      = return_variables
        serialization_info    = serialization_info
      EXCEPTIONS
        wrong_function_called = 1.

    IF sy-subrc <> 0.
      RAISE wrong_function_called.
    ELSE.

*----------------------------------------------------------------------
* <serco>
      COMMIT WORK AND WAIT.
      break gilr.
      DATA : invoicedocnumber LIKE  bapi_incinv_fld-inv_doc_no,
             fiscalyear LIKE  bapi_incinv_fld-fisc_year,
             discount_shift LIKE  bapi_incinv_fld-disc_shift,
             release_return LIKE bapiret2 OCCURS 0 WITH HEADER LINE.
      DATA : xrbkpb LIKE rbkpb.
      READ TABLE return_variables WITH KEY wf_param = 'Appl_Objects'.
*JMJ002 - whatever the status is, we have to transfer the Arclink Image to
* the invoice created.
      if sy-subrc eq 0.
        MOVE return_variables-doc_number TO l_objkey.
      endif.
*>> Begin of FI_282
      SELECT SINGLE * FROM rbkpb INTO xrbkpb
                        WHERE belnr = return_variables-doc_number(10)
                          AND gjahr = return_variables-doc_number+10(4).
*{   REPLACE MAK001 MS4K915623 Don't park an assets invoice anymore   1
*/      IF sy-subrc = 0 AND xrbkpb-rbstat = 'A' "AND w_fact_immo = ' '
*/     and ws_avoir = ' '.
      IF sy-subrc = 0 AND xrbkpb-rbstat = 'A' AND ws_avoir = ' '.
*}   REPLACE MAK001 MS4K915623 Don't park an assets invoice anymore   1
**        IF xrbkpb-diffn IS INITIAL.
        PERFORM park_to_invoice_post USING return_variables-doc_number.
**        ENDIF.
        MOVE return_variables-doc_number TO l_objkey.
      ENDIF.
*>> END FI_282

*>> ARCHIVAGE
      IF NOT l_objkey IS INITIAL.
* Recherche de la valeur du content repository - table TOAOM
        CLEAR l_archiv_id.
        SELECT SINGLE archiv_id FROM toaom INTO l_archiv_id
                            WHERE sap_object = 'BUS2081'
                              AND ar_object = 'ZMMINVOICE'
                              AND ar_status = 'X'.

        MOVE l_archiv_id TO l_contrep.
        READ TABLE idoc_contrl INTO ls_edidc INDEX 1.
        MOVE ls_edidc-rcvprn TO l_log_sys.
        MOVE ls_edidc-arckey TO l_barcode.

****** début suppression contrôle GTR 06/11/06
** contrôle que l'entrée existe dans la table BDS_BAR_EX
*        SELECT SINGLE * FROM bds_bar_ex WHERE barcode = l_barcode
*                                          AND contrep = l_contrep.
*
*        IF sy-subrc = 0.
****** fin suppression contrôle GTR 06/11/06

* alimentation de la structure S_TOACM pour garder l'enregistrement
*  dans la table BDS_BAR_EX et pour créer un enregistrement dans
*  la table BDS_BAR_IN

        CLEAR ls_toacm.
        MOVE 'X' TO ls_toacm-keep_bar_i.
*        MOVE 'X' TO ls_toacm-keep_bar_e.     "JMJ005 - Parked Document Problem

        CLEAR ls_property.
        ls_property-name = 'BDS_DOCUMENTTYPE'.
        ls_property-value = 'ZMMINVOICE'.
        APPEND ls_property TO lt_property.

        CALL FUNCTION 'BDS_BARCODE_CREATE'
          EXPORTING
            barcode        = l_barcode
            log_system     = l_log_sys
            classname      = 'BUS2081'
            classtype      = 'BO'
            objkey         = l_objkey
            client         = sy-mandt
            s_toacm        = ls_toacm
          TABLES
            properties     = lt_property
          EXCEPTIONS
            error_barcode  = 1
            error_kpro     = 2
            internal_error = 3
            OTHERS         = 4.

        IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
        ELSE.
          COMMIT WORK AND WAIT.
        ENDIF.
      ENDIF.

*>> RGI COMMENT FI_282
*>> WF Discrepancies are launched via SAP standard Event from now.
*>> ALL is managed by MIRO transaction that means that no more
*>> specific screen are called in ZRM03
*      READ TABLE return_variables WITH KEY wf_param = 'Appl_Objects'.
*      SELECT SINGLE * FROM rbkp
*                        WHERE belnr = return_variables-doc_number(10).
*
*      IF w_wf_share IS INITIAL.                   "ITGGH  CHG_02
*
**       quantity discrepance
*        IF w_eq_dtct = 'X'.
*
**         Workflow à déclencher ?
*          SELECT SINGLE * FROM  zbal_params INTO ls_zbal_params
*                 WHERE  param  = 'CB_MEQ'.
*
*          IF sy-subrc EQ 0 AND ls_zbal_params-value = 'X'.
**>> START RGI008 DELETION
*            PERFORM workflow_event_create_updtask
*                    USING rbkp-belnr
*                          rbkp-gjahr
*                          c_event_blockedquant.
*          ENDIF.
*
*        ELSE.
*
*
**         price discrepance
*          IF w_ep_dtct = 'X'.
*
**           Workflow à déclencher ?
*            SELECT SINGLE * FROM  zbal_params INTO ls_zbal_params
*                   WHERE  param  = 'CB_MEP'.
*
*            IF sy-subrc EQ 0 AND ls_zbal_params-value = 'X'.
*
**             raise only by parked documents
*              SELECT SINGLE * FROM  zbal_params INTO ls_zbal_params
*                             WHERE  param  = 'RB_MEP2'.
*              IF sy-subrc EQ 0 AND ls_zbal_params-value = 'X'.
**>> START RGI008 DELETION
*                PERFORM workflow_event_create_updtask
*                  USING rbkp-belnr
*                        rbkp-gjahr
*                        c_event_blockedprice.
*              ENDIF.
*
*            ENDIF.
*          ENDIF.
*        ENDIF.
*      ENDIF.                                    "ITGGH  CHG_02
*>> END RGI COMMENT FI_282


      COMMIT WORK AND WAIT.


**     Flag facture Finale
*      CLEAR wl_factfin.
*
*      LOOP AT wt_factures INTO ws_facture.
*        SELECT * FROM ekpo
*            WHERE ebeln = ws_facture-belnr
*            AND erekz = 'X'.
*          wl_factfin = 'X'.
*        ENDSELECT.
*        IF wl_factfin = 'X'.
*          EXIT.
*        ENDIF.
*      ENDLOOP.
*
*      IF wl_factfin = 'X'.
*        SELECT SINGLE * FROM rbkp WHERE belnr = idoc_status-msgv1.
*        IF sy-subrc EQ 0.
*          PERFORM workflow_event_create_updtask
*                  USING rbkp-belnr
*                        rbkp-gjahr
*                        c_event_blockedquant.
*        ENDIF.
*      ENDIF.

*</serco>
*----------------------------------------------------------------------

      IF wl_start = 'X'
       OR w_wf_share = 'X'.                             "ITGGH  CHG_02

*     Préenregistrement: sauvegarder les propositions de
*     commandes        : RBSELBEST
*     bon de livraison : RBSELLIFS
        READ TABLE idoc_status WITH KEY status = '53'
                                        msgid = 'M8'
                                        msgno = '060'.
        CHECK sy-subrc EQ 0.
        CALL FUNCTION 'ZBALANCE_ECRIRE_SEL' IN UPDATE TASK
          EXPORTING
            i_belnr  = idoc_status-msgv1
          TABLES
            t_docnum = wt_docnum.
      ENDIF.

*-----------------------------------------------------------------------
* ITGGH   09.03.2007   CHG_02                               DE5K900049
* Share workflow
      IF w_wf_share = 'X'.

*       start workflow
*\--> ITVLA20070213+
        CLEAR l_objkey.
        l_objkey = return_variables-doc_number.   "(BELNR+GJAHR)

*   declaration of container
        swc_container cont_reason.


        CALL FUNCTION 'SWE_EVENT_CREATE'
          EXPORTING
            objtype           = 'BUS2081'
            objkey            = l_objkey
            event             = 'Z_MM_SHARE_START'
          TABLES
            event_container   = cont_reason
          EXCEPTIONS
            objtype_not_found = 1
            OTHERS            = 2.

        IF sy-subrc = 0.
          COMMIT WORK.
        ELSE.
*do nothing
        ENDIF.

*\<-- ITVLA20070213+
      ENDIF.
    ENDIF.
  ENDIF.

ENDFUNCTION.
