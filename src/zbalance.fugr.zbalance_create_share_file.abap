FUNCTION zbalance_create_share_file.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(BELNR) TYPE  CHAR10
*"     VALUE(BUKRS) TYPE  BUKRS
*"     VALUE(GJAHR) TYPE  GJAHR
*"     REFERENCE(XBAL_IDOC) TYPE  ZBAL_IDOC
*"  EXCEPTIONS
*"      ERROR_FILE
*"----------------------------------------------------------------------
* HAVYO 30/04/2008 : Adding TAG <H_SUPPLIER_NAME> into file XML
* DIA : FI_00138/ OHA001
*"----------------------------------------------------------------------
* HAVYO 13/05/2008 : Send XML file only for company code in ZF144
* DIA : FI_00184/ OHA002
*"----------------------------------------------------------------------

  DATA : v_char(255) TYPE c,
         xtabix LIKE sy-tabix.

  TABLES : a003, bseg, konp, zf144.


* BEGIN INSERT OHA002 FI_00184
  CLEAR zf144.
  SELECT SINGLE * FROM zf144 WHERE bukrs = bukrs.

  CHECK zf144-zzxml = 'X'.
* END INSERT OHA002.

* récupération nom de fichier physique
  CALL FUNCTION 'FILE_GET_NAME_AND_LOGICAL_PATH'
    EXPORTING
      logical_filename           = 'Z_XML_TO_SHARE'
      parameter_1                = belnr
      parameter_2                = bukrs
    IMPORTING
      file_name                  = vfile
    EXCEPTIONS
      file_not_found             = 1
      operating_system_not_found = 2
      file_system_not_found      = 3
      OTHERS                     = 4.
  IF sy-subrc <> 0.
    RAISE error_file.
  ENDIF.

  CALL FUNCTION 'FILE_GET_NAME_USING_PATH'
    EXPORTING
      logical_path               = 'Z_XML_TO_SHARE'
      file_name                  = vfile
    IMPORTING
      file_name_with_path        = vfile
    EXCEPTIONS
      path_not_found             = 1
      missing_parameter          = 2
      operating_system_not_found = 3
      file_system_not_found      = 4
      OTHERS                     = 5.
  IF sy-subrc <> 0.
    RAISE error_file.
  ENDIF.

* Ouverture file XML
  OPEN DATASET vfile FOR OUTPUT IN TEXT MODE ENCODING DEFAULT.
  IF sy-subrc <> 0.
    RAISE error_file.
  ENDIF.

  CLEAR  bkpf.
  SELECT SINGLE * FROM bkpf
                 WHERE bukrs = bukrs
                   AND belnr = belnr
                   AND gjahr = gjahr.

  CHECK sy-subrc EQ 0.

  DATA : it_a003 TYPE STANDARD TABLE OF a003 WITH HEADER LINE.

  DATA: BEGIN OF xheader,
          filename LIKE vfile,
          id(100) TYPE c,
          bukrs  LIKE bkpf-bukrs,
          number(20),
          wrbtr  LIKE bseg-wrbtr,
          status(20),
          validor LIKE zbal_idoc-validor,
          lifnr  LIKE bseg-lifnr,
* Add FI_138 OHA001
          name1  LIKE lfa1-name1,
* and FI_138
          dmbtr  LIKE bseg-dmbtr,
          vat1   LIKE bseg-dmbtr,
          vat2   LIKE bseg-dmbtr,
          rate1  LIKE konp-kbetr,
          rate2  LIKE konp-kbetr,
          numlot LIKE bkpf-bktxt,
        END OF xheader.

  TYPES: BEGIN OF line,
          hkont  LIKE zbal_idoc-hkont,
          kostl  TYPE char20,
          price  LIKE bseg-dmbtr,
          mwskz  LIKE bseg-mwskz,
          vatb   LIKE bseg-dmbtr,
          aufnr  LIKE zbal_idoc-aufnr,
          rateb  LIKE konp-kbetr,
        END OF line.

  DATA : iline TYPE line.

  DATA : xline LIKE iline OCCURS 0.

  DATA: xml_string TYPE string.
*        result LIKE itab.

  DATA : BEGIN OF i_tva OCCURS 0,
         mwskz LIKE bseg-mwskz,
         dmbtr LIKE bseg-dmbtr,
         rate  LIKE konp-kbetr,
         END OF i_tva.

* Generate header for XML HEADER
  CLEAR xheader.
  xheader-id       = xbal_idoc-refint.
  xheader-bukrs    = bkpf-bukrs.
  xheader-number   = bkpf-xblnr.
  xheader-lifnr    = xbal_idoc-sndprn.
  xheader-dmbtr    = xbal_idoc-betrg.
  xheader-numlot   = xbal_idoc-refint.
  xheader-validor  = xbal_idoc-validor.
  xheader-status   = 'VALIDATION'.

* TAX Rate
  CLEAR bseg.
  REFRESH : i_tva, xline.
  SELECT * FROM bseg WHERE bukrs = bkpf-bukrs
                       AND belnr = bkpf-belnr
                       AND gjahr = bkpf-gjahr.
    CLEAR iline.
    IF  bseg-koart = 'S'
    AND bseg-buzid = ' '.
      iline-hkont    = bseg-hkont.
      IF NOT bseg-kostl IS INITIAL.
        iline-kostl    = bseg-kostl.
      ELSE.
        iline-kostl    = bseg-aufnr.
      ENDIF.
      iline-mwskz    = bseg-mwskz.
      MOVE bseg-dmbtr TO iline-price.
      ADD bseg-dmbtr  TO xheader-wrbtr.
      COLLECT iline INTO xline.
    ENDIF.
    IF bseg-buzid = 'T'.
      i_tva-mwskz = bseg-mwskz.
      i_tva-dmbtr = bseg-dmbtr.
      COLLECT i_tva.
    ENDIF.
  ENDSELECT.

* Taux de TVA 1
  READ TABLE i_tva INDEX 1.
  IF sy-subrc = 0.
    REFRESH it_a003.
    CALL FUNCTION 'RE_KTOSL_TO_MWSKZ_GET'
      EXPORTING
        i_mwskz                        = i_tva-mwskz
        i_bukrs                        = xbal_idoc-bukrs
      TABLES
        e_a003                         = it_a003
      EXCEPTIONS
        no_mwskz_in_t001land           = 1
        no_ktosl_for_mwskz_in_t001land = 2
        t638s_t007b_inconsistency      = 3
        OTHERS                         = 4.
    IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
    ENDIF.

    CLEAR konp.
    READ TABLE it_a003 INDEX 1.
    IF sy-subrc = 0.
      SELECT SINGLE * FROM konp
                     WHERE knumh = it_a003-knumh.
    ENDIF.
    xheader-rate1 = konp-kbetr / 10.
    xheader-vat1  = i_tva-dmbtr.
    i_tva-rate    = xheader-rate1.
    MODIFY i_tva INDEX 1.
  ENDIF.

* Taux de TVA 2
  READ TABLE i_tva INDEX 2.
  IF sy-subrc = 0.
    REFRESH it_a003.
    CALL FUNCTION 'RE_KTOSL_TO_MWSKZ_GET'
      EXPORTING
        i_mwskz                        = i_tva-mwskz
        i_bukrs                        = xbal_idoc-bukrs
      TABLES
        e_a003                         = it_a003
      EXCEPTIONS
        no_mwskz_in_t001land           = 1
        no_ktosl_for_mwskz_in_t001land = 2
        t638s_t007b_inconsistency      = 3
        OTHERS                         = 4.
    IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
    ENDIF.

    CLEAR konp.
    READ TABLE it_a003 INDEX 1.
    IF sy-subrc = 0.
      SELECT SINGLE * FROM konp
                     WHERE knumh = it_a003-knumh.
    ENDIF.
    xheader-rate2 = konp-kbetr / 10.
    xheader-vat2  = i_tva-dmbtr.
    i_tva-rate    = xheader-rate2.
    MODIFY i_tva INDEX 2.
  ENDIF.

  LOOP AT xline INTO iline.
    xtabix = sy-tabix.
    READ TABLE i_tva WITH KEY mwskz = iline-mwskz.
    IF sy-subrc = 0.
      iline-rateb = i_tva-rate.
      iline-vatb  = i_tva-dmbtr.
      MODIFY xline FROM iline INDEX xtabix.
    ENDIF.
  ENDLOOP.

* Add FI_138 OHA001
  SELECT SINGLE name1 INTO xheader-name1
                      FROM lfa1
                     WHERE lifnr = xheader-lifnr.
* and FI_138

  TRY.

      CALL TRANSFORMATION zbalance_share_xml
        SOURCE header = xheader
               line   = xline
        RESULT XML xml_string.

    CATCH cx_st_error.
      ...

  ENDTRY.

  TRANSFER xml_string TO vfile.

*Close File
  CLOSE DATASET vfile.
  IF sy-subrc <> 0.
    RAISE error_file.
  ENDIF.
*}   INSERT
ENDFUNCTION.
