FUNCTION zbalance_idoc_process.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  TABLES
*"      IDOCLIST STRUCTURE  BDIDOCNR
*"      NOPROCESSEDIDOC STRUCTURE  BDIDOCNR
*"  EXCEPTIONS
*"      NO_IDOC_SELECTED
*"----------------------------------------------------------------------


* Contrôler la sélection
  IF idoclist[] IS INITIAL.
    RAISE no_idoc_selected.
  ENDIF.

  LOOP AT idoclist.

    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_READ'
         EXPORTING
              document_number         = idoclist-docnum
         EXCEPTIONS
              document_foreign_lock   = 1
              document_not_exist      = 2
              document_number_invalid = 3
              OTHERS                  = 4.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc locked'(w17).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'IDoc does not exist'(w18).
      ELSEIF sy-subrc EQ 3.
        MESSAGE s208(00) WITH 'IDoc number is invalid'(w19).
      ENDIF.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_READ_LAST_STATUS'
         EXPORTING
              document_number        = idoclist-docnum
         IMPORTING
              status                 = int_edids
         EXCEPTIONS
              document_not_open      = 1
              no_status_record_found = 2
              OTHERS                 = 3.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open'(w15).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'No status record found'(w16).
      ENDIF.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_READ'
         EXPORTING
              document_number   = idoclist-docnum
         EXCEPTIONS
              document_not_open = 1
              parameter_error   = 2
              OTHERS            = 3.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open for reading'(w12).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'Parameter combination is invalid'(w13).
      ENDIF.
    ENDIF.

    SELECT SINGLE statva INTO stacust-statva
                         FROM stacust
                         WHERE status = int_edids-status.
    SELECT SINGLE stalight INTO stalight-stalight
                           FROM stalight
                           WHERE statva = stacust-statva.


    IF stalight-stalight = '1' OR
       stalight-stalight = '2'.
      noprocessedidoc-docnum = idoclist-docnum.
      APPEND noprocessedidoc.
    ELSE.
      ws_docnums = idoclist-docnum.
      w_cpt_append = w_cpt_append + 1.
      APPEND ws_docnums TO ws_info-docnums.
    ENDIF.
  ENDLOOP.

  IF w_cpt_append GT 0.

    ws_info-icount = w_cpt_append.
    istat-icount = w_cpt_append.

    SELECT SINGLE * FROM t000 WHERE mandt EQ sy-mandt.

    CREATE OBJECT ls EXPORTING logsys = t000-logsys.

    istat-fill_bits = 'X      X'.
    APPEND ws_info TO istat-info.

    CALL METHOD ls->process_idocs
    EXPORTING idoc_stat    = istat
    process_mode = 'A'.
  ENDIF.





ENDFUNCTION.
