FUNCTION zbalance_facmm_disp_from_idoc.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(DOCNUM) LIKE  EDIDC-DOCNUM
*"     REFERENCE(DISPLAY_ONLY) OPTIONAL
*"  EXCEPTIONS
*"      DOC_NOT_FOUND
*"      OTHERS
*"----------------------------------------------------------------------
*{   INSERT         MS4K917495                                        2
* JMJ005 - add xblnr structure for deleted parked document
  data: begin of next_doc,
        f1(2),
        belnr type rbkp-belnr,
        gjahr type rbkp-gjahr,
        end of next_doc.
*
*}   INSERT

  SELECT SINGLE * FROM edidc WHERE docnum = docnum.

  IF sy-subrc NE 0.
    RAISE doc_not_found.
  ELSE.

    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_READ'
         EXPORTING
              document_number = docnum.


    CALL FUNCTION 'EDI_DOCUMENT_READ_LAST_STATUS'
         EXPORTING
              document_number        = docnum
         IMPORTING
              status                 = int_edids
         EXCEPTIONS
              document_not_open      = 1
              no_status_record_found = 2
              OTHERS                 = 3.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open'(w15).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'No status record found'(w16).
      ENDIF.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_READ'
         EXPORTING
              document_number   = docnum
         EXCEPTIONS
              document_not_open = 1
              parameter_error   = 2
              OTHERS            = 3.


    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open for reading'(w12).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'Parameter combination is invalid'(w13).
      ENDIF.
    ENDIF.


    SELECT * FROM  rbkp
           WHERE  belnr  = int_edids-stapa1
           ORDER BY gjahr.
    ENDSELECT.
*{   INSERT         MS4K917495                                        1
    if rbkp-rbstat = '2'.                  "JMJ001 add
       move rbkp-xblnr to next_doc.
       select single * from rbkp
               where belnr = next_doc-belnr
                 and gjahr = next_doc-gjahr.
    endif.

*}   INSERT

    IF display_only = 'X'.
      rbkp-rbstat = '5'.
    ENDIF.

    IF rbkp-rbstat CA 'ABCDE3'.
      SET PARAMETER ID 'RBN' FIELD rbkp-belnr.
      SET PARAMETER ID 'GJR' FIELD rbkp-gjahr.
      SET PARAMETER ID 'RBS' FIELD rbkp-rbstat.
      SET PARAMETER ID 'CHG' FIELD 'X'.
      CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.
    ELSEIF rbkp-rbstat CA '1245'.
      SET PARAMETER ID 'RBN' FIELD rbkp-belnr.
      SET PARAMETER ID 'GJR' FIELD rbkp-gjahr.
      CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.
    ENDIF.
*
*    CALL FUNCTION 'ZBALANCE_DOCFAC_DISPLAY'
*         EXPORTING
*              belnr         = rbkp-belnr
*              gjahr         = rbkp-gjahr
*         EXCEPTIONS
*              doc_not_found = 1
*              OTHERS        = 2.
  ENDIF.

ENDFUNCTION.
