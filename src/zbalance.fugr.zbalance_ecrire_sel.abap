FUNCTION zbalance_ecrire_sel.
*"----------------------------------------------------------------------
*"*"Module de fonction de mise à jour :
*"
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_BELNR) TYPE  EDI_STAPA1
*"  TABLES
*"      T_DOCNUM STRUCTURE  ZBALANCE_03
*"----------------------------------------------------------------------
  DATA: wt_rbselbest TYPE STANDARD TABLE OF rbselbest.
  DATA: wt_rbsellifs TYPE STANDARD TABLE OF rbsellifs.

  SELECT  * FROM rbkp WHERE belnr = i_belnr "ex SELECT SINGLE
                              AND cpudt  = sy-datum.
  ENDSELECT.
  CHECK sy-subrc EQ 0.
  MOVE: rbkp-belnr TO rbselbest-belnr,
        rbkp-gjahr TO rbselbest-gjahr,
        rbkp-belnr TO rbsellifs-belnr,
        rbkp-gjahr TO rbsellifs-gjahr.
  LOOP AT t_docnum.
    IF NOT ( t_docnum-lfsnr IS INITIAL ).
      MOVE t_docnum-lfsnr TO rbsellifs-lfsnr.
      COLLECT rbsellifs INTO wt_rbsellifs.
      CONTINUE.
    ENDIF.
    IF NOT ( t_docnum-ebeln IS INITIAL ).
      MOVE t_docnum-ebeln TO rbselbest-ebeln.
      COLLECT rbselbest INTO wt_rbselbest.
    ENDIF.
  ENDLOOP.
  INSERT rbselbest FROM TABLE wt_rbselbest.
  INSERT rbsellifs FROM TABLE wt_rbsellifs.
ENDFUNCTION.
