***********************************************************************
* G. TRIMOREAU 18/09/06
* C. DUMONT    04/10/06 - Utilisation du MF zbalance_role_reception_prim
* Création d'un rôle : recherche du user ayant effectué l'entrée de
*                      marchandises.
***********************************************************************
FUNCTION zbalance_role_reception.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  TABLES
*"      AC_CONTAINER STRUCTURE  SWCONT
*"      ACTOR_TAB STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOBODY_FOUND
*"----------------------------------------------------------------------
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Default Role determination                       *
* ----------------- * ------------------------------------------------ *
* Author            : JM.JOURON                                JMJ001  *
* Date              : 22.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : New Role determination                           *
* ----------------- * ------------------------------------------------ *

  INCLUDE <cntain>.

  DATA w_ebeln TYPE ekko-ebeln.
  DATA w_ebelp TYPE rseg-ebelp.
  break gilr.
*>> START RGI002
  DATA : l_custom_data LIKE swd_custom.
  DATA w_bukrs TYPE bukrs.
*>> END RGI002
*--- JMJ001 le 121107 - acces ZBAL_WFROLE pour regles spécifiques +
  constants: c_wf_2 type ZBAL_WF_NUMBER_D value 2.
  data: l_dft_usr type usnam, "user by default
        l_spec_usr type usnam. "specific rules user
*--- JMJ001 le 121107 fin +


  swc_get_element ac_container 'NoCommande' w_ebeln.
  swc_get_element ac_container 'NoPoste' w_ebelp.


  REFRESH actor_tab.
*JMJ001 - 131107 - add an access to z-table for specific rules
    CALL FUNCTION 'Z_BALANCE_USER_DETERMINE'
      EXPORTING
        wf_number              = c_wf_2
        ebeln                  = w_ebeln
     IMPORTING
       DEFAULT_USER           = l_dft_usr
       SPEC_RULE_USER         = l_spec_usr
     EXCEPTIONS
       INVALID_DOCUMENT       = 1
       OTHERS                 = 2.

    if sy-subrc ne 0 or l_spec_usr eq space.
*JMJ001 - 131107 - current treatment

  CALL FUNCTION 'ZBALANCE_ROLE_RECEPTION_PRIM'
    EXPORTING
      w_ebeln      = w_ebeln
      w_ebelp      = w_ebelp
    TABLES
      actor_tab    = actor_tab
    EXCEPTIONS
      nobody_found = 1
      OTHERS       = 2.

  IF sy-subrc <> 0.
*JMJ001 - 131107 - default user for the key ? - ADD
      if l_dft_usr ne space.
        actor_tab-otype = 'US'.
        actor_tab-objid = l_dft_usr.
        APPEND actor_tab.
      else.
*JMJ001 - 131107 - END/ADD
*{DEL APPIA 25.10.2006
*    RAISE nobody_found.
*}DEL APPIA 25.10.2006

*>> START RGI002
*>> Now we kill Miss BONAVENTURE to replace her by a table
*{INS APPIA 25.10.2006
*    actor_tab-otype = 'US'.
*    actor_tab-objid = 'BONAVENTURE'.
*    APPEND actor_tab.
*}INS APPIA 25.10.2006

*>>      Now we get Administator of System from WF point of view
*        get customizing information
    CALL FUNCTION 'SWD_GET_CUSTOMIZING'
      IMPORTING
        custom_data = l_custom_data.
* default administrator was found in customizing
    IF NOT l_custom_data-def_admin IS INITIAL.
      actor_tab      = l_custom_data-def_admin.
      APPEND actor_tab.
    ENDIF.
   endif. "JMJ 131107 - add

  ENDIF.
  else.
* Specific user determined with ZBAL_WFROLE
    actor_tab-otype = 'US'.
    actor_tab-objid = l_spec_usr.
    APPEND actor_tab.
  endif.
ENDFUNCTION.
