************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Initialisation des tables des lignes de factures    *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Gaëlle TRIMOREAU (Netinside)                                *
*                                                                      *
* Date   : 21/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport: DE2K900720                                       *
*									         *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_modif_pce_pre_enr.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(I_BUKRS) LIKE  BKPF-BUKRS
*"     REFERENCE(I_BELNR) LIKE  BKPF-BELNR
*"     REFERENCE(I_GJAHR) LIKE  BKPF-GJAHR
*"     REFERENCE(I_XBLNR) LIKE  BKPF-XBLNR OPTIONAL
*"     REFERENCE(I_BLART) LIKE  BKPF-BLART OPTIONAL
*"  EXPORTING
*"     REFERENCE(E_STATUS) LIKE  SY-INDEX
*"  TABLES
*"      T_SEG STRUCTURE  ZSBALANCE_SEG
*"      T_BDCMSGCOLL STRUCTURE  BDCMSGCOLL OPTIONAL
*"----------------------------------------------------------------------
************************************************************************
*                   DECLARATION DE DONNEES                             *
************************************************************************
***************************** Tables ***********************************
  TABLES: vbsegk.

***************************** Données **********************************
  DATA: w_hkont LIKE vbsegk-hkont,
        w_index LIKE sy-tabix,
        w_cursor(15),
        w_buzei LIKE vbsegs-buzei,
        w_buzei_old LIKE vbsegs-buzei,
        w_konto(16),
        w_kostl(15),
        w_gsber(15),
        w_aufnr(15),
        w_mwskz(15),
        w_bschl(15),
        w_wrbtr(15),
        w_wrbtr_value(20),
        w_count TYPE i,
        w_mode  TYPE c VALUE 'N',
        w_line  TYPE i,
        w_line2 TYPE i,
*-----  G.Ghotra IT-Informatik GmbH    27.10.2006 ------------------*
        w_lines TYPE i,
        w_delet TYPE i,
        w_anzdt(15),
*-------------------------------------------------------------------*
        w_flag(1),
        w_statut LIKE sy-index.

  DATA: w_zhkont_old(10) TYPE n.

***************************** Constantes *******************************
* Transaction d'affichage de la facture
  CONSTANTS : k_tcode LIKE sy-tcode VALUE 'FBV2',
              k_bschl31 LIKE vbsegk-bschl VALUE '31'.

***************************** Tables internes **************************
  DATA: BEGIN OF i_bdcmsgcoll OCCURS 0.
          INCLUDE STRUCTURE bdcmsgcoll.
  DATA: END OF i_bdcmsgcoll.
  DATA: opt TYPE ctu_params,
        i_seg TYPE zsbalance_seg OCCURS 0 WITH HEADER LINE.


***************************** Structures *******************************

************************************************************************
*                             TRAITEMENT                               *
************************************************************************

* Recherche du n° de compte général lié au fournisseur.
  CLEAR vbsegk.
  SELECT hkont INTO w_hkont "ex SELECT SINGLE
              FROM vbsegk
              WHERE ausbk = i_bukrs
                AND belnr = i_belnr
                AND gjahr = i_gjahr
                AND bschl = k_bschl31.
  ENDSELECT.

  i_seg[] = t_seg[].
  CLEAR i_seg.
  DESCRIBE TABLE i_seg LINES w_line.

*-----  G.Ghotra IT-Informatik GmbH    27.10.2006 ------------------*
* w_count = 15 - w_line.
*  DO w_count TIMES.
*    CLEAR i_seg.
*    APPEND i_seg.
*  ENDDO.
  SELECT COUNT( * ) FROM vbsegs INTO w_lines
                WHERE ausbk = i_bukrs
                AND belnr = i_belnr
                AND gjahr = i_gjahr.
  w_count = w_lines - w_line.

  CLEAR i_bdcdata.
  REFRESH i_bdcdata.

* standard size of windows for numbers of lines in batch input
  opt-defsize = 'X'.
  opt-dismode = w_mode.

* 1er écran de la transaction FBV2
  PERFORM f1_dynpro USING:  'X' 'SAPMF05V'      '0100',
                            ' ' 'RF05V-BUKRS' i_bukrs,
                            ' ' 'RF05V-BELNR' i_belnr,
                            ' ' 'RF05V-GJAHR' i_gjahr,
                            ' ' 'BDC_OKCODE'  '/00'.

* Modification de la référence de la facture
  PERFORM f1_dynpro USING:'X' 'SAPLF040'      '0700'.
  IF NOT i_xblnr IS INITIAL.
    PERFORM f1_dynpro USING:  ' ' 'BKPF-XBLNR'  i_xblnr.
  ENDIF.
  IF NOT i_blart IS INITIAL.
    PERFORM f1_dynpro USING  ' ' 'BDC_OKCODE'  '=BK'.
    PERFORM f1_dynpro USING  'X' 'SAPLF040'    '0600'.
    PERFORM f1_dynpro USING  ' ' 'BKPF-BLART'  i_blart.
    PERFORM f1_dynpro USING  ' ' 'BDC_OKCODE'  'AB'.
    PERFORM f1_dynpro USING  'X' 'SAPLF040'    '0700'.
  ENDIF.
  PERFORM f1_dynpro USING:  ' ' 'BDC_OKCODE'  '=SB'.

  IF w_line > 0.

    CLEAR  w_buzei_old.
    PERFORM f1_dynpro USING     'X' 'SAPLF040'      '0310'.

    LOOP AT i_seg WHERE zhkont <> w_hkont.

      CHECK i_seg-umskz IS INITIAL.

      MOVE sy-tabix TO w_index.
      CLEAR vbsegs.
      CLEAR: w_buzei, w_konto, w_kostl, w_gsber, w_aufnr,
             w_mwskz, w_bschl, w_wrbtr.

      ADD 1 TO w_buzei_old.
      MOVE w_buzei_old TO w_buzei.

      CONCATENATE 'RF05V-KONTO(' w_buzei+1(2) ')' INTO w_konto.
      CONCATENATE 'BSEG-KOSTL('  w_buzei+1(2) ')' INTO w_kostl.
      CONCATENATE 'BSEG-GSBER('  w_buzei+1(2) ')' INTO w_gsber.
      CONCATENATE 'BSEG-AUFNR('  w_buzei+1(2) ')' INTO w_aufnr.
      CONCATENATE 'BSEG-MWSKZ('  w_buzei+1(2) ')' INTO w_mwskz.
      CONCATENATE 'BSEG-BSCHL('  w_buzei+1(2) ')' INTO w_bschl.
      CONCATENATE 'BSEG-WRBTR('  w_buzei+1(2) ')' INTO w_wrbtr.

      WRITE i_seg-zwrbtr TO w_wrbtr_value.
      CONDENSE w_wrbtr_value.

*     Modification: cpte gal, centre de coûts, domaine d'activité,
*     code TVA, ordre
      PERFORM f1_dynpro USING:      ' ' w_konto   i_seg-zhkont,
*                                    ' ' w_kostl   i_seg-zkostl,
                                    ' ' w_gsber   i_seg-zgsber,
                                    ' ' w_aufnr   i_seg-zaufnr,
                                    ' ' w_mwskz   i_seg-zmwskz,
                                    ' ' w_bschl   i_seg-bschl,
                                    ' ' w_wrbtr   w_wrbtr_value.

      MOVE w_buzei TO w_buzei_old.

*     last item: go back
      IF w_index = w_line.
        PERFORM f1_dynpro USING ' ' 'BDC_OKCODE'  '=RW'.
      ELSE.

*       next page
        IF w_buzei_old = 14.
          w_buzei_old = 1.
          PERFORM f1_dynpro USING: ' ' 'BDC_OKCODE'  '=P+',
                                   'X' 'SAPLF040'    '0310'.
        ENDIF.
      ENDIF.
    ENDLOOP.

*   Modification de l'élément d'OTP
    w_buzei = '001'.
    LOOP AT i_seg.
      MOVE sy-tabix TO w_index.
*      IF i_seg-zprojk <> i_seg-zprojk_old.
      CONCATENATE 'RF05V-ANZDT(' w_buzei+1(2) ')' INTO w_anzdt.
      PERFORM f1_dynpro USING: 'X' 'SAPLF040'      '0700',
                               ' ' 'BDC_CURSOR'    w_anzdt,
                               ' ' 'BDC_OKCODE'    '=PI',

                               'X' 'SAPLF040'      '0300',
                               ' ' 'BDC_OKCODE'    '=ZK',

                               'X' 'SAPLKACB'      '0002',
                               ' ' 'COBL-PS_POSID' i_seg-zprojk,
                               ' ' 'COBL-KOSTL'    i_seg-zkostl,
                               ' ' 'BDC_OKCODE'    '=ENTE',

                               'X' 'SAPLF040'      '0330',
                               ' ' 'BDC_OKCODE'    '=RW'.
*      ENDIF.
      ADD 1 TO w_buzei.
      IF w_buzei = '002'.
        ADD 1 TO w_buzei.
      ENDIF.
      IF w_buzei = '010'.
        w_buzei = '001'.
        PERFORM f1_dynpro USING: 'X' 'SAPLF040'      '0700',
                                 ' ' 'BDC_OKCODE'    '=P+'.
      ENDIF.
    ENDLOOP.

  ENDIF.

* If there were more items before, delete these lines
  IF w_count > 0.
    w_buzei = '010'.
    DO w_count TIMES.
      SUBTRACT 1 FROM w_buzei.
      CONCATENATE 'RF05V-ANZDT(' w_buzei+1(2) ')' INTO w_anzdt.

*     go to last page
      PERFORM f1_dynpro USING: 'X' 'SAPLF040'    '0700',
                               ' ' 'BDC_OKCODE'  '=P++'.

*     go back if necessary
      IF w_delet > 0.
        DO w_delet TIMES.
          PERFORM f1_dynpro USING: 'X' 'SAPLF040'    '0700',
                                   ' ' 'BDC_OKCODE'  '=P-'.
        ENDDO.
      ENDIF.

*     delete
      PERFORM f1_dynpro USING: 'X' 'SAPLF040'    '0700',
                               ' ' 'BDC_CURSOR'   w_anzdt,
                               ' ' 'BDC_OKCODE'  '=ZLO'.

*     if all items are deleted on this page,
*     go back another page next time
      IF w_buzei = '001'.
        w_buzei = '010'.
        ADD 1 TO w_delet.
      ENDIF.
    ENDDO.
  ENDIF.

*   posting
    PERFORM f1_dynpro USING: 'X' 'SAPLF040'    '0700',
                             ' ' 'BDC_OKCODE'  '=BP'.
*-------------------------------------------------------------------*

  CALL TRANSACTION k_tcode USING i_bdcdata OPTIONS FROM opt
                    MESSAGES INTO i_bdcmsgcoll.

  CLEAR i_bdcmsgcoll.
  DESCRIBE TABLE i_bdcmsgcoll LINES w_line2.
  IF w_line2 <> 0.
    READ TABLE i_bdcmsgcoll WITH KEY msgtyp = 'E'.
    IF sy-subrc = 0.
      w_statut = 2.
    ELSE.
      READ TABLE i_bdcmsgcoll WITH KEY msgtyp = 'A'.
      IF sy-subrc = 0.
        w_statut = 2.
      ELSE.
        w_statut = 1.
      ENDIF.
    ENDIF.
  ENDIF.
  MOVE w_statut TO e_status.

  t_bdcmsgcoll[] = i_bdcmsgcoll[].
ENDFUNCTION.
