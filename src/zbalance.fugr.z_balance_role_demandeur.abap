*" APPIA Consulting  - 23.10.2006
*" Module fonction de recherche du demandeur
*" à l'initiative de la commande s'il y a une demande d'achat
*" Module fonction utilisé pour la réception manquante
FUNCTION z_balance_role_demandeur.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  TABLES
*"      AC_CONTAINER STRUCTURE  SWCONT
*"      ACTOR_TAB STRUCTURE  SWHACTOR
*"  EXCEPTIONS
*"      NOBODY_FOUND
*"----------------------------------------------------------------------
* Author            : Reynald GIL                                RGI002*
* Date              : 09.02.2007                                       *
* Document  DIA     : FI_281_DIA                                       *
* Description       : Default Role determination                       *
* ----------------- * ------------------------------------------------ *
* Author            : JM.JOURON                                JMJ001  *
* Date              : 22.11.2007                                       *
* Document  DIA     : FI_346_DIA                                       *
* Description       : New Role determination                           *
* ----------------- * ------------------------------------------------ *

  INCLUDE <cntain>.

  DATA w_ebeln TYPE ekko-ebeln.
  DATA : l_custom_data LIKE swd_custom.
*--- JMJ001 le 121107 - acces ZBAL_WFROLE pour regles spécifiques +
  constants: c_wf_3 type ZBAL_WF_NUMBER_D value 3.
  data: l_dft_usr type usnam, "user by default
        l_spec_usr type usnam. "specific rules user
*--- JMJ001 le 121107 fin +

  swc_get_element ac_container 'NoCommande' w_ebeln.

*{INS APPIA 25.10.2006
  REFRESH actor_tab.
*}INS APPIA 25.10.2006

*>> START RGI002 -
*>> Here we must admit a problem in WF Container
*>> So Admin User of WF must be alerted
  IF w_ebeln IS INITIAL.
*{DEL APPIA 25.10.2006
*    RAISE nobody_found.
*}DEL APPIA 25.10.2006

**{INS APPIA 25.10.2006
*    actor_tab-otype = 'US'.
*    actor_tab-objid = 'BONAVENTURE'.
*    APPEND actor_tab.
*}INS APPIA 25.10.2006
    CALL FUNCTION 'SWD_GET_CUSTOMIZING'
      IMPORTING
        custom_data = l_custom_data.
* default administrator was found in customizing
    IF NOT l_custom_data-def_admin IS INITIAL.
      actor_tab      = l_custom_data-def_admin.
      APPEND actor_tab.
    ENDIF.

  ELSE.
*JMJ001 - 131107 - add an access to z-table for specific rules
    CALL FUNCTION 'Z_BALANCE_USER_DETERMINE'
      EXPORTING
        wf_number              = c_wf_3
        ebeln                  = w_ebeln
     IMPORTING
       DEFAULT_USER           = l_dft_usr
       SPEC_RULE_USER         = l_spec_usr
     EXCEPTIONS
       INVALID_DOCUMENT       = 1
       OTHERS                 = 2.

    if sy-subrc ne 0 or l_spec_usr eq space.
*JMJ001 - 131107 - current treatment

    CALL FUNCTION 'Z_BALANCE_ROLE_ACHETEUR_SECOND'
      EXPORTING
        x_ebeln      = w_ebeln
      TABLES
        w_actor      = actor_tab
      EXCEPTIONS
        nobody_found = 1
        OTHERS       = 2.

    IF sy-subrc <> 0.
*{DEL APPIA 25.10.2006
*    RAISE nobody_found.
*}DEL APPIA 25.10.2006
*JMJ001 - 131107 - default user for the key ? - ADD
      if l_dft_usr ne space.
        actor_tab-otype = 'US'.
        actor_tab-objid = l_dft_usr.
        APPEND actor_tab.
      else.
*JMJ001 - 131107 - END/ADD

*>> START RGI002
*>> Now we kill Miss BONAVENTURE to replace her by a table
*>> RGI DELETION
*{INS APPIA 25.10.2006
*      actor_tab-otype = 'US'.
*      actor_tab-objid = 'BONAVENTURE'.
*      APPEND actor_tab.
*}INS APPIA 25.10.2006
*>> END RGI DELETION
      CALL FUNCTION 'SWD_GET_CUSTOMIZING'
        IMPORTING
          custom_data = l_custom_data.
* default administrator was found in customizing
      IF NOT l_custom_data-def_admin IS INITIAL.
        actor_tab      = l_custom_data-def_admin.
        APPEND actor_tab.
      ENDIF.
     endif. "JMJ 131107 - add
    ENDIF.
  else.
* Specific user determined with ZBAL_WFROLE
    actor_tab-otype = 'US'.
    actor_tab-objid = l_spec_usr.
    APPEND actor_tab.
  endif.

  ENDIF.

ENDFUNCTION.
