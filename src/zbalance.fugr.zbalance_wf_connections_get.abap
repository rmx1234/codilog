FUNCTION zbalance_wf_connections_get.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_OBJECT) TYPE  SWOTOBJID OPTIONAL
*"     VALUE(OBJECTTYPE) LIKE  SWOTOBJID-OBJTYPE OPTIONAL
*"     VALUE(OBJECTID) LIKE  SWOTOBJID-OBJKEY OPTIONAL
*"     VALUE(HELP_INFOS) TYPE  HELP_INFO OPTIONAL
*"     VALUE(POPUP) TYPE  XFELD DEFAULT 'X'
*"  TABLES
*"      OBJECT STRUCTURE  OBJECTCONN OPTIONAL
*"      PARAMETER STRUCTURE  PARAMETERS OPTIONAL
*"      T_WORKLIST STRUCTURE  SWR_WIHDR OPTIONAL
*"  EXCEPTIONS
*"      NO_WORKFLOWS_FOUND
*"----------------------------------------------------------------------

* Cette MF est une copie de: SWI_WF_CONNECTIONS_DISPLAY
* Modifications: mise en commentaire de la partie d'affichage (fin)
* Ajoute du paramètre d'export

  DATA:
    l_flush_me         TYPE xfeld,
    lt_worklist_part   TYPE swr_wihdr OCCURS 10 WITH HEADER LINE,
    lt_worklist        TYPE swr_wihdr OCCURS 10 WITH HEADER LINE,
    ls_por             TYPE swotobjid,
    lt_pors            TYPE swotobjid OCCURS 3 WITH HEADER LINE,
    l_cont_value       TYPE swcont-value,
    ls_object_info     TYPE swlobjects.

  IF object[] IS INITIAL.
    ls_por = i_object.
    IF i_object IS INITIAL.
      i_object-objtype = objecttype.
      i_object-objkey  = objectid.
    ENDIF.
    IF NOT i_object IS INITIAL.
      MOVE-CORRESPONDING i_object TO lt_pors.
      APPEND lt_pors.
    ENDIF.
  ELSE.
    LOOP AT object.
      CLEAR lt_pors.
      lt_pors-objtype = object-objecttype.
      lt_pors-objkey  = object-objectid.
      APPEND lt_pors.
    ENDLOOP.
  ENDIF.

  LOOP AT lt_pors.
    CLEAR lt_worklist_part[].
    CALL FUNCTION 'SAP_WAPI_WORKITEMS_TO_OBJECT'
         EXPORTING
              objtype                  = lt_pors-objtype
              objkey                   = lt_pors-objkey
              top_level_items          = space  "note 0000489552
              selection_status_variant = 0000
         TABLES
              worklist                 = lt_worklist_part[].
    APPEND LINES OF lt_worklist_part[] TO lt_worklist[].
  ENDLOOP.

  SORT lt_worklist BY wi_id.
  DELETE ADJACENT DUPLICATES FROM lt_worklist COMPARING wi_id.

  "note 0000489552
  DATA lv_tabix         TYPE sytabix.
  DATA lt_worklist_2    TYPE TABLE OF swr_wihdr.
  DATA lt_top_fis       TYPE swwwihead OCCURS 1 WITH HEADER LINE.
  DATA lt_top_fis_ex    TYPE swr_wihdr OCCURS 1 WITH HEADER LINE.
  DATA lt_path_fis      TYPE TABLE OF swwwihead.
  DATA lt_all_path_fis  TYPE TABLE OF swwwihead.
  lt_worklist_2[] = lt_worklist[].
  LOOP AT lt_worklist WHERE wi_chckwi <> 0.
    lv_tabix = sy-tabix.
    READ TABLE lt_worklist_2 WITH KEY wi_id = lt_worklist-wi_chckwi
         TRANSPORTING NO FIELDS.
    IF sy-subrc <> 0.
      READ TABLE lt_all_path_fis WITH KEY wi_id = lt_worklist-wi_chckwi
           TRANSPORTING NO FIELDS.
      IF sy-subrc <> 0.
        CALL FUNCTION 'SWP_TOP_LEVEL_WORKFLOW_GET'
             EXPORTING
                  workitem_id        = lt_worklist-wi_id
             IMPORTING
                  top_level_flowitem = lt_top_fis
             TABLES
                  fis_on_the_path    = lt_path_fis[]
             EXCEPTIONS
                  OTHERS             = 1.
        IF sy-subrc = 0.
          APPEND LINES OF lt_path_fis[] TO lt_all_path_fis[].
          READ TABLE lt_worklist WITH KEY wi_id = lt_top_fis-wi_id
               TRANSPORTING NO FIELDS.
          IF sy-subrc <> 0.
            APPEND lt_top_fis.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
    DELETE lt_worklist INDEX lv_tabix.
  ENDLOOP.
  IF NOT lt_top_fis[] IS INITIAL.
    SORT lt_top_fis BY wi_id.
    DELETE ADJACENT DUPLICATES FROM lt_top_fis COMPARING wi_id.
    PERFORM set_status_and_type(saplswri) TABLES lt_top_fis
                                                 lt_top_fis_ex.
    PERFORM set_user_name(saplswri) TABLES lt_top_fis_ex.
    PERFORM set_task_text(saplswri) TABLES lt_top_fis_ex.
    APPEND LINES OF lt_top_fis_ex[] TO lt_worklist[].
  ENDIF.
  "end note 0000489552

*  Exporter les valeurs
  CLEAR t_worklist[].

  IF lt_worklist[] IS INITIAL.
    sy-msgid = 'SWL'.
    sy-msgno = '013'.
    sy-msgty = 'S'.
    MESSAGE s013(swl) RAISING no_workflows_found.
  ENDIF.

  APPEND LINES OF lt_worklist TO t_worklist.
*  clear gt_wp_items. refresh gt_wp_items.
*  loop at lt_worklist.
*    clear gt_wp_items.
*    move-corresponding lt_worklist to gt_wp_items.
*    append gt_wp_items.
*  endloop.
*
*  clear g_title.
*  if not ls_por is initial.
*    l_cont_value = ls_por.
*    call function 'SWL_GET_OBJECT_INFORMATION'
*         exporting
*              key_value   = l_cont_value
*         importing
*              object_info = ls_object_info.
*    concatenate text-004 ls_object_info-def_attrib
*                into g_title separated by space.
*  else.
*    g_title = text-003.
*  endif.
*
*  if help_infos is initial.
*    clear g_parent.
*  else.
*    g_parent = cl_gui_container=>screen1.
*  endif.
*
*  if popup eq 'X'.
*    call screen 0200    starting at 2 2
*                        ending   at 100 20.
*  else.
*    call screen 0300.
*  endif.
*
*  if not my_workflow_viewer is initial.
*    call method my_workflow_viewer->free
*      exceptions
*        others = 1.
*    clear my_workflow_viewer.
*    l_flush_me = 'X'.
*  endif.
*
*  if not my_container is initial.
*    call method my_container->free
*      exceptions
*        others = 1.
*    clear my_container.
*    l_flush_me = 'X'.
*  endif.
*
*  if l_flush_me eq 'X'.
*    call method cl_gui_cfw=>update_view  "flush
*      exceptions
*        others = 1.
*  endif.

ENDFUNCTION.
