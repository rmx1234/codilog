*----------------------------------------------------------------------*
*   INCLUDE LZBALANCEF02                                               *
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  f_210_lire_commande
*&---------------------------------------------------------------------*
*       Lire les données de base SAP pour la liste des postes
*----------------------------------------------------------------------*
*      -->PT_COMMANDES  Données des postes de la commande
*      -->PS_FACTURE    Ligne de la facture
*----------------------------------------------------------------------*
FORM f_210_lire_commande TABLES pt_commandes STRUCTURE zbalance_02
                          USING ps_facture   TYPE zbalance_01.

  MOVE ps_facture-belnr TO ekko-ebeln.
  SELECT SINGLE * FROM ekko WHERE ebeln EQ ekko-ebeln.
  IF sy-subrc NE 0.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = e1edp02-belnr
      IMPORTING
        output = ekko-ebeln.
    SELECT SINGLE * FROM ekko WHERE ebeln EQ ekko-ebeln.
    IF sy-subrc NE 0.
      ps_facture-ident = '3'.
      ps_facture-belnr = e1edp02-belnr.
      EXIT.
    ENDIF.
  ENDIF.
  READ TABLE pt_commandes WITH KEY ebeln = ekko-ebeln.
  CHECK sy-subrc NE 0.
  SELECT * FROM ekpo
    APPENDING CORRESPONDING FIELDS OF TABLE pt_commandes
    WHERE ebeln EQ ekko-ebeln.
  IF sy-subrc NE 0.
    ps_facture-ident = '3'.
    ps_facture-belnr = e1edp02-belnr.
    EXIT.
  ENDIF.
  LOOP AT pt_commandes.
    MOVE: pt_commandes-ebeln TO pt_commandes-belnr.
    MOVE: pt_commandes-ebelp TO pt_commandes-zeile.
*>> RGI008 - Balance V2
    pt_commandes-netpr = pt_commandes-netpr * ( pt_commandes-bpumz / pt_commandes-bpumn ).
    MODIFY pt_commandes.
  ENDLOOP.
ENDFORM.                    " f_210_lire_commande

*&---------------------------------------------------------------------*
*&      Form  f_210_lire_bdl
*&---------------------------------------------------------------------*
*       Lire les postes de commandes appartenant au bon de livraison
*----------------------------------------------------------------------*
*      -->PT_COMMANDES  Données des postes du bon de livraison
*      -->PS_FACTURE    Ligne de la facture
*----------------------------------------------------------------------*
FORM f_210_lire_bdl TABLES pt_commandes STRUCTURE zbalance_02
                     USING ps_facture   TYPE zbalance_01.
  DATA: wl_i TYPE int4.
*  RANGES: wr_ebeln FOR ekbe-ebeln.
*  RANGES: wr_vbeln FOR likp-vbeln.
*  RANGES: wr_lifnr FOR likp-lifnr.
*
  MOVE ps_facture-verur TO ekbe-xblnr.
  SELECT DISTINCT ebeln ebelp FROM ekbe
                              INTO (ekpo-ebeln,ekpo-ebelp)
                             WHERE xblnr EQ ekbe-xblnr
                               AND vgabe EQ '1'
                               AND bwart EQ '101'.

    SELECT SINGLE * FROM ekpo
      INTO CORRESPONDING FIELDS OF pt_commandes
                   WHERE ebeln EQ ekpo-ebeln
                     AND ebelp EQ ekpo-ebelp.
    IF sy-subrc = 0.
      MOVE: ekbe-xblnr TO pt_commandes-vbeln,
            ekpo-ebeln TO pt_commandes-belnr,
            ekpo-ebelp TO pt_commandes-zeile.
      APPEND pt_commandes.
    ENDIF.
  ENDSELECT.

  IF sy-subrc NE 0.
*    MESSAGE s398(00)
    MESSAGE s533(0u)
    WITH
'Aucune entrée correspondante dans table Poste document d''achat'(w37)
 'pour Num du doc d''achat: '(w46) ekpo-ebeln.
  ENDIF.

  IF pt_commandes[] IS INITIAL.
    ps_facture-ident = '5'.
    ps_facture-belnr = e1edp02-belnr.
    EXIT.
  ENDIF.

*  wr_vbeln-sign   = 'I'.
*  wr_vbeln-option = 'EQ'.
** Si e1edp02-belnr véhicule VBELN
** MOVE ps_facture-verur TO likp-vbeln.
**  SELECT SINGLE * FROM likp WHERE vbeln EQ likp-vbeln.
**  IF sy-subrc NE 0.
**    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
**         EXPORTING
**              input  = e1edp02-belnr
**         IMPORTING
**              output = likp-vbeln.
**    SELECT SINGLE * FROM likp WHERE vbeln EQ likp-vbeln.
**    IF sy-subrc NE 0.
**      ps_facture-ident = '4'.
**      ps_facture-belnr = e1edp02-belnr.
**      EXIT.
**    ENDIF.
**  ENDIF.
**  wr_vbeln-low = likp-vbeln.
**  APPEND wr_vbeln.
*
** Si e1edp02-belnr véhicule XBLNR
*  MOVE ps_facture-verur TO likp-verur.
*  PERFORM f_170_ctrl_lifnr USING ps_facture-lifnr.
*  IF NOT ( ps_facture-lifnr IS INITIAL ).
*    wr_lifnr-sign   = 'I'.
*    wr_lifnr-option = 'EQ'.
*    wr_lifnr-low = ps_facture-lifnr.
*    APPEND wr_lifnr.
*  ENDIF.
*
*  SELECT vbeln FROM likp
*               INTO wr_vbeln-low
*              WHERE verur = likp-verur
*                AND lifnr IN wr_lifnr.
*    APPEND wr_vbeln.
*  ENDSELECT.
*
** Charger les commandes
*  IF NOT ( wr_vbeln[] IS INITIAL ).
*    wr_ebeln-sign   = 'I'.
*    wr_ebeln-option = 'EQ'.
*    SELECT * FROM lips WHERE vbeln IN wr_vbeln.
*      CLEAR pt_commandes.
*      wr_ebeln-low = lips-vgbel.
*      COLLECT wr_ebeln.
*      SELECT SINGLE * FROM ekpo
*        INTO CORRESPONDING FIELDS OF pt_commandes
*                     WHERE ebeln EQ lips-vgbel
*                       AND ebelp EQ lips-vgpos.
*      MOVE: " lips-vbeln TO pt_commandes-belnr,
*            " lips-posnr TO pt_commandes-zeile,
*            lips-vbeln TO pt_commandes-vbeln,
*            lips-vgbel TO pt_commandes-belnr,
*            lips-vgpos TO pt_commandes-zeile,
*            lips-matnr TO pt_commandes-matnr,
*            lips-arktx TO pt_commandes-txz01.
*      APPEND pt_commandes.
*    ENDSELECT.
*  ELSE.
*    sy-subrc = 4.
*  ENDIF.
*  IF sy-subrc NE 0.
*    ps_facture-ident = '4'.
*    ps_facture-belnr = e1edp02-belnr.
*    EXIT.
*  ENDIF.
*
** Contrôler s'il y a eu au moins 1 entrée de marchandise
** pour le bon de livraison
*  SELECT SINGLE * FROM ekbe WHERE ebeln IN wr_ebeln
*                              AND xblnr EQ likp-verur
*                              AND vgabe EQ '1'
*                              AND bwart EQ '101'.
*  IF sy-subrc NE 0.
*    ps_facture-ident = '5'.
*    ps_facture-belnr = e1edp02-belnr.
*    EXIT.
*  ENDIF.
ENDFORM.                    " f_210_lire_bdl

*&---------------------------------------------------------------------*
*&      Form  f_210_monoposte
*&---------------------------------------------------------------------*
*       Rapprochement monoposte
*----------------------------------------------------------------------*
*      -->PT_COMMANDES  Données de commande (ou bon de livraison)
*      -->PS_FACTURE    Ligne de la facture
*      -->P_SUIVANTE    Sélection de la branche suivante de l'algorithme
*----------------------------------------------------------------------*
FORM f_210_monoposte TABLES pt_commandes STRUCTURE zbalance_02
                     USING  ps_facture   TYPE zbalance_01
                            p_suivante   TYPE c.
  DATA: wl_i TYPE int4.

  DESCRIBE TABLE pt_commandes LINES wl_i.
  CHECK wl_i EQ 1.

  READ TABLE pt_commandes INDEX 1.
  ps_facture-belnr = pt_commandes-belnr.
  ps_facture-zeile = pt_commandes-zeile.
  ps_facture-vbeln = pt_commandes-vbeln.
  ps_facture-ident = 'X'.
ENDFORM.                    " f_210_monoposte

*&---------------------------------------------------------------------*
*&      Form  f_220_fia
*&---------------------------------------------------------------------*
*       Déterminer le rapprochement via fiche info-achat, contrat cadre
*       et article SAP
*----------------------------------------------------------------------*
*      -->PT_COMMANDES  Données de commande (ou bon de livraison)
*      -->PS_FACTURE    Ligne de la facture
*      -->P_SUIVANTE    Sélection de la branche suivante de l'algorythme
*----------------------------------------------------------------------*
FORM f_220_fia TABLES pt_commandes STRUCTURE zbalance_02
               USING  ps_facture   TYPE zbalance_01
                      p_suivante   TYPE c.
  DATA: wl_i      TYPE int4.
  DATA: wl_matnr TYPE mara-matnr.
  DATA : wl_ekpo_ebeln LIKE ekpo-ebeln.
  DATA : wl_ekpo_ebelp LIKE ekpo-ebelp.

  CHECK NOT ( ps_facture-matnr IS INITIAL ) AND
        NOT ( ps_facture-lifnr IS INITIAL ).
* PERFORM f_160_ctrl_matnr USING ps_facture-matnr.
  PERFORM f_170_ctrl_lifnr USING ps_facture-lifnr.
  CHECK NOT ( ps_facture-lifnr IS INITIAL ).
*       AND NOT ( ps_facture-matnr IS INITIAL ).

* Selection sur les contrats cadres  - TBR 31082004
  CLEAR wl_i.
  SELECT COUNT(*) FROM ekpo INTO wl_i
    WHERE infnr = ps_facture-matnr
      AND ebeln = ps_facture-belnr.
  IF wl_i EQ 0.
    SHIFT ps_facture-matnr LEFT DELETING LEADING space.
    SELECT COUNT(*) FROM ekpo INTO wl_i
          WHERE infnr = ps_facture-matnr
          AND ebeln = ps_facture-belnr.
  ENDIF.
  IF wl_i EQ 1.
    SELECT  matnr FROM ekpo INTO mara-matnr
      WHERE infnr = ps_facture-matnr
      AND ebeln = ps_facture-belnr.
    ENDSELECT.
    IF sy-subrc EQ 0.
      CLEAR wl_i.
      IF mara-matnr IS INITIAL.
        ADD 1 TO wl_i.
      ELSE.
        LOOP AT pt_commandes WHERE matnr = mara-matnr.
          ADD 1 TO wl_i.
        ENDLOOP.
      ENDIF.
      CHECK wl_i EQ 1.
      ps_facture-belnr = pt_commandes-belnr.
      ps_facture-zeile = pt_commandes-zeile.
      ps_facture-vbeln = pt_commandes-vbeln.
      ps_facture-ident = 'X'.
    ENDIF.
  ENDIF.

  CHECK ps_facture-ident NE 'X'.

*   Selection sur article fournisseur (fiche info-achat) IDNLF
  CLEAR wl_i.
  SELECT COUNT(*) FROM eina INTO wl_i
    WHERE idnlf = ps_facture-matnr
      AND lifnr = ps_facture-lifnr.
  IF wl_i EQ 0.
    SHIFT ps_facture-matnr LEFT DELETING LEADING space.
    SELECT COUNT(*) FROM eina INTO wl_i
      WHERE idnlf = ps_facture-matnr
        AND lifnr = ps_facture-lifnr.
  ENDIF.
  IF wl_i EQ 1.

    SELECT  matnr FROM eina INTO mara-matnr "ex SELECT SINGLE
        WHERE idnlf = ps_facture-matnr
          AND lifnr = ps_facture-lifnr.
    ENDSELECT.
    IF sy-subrc EQ 0.
      CLEAR wl_i.
      IF mara-matnr IS INITIAL.
        ADD 1 TO wl_i.
      ELSE.
        LOOP AT pt_commandes WHERE matnr = mara-matnr.
          ADD 1 TO wl_i.
        ENDLOOP.
      ENDIF.
      CHECK wl_i EQ 1.

      ps_facture-belnr = pt_commandes-belnr.
      ps_facture-zeile = pt_commandes-zeile.
      ps_facture-vbeln = pt_commandes-vbeln.
      ps_facture-ident = 'X'.
    ENDIF.

  ENDIF.

  CHECK ps_facture-ident NE 'X'.

*   Selection sur article fournisseur (fiche info-achat) MATNR
  CLEAR wl_i.
  SELECT COUNT(*) FROM eina INTO wl_i
    WHERE matnr = ps_facture-matnr
      AND lifnr = ps_facture-lifnr.
  IF wl_i EQ 0.
    SHIFT ps_facture-matnr LEFT DELETING LEADING space.
    SELECT COUNT(*) FROM eina INTO wl_i
      WHERE matnr = ps_facture-matnr
        AND lifnr = ps_facture-lifnr.
  ENDIF.
  IF wl_i EQ 1.

    SELECT  matnr FROM eina INTO mara-matnr "ex SELECT SINGLE
        WHERE matnr = ps_facture-matnr
          AND lifnr = ps_facture-lifnr.
    ENDSELECT.
    IF sy-subrc EQ 0.
      CLEAR wl_i.
      IF mara-matnr IS INITIAL.
        ADD 1 TO wl_i.
      ELSE.
        LOOP AT pt_commandes WHERE matnr = mara-matnr.
          ADD 1 TO wl_i.
        ENDLOOP.
      ENDIF.
      CHECK wl_i EQ 1.

      ps_facture-belnr = pt_commandes-belnr.
      ps_facture-zeile = pt_commandes-zeile.
      ps_facture-vbeln = pt_commandes-vbeln.
      ps_facture-ident = 'X'.
    ENDIF.

  ENDIF.

  CHECK ps_facture-ident NE 'X'.


* Selection sur article fournisseur (commande)
  CLEAR wl_i.
  SELECT COUNT(*) FROM ekpo INTO wl_i
                 WHERE idnlf = ps_facture-matnr
                   AND ebeln = ps_facture-belnr.
  IF wl_i EQ 0.
    SHIFT wl_matnr LEFT DELETING LEADING space.
    SELECT COUNT(*) FROM ekpo INTO wl_i
                   WHERE matnr = ps_facture-matnr
                     AND ebeln = ps_facture-belnr.
  ENDIF.
  IF wl_i EQ 1.
    SELECT  ebeln ebelp matnr FROM ekpo
    INTO (wl_ekpo_ebeln, wl_ekpo_ebelp, mara-matnr)
      WHERE idnlf = ps_facture-matnr
      AND ebeln = ps_facture-belnr.
    ENDSELECT.
    IF sy-subrc EQ 0.
      READ TABLE pt_commandes WITH KEY belnr = wl_ekpo_ebeln
                                       zeile = wl_ekpo_ebelp.
      IF sy-subrc EQ 0.
        ps_facture-belnr = pt_commandes-belnr.
        ps_facture-zeile = pt_commandes-zeile.
        ps_facture-vbeln = pt_commandes-vbeln.
        ps_facture-ident = 'X'.
      ENDIF.
    ENDIF.
  ENDIF.


  CHECK ps_facture-ident NE 'X'.

* Selection sur article SAP
  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
    EXPORTING
      input  = ps_facture-matnr
    IMPORTING
      output = wl_matnr.

  CLEAR wl_i.
  SELECT COUNT(*) FROM ekpo INTO wl_i
                 WHERE matnr = wl_matnr
                   AND ebeln = ps_facture-belnr.
  IF wl_i EQ 0.
    SHIFT wl_matnr LEFT DELETING LEADING space.
    SELECT COUNT(*) FROM ekpo INTO wl_i
                   WHERE matnr = wl_matnr
                     AND ebeln = ps_facture-belnr.
  ENDIF.
  IF wl_i EQ 1.
    SELECT  matnr FROM ekpo INTO mara-matnr
      WHERE matnr = wl_matnr
      AND ebeln = ps_facture-belnr.
    ENDSELECT.
    IF sy-subrc EQ 0.
      CLEAR wl_i.
      IF mara-matnr IS INITIAL.
        ADD 1 TO wl_i.
      ELSE.
        LOOP AT pt_commandes WHERE matnr = mara-matnr.
          ADD 1 TO wl_i.
        ENDLOOP.
      ENDIF.
      CHECK wl_i EQ 1.
      ps_facture-belnr = pt_commandes-belnr.
      ps_facture-zeile = pt_commandes-zeile.
      ps_facture-vbeln = pt_commandes-vbeln.
      ps_facture-ident = 'X'.
    ENDIF.

  ENDIF.



ENDFORM.                                                    " f_220_fia

*&---------------------------------------------------------------------*
*&      Form  f_230_couple_pq
*&---------------------------------------------------------------------*
*       Déterminer le rapprochement via couple prix/quantité
*----------------------------------------------------------------------*
*      -->PT_COMMANDES  Données de commande (ou bon de livraison)
*      -->PS_FACTURE    Ligne de la facture
*      -->P_SUIVANTE    Sélection de la branche suivante de l'algorythme
*                       = 5 : étape suivante: vérifier montant facturé
*----------------------------------------------------------------------*
FORM f_230_couple_pq TABLES  pt_commandes STRUCTURE zbalance_02
                      USING  ps_facture   TYPE zbalance_01
                             p_suivante   TYPE c.

  DATA:  wl_netpr LIKE ekpo-netwr,
         wl_belnr LIKE zbalance_01-belnr,
         wl_vbeln LIKE zbalance_01-vbeln,
         wl_zeile LIKE zbalance_01-zeile,
         wl_menge LIKE zbalance_02-menge.
  DATA:  wt_temp  LIKE pt_commandes OCCURS 0.
  DATA:  wl_i     TYPE int4.

  DATA : w_dummy TYPE icon_d.

* Calculer le montant réceptionné restant à facturer par poste
* LE CHAMPS VGABE
* 1 EM
* 2 facture
* 9 BAT
  LOOP AT pt_commandes WHERE belnr   EQ ps_facture-belnr AND
                             em_calc IS INITIAL.
    PERFORM f_180_reste_fact USING
      pt_commandes-ebeln
      pt_commandes-ebelp
      pt_commandes-montant
      pt_commandes-quantite
      w_dummy.

    pt_commandes-em_calc = 'X'.
    MODIFY pt_commandes.
  ENDLOOP.

* Contrôle du montant attendu = somme du réceptionné
* on traivaille toujours sur une seule poste de la facture
  CLEAR wl_i.
  LOOP AT pt_commandes WHERE belnr EQ ps_facture-belnr.
    IF pt_commandes-montant = ps_facture-betrg.
      wl_belnr = pt_commandes-belnr.
      wl_vbeln = pt_commandes-vbeln.
      wl_zeile = pt_commandes-zeile.
      wl_menge = pt_commandes-quantite.
      ADD 1 TO wl_i.
    ENDIF.
  ENDLOOP.

  IF wl_i EQ 1.
    IF ps_facture-menge = wl_menge.
      ps_facture-belnr = wl_belnr.
      ps_facture-vbeln = wl_vbeln.
      ps_facture-zeile = wl_zeile.
      ps_facture-ident = 'X'.
    ELSE.
      ps_facture-ident = '2'.
    ENDIF.
    EXIT.
  ENDIF.

* Contrôle du couple prix unitaire/quantité attendu
  IF NOT ( ps_facture-netpr IS INITIAL ).
    CLEAR wl_i.
    LOOP AT pt_commandes WHERE belnr EQ ps_facture-belnr.
      IF pt_commandes-netpr    = ps_facture-netpr AND
         pt_commandes-quantite = ps_facture-menge.
        wl_belnr = pt_commandes-belnr.
        wl_vbeln = pt_commandes-vbeln.
        wl_zeile = pt_commandes-zeile.
        ADD 1 TO wl_i.
      ENDIF.
    ENDLOOP.

    IF wl_i EQ 1.
      ps_facture-belnr = wl_belnr.
      ps_facture-vbeln = wl_vbeln.
      ps_facture-zeile = wl_zeile.
      ps_facture-ident = 'X'.
      EXIT.
    ENDIF.
  ENDIF.

* Contrôle du couple prix unitaire calc./quantité attendue
  CLEAR wl_i.
  LOOP AT pt_commandes WHERE belnr EQ ps_facture-belnr.
    IF NOT ( ps_facture-menge IS INITIAL ).
      wl_netpr = ps_facture-betrg / ps_facture-menge.
    ELSE.
      CLEAR wl_netpr.
    ENDIF.
    IF pt_commandes-netpr    = wl_netpr AND
       pt_commandes-quantite = ps_facture-menge.
      wl_belnr = pt_commandes-belnr.
      wl_vbeln = pt_commandes-vbeln.
      wl_zeile = pt_commandes-zeile.
      ADD 1 TO wl_i.
    ENDIF.
  ENDLOOP.

  IF wl_i EQ 1.
    ps_facture-belnr = wl_belnr.
    ps_facture-vbeln = wl_vbeln.
    ps_facture-zeile = wl_zeile.
    ps_facture-ident = 'X'.
  ENDIF.

* Contrôle du couple prix unitaire/quantité attendu
* Gestion des inversions prix/qté

* Les postes identifiés par cette méthode sont repérés par la lettre "Z"
* demanière à pouvoir corriger l'idoc par la suite

  SELECT SINGLE * FROM  zbal_params
         WHERE  param  = 'INVQTEPX'.

  IF zbal_params-value = 'X'.
    IF NOT ( ps_facture-netpr IS INITIAL ).
      CLEAR wl_i.
      LOOP AT pt_commandes WHERE belnr EQ ps_facture-belnr.
        IF pt_commandes-netpr    = ps_facture-menge AND
           pt_commandes-quantite = ps_facture-netpr.
          wl_belnr = pt_commandes-belnr.
          wl_vbeln = pt_commandes-vbeln.
          wl_zeile = pt_commandes-zeile.
          ADD 1 TO wl_i.
        ENDIF.
      ENDLOOP.

      IF wl_i EQ 1.
        ps_facture-belnr = wl_belnr.
        ps_facture-vbeln = wl_vbeln.
        ps_facture-zeile = wl_zeile.
        ps_facture-ident = 'Z'.
        EXIT.
      ENDIF.
    ENDIF.
  ENDIF.
ENDFORM.                                                    " f_200_fia


*&---------------------------------------------------------------------*
*&      Form  f_240_libelle
*&---------------------------------------------------------------------*
*       Déterminer le rapprochement via libellés
*----------------------------------------------------------------------*
*      -->PT_COMMANDES  Données de commande (ou bon de livraison)
*      -->PS_FACTURE    Ligne de la facture
*      -->P_SUIVANTE    Sélection de la branche suivante de l'algorythme
*----------------------------------------------------------------------*

FORM f_240_libelle   TABLES  pt_commandes STRUCTURE zbalance_02
                             pt_result    TYPE      tt_result
                      USING  ps_facture   TYPE      zbalance_01
                             p_suivante   TYPE      c.

  DATA:  wt_cntrl       TYPE tt_result WITH HEADER LINE.

  READ TABLE pt_result WITH KEY belnr = ps_facture-belnr.
  IF sy-subrc NE 0.
*   Aucune libellée n'a été encore comparée
*   D'abord on compare les libellées des commandes. Il ne faut pas avoir
*   plus grande probabilité que 80% pour les commandes
    LOOP AT pt_commandes.
      CHECK ps_facture-belnr EQ pt_commandes-belnr OR
            ps_facture-belnr IS INITIAL.
      PERFORM f_241_text_comparaison
        TABLES pt_commandes
               wt_cntrl
         USING ''
               pt_commandes-txz01  " Le texte du poste
               pt_commandes-belnr  " Seulement pour la commande
*              ''                  " Pas de bon de livraison
               pt_commandes-zeile. " Pas pour soit-même.
    ENDLOOP.

    SORT wt_cntrl BY pourcent DESCENDING.
    READ TABLE wt_cntrl INDEX 1.
    IF sy-subrc EQ 0 AND wt_cntrl-pourcent > 80.
*     Les libellées sont trop proches: on génère une entrée '0%'
      pt_result-belnr = ps_facture-belnr.
      APPEND pt_result.
      EXIT.
    ENDIF.
    CLEAR wt_cntrl[].
  ELSE.
* Contrôler l'entrée 0% si la facture est déjà traitée une fois
    CHECK NOT ( pt_result-posex IS INITIAL ).
  ENDIF.

* Effectuer la comparaison
  PERFORM f_241_text_comparaison
    TABLES pt_commandes
           wt_cntrl
     USING ps_facture-posex
           ps_facture-ktext
           ps_facture-belnr " Seulemrnt pour la commande
*          ps_facture-vbeln " Bon de livraison si...
           ''.              " Avec tous les commandes
  SORT wt_cntrl BY pourcent DESCENDING.
  READ TABLE wt_cntrl INDEX 1.
  APPEND wt_cntrl TO pt_result.

  IF wt_cntrl-pourcent GT 50.
    MOVE wt_cntrl-belnr TO ps_facture-belnr.
    MOVE wt_cntrl-vbeln TO ps_facture-vbeln.
    MOVE wt_cntrl-zeile TO ps_facture-zeile.
    MOVE 'X' TO ps_facture-ident.
  ENDIF.

*  READ TABLE pt_result
*    INTO ws_result WITH KEY belnr = ps_facture-belnr
*                            posex = ps_facture-posex
*    BINARY SEARCH.
*  IF sy-subrc NE 0.
*    PERFORM f_241_text_comparaison
*      TABLES pt_commandes pt_result
*       USING ps_facture-posex
*             ps_facture-ktext
*             ps_facture-belnr
*             ''.
*    SORT pt_result BY belnr posex pourcent DESCENDING.
*    READ TABLE pt_result
*      INTO ws_result WITH KEY belnr = ps_facture-belnr
*                              posex = ps_facture-posex
*      BINARY SEARCH.
*  ENDIF.
*
*  IF sy-subrc EQ 0 AND ws_result-pourcent GT 50.
*    MOVE ws_result-belnr TO ps_facture-belnr.
*    MOVE ws_result-zeile TO ps_facture-zeile.
*    MOVE 'X' TO ps_facture-ident.
*  ENDIF.

ENDFORM.                    " f_240_libelle


*&---------------------------------------------------------------------*
*&      Form  f_241_text_comparaison
*&---------------------------------------------------------------------*
*       Comparer des texts
*----------------------------------------------------------------------*
*      -->P_TEXT        on compare ce text par rapport
*      -->PT_BASE_COMP  à une liste de texts
*      -->P_BELNR       en limitant la recherche sur
*      -->P_POSEX       juste pour sauvegarder la clé complète
*      -->P_ZEILE       si P_ZEILE est vide: pas d'effet
*                       sinon, on exclut cette clé
*      -->PT_PROBAS     le résultat...
*----------------------------------------------------------------------*
FORM f_241_text_comparaison TABLES pt_base_comp   STRUCTURE zbalance_02
                                   pt_probas      TYPE      tt_result
                             USING value(p_posex) TYPE zbalance_01-posex
                                   value(p_text)
                                   value(p_belnr) TYPE zbalance_02-belnr
*                                  value(p_vbeln) TYPE zbalance_02-vbeln
                                   value(p_excl)  TYPE zbalance_02-zeile
.


  DATA:  wt_reschar     TYPE tt_reschar WITH HEADER LINE.
  DATA:  ws_result      TYPE t_result.
* WT_FOR représente les mots de 3 lettres et plus contenus dans le
*                                libellé du poste la facture
  DATA:  BEGIN OF wt_for OCCURS 0,
           word(100) TYPE c,
         END OF wt_for.
* WT_IN  représente les mots de 3 lettres et plus contenus dans le
*                                libellé du poste de la commande
  DATA:  BEGIN OF wt_in OCCURS 0,
           word(100) TYPE c,
         END OF wt_in.

  DATA:  w_len TYPE i.
  DATA:  w_concorde(5) TYPE p DECIMALS 2.
  DATA:  w_nbre_mot TYPE i,
         w_nbre_mot_in TYPE i,
         w_nbre_mot_for TYPE i.

  CLEAR: wt_for[], wt_in[].

  PERFORM f_150_suppression_caracteres USING p_text.
  SPLIT p_text AT ' ' INTO TABLE wt_for.
  LOOP AT wt_for.
    w_len = STRLEN( wt_for-word ).
    IF w_len LE 2.
      DELETE wt_for.
    ENDIF.
  ENDLOOP.

* recherche du libellé d'un poste de la commande le plus proche
  LOOP AT pt_base_comp.
    CHECK p_belnr EQ pt_base_comp-belnr OR
          p_belnr IS INITIAL.
    CHECK p_excl IS INITIAL OR
          pt_base_comp-zeile NE p_excl.
    PERFORM f_150_suppression_caracteres USING pt_base_comp-txz01.
    SPLIT pt_base_comp-txz01 AT ' ' INTO TABLE wt_in.
    LOOP AT wt_in.
      w_len = STRLEN( wt_in-word ).
      IF w_len LE 2.
        DELETE wt_in.
      ENDIF.
    ENDLOOP.

    CLEAR w_concorde.
    LOOP AT wt_for.
      READ TABLE wt_in WITH KEY word = wt_for-word.
      IF sy-subrc IS INITIAL.
        ADD 1 TO w_concorde.
      ELSE.
*     recherche par caractère
*     principe : calculer la probabilité de "proximité"
*     entre le mot du libellé du poste de la facture
*     et tous les mots du libellé du poste de la commande
        LOOP AT wt_in.
          PERFORM f_140_recherche_caractere TABLES wt_reschar
                                             USING wt_in-word
                                                   wt_for-word.
        ENDLOOP.
*     puis ne reprendre que la plus grande probalité
        SORT wt_reschar BY proba DESCENDING.
        READ TABLE wt_reschar INDEX 1.
        ADD wt_reschar-proba TO w_concorde.
        REFRESH wt_reschar.
        CLEAR wt_reschar.
      ENDIF.
    ENDLOOP.

    DESCRIBE TABLE wt_for LINES w_nbre_mot_for.
    DESCRIBE TABLE wt_in LINES w_nbre_mot_in.

*     calcul de la proba est  fonction de la chaine la plus longue
    IF w_nbre_mot_for GT w_nbre_mot_in.
      w_nbre_mot = w_nbre_mot_for.
    ELSE.
      w_nbre_mot = w_nbre_mot_in.
    ENDIF.
    ws_result-posex = p_posex.
    ws_result-pourcent = w_concorde / w_nbre_mot * 100.
    ws_result-belnr = pt_base_comp-belnr.
    ws_result-vbeln = pt_base_comp-vbeln.
    ws_result-zeile = pt_base_comp-zeile.
    APPEND ws_result TO pt_probas.
  ENDLOOP.

ENDFORM.                    " f_241_text_comparaison

*&---------------------------------------------------------------------*
*&      Form  f_250_libelle_ok
*&---------------------------------------------------------------------*
*       Contrôler les libellés de la facture
*----------------------------------------------------------------------*
FORM f_250_libelle_ok TABLES   pt_factures STRUCTURE zbalance_01
                               pt_libs     TYPE      tt_libs
                      USING    ps_facture  TYPE      zbalance_01
                               p_result    TYPE      flag.

  DATA: ws_libs   TYPE t_libs.
  DATA: ws_bc     TYPE zbalance_02.
  DATA: wt_cntrl  TYPE tt_result WITH HEADER LINE.
  DATA: w_idx     TYPE sy-tabix.

  IF pt_libs[] IS INITIAL.
    LOOP AT pt_factures.
      READ TABLE pt_libs WITH KEY belnr = pt_factures-belnr
      INTO ws_libs.
      w_idx = sy-tabix.
      IF sy-subrc NE 0.
        CLEAR ws_libs.
        ws_libs-belnr = pt_factures-belnr.
        ws_libs-ktext = pt_factures-ktext.
        APPEND ws_libs TO pt_libs.
      ELSE.
        ws_bc-belnr = pt_factures-belnr.
        ws_bc-txz01 = pt_factures-ktext.
        APPEND ws_bc TO ws_libs-t_bc.
        MODIFY pt_libs FROM ws_libs INDEX w_idx.
      ENDIF.
    ENDLOOP.
    LOOP AT pt_libs INTO ws_libs.
      CHECK NOT ( ws_libs-t_bc[] IS INITIAL ).
      CLEAR: wt_cntrl[], wt_cntrl.
      PERFORM f_241_text_comparaison
        TABLES ws_libs-t_bc
               wt_cntrl
         USING ''
               ws_libs-ktext       " Le texte du poste
               ws_libs-belnr       " Seulement pour la commande
*              ''                  " Pas de bon de livraison
               ''.                 " Pas de poste
      SORT wt_cntrl BY pourcent DESCENDING.
      READ TABLE wt_cntrl INDEX 1.
      IF wt_cntrl-pourcent GT 80.
*       Les libellées sont trop proches: on fait pas de comparaison
      ELSE.
        ws_libs-ok = 'X'.
        MODIFY pt_libs FROM ws_libs.
      ENDIF.
    ENDLOOP.
  ENDIF.

  READ TABLE pt_libs INTO ws_libs WITH KEY belnr = ps_facture-belnr.
  CHECK sy-subrc EQ 0.
  p_result = ws_libs-ok.
ENDFORM.                    " f_250_libelle_ok

*&---------------------------------------------------------------------*
*&      Form  f_250_libelle_ok2
*&---------------------------------------------------------------------*
*       Contrôler les libellés de la facture
*----------------------------------------------------------------------*
FORM f_250_libelle_ok2 TABLES   pt_factures STRUCTURE zbalance_01
                                pt_libs     TYPE      tt_libs
                       USING    ps_facture  TYPE      zbalance_01
                                p_result    TYPE      flag.

  DATA: ws_libs   TYPE t_libs.
  DATA: ws_bc     TYPE zbalance_02.
  DATA: wt_cntrl  TYPE tt_result WITH HEADER LINE.
  DATA: w_idx     TYPE sy-tabix.

  IF pt_libs[] IS INITIAL.
    LOOP AT pt_factures.
      IF sy-tabix = 1.
        CLEAR ws_libs.
        ws_libs-belnr = pt_factures-belnr.
        ws_libs-ktext = pt_factures-ktext.
      ELSE.
        ws_bc-belnr = pt_factures-belnr.
        ws_bc-txz01 = pt_factures-ktext.
        APPEND ws_bc TO ws_libs-t_bc.
      ENDIF.
    ENDLOOP.
    APPEND ws_libs TO pt_libs.
    CHECK NOT ( ws_libs-t_bc[] IS INITIAL ).
    CLEAR: wt_cntrl[], wt_cntrl.
    PERFORM f_241_text_comparaison
      TABLES ws_libs-t_bc
             wt_cntrl
       USING ''
             ws_libs-ktext       " Le texte du poste
             ws_libs-belnr       " Seulement pour la commande
*            ''                  " Pas de bon de livraison
             ''.                 " Pas de poste
    SORT wt_cntrl BY pourcent DESCENDING.
    READ TABLE wt_cntrl INDEX 1.
    IF wt_cntrl-pourcent GT 80.
*       Les libellées sont trop proches: on fait pas de comparaison
      EXIT.
    ELSE.
      ws_libs-ok = 'X'.
      MODIFY pt_libs FROM ws_libs INDEX 1.
    ENDIF.
  ENDIF.

  READ TABLE pt_libs INTO ws_libs INDEX 1.
  CHECK sy-subrc EQ 0.
  p_result = ws_libs-ok.
ENDFORM.                    " f_250_libelle_ok2
