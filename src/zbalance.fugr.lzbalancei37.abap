*----------------------------------------------------------------------*
*   INCLUDE LZBALANCEI37                                               *
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*& Modifications                                                       *
*&---------------------------------------------------------------------*
*& Project           : SGS-SAP Upgrade ECC5_TO_ECC6                    *
*& Author SGID       : T3560272                                        *
*& Author name       : Tharani Kanagarajan                             *
*& Modifications TAG : TKA001                                          *
*& Date              : 10.11.2010                                      *
*& QB DevT           : 6692                                            *
*& Panaya Task       : 491                                             *
*& Description       : UPG_ECC5_TO_ECC6 Code Correction                *
*&---------------------------------------------------------------------*
************************************************************************
* ROUTINES                                                             *
************************************************************************

*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_qte DEFINITION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_qte DEFINITION.

  PUBLIC SECTION.

*** METHOD handle_double_click.
    METHODS:
    handle_double_click
        FOR EVENT double_click OF cl_gui_alv_grid
            IMPORTING e_row e_column.
*** METHOD handle_hotspot_click.
    METHODS:
        handle_hotspot_click
            FOR EVENT hotspot_click OF cl_gui_alv_grid
                IMPORTING e_row_id e_column_id.

*** METHOD set_select_cell.
*    METHODS:
*        SET_SELECTED_CELL
*            FOR EVENT USER_COMMAND OF cl_gui_alv_grid
*                IMPORTING IT_CELLS.


  PRIVATE SECTION.

ENDCLASS.                    "cl_event_receiver_qte DEFINITION

*---------------------------------------------------------------------*
*       CLASS cl_event_receiver_qte IMPLEMENTATION
*---------------------------------------------------------------------*
*       ........                                                      *
*---------------------------------------------------------------------*
CLASS cl_event_receiver_qte IMPLEMENTATION.

*** METHOD handle_double_click.
  METHOD handle_double_click.

    READ TABLE wt_alv_qte INDEX e_row-index INTO ls_alv_qte.
*
    CASE e_column-fieldname.
      WHEN 'EBELN'.

        CALL FUNCTION 'MIGO_DIALOG'
         EXPORTING
           i_action                  = 'A01'
           i_refdoc                  = 'R01'
           i_ebeln                   = ls_alv_qte-ebeln
           i_mjahr                   = ls_alv_qte-gjahr
* EXCEPTIONS
*   ILLEGAL_COMBINATION       = 1
*   OTHERS                    = 2
*
  .
    ENDCASE.

  ENDMETHOD.                           "handle_double_click

*** METHOD handle_hotspot_click.
  METHOD handle_hotspot_click.

*    READ TABLE t_alv_p2001 INDEX e_row_id-index INTO ls_p2001.
*
*    CASE e_column_id-fieldname.
**      WHEN 'PERNR' or 'NACHN' or 'VORNA' or 'BEGDA' or 'ENDDA' or
**'SUBTY' or 'ATEXT' or 'IMPRI'.
*
*
*
*
*    ENDCASE.

  ENDMETHOD.                           "handle_hotspot_click

ENDCLASS.                    "cl_event_receiver_qte IMPLEMENTATION


*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9201  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9201 INPUT.

*data: t_row type table of lvc_s_row.
*data: t_roid type table of lvc_s_roid.
  DATA:ls_row_qte TYPE lvc_s_row.
  DATA:ls_roid_qte TYPE lvc_s_roid.

  CASE w_9201_okcode.

    WHEN 'DISPLAY'.
* Affichage facture
      SET PARAMETER ID 'RBN' FIELD w_belnr.
      SET PARAMETER ID 'GJR' FIELD w_gjahr.
      CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.

    WHEN 'POSTVAL'. "Postes valides
      CLEAR w_answer.
* Begin of UPG_ECC5_TO_ECC6 DEL TKA001
*      CALL FUNCTION 'POPUP_TO_CONFIRM_WITH_MESSAGE'
*        EXPORTING
**       DEFAULTOPTION        = 'Y'
*          diagnosetext1        = 'Déblocage facture'(t38)
**       DIAGNOSETEXT2        = ' '
**       DIAGNOSETEXT3        = ' '
*       textline1            = 'Tous les postes vont être débloqués'(t41)
**       TEXTLINE2            = ' '
*          titel                = 'Déblocage facture'(t38)
**       START_COLUMN         = 25
**       START_ROW            = 6
**       CANCEL_DISPLAY       = 'X'
*       IMPORTING
*         answer               = w_answer
                .
* End of   UPG_ECC5_TO_ECC6 DEL TKA001

* Begin of UPG_ECC5_TO_ECC6 INS TKA001
      CALL FUNCTION 'POPUP_TO_CONFIRM'
        EXPORTING
         TITLEBAR                    = 'Déblocage facture'(t38)
         DIAGNOSE_OBJECT             = 'Déblocage facture'(t38)
          TEXT_QUESTION               = 'Tous les postes vont être débloqués'(t41)
       IMPORTING
         ANSWER                      = w_answer
       EXCEPTIONS
         TEXT_NOT_FOUND              = 1
         OTHERS                      = 2
                .
* End of   UPG_ECC5_TO_ECC6 INS TKA001

      IF w_answer = 'J'.
* Récupération des lignes sélectionnées
        REFRESH: wt_row_qte, wt_roid_qte.
        CALL METHOD o_qte_grid->get_selected_rows
          IMPORTING
            et_index_rows = wt_row_qte
            et_row_no     = wt_roid_qte.

        t_item_qte[] = wt_alv_qte[].
        LOOP AT wt_row_qte INTO ls_row_qte.
          READ TABLE t_item_qte INDEX ls_row_qte-index.
          t_item_qte-sel = 'X'.
          MODIFY t_item_qte INDEX ls_row_qte-index.
        ENDLOOP.
        w_decision = 'POSTVAL'.
        SET SCREEN 0.LEAVE SCREEN .
      ELSE.
        EXIT.
      ENDIF.

    WHEN 'ATTAVOIR'. "Attente avoir
      w_decision = 'ATTAVOIR'.
      SET SCREEN 0.LEAVE SCREEN .

    WHEN 'RETCOMPTA'. "Retour litige à la compta
      w_decision = 'RETCOMPTA'.
      SET SCREEN 0.LEAVE SCREEN .

*----------------------------------------------------------------------*
* ITGGH   16.04.2007      Display image
    WHEN 'IMGF'.       "Display image
*     get idoc number
      SELECT docnum credat FROM edids
        INTO CORRESPONDING FIELDS OF TABLE t_idoc
        WHERE stapa1 = w_belnr
        AND   status = '53'.
      IF sy-subrc = 0.
        SORT t_idoc BY credat DESCENDING.
        READ TABLE t_idoc INTO w_idoc INDEX 1.

*       display image
        CALL FUNCTION 'ZBALANCE_IMAGE_DISPLAY'
          EXPORTING
            docnum                   = w_idoc-docnum
          EXCEPTIONS
            error_archiv             = 1
            error_communicationtable = 2
            error_kernel             = 3
            balance_config           = 4
            OTHERS                   = 5.
      ENDIF.
      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.
*----------------------------------------------------------------------*

    WHEN 'BACK' OR 'CANCEL'.
      SET SCREEN 0.LEAVE SCREEN .
*
  ENDCASE.
*

ENDMODULE.                 " USER_COMMAND_9201  INPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_4200  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9201 OUTPUT.

  CASE w_listmode.
    WHEN '1'.  " Affichage
      SET PF-STATUS 'MAIN_9200'.
    WHEN '2'.  " Validation acheteur
      SET PF-STATUS 'MAIN_9110'.
    WHEN '3'.  " Validation compta
      SET PF-STATUS 'MAIN_9120'.
  ENDCASE.
  SET TITLEBAR 'TITRE_MM_DNP_9201'.

* create objects
  IF o_qte_container IS INITIAL.
    CREATE OBJECT o_qte_container
      EXPORTING container_name = 'ALV_QTE'.

    CREATE OBJECT o_qte_grid
      EXPORTING
        i_parent = o_qte_container.

    PERFORM load_data_qte.
  ENDIF.



ENDMODULE.                 " STATUS_9201  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  exit_9201  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE exit_9201 INPUT.

  CASE w_9201_okcode.
    WHEN 'EXIT'.
      SET SCREEN 0.
      LEAVE SCREEN .
  ENDCASE.

ENDMODULE.                 " exit_9201  INPUT

*&---------------------------------------------------------------------*
*&      Form  load_data_qte
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM load_data_qte.
  DATA : ws_variante_qte TYPE disvariant.
  DATA : ws_layout_qte   TYPE lvc_s_layo.
  DATA : ws_toolbar_qte TYPE ui_functions.
  DATA : w_save.

  SORT t_item_qte DESCENDING BY belnr gjahr buzei.

  PERFORM sort_params_qte.

  PERFORM fieldcatalog_params_qte.

  PERFORM toolbar_params_qte CHANGING ws_toolbar_qte.

  ws_layout_qte-edit = 'X'.
* ws_layout_qte-SEL_MODE ='X'.
* ws_layout_qte-weblook = 'X'.
  ws_layout_qte-zebra = 'X'.
  ws_layout_qte-cwidth_opt = 'X'.
  ws_layout_qte-grid_title = 'Gestion litige facture (écart prix)'(t37).

  wt_alv_qte[] = t_item_qte[].
  ws_variante_qte = sy-uname.
  ws_variante_qte = 'ZBALANCE_LISTPOSTBLO_QTE'.
  w_save = 'X'.

  CALL METHOD o_qte_grid->set_table_for_first_display
    EXPORTING
      i_structure_name     = 'tp_item_qte'
      is_variant           = ws_variante_qte
      is_layout            = ws_layout_qte
      i_save               = w_save
      it_toolbar_excluding = ws_toolbar_qte
    CHANGING
      it_fieldcatalog      = wt_catalog_qte
      it_sort              = wt_sort_qte
      it_outtab            = wt_alv_qte.


  CREATE OBJECT event_receiver_qte.
  SET HANDLER event_receiver_qte->handle_double_click FOR o_qte_grid.
  SET HANDLER event_receiver_qte->handle_hotspot_click FOR o_qte_grid.
*  SET HANDLER event_receiver->set_select_cell FOR go_grid.


ENDFORM.                    " load_data_qte
*&---------------------------------------------------------------------*
*&      Form  sort_params_qte
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM sort_params_qte.

  DATA: wa_sort_qte TYPE lvc_s_sort.

*  wa_sort_qte-fieldname = 'BELNR'.
*  wa_sort_qte-up = 'X'.
*  wa_sort_qte-group = '01'.
*  APPEND wa_sort_qte TO wt_sort_qte.
*
*  wa_sort_qte-fieldname = 'GJAHR'.
*  wa_sort_qte-group = '01'.
*  APPEND wa_sort_qte TO wt_sort_prix.

  wa_sort_qte-fieldname = 'BUZEI'.
  wa_sort_qte-group = '01'.
  APPEND wa_sort_qte TO wt_sort_qte.


ENDFORM.                    " sort_params_qte
*&---------------------------------------------------------------------*
*&      Form  fieldcatalog_params_qte
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fieldcatalog_params_qte.

  DATA: wa_fieldcatalog_qte TYPE lvc_s_fcat.

*  wa_fieldcatalog-fieldname = 'SEL'.
*  wa_fieldcatalog-ref_table = 'SYST'.
*  wa_fieldcatalog-ref_field = 'SUBRC'.
*  wa_fieldcatalog-checkbox   = 'X'.
**  wa_fieldcatalog-edit   = 'X'.
*  wa_fieldcatalog-coltext   = 'Impression'.
*  APPEND wa_fieldcatalog TO t_catalog.
*  CLEAR wa_fieldcatalog.


*  wa_fieldcatalog_qte-fieldname = 'BELNR'.
*  wa_fieldcatalog_qte-ref_table = 'RBKP'.
*  wa_fieldcatalog_qte-ref_field = 'BELNR'.
*  wa_fieldcatalog_qte-hotspot   = 'X'.
*  wa_fieldcatalog_qte-coltext   = 'N° facture'.
**  wa_fieldcatalog_qte-edit = 'X'.
*  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
*  CLEAR wa_fieldcatalog_qte.
*
*  wa_fieldcatalog_qte-fieldname = 'GJAHR'.
*  wa_fieldcatalog_qte-ref_table = 'RBKP'.
*  wa_fieldcatalog_qte-ref_field = 'GJAHR'.
*  wa_fieldcatalog_qte-hotspot   = 'X'.
*  wa_fieldcatalog_qte-coltext   = 'Exercice'.
*  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
*  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'BUZEI'.
  wa_fieldcatalog_qte-ref_table = 'RSEG'.
  wa_fieldcatalog_qte-ref_field = 'BUZEI'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Poste facture'(c07).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.


  wa_fieldcatalog_qte-fieldname = 'EBELN'.
  wa_fieldcatalog_qte-ref_table = 'RSEG'.
  wa_fieldcatalog_qte-ref_field = 'EBELN'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'N° Commande'(c06).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'EBELP'.
  wa_fieldcatalog_qte-ref_table = 'RSEG'.
  wa_fieldcatalog_qte-ref_field = 'EBELP'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Poste commande'(c05).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'NETPR'.
  wa_fieldcatalog_qte-ref_table = 'EKPO'.
  wa_fieldcatalog_qte-ref_field = 'NETPR'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Prix net commande'(c04).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'NETPR2'.
  wa_fieldcatalog_qte-ref_table = 'EKPO'.
  wa_fieldcatalog_qte-ref_field = 'NETPR'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Prix net facture'(c03).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'DWERT'.
  wa_fieldcatalog_qte-ref_table = 'ARSEG'.
  wa_fieldcatalog_qte-ref_field = 'DWERT'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Ecart'(c02).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'WAERS'.
  wa_fieldcatalog_qte-ref_table = 'EKKO'.
  wa_fieldcatalog_qte-ref_field = 'WAERS'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Devise'(c01).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.



ENDFORM.                    " fieldcatalog_params_qte
*&---------------------------------------------------------------------*
*&      Form  toolbar_params_qte
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM toolbar_params_qte CHANGING pt_exclude TYPE ui_functions.


  DATA ls_exclude TYPE ui_func.

  ls_exclude = cl_gui_alv_grid=>mc_fc_auf.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_average.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_back_classic.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_abc.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_chain.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_crbatch.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_crweb.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_lineitems.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_master_data.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_more.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_report.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_xint.
*  APPEND ls_exclude TO pt_exclude.
*
*  ls_exclude = cl_gui_alv_grid=>mc_fc_call_xxl.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_check.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_col_invisible.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_col_optimize.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_current_variant.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_data_save.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_delete_filter.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_deselect_all.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_detail.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_excl_all.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrdata.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrdesig.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expcrtempl.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_expmdb.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_extend.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_f4.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_filter.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_find.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_fix_columns.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_graph.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_help.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_html.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_info.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_load_variant.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_append_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_cut.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_delete_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_move_row.
  APPEND ls_exclude TO pt_exclude.
*
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste_new_row.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_undo.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_maintain_variant.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_maximum.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_minimum.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_pc_file.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_print.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_print_back.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_print_prev.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_refresh.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_reprep.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_save_variant.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_select_all.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_send.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_separator.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_sort.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_sort_asc.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_sort_dsc.
*  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_subtot.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_sum.
  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_to_office.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_to_rep_tree.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_unfix_columns.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_views.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_view_crystal.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_view_excel.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_view_grid.
*  APPEND ls_exclude TO pt_exclude.

*  ls_exclude = cl_gui_alv_grid=>mc_fc_word_processor.
*  APPEND ls_exclude TO pt_exclude.


ENDFORM.                               " TOOLBAR_PARAMS_qte
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9101  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9101 INPUT.
  CASE sy-ucomm.
    WHEN 'SAVE'.
      PERFORM recalculate_line.
      PERFORM check_line.
      IF w_subrc EQ 0.
        PERFORM save_line.
        SET SCREEN 0.
      ENDIF.
    WHEN 'ERASE'.
    WHEN 'CANCEL'.
      SET SCREEN 0.
    WHEN OTHERS.
      PERFORM recalculate_line.
  ENDCASE.

ENDMODULE.                 " USER_COMMAND_9101  INPUT
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9102  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9102 INPUT.

  CASE sy-ucomm.
    WHEN 'SAVE'.

      PERFORM recalculate_line_cadd.
      PERFORM check_line_cadd.
      IF w_subrc EQ 0.
        PERFORM save_line_cadd.
        SET SCREEN 0.
      ENDIF.
    WHEN 'ERASE'.
      CLEAR wa_alv_cadd.
    WHEN 'CANCEL'.
      SET SCREEN 0.
    WHEN OTHERS.
      PERFORM recalculate_line_cadd.
  ENDCASE.


ENDMODULE.                 " USER_COMMAND_9102  INPUT






































***

*{   INSERT         &$&$&$&$                                          1
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9202  INPUT
*&---------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 21.08.2006
* Copie du dynpro 9201
*----------------------------------------------------------------------*
MODULE user_command_9202 INPUT.

  CASE w_9201_okcode.

    WHEN 'DISPLAY'.
* Affichage facture
      SET PARAMETER ID 'RBN' FIELD w_belnr.
      SET PARAMETER ID 'GJR' FIELD w_gjahr.
      CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.

    WHEN 'EM'. "Faire une EM

* Serco Update to check open quantities of PO


      SELECT SINGLE * FROM ekpo WHERE ebeln = t_item_qte-ebeln
                                AND ebelp = t_item_qte-ebelp.
      IF ekpo-menge = t_item_qte-wemng.
* There are no open quantities for this P.O. item!
*Please change the P.O. quantity first!' TYPE 'I'.
        MESSAGE i103(zsapbalance).

      ENDIF.
*        w_decision = 'HOLD'.
*        SET SCREEN 0.LEAVE SCREEN.
*      ELSE.
      w_decision = 'POSTVAL'.
      SET SCREEN 0.LEAVE SCREEN.
*      ENDIF.

    WHEN 'REFUS'. "Refus
      w_decision = 'REFUS'.
      SET SCREEN 0.LEAVE SCREEN.

*----------------------------------------------------------------------*
* ITGGH   16.04.2007      Display image
    WHEN 'IMGF'.       "Display image
*     get idoc number
      IF w_idoc-docnum IS INITIAL.
*>> ajout rgi010.
        SELECT SINGLE * FROM rbkp WHERE belnr = w_belnr
                                    AND gjahr = w_gjahr.
        IF sy-subrc = 0.
          CLEAR objkey.
          CONCATENATE w_belnr w_gjahr INTO objkey.
          SELECT SINGLE * FROM srrelroles WHERE objkey = objkey
                                            AND objtype = 'BUS2081'.
          IF sy-subrc = 0.
            SELECT SINGLE * FROM idocrel WHERE role_b = srrelroles-roleid.
            IF sy-subrc = 0.
              SELECT SINGLE * FROM srrelroles WHERE roleid = idocrel-role_a.
*                                      AND objtype = 'BUS2081'.
              IF sy-subrc = 0.
                MOVE srrelroles-objkey TO w_idoc-docnum.
              ELSE.
* Begin of RHA                        "RHA001
*              SELECT docnum credat FROM edids
                SELECT docnum credat FROM zbal_idoc
* End of RHA                          "RHA001
                          INTO CORRESPONDING FIELDS OF TABLE t_idoc
                          WHERE stapa1 = w_belnr
                          AND   status = '53'.
                IF sy-subrc = 0.
                  SORT t_idoc BY credat DESCENDING.
                  READ TABLE t_idoc INTO w_idoc INDEX 1.
                ENDIF.
              ENDIF.
            ENDIF.
          ENDIF.

        ENDIF.
      ENDIF.
*>> Ajout RGI010
* Begin of RHA                        "RHA002
      IF w_idoc-docnum IS INITIAL.
        SELECT docnum credat FROM zbal_idoc
                  INTO CORRESPONDING FIELDS OF TABLE t_idoc
                  WHERE stapa1 = w_belnr
                  AND   status = '53'.
        IF sy-subrc = 0.
          SORT t_idoc BY credat DESCENDING.
          READ TABLE t_idoc INTO w_idoc INDEX 1.
        ENDIF.
      ENDIF.
* End of RHA                         "RHA002
      CHECK NOT w_idoc-docnum IS INITIAL.
*       display image
      CALL FUNCTION 'ZBALANCE_IMAGE_DISPLAY'
        EXPORTING
          docnum                   = w_idoc-docnum
        EXCEPTIONS
          error_archiv             = 1
          error_communicationtable = 2
          error_kernel             = 3
          balance_config           = 4
          OTHERS                   = 5.
      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ELSE.
        CLEAR w_idoc-docnum.

      ENDIF.
*----------------------------------------------------------------------*
*----------------------------------------------------------------------*

    WHEN 'BACK' OR 'CANCEL'.

*     <Serco> 20.02.07
*      if difference equal 0, then enable user to post invoice
*      => set Flagfin = 1
      w_decision = 'BACK'.
*     </Serco>

      SET SCREEN 0.LEAVE SCREEN .
*
  ENDCASE.
*

ENDMODULE.                 " USER_COMMAND_9202  INPUT

*}   INSERT
*&---------------------------------------------------------------------*
*&      Module  STATUS_9202  OUTPUT
*&---------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 21.08.2006
* Copie du dynpro 9201
*----------------------------------------------------------------------*
MODULE status_9202 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.

  CASE w_listmode.
    WHEN '1'.  " Affichage
      SET PF-STATUS 'MAIN_9200'.
    WHEN '2'.  " Validation acheteur
      SET PF-STATUS 'MAIN_9202'.
    WHEN '3'.  " Validation compta
      SET PF-STATUS 'MAIN_9120'.
  ENDCASE.

  SET TITLEBAR 'TITRE_MM_DNP_9201'.

  IF o_qte_grid IS BOUND.
    CALL METHOD o_qte_grid->free.
  ENDIF.

  IF o_qte_container IS BOUND.
    CALL METHOD o_qte_container->free.
  ENDIF.


  CLEAR: o_qte_container, o_qte_grid.
  FREE: o_qte_container, o_qte_grid.
* Control Framework aktualisieren
*  call method cl_gui_cfw=>flush.

* create objects
*  IF o_qte_container IS INITIAL.
  CREATE OBJECT o_qte_container
    EXPORTING container_name = 'ALV_QTE'.

  CREATE OBJECT o_qte_grid
    EXPORTING
      i_parent = o_qte_container.

* T. NGUYEN (Appia Consulting) - 22.08.2006
*    PERFORM load_data_qte.
  PERFORM load_data_qte_2.

*  ENDIF.

ENDMODULE.                 " STATUS_9202  OUTPUT

*&---------------------------------------------------------------------*
*&      Module  exit_9202  INPUT
*&---------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 21.08.2006
* Copie du dynpro 9201
*----------------------------------------------------------------------*
MODULE exit_9202 INPUT.

  CASE w_9202_okcode.
    WHEN 'EXIT'.
      SET SCREEN 0.
      LEAVE SCREEN .
  ENDCASE.

ENDMODULE.                 " exit_9202  INPUT


*&--------------------------------------------------------------------*
*&      Form  load_data_qte_2
*&--------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 21.08.2006
* Copie de load_data_qte
*---------------------------------------------------------------------*
FORM load_data_qte_2.
  DATA : ws_variante_qte TYPE disvariant.
  DATA : ws_layout_qte   TYPE lvc_s_layo.
  DATA : ws_toolbar_qte TYPE ui_functions.
  DATA : w_save.

  SORT t_item_qte DESCENDING BY belnr gjahr buzei.

  PERFORM sort_params_qte.

  PERFORM fieldcatalog_params_qte_2.

  PERFORM toolbar_params_qte CHANGING ws_toolbar_qte.

  ws_layout_qte-edit = 'X'.
* ws_layout_qte-SEL_MODE ='X'.
* ws_layout_qte-weblook = 'X'.
  ws_layout_qte-zebra = 'X'.
  ws_layout_qte-cwidth_opt = 'X'.
  ws_layout_qte-grid_title = 'Gestion litige facture (écart prix)'(t57).

  wt_alv_qte[] = t_item_qte[].
  ws_variante_qte = sy-uname.
  ws_variante_qte = 'ZBALANCE_LISTPOSTBLO_QTE'.
  w_save = 'X'.

  CALL METHOD o_qte_grid->set_table_for_first_display
    EXPORTING
      i_structure_name     = 'tp_item_qte'
      is_variant           = ws_variante_qte
      is_layout            = ws_layout_qte
      i_save               = w_save
      it_toolbar_excluding = ws_toolbar_qte
    CHANGING
      it_fieldcatalog      = wt_catalog_qte
      it_sort              = wt_sort_qte
      it_outtab            = wt_alv_qte.


  CREATE OBJECT event_receiver_qte.
  SET HANDLER event_receiver_qte->handle_double_click FOR o_qte_grid.
  SET HANDLER event_receiver_qte->handle_hotspot_click FOR o_qte_grid.
*  SET HANDLER event_receiver->set_select_cell FOR go_grid.


ENDFORM.                    " load_data_qte


*&--------------------------------------------------------------------*
*&      Form  fieldcatalog_params_qte_2
*&--------------------------------------------------------------------*
* T. NGUYEN (Appia Consulting) - 21.08.2006
* Copie de fieldcatalog_params_qte
*---------------------------------------------------------------------*
FORM fieldcatalog_params_qte_2.

  DATA: wa_fieldcatalog_qte TYPE lvc_s_fcat.

*  wa_fieldcatalog-fieldname = 'SEL'.
*  wa_fieldcatalog-ref_table = 'SYST'.
*  wa_fieldcatalog-ref_field = 'SUBRC'.
*  wa_fieldcatalog-checkbox   = 'X'.
**  wa_fieldcatalog-edit   = 'X'.
*  wa_fieldcatalog-coltext   = 'Impression'.
*  APPEND wa_fieldcatalog TO t_catalog.
*  CLEAR wa_fieldcatalog.


*  wa_fieldcatalog_qte-fieldname = 'BELNR'.
*  wa_fieldcatalog_qte-ref_table = 'RBKP'.
*  wa_fieldcatalog_qte-ref_field = 'BELNR'.
*  wa_fieldcatalog_qte-hotspot   = 'X'.
*  wa_fieldcatalog_qte-coltext   = 'N° facture'.
**  wa_fieldcatalog_qte-edit = 'X'.
*  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
*  CLEAR wa_fieldcatalog_qte.
*
*  wa_fieldcatalog_qte-fieldname = 'GJAHR'.
*  wa_fieldcatalog_qte-ref_table = 'RBKP'.
*  wa_fieldcatalog_qte-ref_field = 'GJAHR'.
*  wa_fieldcatalog_qte-hotspot   = 'X'.
*  wa_fieldcatalog_qte-coltext   = 'Exercice'.
*  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
*  CLEAR wa_fieldcatalog_qte.

*DATA: BEGIN OF t_item_qte OCCURS 100,
*          sel(1),
*        belnr TYPE belnr_d,
*        gjahr TYPE gjahr,
*        buzei TYPE rblgp,
*        ebeln TYPE ebeln,
*        ebelp TYPE ebelp,
*        wemng TYPE wemng,
*        remng TYPE remng,
*        dmeng TYPE dmeng,
*        bstme TYPE bstme.

  wa_fieldcatalog_qte-fieldname = 'BUZEI'.
  wa_fieldcatalog_qte-ref_table = 'RSEG'.
  wa_fieldcatalog_qte-ref_field = 'BUZEI'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Poste facture'(c07).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.


  wa_fieldcatalog_qte-fieldname = 'EBELN'.
  wa_fieldcatalog_qte-ref_table = 'RSEG'.
  wa_fieldcatalog_qte-ref_field = 'EBELN'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'N° Commande'(c06).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'EBELP'.
  wa_fieldcatalog_qte-ref_table = 'RSEG'.
  wa_fieldcatalog_qte-ref_field = 'EBELP'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Poste commande'(c05).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'WEMNG'.
  wa_fieldcatalog_qte-ref_table = 'MSEG'.
  wa_fieldcatalog_qte-ref_field = 'MENGE'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Qté réceptionnée'(c08).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'REMNG'.
  wa_fieldcatalog_qte-ref_table = 'MSEG'.
  wa_fieldcatalog_qte-ref_field = 'MENGE'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Qté facturée'(c09).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'DMENG'.
  wa_fieldcatalog_qte-ref_table = 'MSEG'.
  wa_fieldcatalog_qte-ref_field = 'MENGE'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Ecart'(c10).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.

  wa_fieldcatalog_qte-fieldname = 'BSTME'.
  wa_fieldcatalog_qte-ref_table = 'MSEG'.
  wa_fieldcatalog_qte-ref_field = 'MEINS'.
  wa_fieldcatalog_qte-hotspot   = 'X'.
  wa_fieldcatalog_qte-coltext   = 'Unité de quantité de base'(c11).
  APPEND wa_fieldcatalog_qte TO wt_catalog_qte.
  CLEAR wa_fieldcatalog_qte.



ENDFORM.                    " fieldcatalog_params_qte
*&---------------------------------------------------------------------*
*&      Module  help_tva_code  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE help_tva_code INPUT.
  DATA: progname TYPE sy-repid,
        dynnum   TYPE sy-dynnr,
        dynpro_values TYPE TABLE OF dynpread,
        field_value LIKE LINE OF dynpro_values,
        values_tab TYPE TABLE OF t076m.
  progname = sy-repid.
  dynnum   = sy-dynnr.
  CLEAR: field_value, dynpro_values.
  field_value-fieldname = 'CARRIER'.
  APPEND field_value TO dynpro_values.

*  CALL FUNCTION 'DYNP_VALUES_READ'
*    EXPORTING
*      dyname             = sy-cprog
*      dynumb             = '9101'
*      translate_to_upper = 'X'
*    TABLES
*      dynpfields         = dynpro_values.
  DATA konto LIKE t076m-konto.
*  READ TABLE dynpro_values with key FIELDNAME = 'WA_ALV_FACT-NUMCMDE' .
  SELECT SINGLE lifnr INTO konto FROM ekko WHERE ebeln = wa_alv_fact-numcmde.
  SELECT * FROM t076m INTO TABLE values_tab
                 WHERE parart = 'LI'
                   AND konto = konto.


  CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
    EXPORTING
      retfield    = 'MWART'
      dynpprog    = progname
      dynpnr      = dynnum
      dynprofield = 'WA_ALV_FACT-CODETVA'
      value_org   = 'S'
    TABLES
      value_tab   = values_tab.


ENDMODULE.                 " help_tva_code  INPUT
