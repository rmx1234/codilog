FUNCTION z_balance_cfb_missinggr.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(OBJTYPE) LIKE  SWETYPECOU-OBJTYPE
*"     VALUE(OBJKEY) LIKE  SWEINSTCOU-OBJKEY
*"     VALUE(EVENT) LIKE  SWETYPECOU-EVENT
*"     VALUE(RECTYPE) LIKE  SWETYPECOU-RECTYPE
*"  TABLES
*"      EVENT_CONTAINER STRUCTURE  SWCONT
*"  EXCEPTIONS
*"      NO_WORKFLOW
*"----------------------------------------------------------------------
*----------------------------------------------------------------------*
* Projet : Balance                                                     *
* Auteur : Jean-Michel BRUNOD                                          *
*                                                                      *
* Date :   16.02.2007                                                  *
*                                                                      *
*                                                                      *
* Theme: Check-FM for start Missing Good Receipt Workflow              *
*                                                                      *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

  DATA : w_docnum LIKE edidc-docnum.
  DATA : st_header TYPE zbalance_04.
  DATA : w_return(1) TYPE c.

  DATA : st_event_container TYPE swcont.

************************************************************************
* Init
  CLEAR : st_event_container,
          w_docnum.

* Retreive invoice document key from the event container
  READ TABLE event_container
  INTO st_event_container
  WITH KEY element = '_EVT_OBJKEY'.

  IF sy-subrc EQ 0.

    w_docnum = st_event_container-value.

    CALL FUNCTION 'ZBALANCE_INVOIC_MM_DETAILS'
      EXPORTING
        i_docnum = w_docnum
      IMPORTING
        e_header = st_header.

    IF sy-subrc EQ 0.
*     Call the filter function
      CALL FUNCTION 'ZBALANCE_WF_START_CHECK'
        EXPORTING
          bukrs  = st_header-bukrs
          lifnr  = st_header-lifnr
        IMPORTING
          return = w_return.
    ENDIF.

  ELSE.
    w_return = '8'.
  ENDIF.

  IF w_return NE '0'.
    RAISE no_workflow.
  ENDIF.

ENDFUNCTION.
