FUNCTION zbalance_docfac_disp_from_idoc.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(DOCNUM) LIKE  EDIDC-DOCNUM
*"  EXCEPTIONS
*"      DOC_NOT_FOUND
*"      OTHERS
*"----------------------------------------------------------------------

  SELECT SINGLE * FROM edidc WHERE docnum = docnum.

  IF sy-subrc NE 0.
    RAISE doc_not_found.
  ELSE.

    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_READ'
         EXPORTING
              document_number = docnum.


    CALL FUNCTION 'EDI_DOCUMENT_READ_LAST_STATUS'
         EXPORTING
              document_number        = docnum
         IMPORTING
              status                 = int_edids
         EXCEPTIONS
              document_not_open      = 1
              no_status_record_found = 2
              OTHERS                 = 3.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open'(w15).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'No status record found'(w16).
      ENDIF.
    ENDIF.

    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_READ'
         EXPORTING
              document_number   = docnum
         EXCEPTIONS
              document_not_open = 1
              parameter_error   = 2
              OTHERS            = 3.

    IF sy-subrc NE 0.
      IF sy-subrc EQ 1.
        MESSAGE s208(00) WITH 'IDoc is not open for reading'(w12).
      ELSEIF sy-subrc EQ 2.
        MESSAGE s208(00) WITH 'Parameter combination is invalid'(w13).
      ENDIF.
    ENDIF.

    SELECT * FROM  rbkp
           WHERE  belnr  = int_edids-stapa1
           ORDER BY gjahr.
    ENDSELECT.


    IF rbkp-rbstat CA 'ABCDE'.
      PERFORM bdc_dynpro      USING 'SAPMM08N'   '0100'.
      PERFORM bdc_field       USING 'SO_BELNR-LOW' rbkp-belnr.
      PERFORM bdc_field       USING 'SO_GJAHR-LOW' rbkp-gjahr.
      PERFORM bdc_field       USING 'P_IV_BG' 'X'.
      PERFORM bdc_field       USING 'P_IV_OV' 'X'.
      PERFORM bdc_field       USING 'P_IV_EDI' 'X'.
      PERFORM bdc_field       USING 'P_IV_BAP' 'X'.
      PERFORM bdc_field       USING 'P_IV_ERS' 'X'.
      PERFORM bdc_field       USING 'P_IV_IS' 'X'.
      PERFORM bdc_field       USING 'P_IV_STO' 'X'.
      PERFORM bdc_field       USING 'P_IV_PAR' 'X'.
      PERFORM bdc_field       USING 'P_ST_BG' 'X'.
      PERFORM bdc_field       USING 'P_ST_ERR' 'X'.
      PERFORM bdc_field       USING 'P_ST_OK' 'X'.
      PERFORM bdc_field       USING 'P_ST_PO' 'X'.
      PERFORM bdc_field       USING 'P_ST_SA' 'X'.
      PERFORM bdc_field       USING 'P_ST_PA' 'X'.
      PERFORM bdc_field       USING 'P_ST_CO' 'X'.
      PERFORM bdc_field       USING 'BDC_SUBSCR'
      'SAPMM08N                                1010%_SUBSCREEN_BLTAB'.
      PERFORM bdc_field       USING 'BDC_OKCODE' '=CRET'.
      CALL TRANSACTION 'MIR6' USING w_bdcdata MODE 'E'.
    ELSEIF rbkp-rbstat CA '12345'.
      SET PARAMETER ID 'RBN' FIELD rbkp-belnr.
      SET PARAMETER ID 'GJR' FIELD rbkp-gjahr.
      CALL TRANSACTION 'MIR4' AND SKIP FIRST SCREEN.
    ENDIF.

    CALL FUNCTION 'ZBALANCE_DOCFAC_DISPLAY'
         EXPORTING
              belnr         = rbkp-belnr
              gjahr         = rbkp-gjahr
         EXCEPTIONS
              doc_not_found = 1
              OTHERS        = 2.

    IF sy-subrc EQ 1.
      MESSAGE s532(0U)
      WITH 'Document de facturation non trouvé: '(w31) rbkp-belnr.
    ENDIF.

  ENDIF.

ENDFUNCTION.
