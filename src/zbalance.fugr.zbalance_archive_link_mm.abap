FUNCTION ZBALANCE_ARCHIVE_LINK_MM.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IDOC_CONTRL) LIKE  EDIDC STRUCTURE  EDIDC
*"  TABLES
*"      IDOC_DATA STRUCTURE  EDIDD
*"      IDOC_STATUS STRUCTURE  BDIDOCSTAT OPTIONAL
*"  CHANGING
*"     VALUE(E_RETURN) TYPE  BAL_T_MSG
*"----------------------------------------------------------------------
*{   INSERT         DE5K900049                                        1

*"---------------------------------------------------------------------
** Identification :
** Description    : archive invoice
**----------------------------------------------------------------------
** Eléments de développements associés :
**----------------------------------------------------------------------
**
** Projet : SAP BALANCE
** Auteur : Gabriele Ghotra (IT-Informatik GmbH)
** Date   : 12/02/07
** Frequence :
** Ordre de transport: DE5K900049
************************************************************************
**
** Modifié  !     Par      !                Description
**
************************************************************************
**
**          !              !
**
**          !              !
**
**----------!--------------!--------------------------------------------
**
**          !              !
**
**          !              !
**
************************************************************************
**
*
*  DATA: posted(1),
*        completed(1),
*        w_mimetype(20),
*        c_ar_object(20),
*        c_bsart_cremem   LIKE e1edk01-bsart VALUE 'CRME',
*        w_ar_object      LIKE toaom-ar_object,
*        w_conn           LIKE toaom-connection,
*        w_doc_type       LIKE toaom-doc_type,
*        w_ar_date        LIKE toa01-ar_date,
*        size             TYPE i,
*        BEGIN OF data OCCURS 1,
*          line       TYPE docs,
*        END OF data,
*        int_edids      TYPE edids,
*        st_e1edk01     TYPE e1edk01.
*
*  DATA: wa_toa LIKE toa01.
*
*  CLEAR: posted, completed, c_ar_object,w_ar_object, w_object_id,
*         w_archiv_id, w_doc_type, w_conn, w_mimetype.
*
** last status of the IDOC
** parked/posted?
*  IF NOT idoc_status[] IS INITIAL.
*    READ TABLE idoc_status WITH KEY status = '53'
*                                    msgid = 'M8'
*                                    msgno = '060'.
**   yes
*    IF sy-subrc EQ 0.
*      w_objecttype = 'BUS2081'.
*      CONCATENATE idoc_status-msgv1 sy-datum(4) INTO w_object_id.
*      posted = 'X'.
*    ENDIF.
*  ENDIF.
*
** no
*  IF posted IS INITIAL.
*    CALL FUNCTION 'EDI_DOCUMENT_OPEN_FOR_READ'
*      EXPORTING
*          document_number = idoc_contrl-docnum.
*    CALL FUNCTION 'EDI_DOCUMENT_READ_LAST_STATUS'
*       EXPORTING
*          document_number        = idoc_contrl-docnum
*       IMPORTING
*          status                 = int_edids
*       EXCEPTIONS
*          document_not_open      = 1
*          no_status_record_found = 2
*          OTHERS                 = 3.
*    IF sy-subrc EQ 1.
*      PERFORM f_130_message TABLES e_return
*          USING '006' 'E' 'IDoc is not open'(w15) ' ' ' ' ' '.
*    ELSEIF sy-subrc EQ 2.
*      PERFORM f_130_message TABLES e_return
*          USING '006' 'E' 'No status record found'(w16) ' ' ' ' ' '.
*    ENDIF.
*    CALL FUNCTION 'EDI_DOCUMENT_CLOSE_READ'
*      EXPORTING
*          document_number = idoc_contrl-docnum.
*
*    w_objecttype = 'IDOC'.
*    MOVE idoc_contrl-docnum TO w_object_id.
*  ENDIF.
*
*  LOOP AT idoc_data.
*    IF idoc_data-segnam = 'E1EDK01'.
*      MOVE idoc_data-sdata TO st_e1edk01.
**     Document type: credit memo
*      IF st_e1edk01-bsart = C_BSART_CREMEM.
*        MOVE 'AR_OBJECT_MM_CRED' TO c_ar_object.
**     Document type: invoice
*      ELSE.
*        MOVE 'AR_OBJECT_MM_INVO' TO c_ar_object.
*      ENDIF.
*    ENDIF.
*  ENDLOOP.
*  SELECT SINGLE * FROM  zbal_params CLIENT SPECIFIED
*       WHERE  mandt  = sy-mandt
*       AND    param  = c_ar_object.
*  IF sy-subrc EQ 0.
*    w_ar_object = zbal_params-value.
*  ENDIF.
*
*  SELECT SINGLE * FROM toaom
*    WHERE sap_object = w_objecttype
*    AND   ar_object  = w_ar_object
*    AND   ar_status   = 'X'.
*  w_archiv_id = toaom-archiv_id.
*  w_doc_type  = toaom-doc_type.
*  w_conn      = toaom-connection.
*
** Was the document already linked to the archive?
*  CASE w_conn.
*    WHEN 'TOA01'.
*      SELECT SINGLE * FROM toa01
*        WHERE sap_object = w_objecttype
*        AND   object_id  = w_object_id
*        AND   archiv_id  = w_archiv_id.
*    WHEN 'TOA02'.
*      SELECT SINGLE * FROM toa02
*        WHERE sap_object = w_objecttype
*        AND   object_id  = w_object_id
*        AND   archiv_id  = w_archiv_id.
*    WHEN 'TOA03'.
*      SELECT SINGLE * FROM toa03
*        WHERE sap_object = w_objecttype
*        AND   object_id  = w_object_id
*        AND   archiv_id  = w_archiv_id.
*  ENDCASE.
*
** if found, the document was already linked to the archive
** ==> do nothing
*  CHECK sy-subrc NE 0.
*
** Was the IDOC already linked to the archive?
*  IF w_objecttype <> 'IDOC'.              "then it was checked before
*    CASE w_conn.
*      WHEN 'TOA01'.
*        SELECT * FROM toa01 INTO wa_toa UP TO 1 ROWS
*          WHERE sap_object = 'IDOC'
*          AND   object_id  = idoc_contrl-docnum
*          AND   archiv_id  = w_archiv_id.
*        ENDSELECT.
*      WHEN 'TOA02'.
*        SELECT * FROM toa02 INTO wa_toa UP TO 1 ROWS
*          WHERE sap_object = 'IDOC'
*          AND   object_id  = idoc_contrl-docnum
*          AND   archiv_id  = w_archiv_id.
*        ENDSELECT.
*      WHEN 'TOA03'.
*        SELECT * FROM toa03 INTO wa_toa UP TO 1 ROWS
*          WHERE sap_object = 'IDOC'
*          AND   object_id  = idoc_contrl-docnum
*          AND   archiv_id  = w_archiv_id.
*        ENDSELECT.
*    ENDCASE.
*
**   IDOC found in the archive
*    IF sy-subrc = 0.
**     link invoice with archive
*      w_ar_date         = wa_toa-ar_date.
*      wa_toa-sap_object = w_objecttype.
*      wa_toa-object_id  = w_object_id.
*      wa_toa-ar_date    = sy-datum.
*      CASE w_conn.
*        WHEN 'TOA01'.
*          INSERT toa01 FROM wa_toa.
*        WHEN 'TOA02'.
*          INSERT toa02 FROM wa_toa.
*        WHEN 'TOA03'.
*          INSERT toa03 FROM wa_toa.
*      ENDCASE.
**     and delete the link between Idoc and archive
*      IF sy-subrc = 0.
*        COMMIT WORK.
*        wa_toa-sap_object = 'IDOC'.
*        wa_toa-object_id  = idoc_contrl-docnum.
*        wa_toa-ar_date    = w_ar_date.
*        CASE w_conn.
*          WHEN 'TOA01'.
*            DELETE toa01 FROM wa_toa.
*          WHEN 'TOA02'.
*            DELETE toa02 FROM wa_toa.
*          WHEN 'TOA03'.
*            DELETE toa03 FROM wa_toa.
*        ENDCASE.
*        IF sy-subrc = 0.
*          COMMIT WORK.
*        ELSE.
*          ROLLBACK WORK.
*          PERFORM f_130_message TABLES e_return
*             USING '006' 'E'
*                   'Error: Document could not be archived'(w66)
*                   ' ' ' ' ' '.
*        ENDIF.
*      ELSE.
*        ROLLBACK WORK.
*        PERFORM f_130_message TABLES e_return
*           USING '006' 'E'
*                 'Error: Document could not be archived'(w66)
*                 ' ' ' ' ' '.
*      ENDIF.
*      completed = 'X'.
*    ENDIF.
*  ENDIF.
*
*  IF completed IS INITIAL.
*
**   get TIF-file
*    CALL FUNCTION 'SCMS_UPLOAD'
*      EXPORTING
*        filename = idoc_contrl-arckey
*        binary   = 'X'
*        frontend = ' '
*      IMPORTING
*        filesize = size
*      TABLES
*        data     = data
*      EXCEPTIONS
*        error    = 1
*        others   = 2.
*    IF sy-subrc <> 0.
*      PERFORM f_130_message TABLES e_return
*         USING '006' 'E'
*               'Error: TIF-file could not be uploaded'(w67)
*                 ' ' ' ' ' '.
*      EXIT.
*    ENDIF.
*
**   put invoice into archive per HTTP, RFC or online
*    SELECT SINGLE * FROM toaar WHERE archiv_id = w_archiv_id.
*
**   HTTP
*    IF toaar-http = 'X'.
*      IF w_doc_type = 'PDF'.                      "PDF
*        w_mimetype = 'application/pdf'.
*      ELSE.                                       "TIF
*        w_mimetype = 'image/tiff'.
*      ENDIF.
*      CALL FUNCTION 'SCMS_HTTP_CREATE'
*        EXPORTING
*          crep_id               = w_archiv_id
*          mimetype              = w_mimetype
*          length                = size
*          accessmode            = 'C'
*        IMPORTING
*          doc_id_out            = w_archiv_doc_id
*        TABLES
*          data                  = data
*        EXCEPTIONS
*          bad_request           = 1
*          unauthorized          = 2
*          forbidden             = 3
*          conflict              = 4
*          internal_server_error = 5
*          error_http            = 6
*          error_url             = 7
*          error_signature       = 8
*          error_parameter       = 9
*          others                = 10.
*      IF sy-subrc <> 0.
*        PERFORM f_130_message TABLES e_return
*           USING '006' 'E'
*                 'Error: Document could not be archived'(w66)
*                 ' ' ' ' ' '.
*        EXIT.
*      ENDIF.
*
**   RFC
*    ELSEIF toaar-rfc = 'X'.
*      CALL FUNCTION 'SCMS_RFC_TABLE_PUT'
*        EXPORTING
*          arc_id       = w_archiv_id
*          doc_type     = w_doc_type
*          length       = size
*        IMPORTING
*          doc_id       = w_archiv_doc_id
*        TABLES
*          data         = data
*        EXCEPTIONS
*          error_kernel = 1
*          error_archiv = 2
*          error_config = 3
*          others       = 4.
*      IF sy-subrc <> 0.
*        PERFORM f_130_message TABLES e_return
*           USING '006' 'E'
*                 'Error: Document could not be archived'(w66)
*                 ' ' ' ' ' '.
*        EXIT.
*      ENDIF.
*
**   Online
*    ELSE.
*      CALL FUNCTION 'ARCHIV_DTFILE_SAVE'
*        EXPORTING
*          archiv_id         = w_archiv_id
*          doc_type          = w_doc_type
*          frontendfile      = idoc_contrl-arckey
*        IMPORTING
*          arc_doc_id        = w_archiv_doc_id
*        EXCEPTIONS
*          error_application = 1
*          OTHERS            = 2.
*      IF sy-subrc <> 0.
*        PERFORM f_130_message TABLES e_return
*           USING '006' 'E'
*                 'Error: Document could not be archived'(w66)
*                 ' ' ' ' ' '.
*        EXIT.
*      ENDIF.
*    ENDIF.
*
**   link with archive
*    CLEAR wa_toa.
*    wa_toa-sap_object       = w_objecttype.
*    wa_toa-object_id        = w_object_id.
*    wa_toa-archiv_id        = w_archiv_id.
*    wa_toa-arc_doc_id       = w_archiv_doc_id.
*    wa_toa-ar_object        = w_ar_object.
*    wa_toa-ar_date          = sy-datum.
*    wa_toa-reserve          = w_doc_type.
*    CASE w_conn.
*      WHEN 'TOA01'.
*        INSERT toa01 FROM wa_toa.
*      WHEN 'TOA02'.
*        INSERT toa02 FROM wa_toa.
*      WHEN 'TOA03'.
*        INSERT toa03 FROM wa_toa.
*    ENDCASE.
*    IF sy-subrc = 0.
*      COMMIT WORK.
*    ELSE.
*      ROLLBACK WORK.
*        PERFORM f_130_message TABLES e_return
*           USING '006' 'E'
*                 'Error: Document could not be archived'(w66)
*                 ' ' ' ' ' '.
*    ENDIF.
*  ENDIF.
*
*}   INSERT





ENDFUNCTION.
