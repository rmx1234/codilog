FUNCTION ZBALANCE_ARCHIVE_LINK_FI_2.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(I_DOCNUM) LIKE  EDIDC-DOCNUM
*"  EXPORTING
*"     VALUE(E_ARC_DOC_ID) LIKE  SAPB-SAPADOKID
*"  EXCEPTIONS
*"      ARCHIVE_ERROR
*"      UPLOAD_ERRROR
*"----------------------------------------------------------------------




*{   INSERT         DE5K900049                                        1
**
** Identification :
**
**
**
** Description    : archive FI invoice
**
**
**
**----------------------------------------------------------------------
**
** Eléments de développements associés :
**
**
**
**
**
**----------------------------------------------------------------------
**
** Projet : SAP BALANCE
**
**
**
** Auteur : Gabriele Ghotra (IT-Informatik GmbH)
**
**
**
** Date   : 16/02/07
**
**
**
** Frequence :
**
**		                                                         *
** Ordre de transport: DE5K900049
**
**							                       *
************************************************************************
**
** Modifié  !     Par      !                Description
**
************************************************************************
**
**          !              !
**
**          !              !
**
**----------!--------------!--------------------------------------------
**
**          !              !
**
**          !              !
**
************************************************************************
**
**  TABLES: toaom, toaar, toa02, toa03.
*
*  DATA: w_mimetype(20),
*        c_bsart_cremem   LIKE e1edk01-bsart VALUE 'CRME',
*        w_ar_object      LIKE toaom-ar_object,
*        w_conn           LIKE toaom-connection,
*        w_doc_type       LIKE toaom-doc_type,
*        w_ar_date        LIKE toa01-ar_date,
*        w_credat         LIKE edidc-credat,
*        w_arckey         LIKE edidc-arckey,
*        wa_edids         TYPE edids,
*        size             TYPE i,
*        BEGIN OF data OCCURS 1,
*          line       TYPE docs,
*        END OF data.
*
*  DATA: wa_toa LIKE toa01.
*
*  CLEAR: w_ar_object, w_object_id, w_archiv_id,
*         w_doc_type, w_conn, w_mimetype.
*
** get information about IDOC
*  SELECT SINGLE credat arckey INTO (w_credat, w_arckey) FROM edidc
*         WHERE docnum = i_docnum.
*
*  SELECT SINGLE * FROM edids INTO wa_edids
*         WHERE docnum = i_docnum
*         AND   status = '53'.
*
** get customizing parameter
*  SELECT SINGLE * FROM  zbal_params CLIENT SPECIFIED
*       WHERE  mandt  = sy-mandt
*       AND    param  = 'AR_OBJECT_FI_PREL'.
*  IF sy-subrc EQ 0.
*    w_ar_object = zbal_params-value.
*  ENDIF.
*
** get information about archive
*  SELECT SINGLE * FROM toaom
*    WHERE sap_object = 'BKPF'
*    AND   ar_object  = w_ar_object
*    AND   ar_status   = 'X'.
*  w_archiv_id = toaom-archiv_id.
*  w_doc_type  = toaom-doc_type.
*  w_conn      = toaom-connection.
*  CONCATENATE wa_edids-stapa2 wa_edids-stapa1 w_credat(4)
*         INTO w_object_id.
*
** document already linked to the archive?
*  CASE w_conn.
*    WHEN 'TOA01'.
*      SELECT SINGLE * FROM toa01
*        WHERE sap_object = 'BKPF'
*        AND   object_id  = w_object_id
*        AND   archiv_id  = w_archiv_id
*        AND   ar_object  = w_ar_object.
*    WHEN 'TOA02'.
*      SELECT SINGLE * FROM toa02
*        WHERE sap_object = 'BKPF'
*        AND   object_id  = w_object_id
*        AND   archiv_id  = w_archiv_id
*        AND   ar_object  = w_ar_object.
*    WHEN 'TOA03'.
*      SELECT SINGLE * FROM toa03
*        WHERE sap_object = 'BKPF'
*        AND   object_id  = w_object_id
*        AND   archiv_id  = w_archiv_id
*        AND   ar_object  = w_ar_object.
*  ENDCASE.
*
** if found, the document was already linked to the archive
** ==> do nothing
*  CHECK sy-subrc NE 0.
*
** get TIF-file
*  CALL FUNCTION 'SCMS_UPLOAD'
*    EXPORTING
*      filename = w_arckey
*      binary   = 'X'
*      frontend = ' '
*    IMPORTING
*      filesize = size
*    TABLES
*      data     = data
*    EXCEPTIONS
*      error    = 1
*      others   = 2.
*  IF sy-subrc <> 0.
*    RAISE upload_error.
*    EXIT.
*  ENDIF.
*
** put invoice into archive per HTTP, RFC or online
*  SELECT SINGLE * FROM toaar WHERE archiv_id = w_archiv_id.
*
** HTTP
*  IF toaar-http = 'X'.
*    IF w_doc_type = 'PDF'.                      "PDF
*      w_mimetype = 'application/pdf'.
*    ELSE.                                       "TIF
*      w_mimetype = 'image/tiff'.
*    ENDIF.
*    CALL FUNCTION 'SCMS_HTTP_CREATE'
*      EXPORTING
*        crep_id               = w_archiv_id
*        mimetype              = w_mimetype
*        length                = size
*        accessmode            = 'C'
*      IMPORTING
*        doc_id_out            = w_archiv_doc_id
*      TABLES
*        data                  = data
*      EXCEPTIONS
*        bad_request           = 1
*        unauthorized          = 2
*        forbidden             = 3
*        conflict              = 4
*        internal_server_error = 5
*        error_http            = 6
*        error_url             = 7
*        error_signature       = 8
*        error_parameter       = 9
*        others                = 10.
*    IF sy-subrc <> 0.
*      RAISE archive_error.
*      EXIT.
*    ENDIF.
*
** RFC
*  ELSEIF toaar-rfc = 'X'.
*    CALL FUNCTION 'SCMS_RFC_TABLE_PUT'
*      EXPORTING
*        arc_id       = w_archiv_id
*        doc_type     = w_doc_type
*        length       = size
*      IMPORTING
*        doc_id       = w_archiv_doc_id
*      TABLES
*        data         = data
*      EXCEPTIONS
*        error_kernel = 1
*        error_archiv = 2
*        error_config = 3
*        others       = 4.
*    IF sy-subrc <> 0.
*      RAISE archive_error.
*      EXIT.
*    ENDIF.
*
** Online
*  ELSE.
*    CALL FUNCTION 'ARCHIV_DTFILE_SAVE'
*      EXPORTING
*        archiv_id         = w_archiv_id
*        doc_type          = w_doc_type
*        frontendfile      = w_arckey
*      IMPORTING
*        arc_doc_id        = w_archiv_doc_id
*      EXCEPTIONS
*        error_application = 1
*        OTHERS            = 2.
*    IF sy-subrc <> 0.
*      RAISE archive_error.
*      EXIT.
*    ENDIF.
*  ENDIF.
*
** link to archive
*  CLEAR wa_toa.
*  wa_toa-sap_object       = 'BKPF'.
*  wa_toa-object_id        = w_object_id.
*  wa_toa-archiv_id        = w_archiv_id.
*  wa_toa-arc_doc_id       = w_archiv_doc_id.
*  wa_toa-ar_object        = w_ar_object.
*  wa_toa-ar_date          = sy-datum.
*  wa_toa-reserve          = w_doc_type.
*  CASE w_conn.
*    WHEN 'TOA01'.
*      INSERT toa01 FROM wa_toa.
*    WHEN 'TOA02'.
*      INSERT toa02 FROM wa_toa.
*    WHEN 'TOA03'.
*      INSERT toa03 FROM wa_toa.
*  ENDCASE.
*  IF sy-subrc = 0.
*    COMMIT WORK.
*    e_arc_doc_id = w_archiv_doc_id.
*  ELSE.
*    ROLLBACK WORK.
*    RAISE archive_error.
*  ENDIF.
*
*}   INSERT

ENDFUNCTION.
