*"----------------------------------------------------------------------------
*" APPIA - 24.10.2006
*" Déclenchement des workflows MM
*" à condition que le type de pièce de la facture MM
*" soit égale à RB.
*" Si le type de pièce est différent de RB le workflow ne sera pas déclenché.
*"-----------------------------------------------------------------------------
* Author            : Erwan LECAILLE                            ELE001 *
* Date              : 12.11.2007                                       *
* Document  DIA     : FI_344_DIA                                       *
* Description       : No workflow when calling from RBDMANI2 in        *
*                     background                                       *
* ----------------- * ------------------------------------------------ *
* Author            : Reynald GIL                               RGI001 *
* Date              : 12.02.2008                                       *
* Document  DIA     : FI_393_DIA                                       *
* Description       : addition MM document Type
* ----------------- * ------------------------------------------------ *
* Author            : Jean-Marc JOURON                          JMJ001 *
* Date              : 10.02.2009                                       *
* Document  DIA     : 976                                              *
* Description       : New solution must not start an old Workflow      *
* ----------------- * ------------------------------------------------ *

FUNCTION z_control_workflow_mm.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(OBJTYPE) LIKE  SWETYPECOU-OBJTYPE
*"     REFERENCE(OBJKEY) LIKE  SWEINSTCOU-OBJKEY
*"     REFERENCE(EVENT) LIKE  SWEINSTCOU-EVENT
*"     REFERENCE(RECTYPE) LIKE  SWETYPECOU-RECTYPE
*"  TABLES
*"      EVENT_CONTAINER STRUCTURE  SWCONT
*"  EXCEPTIONS
*"      NO_WORKFLOW
*"----------------------------------------------------------------------

  INCLUDE <cntain>.

  DATA: object TYPE swc_object,
        lv_docnum     TYPE zbal_idoc-docnum,  "JMJ001 +
        lv_status     TYPE zbal_idoc-status.  "JMJ001 +

  DATA : BEGIN OF key,
           invoicedocnumber LIKE rbkp-belnr,
           fiscalyear LIKE rbkp-gjahr,
         END OF key.

*"--- get object from container
  swc_get_element event_container '_EVT_OBJECT' object.

  swc_get_object_key object key.

*" Verification qu'il s'agit facture venant de balance: RBKP-BLART = RB.
  SELECT SINGLE * FROM rbkp
                  WHERE belnr = key-invoicedocnumber
                    AND gjahr = key-fiscalyear
*§..RGI001 Addtion FI_393_DIA
*                    AND blart = 'RB'.
                     AND blart in ('RB', 'R7', 'R8', 'R9').

  IF sy-subrc <> 0.
    RAISE no_workflow.
  ENDIF.

**>>>ELE001
  IF sy-cprog = 'RBDMANI2'
*  AND sy-batch = 'X'
.
    RAISE no_workflow.
  ENDIF.
**<<<ELE001

*DIA976- JMJ001 > new solution has to be checked to avoid the sending of 2 WF
    SELECT docnum status INTO (lv_docnum, lv_status)
    FROM zbal_idoc
    UP TO 1 ROWS
    WHERE refint = rbkp-bktxt
    ORDER BY docnum DESCENDING.
    ENDSELECT.

    IF sy-subrc NE 0.
*      EXIT.
      RAISE NO_workflow.
    ENDIF.
* JMJ001 > end
ENDFUNCTION.
