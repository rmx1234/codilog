FUNCTION zbalance_order_display.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(ORDERNUM) LIKE  EKKO-EBELN
*"  EXCEPTIONS
*"      DOC_NOT_FOUND
*"----------------------------------------------------------------------

  SELECT SINGLE * FROM ekko WHERE ebeln = ordernum.

  IF sy-subrc NE 0.
    RAISE doc_not_found.
  ELSE.
    SET PARAMETER ID 'BES' FIELD ordernum.
    CALL TRANSACTION 'ME23N' AND SKIP FIRST SCREEN.
  ENDIF.

ENDFUNCTION.
