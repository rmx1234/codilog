FUNCTION Z_BALANCE_CFB_QTYDISCRP_POST.
*"--------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(OBJTYPE) LIKE  SWETYPECOU-OBJTYPE
*"     VALUE(OBJKEY) LIKE  SWEINSTCOU-OBJKEY
*"     VALUE(EVENT) LIKE  SWETYPECOU-EVENT
*"     VALUE(RECTYPE) LIKE  SWETYPECOU-RECTYPE
*"  TABLES
*"      EVENT_CONTAINER STRUCTURE  SWCONT
*"  EXCEPTIONS
*"      NO_WORKFLOW
*"--------------------------------------------------------------------
*----------------------------------------------------------------------*
* Projet : Balance                                                     *
* Auteur : Jean-Michel BRUNOD                                          *
*                                                                      *
* Date :   16.04.2007                                                  *
*                                                                      *
*                                                                      *
* Theme: Check-FM for start Quantity Discrepancy Workflow on posted
* invoices                                                             *
*                                                                      *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*      Modified by Serco
*       Author F. Hofmann
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************

  DATA : w_belnr LIKE rbkp-belnr.
  DATA : w_gjahr LIKE rbkp-gjahr.
  DATA : w_return(1) TYPE c.

  DATA : st_event_container TYPE swcont.

************************************************************************
* Init
  CLEAR : st_event_container,
          w_belnr,
          w_gjahr.


* Retreive invoice document key from the event container
  READ TABLE event_container
  INTO st_event_container
  WITH KEY element = '_EVT_OBJKEY'.

  IF sy-subrc EQ 0.

    w_belnr = st_event_container-value(10).
    w_gjahr = st_event_container-value+10(4).

*   Read the invoice informations
    SELECT SINGLE * FROM  rbkp
         WHERE  belnr  = w_belnr
         AND    gjahr  = w_gjahr.
    IF sy-subrc EQ 0.

 Select single * from zbal_params where param = 'RB_MEQ3'.
* for parked invoices only

      IF rbkp-rbstat EQ '5' and zbal_params-value = 'X'.



*     Call the filter function
        CALL FUNCTION 'ZBALANCE_WF_START_CHECK'
          EXPORTING
            bukrs  = rbkp-bukrs
            lifnr  = rbkp-lifnr
          IMPORTING
            return = w_return.

      else.
            w_return = '8'.
      ENDIF.
    ENDIF.

  ELSE.
    w_return = '8'.
  ENDIF.

  IF w_return NE '0'.
    RAISE no_workflow.
  ENDIF.



ENDFUNCTION.
