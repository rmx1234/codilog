*----------------------------------------------------------------------*
***INCLUDE LZBALANCEF06 .
*----------------------------------------------------------------------*

*{   INSERT         MS4K915548                                        1
*&---------------------------------------------------------------------*
*&      Form  BDC_TRANSACTION
*&---------------------------------------------------------------------*
*&       Appel à la transaction
*&---------------------------------------------------------------------*
form bdc_transaction using    tcode log_type extnumber
                     changing p_p_w_subrc.

  data : messtab type table of BDCMSGCOLL with header line.

data: idoc_number        like edidc-docnum,
      c_mem_idoc_in(20)  type c value 'IDOC_NO_FI'.
data : mode.

data: begin of idoc_status occurs 10.
        include structure bdidocstat.
data: end of idoc_status.

  export idoc_number idoc_status to memory id c_mem_idoc_in.

mode = 'N'.

  call transaction tcode using w_bdcdata
                   mode mode
                   update 'S'
                   messages into messtab.

if sy-subrc = 0.
    p_p_w_subrc = 0.
    exit.
*endif.
*  loop at messtab where msgtyp = 'S'
*                    and msgid  = 'F5'
*                    and msgnr  = '312'.
*  endloop.
*  if sy-subrc = 0.
*    p_p_w_subrc = 0.
  else.
      p_p_w_subrc = 1.
*    LOOP AT messtab WHERE msgtyp = 'A'
*                       OR msgtyp = 'E'.
*      MESSAGE ID messtab-msgid TYPE messtab-msgtyp NUMBER messtab-msgnr
*               WITH messtab-msgv1 messtab-msgv2
*                    messtab-msgv3 messtab-msgv4.
*    ENDLOOP.
  endif.

  data:
  l_log_handle   type balloghndl,
  l_s_log        type bal_s_log,
  l_s_msg        type bal_s_msg,
  lt_log_handle  type bal_t_logh,
  l_msgno        type symsgno.

  data:
    l_s_display_profile type bal_s_prof.


  clear l_log_handle.
  l_s_log-object = ' '.
  l_s_log-subobject = ' '.
  l_s_log-extnumber = ' '.
* this flag is a bit strange: ' ' = deletion before is allowed!!!
  l_s_log-del_before = ' '.

* Create an initial log file
  call function 'BAL_LOG_CREATE'
    exporting
      i_s_log      = l_s_log
    importing
      e_log_handle = l_log_handle
    exceptions
      others       = 1.
  if sy-subrc <> 0.
    message id sy-msgid type sy-msgty number sy-msgno
             with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  endif.

  data st_message like bdcmsgcoll.

  loop at messtab." INTO st_message.
    move-corresponding messtab to l_s_msg.
    move messtab-msgtyp to l_s_msg-msgty.
    move messtab-msgid to l_s_msg-msgid.
    move messtab-msgnr to l_s_msg-msgno.
    move messtab-msgv1 to l_s_msg-msgv1.
    move messtab-msgv2 to l_s_msg-msgv2.
    move messtab-msgv3 to l_s_msg-msgv3.
    move messtab-msgv4 to l_s_msg-msgv4.
    call function 'BAL_LOG_MSG_ADD'
      exporting
        i_log_handle = l_log_handle
        i_s_msg      = l_s_msg
      exceptions
        others       = 1.
    if sy-subrc <> 0.
      message id sy-msgid type sy-msgty number sy-msgno
               with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    endif.

  endloop.

*--> Standard Profil auslesen
  call function 'BAL_DSP_PROFILE_STANDARD_GET'
    importing
      e_s_display_profile = l_s_display_profile.

  call function 'BAL_DSP_LOG_DISPLAY'
    exporting
      i_s_display_profile = l_s_display_profile
    exceptions
      others              = 0.

*  call function 'BAL_LOG_HDR_CHANGE'
*    exporting
*      i_log_handle            = l_log_handle
*      i_s_log                 = l_s_log
*    exceptions
*      log_not_found           = 1
*      log_header_inconsistent = 2
*      others                  = 3.
*  if sy-subrc <> 0.
*    message id sy-msgid type sy-msgty number sy-msgno
*            with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4
*            raising program_error.
*  endif.
*
*  append l_log_handle to lt_log_handle.
*  call function 'BAL_DB_SAVE'
*    exporting
*      i_in_update_task = ' '
*      i_save_all       = ' '
*      i_t_log_handle   = lt_log_handle
*    exceptions
*      log_not_found    = 1
*      save_not_allowed = 2
*      numbering_error  = 3
*      others           = 4.
*
*  if sy-subrc = 2.
*    message id sy-msgid type sy-msgty number sy-msgno
*            with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4
*            raising save_not_allowed.
*  elseif sy-subrc > 0.
*    message id sy-msgid type sy-msgty number sy-msgno
*            with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4
*            raising program_error.
*  endif.

endform.                               " BDC_TRANSACTION

*}   INSERT
