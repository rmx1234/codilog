FUNCTION Z_CONTROL_WORKFLOW_RBDMANI2.
*"--------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(OBJTYPE) LIKE  SWETYPECOU-OBJTYPE
*"     REFERENCE(OBJKEY) LIKE  SWEINSTCOU-OBJKEY
*"     REFERENCE(EVENT) LIKE  SWEINSTCOU-EVENT
*"     REFERENCE(RECTYPE) LIKE  SWETYPECOU-RECTYPE
*"  TABLES
*"      EVENT_CONTAINER STRUCTURE  SWCONT
*"  EXCEPTIONS
*"      NO_WORKFLOW
*"--------------------------------------------------------------------
*"----------------------------------------------------------------------
* Author            : Erwan LECAILLE                            ELE001 *
* Date              : 12.11.2007                                       *
* Document  DIA     : FI_344_DIA                                       *
* Description       : No workflow when calling from RBDMANI2           *
* ----------------- * ------------------------------------------------ *
* Author            : Jean-Marc JOURON                          JMJ001 *
* Date              : 16.01.2008                                       *
* Document  DIA     : FI_344_DIA                                       *
* Description       : RBDMANI2 is called from ZRM03, we need to check  *
*                     if a workflow already exists for this object     *
*                     instead of testing the calling program           *
* ----------------- * ------------------------------------------------ *

*>> JMJ001 - delete old logic
*>>>ELE001
*  if sy-cprog = 'RBDMANI2'
**  AND sy-batch = 'X'
*  .
*    raise no_workflow.
*  endif.
*<<<ELE001
*<<JMJ001 - end delete

*>>JMJ001 - new logic
  REFRESH: xobjectconn.
  xobjectconn-objecttype = objtype.
  xobjectconn-objectid  = objkey.
  append xobjectconn.
* get the workflow linked to the object
  CALL FUNCTION 'ZBALANCE_WF_CONNECTIONS_GET'
    TABLES
      object             = xobjectconn
      t_worklist         = wt_worklist
    EXCEPTIONS
      no_workflows_found = 1
      OTHERS             = 2.
* check if an open-workflow already exists
   loop at wt_worklist.
     if wt_worklist-WI_RH_TASK = RECTYPE
       and wt_worklist-WI_STAT ne c_error
       and wt_worklist-WI_STAT ne c_completed
       and wt_worklist-WI_STAT ne c_cancelled.
       raise no_workflow.
     endif.
   ENDLOOP.
*<<JMJ001 - end new logic
ENDFUNCTION.
