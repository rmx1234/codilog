************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Rapport d'exécution pour le rapprochement facture MM*
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Attila Kovacs (Netinside)                                   *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*			                                             *
* Ordre de transport: DE2K900720                                       *
*							           *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_rappr_rapp.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  TABLES
*"      T_FACTURES STRUCTURE  ZBALANCE_01
*"  CHANGING
*"     REFERENCE(E_RETURN) TYPE  BAL_T_MSG
*"----------------------------------------------------------------------

* Flags dans t_factures
*   wt_factures-ident = '1': présent dans l'idoc
*   wt_factures-ident = '2': impossible à déterminer
*   wt_factures-ident = '3': commande inexistant
*   wt_factures-ident = '4': bon de livraison inexistant
*   wt_factures-ident = '5': bon de livraison - pas de l'entrée de march
*   wt_factures-ident = 'X': rapprochement effectué
*   wt_factures-ident = ' ': rapprochement échoué

* Zone de travail pour les postes de la commande
  DATA:  ws_facture     TYPE zbalance_01.

* Rapport d'exécution
  LOOP AT t_factures INTO ws_facture.
    CASE ws_facture-ident.
      WHEN space.
        IF ws_facture-proba NE 0.
          PERFORM f_130_message
            TABLES e_return
            USING '008' 'E' ws_facture-posex ws_facture-proba
                            ws_facture-belnr ws_facture-zeile.
        ELSE.
          PERFORM f_130_message
            TABLES e_return
            USING '010' 'E' ws_facture-posex '' '' ''.
        ENDIF.
      WHEN '1'.
        PERFORM f_130_message
          TABLES e_return
          USING '004' 'I' ws_facture-posex '' '' ''.
      WHEN '2'.
        PERFORM f_130_message
          TABLES e_return
          USING '010' 'I' ws_facture-posex '' '' ''.
      WHEN '3'.
        PERFORM f_130_message
          TABLES e_return
          USING '005' 'E' ws_facture-posex ws_facture-belnr '' ''.
      WHEN '4'.
        PERFORM f_130_message
          TABLES e_return
          USING '007' 'E'
            ws_facture-posex ws_facture-verur ws_facture-lifnr ''.
      WHEN '5'.
        PERFORM f_130_message
          TABLES e_return
          USING '003' 'E'
            ws_facture-posex ws_facture-verur '' ''.
      WHEN 'X'.
        IF ws_facture-pos_2 IS INITIAL.
          PERFORM f_130_message
            TABLES e_return
            USING '009' 'I'
               ws_facture-posex ws_facture-belnr  ws_facture-zeile ''.
        ELSE.
          PERFORM f_130_message
            TABLES e_return
            USING '011' 'I'
               ws_facture-posex ws_facture-belnr  ws_facture-zeile
               ws_facture-vbeln.
        ENDIF.
    ENDCASE.
  ENDLOOP.

ENDFUNCTION.
