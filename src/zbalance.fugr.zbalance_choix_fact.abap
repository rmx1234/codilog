************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Changement d'assignement - exit 13 de la facture MM *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Attila Kovacs (Netinside)                                   *
*                                                                      *
* Date   : 16/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*			                                             *
* Ordre de transport: DE2K900720                                       *
*							           *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_choix_fact.
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     VALUE(I_ITEMDATA) TYPE  MMCR_F_ITEMDATA
*"  EXPORTING
*"     VALUE(E_EBELN) LIKE  DRSEG-EBELN
*"     VALUE(E_EBELP) LIKE  DRSEG-EBELP
*"     VALUE(E_LFSNR) LIKE  RM08M-LFSNR
*"     VALUE(E_CHANGE) TYPE  C
*"----------------------------------------------------------------------

  DATA: wt_docnum    TYPE SORTED TABLE OF zbalance_03
                     WITH UNIQUE KEY ebeln ebelp
                     WITH HEADER LINE.

* Code pour l'exit EXIT_SAPLMRMH_013
  IMPORT wt_docnum FROM MEMORY ID wc_memory_id_01.
  READ TABLE wt_docnum WITH TABLE KEY ebeln = i_itemdata-ebeln
                                      ebelp = i_itemdata-ebelp.
  IF sy-subrc EQ 0 AND
     NOT ( wt_docnum-lfsnr IS INITIAL ).
    e_ebeln = i_itemdata-ebeln.
    e_ebelp = i_itemdata-ebelp.
    e_lfsnr = wt_docnum-lfsnr.
    e_change = 'X'.
  ENDIF.

*  IF i_itemdata-lfsnr IS INITIAL.
*    SELECT SINGLE * FROM lips WHERE vgbel = i_itemdata-ebeln AND
*                                    vgpos = i_itemdata-ebelp.
*    IF sy-subrc EQ 0.
*      SELECT SINGLE verur FROM likp INTO e_lfsnr
*        WHERE vbeln = lips-vbeln.
*      IF NOT ( e_lfsnr IS INITIAL ).
*        e_ebeln = lips-vgbel.
*        e_ebelp = lips-vgpos.
*        e_change = 'X'.
*      ENDIF.
*    ENDIF.
*  ELSE.
*    e_ebeln = i_itemdata-ebeln.
*    e_ebelp = i_itemdata-ebelp.
*    e_lfsnr = i_itemdata-lfsnr.
*    e_change = 'X'.
*  ENDIF.
ENDFUNCTION.
