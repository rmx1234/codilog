FUNCTION zbalance_user_exit_fi_102.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IDOC_CONTRL_INDEX)
*"  EXPORTING
*"     VALUE(I_FIMSG) LIKE  FIMSG STRUCTURE  FIMSG
*"  TABLES
*"      IDOC_CONTRL STRUCTURE  EDIDC
*"      IDOC_DATA STRUCTURE  EDIDD
*"      DOCUMENT_DATA STRUCTURE  FTPOST1
*"      TAX_DATA STRUCTURE  FTTAX
*"      ADDITIONAL_DATA STRUCTURE  FTPOST1
*"----------------------------------------------------------------------

  TABLES : zbal_cgs.

  DATA : BEGIN OF w_postes OCCURS 0,
           poste TYPE i,
           compte LIKE bseg-hkont,
           codecgs LIKE zbal_cgs-zbal_codecgs,
           ordrint LIKE cobl-aufnr,
         END OF w_postes.

  DATA : st_e1edp19 LIKE e1edp19.
  DATA : st_e1edp30 LIKE e1edp30.

  DATA : w_tabix_compte LIKE sy-tabix.
  DATA : w_tabix_document LIKE sy-tabix.
  DATA : w_blart LIKE bkpf-blart.
  DATA : w_flag_ok(1) TYPE c.
  DATA : w_flag_newko(1) TYPE c.
  DATA : w_length TYPE i.
  DATA : w_count TYPE i.
  DATA : w_document_count(6) TYPE n.
  DATA : w_newko LIKE ftpost1-fval.



************************************************************************
*                  Récupération des données de l'idoc                  *
************************************************************************


* Récupération du type de pièce
  READ TABLE document_data WITH KEY fnam = 'BKPF-BLART'.
  IF sy-subrc EQ 0.
    w_blart = document_data-fval.
  ENDIF.

* Récupération du NEWKO du poste 1
  LOOP AT document_data.
    IF document_data-stype = 'P' AND
       document_data-count = '000001' AND
       document_data-fnam  = 'RF05A-NEWKO'.
      w_newko = document_data-fval.
      EXIT.
    ENDIF.
  ENDLOOP.

* Récupération des postes de l'idoc
  LOOP AT idoc_data.
    IF idoc_data-segnam = 'E1EDP01'.
      IF NOT w_count IS INITIAL.
        APPEND w_postes. CLEAR w_postes.
      ENDIF.

      ADD 1 TO w_count.
      w_postes-poste = w_count.
    ENDIF.

*   Récupération du compte et du code CGS
    IF NOT w_postes-poste IS INITIAL AND
       idoc_data-segnam = 'E1EDP19'.

      MOVE idoc_data-sdata TO st_e1edp19.
      IF st_e1edp19-qualf = '002'.
        MOVE st_e1edp19-idtnr TO w_postes-compte.
        MOVE st_e1edp19-mfrpn TO w_postes-codecgs.

        CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
             EXPORTING
                  input  = w_postes-compte
             IMPORTING
                  output = w_postes-compte.
      ENDIF.
    ENDIF.

*   Récupération de l'ordre interne
    IF NOT w_postes-poste IS INITIAL AND
       idoc_data-segnam = 'E1EDP30'.

      MOVE idoc_data-sdata TO st_e1edp30.
      IF st_e1edp30-qualf = '046'.
        MOVE st_e1edp30-ivkon TO w_postes-ordrint.

        CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
             EXPORTING
                  input  = w_postes-ordrint
             IMPORTING
                  output = w_postes-ordrint.
      ENDIF.
    ENDIF.
  ENDLOOP.
  APPEND w_postes. CLEAR w_postes.


************************************************************************
*                      Paramétrage compte unique                       *
************************************************************************

* On va remplacer les valeur des comptes renseignés par SAP
* par ceux récupérés depuis l'idoc
  LOOP AT w_postes.
    w_document_count = w_postes-poste + 1.
    LOOP AT document_data WHERE count = w_document_count.
      IF document_data-fnam = 'RF05A-NEWKO'.
        document_data-fval = w_postes-compte.
        MODIFY document_data.
      ENDIF.
    ENDLOOP.
  ENDLOOP.

************************************************************************
*                      Gestion de l'ordre interne                      *
************************************************************************

  LOOP AT w_postes WHERE NOT ordrint IS initial.
    w_document_count = w_postes-poste + 1.
    LOOP AT document_data WHERE count = w_document_count.
      w_tabix_document = sy-tabix + 1.
      IF document_data-fnam = 'RF05A-NEWKO'.
        document_data-stype = 'P'.
        document_data-count = w_document_count.
        document_data-fnam  = 'COBL-AUFNR'.
        document_data-fval  = w_postes-ordrint.
        INSERT document_data INDEX w_tabix_document.
      ENDIF.
    ENDLOOP.
  ENDLOOP.

************************************************************************
*                                 CGS                                  *
************************************************************************

* Si la LAD n'envoi pas le code CGS on va le compléter
* via la paramétrage spécifique
  LOOP AT w_postes.
    IF w_postes-codecgs IS INITIAL.
      SELECT SINGLE * FROM  zbal_cgs CLIENT SPECIFIED
             WHERE  mandt         = sy-mandt
             AND    zbal_typepce  = w_blart
             AND    zbal_cptgene  = w_postes-compte.
      IF sy-subrc EQ 0.
        MOVE zbal_cgs-zbal_codecgs TO w_postes-codecgs.
        MODIFY w_postes.
      ENDIF.
    ENDIF.
  ENDLOOP.

* Si la facture comporte un ou plusieurs poste avec un code CGS
* on va modifier la table DOCUMENT_DATA
  LOOP AT w_postes.
    IF NOT w_postes-codecgs IS INITIAL.

      w_document_count = w_postes-poste + 1.

      CLEAR w_flag_newko.

      LOOP AT document_data WHERE count = w_document_count.
        IF document_data-fnam = 'BSEG-BSCHL'.
          document_data-fval = '29'.
          MODIFY document_data.
        ENDIF.
        IF document_data-fnam = 'BSEG-MWSKZ'.
          document_data-fval = '**'.
          MODIFY document_data.
        ENDIF.
        IF document_data-fnam = 'RF05A-NEWKO'.
          document_data-fval = w_newko.
          MODIFY document_data.
          w_flag_newko = 'X'.
          MOVE sy-tabix TO w_tabix_document.
        ENDIF.

        IF w_flag_newko IS INITIAL.
          MOVE sy-tabix TO w_tabix_document.
        ENDIF.
      ENDLOOP.

      IF w_flag_newko IS INITIAL.
        document_data-stype = 'P'.
        document_data-count = w_document_count.
        document_data-fnam  = 'RF05A-NEWKO'.
        document_data-fval  = w_newko.
        INSERT document_data INDEX w_tabix_document.
      ENDIF.

      document_data-stype = 'P'.
      document_data-count = w_document_count.
      document_data-fnam  = 'RF05A-NEWUM'.
      document_data-fval  = w_postes-codecgs.
      INSERT document_data INDEX w_tabix_document.

      document_data-stype = 'P'.
      document_data-count = w_document_count.
      document_data-fnam  = 'BSEG-ZFBDT'.
      CONCATENATE sy-datum+6(2) '.'
                  sy-datum+4(2) '.'
                  sy-datum(4)
             INTO document_data-fval.
      INSERT document_data INDEX w_tabix_document.
    ENDIF.
  ENDLOOP.

ENDFUNCTION.
