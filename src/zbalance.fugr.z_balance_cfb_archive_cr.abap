FUNCTION Z_BALANCE_CFB_ARCHIVE_CR.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(OBJTYPE) LIKE  SWETYPECOU-OBJTYPE
*"     VALUE(OBJKEY) LIKE  SWEINSTCOU-OBJKEY
*"     VALUE(EVENT) LIKE  SWETYPECOU-EVENT
*"     VALUE(RECTYPE) LIKE  SWETYPECOU-RECTYPE
*"  TABLES
*"      EVENT_CONTAINER STRUCTURE  SWCONT
*"  EXCEPTIONS
*"      ANY_EXCEPTION
*"      NO_IDOC
*"----------------------------------------------------------------------
*{   INSERT         DE5K900049                                        1
*----------------------------------------------------------------------*
* Projet : Balance For SHARE / WEB                                     *
* Auteur : Volker Lang, IT-Informatik GmbH, Ulm, Germany               *
*                                                                      *
* Date :   16.02.2007                                                  *
*                                                                      *
*                                                                      *
* Theme: Check-FM for start ArchiveLink CREATE for FI                  *
*                                                                      *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************


DATA: BEGIN OF ls_vbkpf,
        bukrs TYPE vbkpf-bukrs,
        belnr TYPE vbkpf-belnr,
        gjahr TYPE vbkpf-gjahr,
      END OF ls_vbkpf.

DATA: BEGIN OF ls_idoc,
        docnum TYPE edids-docnum,
        logdat TYPE edids-logdat,
        logtim TYPE edids-logtim,
        countr TYPE edids-countr,
      END OF ls_idoc,
      lt_idoc like ls_idoc occurs 0.

clear: lt_idoc[], ls_vbkpf.


*\-- devide OBJKEY into structure
      ls_vbkpf = OBJKEY.

*\-- CHECK existing IDOC
      SELECT docnum LOGDAT LOGTIM COUNTR into table lt_idoc
        FROM  edids
           WHERE    status  = '53'
             AND    stapa1  = ls_vbkpf-belnr
             AND    stapa2  = ls_vbkpf-bukrs
             AND    stapa3  = ls_vbkpf-gjahr.

      if lt_idoc[] is initial.
         raise NO_IDOC.
      endif.





*}   INSERT
ENDFUNCTION.
