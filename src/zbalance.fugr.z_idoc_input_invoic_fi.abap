FUNCTION z_idoc_input_invoic_fi.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(INPUT_METHOD) LIKE  BDWFAP_PAR-INPUTMETHD
*"     VALUE(MASS_PROCESSING) LIKE  BDWFAP_PAR-MASS_PROC
*"  EXPORTING
*"     VALUE(WORKFLOW_RESULT) LIKE  BDWFAP_PAR-RESULT
*"     VALUE(APPLICATION_VARIABLE) LIKE  BDWFAP_PAR-APPL_VAR
*"     VALUE(IN_UPDATE_TASK) LIKE  BDWFAP_PAR-UPDATETASK
*"     VALUE(CALL_TRANSACTION_DONE) LIKE  BDWFAP_PAR-CALLTRANS
*"  TABLES
*"      IDOC_CONTRL STRUCTURE  EDIDC
*"      IDOC_DATA STRUCTURE  EDIDD
*"      IDOC_STATUS STRUCTURE  BDIDOCSTAT
*"      RETURN_VARIABLES STRUCTURE  BDWFRETVAR
*"      SERIALIZATION_INFO STRUCTURE  BDI_SER
*"----------------------------------------------------------------------
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
* 27.09.06 !  P.HADDADI   ! Link between SAP object and ixos image     *
*          !  (APPIA)     ! Il faut alimenter la table BDS_BAR_IN      *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
* 02.2008  ! R. GIL RGI009!  FI_DIA_595 Force Idoc to status 51 for    *
*          !              !  Italy ans Poland                          *
*----------!--------------!--------------------------------------------*


  DATA l_objkey LIKE bapibds01-objkey.
  DATA lt_property  TYPE STANDARD TABLE OF sdokpropty.
  DATA ls_property  TYPE sdokpropty.
  DATA l_archiv_id LIKE toaom-archiv_id.
  DATA ls_toacm TYPE toacm.
  DATA ls_edidc TYPE edidc.
  DATA ls_toaom TYPE toaom.
  DATA l_barcode LIKE bapibds01-barcode.
  DATA l_contrep LIKE bds_bar_ex-contrep.
  DATA l_log_sys LIKE bapibds01-log_system.

* BEGIN INSERT FI_300_DIA
  DATA : st_e1edp02 TYPE e1edp02,
         st_e1edk14 TYPE e1edk14,
         st_e1edk02 TYPE e1edk02,
         st_e1edk01 TYPE e1edk01,
         st_e1edka1 TYPE e1edka1,
         st_e1edp19 TYPE e1edp19,
         st_e1edp30 TYPE e1edp30.

  DATA : xidtnr TYPE edi_idtnr,
         xkostl TYPE kostl,
         xaufnr TYPE aufnr,
         xbelnr TYPE vbeln,
         xsubrc TYPE subrc,
         xvalidor TYPE char15.
  DATA : ws_avoir  TYPE c.
  DATA r_subrc LIKE sy-subrc.

* END INSERT FI_300_DIA
*§.. DIA_FI_595 - RGI002
  DATA w_status LIKE LINE OF idoc_status.
  DATA ls_zm207 TYPE zm207.
  DATA text TYPE string.

*§.. Begin insert fi_300_dia
  READ TABLE idoc_contrl INDEX 1.
  IF sy-subrc = 0 AND
  ( idoc_contrl-mescod = 'FI' OR idoc_contrl-mescod = 'MM' ).

* Update DB ZBAL_IDOC.
    CLEAR zbal_idoc.

    MOVE-CORRESPONDING idoc_contrl TO zbal_idoc.

* BEGIN INSERT OHA001 FI_332_DIA
* Get if bsart = CRME
    CLEAR ws_avoir.
    LOOP AT idoc_data WHERE segnam = 'E1EDK01'.
      MOVE idoc_data-sdata TO st_e1edk01.
      IF st_e1edk01-bsart = 'CRME'.
        ws_avoir = 'X'.
      ENDIF.
    ENDLOOP.
* END INSERT OHA001 FI_332_DIA

    LOOP AT idoc_data WHERE segnam = 'E1EDK14'.
      MOVE idoc_data-sdata TO st_e1edk14.
      IF st_e1edk14-qualf = '011'.
        WRITE st_e1edk14-orgid TO zbal_idoc-bukrs.
        EXIT.
      ENDIF.
    ENDLOOP.
    IF zbal_idoc-bukrs IS INITIAL.
      CLEAR t076b.
      SELECT * FROM  t076b UP TO 1 ROWS
             WHERE  parart  = 'LI'
             AND    konto   = idoc_contrl-sndprn.
      ENDSELECT.
      IF sy-subrc NE 0.
        CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
          EXPORTING
            input  = idoc_contrl-sndprn
          IMPORTING
            output = idoc_contrl-sndprn.

        SELECT * FROM  t076b UP TO 1 ROWS
              WHERE  parart  = 'LI'
              AND    konto   = idoc_contrl-sndprn.
        ENDSELECT.
      ENDIF.
      zbal_idoc-bukrs = t076b-bukrs.
    ENDIF.

    LOOP AT idoc_data WHERE segnam = 'E1EDKA1'.
      MOVE idoc_data-sdata TO st_e1edka1.
      IF st_e1edka1-parvw = 'LF'.
        WRITE st_e1edka1-partn TO zbal_idoc-sndprn RIGHT-JUSTIFIED.
        TRANSLATE zbal_idoc-sndprn USING ' 0'.
        EXIT.
      ENDIF.
    ENDLOOP.

    LOOP AT idoc_data WHERE segnam = 'E1EDK02'.
      MOVE idoc_data-sdata TO st_e1edk02.
      CHECK st_e1edk02-qualf = '009'.
      MOVE st_e1edk02-belnr TO zbal_idoc-belnr.
      EXIT.
    ENDLOOP.

*>> RGI008 FI_309
    DATA xe1eds01 LIKE e1eds01.
    LOOP AT idoc_data WHERE segnam = 'E1EDS01'.
      MOVE idoc_data-sdata TO xe1eds01.
      REPLACE ',' WITH '.' INTO xe1eds01-summe.
      CASE idoc_data-sdata+0(3).
        WHEN '011'. "TTC
          MOVE xe1eds01-summe TO zbal_idoc-betrg.
        WHEN '010'. "HT
          MOVE xe1eds01-summe TO zbal_idoc-wrbtr.
      ENDCASE.
      IF xe1eds01-sunit IS INITIAL.
        MOVE xe1eds01-waerq TO zbal_idoc-waers.
      ELSE.
        MOVE xe1eds01-sunit TO zbal_idoc-waers.
      ENDIF.
    ENDLOOP.

    SELECT SINGLE * INTO ls_zm207 FROM zm207 WHERE mescod = idoc_contrl-mescod
                                  AND mesfct = idoc_contrl-mesfct
                                  AND bukrs = zbal_idoc-bukrs
                                  AND lifnr = zbal_idoc-sndprn
                                  AND zidoc51 = 'X'.
    IF sy-subrc NE 0.
      SELECT SINGLE * INTO ls_zm207 FROM zm207 WHERE mescod = idoc_contrl-mescod
                                       AND mesfct = idoc_contrl-mesfct
                                       AND bukrs = zbal_idoc-bukrs
                                       AND lifnr = space
                                       AND zidoc51 = 'X'.
    ENDIF.
    r_subrc = sy-subrc.

    IF r_subrc = 0.
*§..we must check if accouting data are good.
      LOOP AT idoc_data WHERE segnam = 'E1EDP30'.
        MOVE idoc_data-sdata TO st_e1edp30.
        IF st_e1edp30-qualf = '045'.
          MOVE st_e1edp30-ivkon TO xkostl.
        ENDIF.
        IF st_e1edp30-qualf = '066'.
          MOVE st_e1edp30-ivkon TO xaufnr.
        ENDIF.
      ENDLOOP.

* Get Account in IDOC
      LOOP AT idoc_data WHERE segnam = 'E1EDP19'.
        MOVE idoc_data-sdata TO st_e1edp19.
        IF st_e1edp19-qualf = '002'.
          MOVE st_e1edp19-idtnr TO xidtnr.
          EXIT.
        ENDIF.
      ENDLOOP.
      IF  ( xidtnr IS NOT INITIAL AND xidtnr NE c_699999 )
                AND ( xkostl IS INITIAL OR ( xkostl NP c_scan AND xkostl NP c_cont ) ).
        r_subrc = 4.
      ELSE.
        r_subrc = 0.
      ENDIF.
    ENDIF.

    IF r_subrc = 0.

      MESSAGE e110(zsapbalance) WITH zbal_idoc-bukrs INTO text.
      PERFORM status_fuellen_idoc
        TABLES idoc_status return_variables
        USING '51'
              sy-msgty
              sy-msgid
              sy-msgno
              sy-msgv1
              sy-msgv2
              sy-msgv3
              sy-msgv4
              sy-repid
              'PROCESS_IDOC_INVOIC_FI'
              ' '
              ' '
              idoc_contrl-docnum
              workflow_result
              mass_processing.

    ELSE.
*§..END DIA_FI_595 - RGI002.
      CLEAR idoc_contrl.
      CALL FUNCTION 'IDOC_INPUT_INVOIC_FI'
        EXPORTING
          input_method          = input_method
          mass_processing       = mass_processing
        IMPORTING
          workflow_result       = workflow_result
          application_variable  = application_variable
          in_update_task        = in_update_task
          call_transaction_done = call_transaction_done
        TABLES
          idoc_contrl           = idoc_contrl
          idoc_data             = idoc_data
          idoc_status           = idoc_status
          return_variables      = return_variables
          serialization_info    = serialization_info
        EXCEPTIONS
          wrong_function_called = 1
          OTHERS                = 2.

      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ELSE.
*§.. No pb with call integration FI process
        r_subrc = 0.
      ENDIF.
*    ELSE.
*      r_subrc = 0.
    ENDIF.
*  ENDIF.

*§..No PB on call Function or Test for FI PreProcess
    IF r_subrc = 0.
* BEGIN INSERT OHA001 FI_331_DIA
      CLEAR : xidtnr, xkostl, xaufnr, xvalidor.
* Get Account in IDOC
      LOOP AT idoc_data WHERE segnam = 'E1EDP19'.
        MOVE idoc_data-sdata TO st_e1edp19.
        IF st_e1edp19-qualf = '002'.
          MOVE st_e1edp19-idtnr TO xidtnr.
          EXIT.
        ENDIF.
      ENDLOOP.

* Get CO Data
      LOOP AT idoc_data WHERE segnam = 'E1EDP30'.
        MOVE idoc_data-sdata TO st_e1edp30.
        IF st_e1edp30-qualf = '045'.
          MOVE st_e1edp30-ivkon TO xkostl.
        ENDIF.
        IF st_e1edp30-qualf = '066'.
          MOVE st_e1edp30-ivkon TO xaufnr.
        ENDIF.
      ENDLOOP.

* Must Posted or NOT
      READ TABLE idoc_status WITH KEY status = '53'.
      IF sy-subrc = 0.
        IF idoc_status-msgv1 CO '0123456789 '.
          UNPACK idoc_status-msgv1(10) TO xbelnr.
        ELSE.
          xbelnr = idoc_status-msgv1.
        ENDIF.

        IF xaufnr CO '0123456789 ' AND NOT xaufnr IS INITIAL.
          UNPACK xaufnr TO xaufnr.
        ENDIF.

        CLEAR bkpf.
        SELECT SINGLE * FROM bkpf WHERE bukrs = zbal_idoc-bukrs
                                    AND belnr = xbelnr
                                    AND gjahr = idoc_contrl-credat(4).
        IF sy-subrc = 0 AND bkpf-bstat = 'V' AND ws_avoir IS INITIAL.
          IF  ( xidtnr IS NOT INITIAL AND xidtnr NE c_699999 )
          AND ( xkostl IS INITIAL OR ( xkostl NP c_scan AND xkostl NP c_cont ) ).
            SELECT SINGLE zzuswin INTO xvalidor
                                  FROM zbal_validor
                                 WHERE bukrs = zbal_idoc-bukrs
                                   AND lifnr = zbal_idoc-sndprn.
            IF sy-subrc NE 0.
              SELECT SINGLE zzuswin INTO xvalidor
                                    FROM zbal_validor
                                   WHERE bukrs = zbal_idoc-bukrs
                                     AND ( ( aufnr = xaufnr
                                       AND aufnr NE space )
                                      OR ( kostl = xkostl
                                       AND kostl NE space ) ).
            ENDIF.
* Find VALIDOR
            IF xvalidor IS NOT INITIAL.
* Posted document with blocked 'S'.
              CLEAR xsubrc.
              CALL FUNCTION 'ZBALANCE_POST_AND_BLOCK'
                EXPORTING
                  bukrs   = bkpf-bukrs
                  belnr   = bkpf-belnr
                  gjahr   = bkpf-gjahr
                  kostl   = xkostl
                  aufnr   = xaufnr
                IMPORTING
                  p_subrc = xsubrc.

              IF xsubrc NE 0.
                CLEAR xvalidor.
              ELSE.
* Send File to SHARE.
                MOVE xidtnr   TO zbal_idoc-hkont.
                MOVE xkostl   TO zbal_idoc-kostl.
                MOVE xaufnr   TO zbal_idoc-aufnr.
                MOVE xvalidor TO zbal_idoc-validor.
                CALL FUNCTION 'ZBALANCE_CREATE_SHARE_FILE'
                  EXPORTING
                    belnr      = bkpf-belnr
                    bukrs      = bkpf-bukrs
                    gjahr      = bkpf-gjahr
                    xbal_idoc  = zbal_idoc
                  EXCEPTIONS
                    error_file = 1
                    OTHERS     = 2.
                IF sy-subrc <> 0.
                  CLEAR sy-subrc.
                  DATA:
                  l_log_handle   TYPE balloghndl,
                  l_s_log        TYPE bal_s_log,
                  l_s_msg        TYPE bal_s_msg,
                  lt_log_handle  TYPE bal_t_logh,
                  l_msgno        TYPE symsgno.

                  DATA:
                    l_s_display_profile TYPE bal_s_prof.

                  CLEAR l_log_handle.
                  l_s_log-object = 'ZSHARE'."I_BALOBJECT.
                  l_s_log-subobject = 'UNLOCK'.
                  l_s_log-extnumber = ' '.
                  l_s_log-aldate_del = sy-datum + 10.
* this flag is a bit strange: ' ' = deletion before is allowed!!!
                  l_s_log-del_before = ' '.

* Create an initial log file
                  CALL FUNCTION 'BAL_LOG_CREATE'
                    EXPORTING
                      i_s_log      = l_s_log
                    IMPORTING
                      e_log_handle = l_log_handle
                    EXCEPTIONS
                      OTHERS       = 1.
                  IF sy-subrc <> 0.
                    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                             WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
                  ENDIF.

                  CLEAR text.
                  MESSAGE e123(zsapbalance) INTO text.
                  MOVE sy-msgty TO l_s_msg-msgty.
                  MOVE sy-msgid TO l_s_msg-msgid.
                  MOVE sy-msgno TO l_s_msg-msgno.
                  MOVE sy-msgv1 TO l_s_msg-msgv1.
                  MOVE sy-msgv2 TO l_s_msg-msgv2.
                  MOVE sy-msgv3 TO l_s_msg-msgv3.
                  MOVE sy-msgv4 TO l_s_msg-msgv4.
                  CALL FUNCTION 'BAL_LOG_MSG_ADD'
                    EXPORTING
                      i_log_handle = l_log_handle
                      i_s_msg      = l_s_msg
                    EXCEPTIONS
                      OTHERS       = 1.
                  IF sy-subrc <> 0.
                    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                             WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
                  ENDIF.

                  CALL FUNCTION 'BAL_LOG_HDR_CHANGE'
                    EXPORTING
                      i_log_handle            = l_log_handle
                      i_s_log                 = l_s_log
                    EXCEPTIONS
                      log_not_found           = 1
                      log_header_inconsistent = 2
                      OTHERS                  = 3.
                  IF sy-subrc <> 0.
                    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4
                            RAISING program_error.
                  ENDIF.

                  APPEND l_log_handle TO lt_log_handle.
                  CALL FUNCTION 'BAL_DB_SAVE'
                    EXPORTING
                      i_in_update_task = ' '
                      i_save_all       = ' '
                      i_t_log_handle   = lt_log_handle
                    EXCEPTIONS
                      log_not_found    = 1
                      save_not_allowed = 2
                      numbering_error  = 3
                      OTHERS           = 4.

                  IF sy-subrc = 2.
                    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4
                            RAISING save_not_allowed.
                  ELSEIF sy-subrc > 0.
                    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4
                            RAISING program_error.
                  ENDIF.
                ENDIF.

              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
*END INSERT OHA001 FI_331_DIA
      MOVE xidtnr   TO zbal_idoc-hkont.
      MOVE xkostl   TO zbal_idoc-kostl.
      MOVE xaufnr   TO zbal_idoc-aufnr.
      MOVE xvalidor TO zbal_idoc-validor.

      READ TABLE idoc_status INDEX 1.
      IF sy-subrc = 0.
        MOVE idoc_status-status TO zbal_idoc-status.
        MOVE idoc_status-msgty  TO zbal_idoc-statyp.
        MOVE idoc_status-msgid  TO zbal_idoc-stamid.
        MOVE idoc_status-msgno  TO zbal_idoc-stamno.
        MOVE idoc_status-msgv1  TO zbal_idoc-stapa1.
        MOVE idoc_status-msgv2  TO zbal_idoc-stapa2.
        MOVE idoc_status-msgv3  TO zbal_idoc-stapa3.
        MOVE idoc_status-msgv4  TO zbal_idoc-stapa4.
      ENDIF.

      zbal_idoc-upddat = sy-datum.
      zbal_idoc-updtim = sy-uzeit.
      MODIFY zbal_idoc.
*    ENDIF.
*  ENDIF.
* END INSERT FI_300_DIA

      IF idoc_contrl-mesfct NE 'OPT'.
        READ TABLE idoc_status WITH KEY status = '53'.
        IF sy-subrc = 0.
** Link between SAP object and ixos image
** Put datas into table BDS_BAR_IN
* Recherche des données de l'idoc
          CLEAR ls_edidc.
          READ TABLE idoc_contrl INTO ls_edidc INDEX 1.
          MOVE ls_edidc-rcvprn TO l_log_sys.
          MOVE ls_edidc-arckey TO l_barcode.

* Clé de l'objet = n° de facture + code société + année de comptabilisation
          READ TABLE return_variables WITH KEY wf_param = 'Appl_Objects'.
          MOVE return_variables-doc_number TO l_objkey.

* Recherche de la valeur du content repository - table TOAOM
          CLEAR l_archiv_id.
          SELECT SINGLE archiv_id FROM toaom INTO l_archiv_id
                              WHERE sap_object = 'FIPP'
                                AND ar_object = 'ZFIINVOICE'
                                AND ar_status = 'X'.
          CHECK sy-subrc = 0.
          MOVE l_archiv_id TO l_contrep.

* alimentation de la structure S_TOACM pour garder l'enregistrement
*  dans la table BDS_BAR_EX et pour créer un enregistrement dans
*  la table BDS_BAR_IN

          CLEAR ls_toacm.
          MOVE 'X' TO ls_toacm-keep_bar_i.
          MOVE 'X' TO ls_toacm-keep_bar_e.

          CLEAR ls_property.
          ls_property-name = 'BDS_DOCUMENTTYPE'.
          ls_property-value = 'ZFIINVOICE'.
          APPEND ls_property TO lt_property.

          CALL FUNCTION 'BDS_BARCODE_CREATE'
            EXPORTING
              barcode        = l_barcode
              log_system     = l_log_sys
              classname      = 'FIPP'
              classtype      = 'BO'
              objkey         = l_objkey
              client         = sy-mandt
              s_toacm        = ls_toacm
            TABLES
              properties     = lt_property
            EXCEPTIONS
              error_barcode  = 1
              error_kpro     = 2
              internal_error = 3
              OTHERS         = 4.

          IF sy-subrc <> 0.
* MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
*         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
          ELSE.
            COMMIT WORK AND WAIT.
          ENDIF.
        ENDIF.
      ENDIF.
*§..FIN TEST R_SUBRC = 0.
    ENDIF.
*§.. FIN TEST FI OU MM
  ENDIF.
ENDFUNCTION.

*&---------------------------------------------------------------------*
*&      Form  STATUS_FUELLEN_IDOC
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->VALUE(STATUS)  text
*      -->VALUE(MSGTY)   text
*      -->VALUE(MSGID)   text
*      -->VALUE(MSGNR)   text
*      -->VALUE(MSGPA1)  text
*      -->VALUE(MSGPA2)  text
*      -->VALUE(MSGPA3)  text
*      -->VALUE(MSGPA4)  text
*      -->VALUE(REPID)   text
*      -->VALUE(ROUTID)  text
*      -->VALUE(SEGFLD)  text
*      -->VALUE(SEGNUM)  text
*      -->VALUE(DOCNUM)  text
*----------------------------------------------------------------------*
FORM status_fuellen_idoc TABLES tidoc_status STRUCTURE bdidocstat
                                treturn_variables STRUCTURE bdwfretvar
                         USING value(status)
                               value(msgty)
                               value(msgid)
                               value(msgnr)
                               value(msgpa1)
                               value(msgpa2)
                               value(msgpa3)
                               value(msgpa4)
                               value(repid)
                               value(routid)
                               value(segfld)
                               value(segnum)
                               value(docnum)
                               value(wf_result)
                               value(mass_processing).

*------ Test für Rückkehr im Fehlerfall ------------------------------*
* IF MSGTY = MSG_ERROR.
  IF status = '51'.                                         "<-- 3.0F
*    msgty = msgty.                                      "<-- 3.0F
    wf_result = '99999'.
    IF mass_processing = 'X'.
      CLEAR treturn_variables.
      REFRESH treturn_variables.                            "<-- 3.0F
      treturn_variables-wf_param  = 'Error_IDOCs'.
      treturn_variables-doc_number = docnum.
      APPEND treturn_variables.
    ENDIF.
  ENDIF.

*------ Rückkehr mit erfolgreich gebuchtem Beleg im MM ---------------*
  IF msgty = 'S'.
*   WORKFLOW_RESULT = C_WF_RESULT_OK.
    IF mass_processing = 'X'.
      CLEAR treturn_variables.
      treturn_variables-wf_param  = 'Appl_Object'.
*      treturn_variables-doc_number = beleg_nummern-belnr.
      APPEND treturn_variables.
    ENDIF.
  ENDIF.

*  CHECK xfepoin = ' '.                                      "4.5B
  CLEAR tidoc_status.
  tidoc_status-docnum = docnum.
  tidoc_status-status = status.
  tidoc_status-msgty  = msgty.
  tidoc_status-msgid  = msgid.
  tidoc_status-msgno  = msgnr.
  tidoc_status-msgv1  = msgpa1.
  tidoc_status-msgv2  = msgpa2.
  tidoc_status-msgv3  = msgpa3.
  tidoc_status-msgv4  = msgpa4.
  tidoc_status-segnum = segnum.
  tidoc_status-segfld = segfld.
  tidoc_status-uname  = sy-uname.
  tidoc_status-repid  = repid.
  tidoc_status-routid = routid.
  COLLECT tidoc_status.

ENDFORM.                    "STATUS_FUELLEN_IDOC
