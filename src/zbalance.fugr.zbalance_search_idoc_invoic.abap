************************************************************************
* Identification :                                                     *
*                                                                      *
* Description    : Permet de contrôler que les factures sont intégrées *
*                  en automatique 						  *
*                                                                      *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Eléments de développements associés :                                *
*                                                                      *
*                                                                      *
*----------------------------------------------------------------------*
* Projet : SAP BALANCE                                                 *
*                                                                      *
* Auteur : Gaëlle TRIMOREAU (Netinside)                                *
*                                                                      *
* Date   : 13/01/04                                                    *
*                                                                      *
* Frequence :                                                          *
*								                *
* Ordre de transport: DE2K900720                                       *
*									         *
************************************************************************
* Modifié  !     Par      !                Description                 *
************************************************************************
*          !              !                                            *
*          !              !                                            *
*----------!--------------!--------------------------------------------*
*          !              !                                            *
*          !              !                                            *
************************************************************************
FUNCTION zbalance_search_idoc_invoic .
*"----------------------------------------------------------------------
*"*"Interface locale :
*"  IMPORTING
*"     REFERENCE(I_OBJTYPE) LIKE  SRRELROLES-OBJTYPE
*"     REFERENCE(I_NB_INVOIC) LIKE  VBKPF-BELNR
*"  EXPORTING
*"     REFERENCE(E_NB_IDOC) LIKE  SRRELROLES-OBJKEY
*"  EXCEPTIONS
*"      NO_IDOC_LINKED
*"----------------------------------------------------------------------

************************************************************************
*                   DECLARATION DE DONNEES                             *
************************************************************************
***************************** Tables ***********************************
  TABLES: srrelroles, idocrel, vbkpf.

***************************** Données **********************************
  DATA: w_role_a LIKE idocrel-role_a,
        w_objkey LIKE srrelroles-objkey,
        w_objkey2 LIKE srrelroles-objkey,
        w_bukrs  LIKE vbkpf-bukrs,
        w_gjahr  LIKE vbkpf-gjahr.

***************************** Constantes *******************************
* Constante pour trouver l'id associé à la facture
  CONSTANTS: k_inbeleg LIKE srrelroles-roletype VALUE 'INBELEG',
* Constante pour trouver le n° de l'idoc
             k_inidoc  LIKE srrelroles-roletype VALUE 'INIDOC',
             k_objtype LIKE srrelroles-objtype  VALUE 'IDOC',
             k_bkpf    LIKE srrelroles-objtype VALUE 'BKPF',
             k_bus2081 LIKE srrelroles-objtype VALUE 'BUS2081'.

************************************************************************
*                             TRAITEMENT                               *
************************************************************************
  IF i_objtype = k_bkpf.
* Recherche de la société et exercice comptable.
    CLEAR vbkpf.
    CLEAR: w_bukrs, w_gjahr, w_objkey.
    SELECT  bukrs gjahr INTO (w_bukrs, w_gjahr) "ex SELECT SINGLE
                  FROM vbkpf WHERE belnr = i_nb_invoic.
    ENDSELECT.
    IF sy-subrc EQ 0.
      CONCATENATE w_bukrs i_nb_invoic w_gjahr INTO w_objkey.
    ELSE.
    MESSAGE s533(0U)
    with 'Aucune entrée correspondante dans la table'(w47)
    'En-tête Préenregistrement des pièces'(w43).
    ENDIF.
  ELSE.

  ENDIF.

  CLEAR: srrelroles, idocrel.
* Recherche de la valeur de l'id de l'idoc.
  SELECT  b~role_a INTO w_role_a "ex SELECT SINGLE
                FROM srrelroles AS a INNER JOIN idocrel AS b
                                     ON a~roleid = b~role_b
                WHERE a~objtype = i_objtype
                  AND a~objkey = w_objkey
                  AND a~roletype = k_inbeleg.
  ENDSELECT.

  IF sy-subrc = 0.
* Recherche du n° d'idoc à pertir de l'id trouvé précédemment
    CLEAR srrelroles.
    SELECT  objkey INTO w_objkey2 "ex SELECT SINGLE
                         FROM srrelroles
                         WHERE objtype = k_objtype
                           AND roletype = k_inidoc
                           AND roleid = w_role_a.
     ENDSELECT.
    IF sy-subrc = 0.
      MOVE w_objkey2 TO e_nb_idoc.
    ELSE.
* Il n'y a pas d'idoc associé
      RAISE no_idoc_linked.
    ENDIF.
  ELSE.
* Il n'y a pas d'idoc associé
    RAISE no_idoc_linked.
  ENDIF.

ENDFUNCTION.
